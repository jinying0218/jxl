#!/bin/sh
set -e

echo "mkdir -p ${CONFIGURATION_BUILD_DIR}/${FRAMEWORKS_FOLDER_PATH}"
mkdir -p "${CONFIGURATION_BUILD_DIR}/${FRAMEWORKS_FOLDER_PATH}"

SWIFT_STDLIB_PATH="${DT_TOOLCHAIN_DIR}/usr/lib/swift/${PLATFORM_NAME}"

install_framework()
{
  if [ -r "${BUILT_PRODUCTS_DIR}/$1" ]; then
    local source="${BUILT_PRODUCTS_DIR}/$1"
  elif [ -r "${BUILT_PRODUCTS_DIR}/$(basename "$1")" ]; then
    local source="${BUILT_PRODUCTS_DIR}/$(basename "$1")"
  elif [ -r "$1" ]; then
    local source="$1"
  fi

  local destination="${CONFIGURATION_BUILD_DIR}/${FRAMEWORKS_FOLDER_PATH}"

  if [ -L "${source}" ]; then
      echo "Symlinked..."
      source="$(readlink "${source}")"
  fi

  # use filter instead of exclude so missing patterns dont' throw errors
  echo "rsync -av --filter \"- CVS/\" --filter \"- .svn/\" --filter \"- .git/\" --filter \"- .hg/\" --filter \"- Headers\" --filter \"- PrivateHeaders\" --filter \"- Modules\" \"${source}\" \"${destination}\""
  rsync -av --filter "- CVS/" --filter "- .svn/" --filter "- .git/" --filter "- .hg/" --filter "- Headers" --filter "- PrivateHeaders" --filter "- Modules" "${source}" "${destination}"

  local basename
  basename="$(basename -s .framework "$1")"
  binary="${destination}/${basename}.framework/${basename}"
  if ! [ -r "$binary" ]; then
    binary="${destination}/${basename}"
  fi

  # Strip invalid architectures so "fat" simulator / device frameworks work on device
  if [[ "$(file "$binary")" == *"dynamically linked shared library"* ]]; then
    strip_invalid_archs "$binary"
  fi

  # Resign the code if required by the build settings to avoid unstable apps
  code_sign_if_enabled "${destination}/$(basename "$1")"

  # Embed linked Swift runtime libraries. No longer necessary as of Xcode 7.
  if [ "${XCODE_VERSION_MAJOR}" -lt 7 ]; then
    local swift_runtime_libs
    swift_runtime_libs=$(xcrun otool -LX "$binary" | grep --color=never @rpath/libswift | sed -E s/@rpath\\/\(.+dylib\).*/\\1/g | uniq -u  && exit ${PIPESTATUS[0]})
    for lib in $swift_runtime_libs; do
      echo "rsync -auv \"${SWIFT_STDLIB_PATH}/${lib}\" \"${destination}\""
      rsync -auv "${SWIFT_STDLIB_PATH}/${lib}" "${destination}"
      code_sign_if_enabled "${destination}/${lib}"
    done
  fi
}

# Signs a framework with the provided identity
code_sign_if_enabled() {
  if [ -n "${EXPANDED_CODE_SIGN_IDENTITY}" -a "${CODE_SIGNING_REQUIRED}" != "NO" -a "${CODE_SIGNING_ALLOWED}" != "NO" ]; then
    # Use the current code_sign_identitiy
    echo "Code Signing $1 with Identity ${EXPANDED_CODE_SIGN_IDENTITY_NAME}"
    echo "/usr/bin/codesign --force --sign ${EXPANDED_CODE_SIGN_IDENTITY} --preserve-metadata=identifier,entitlements \"$1\""
    /usr/bin/codesign --force --sign ${EXPANDED_CODE_SIGN_IDENTITY} --preserve-metadata=identifier,entitlements "$1"
  fi
}

# Strip invalid architectures
strip_invalid_archs() {
  binary="$1"
  # Get architectures for current file
  archs="$(lipo -info "$binary" | rev | cut -d ':' -f1 | rev)"
  stripped=""
  for arch in $archs; do
    if ! [[ "${VALID_ARCHS}" == *"$arch"* ]]; then
      # Strip non-valid architectures in-place
      lipo -remove "$arch" -output "$binary" "$binary" || exit 1
      stripped="$stripped $arch"
    fi
  done
  if [[ "$stripped" ]]; then
    echo "Stripped $binary of architectures:$stripped"
  fi
}


if [[ "$CONFIGURATION" == "Debug" ]]; then
  install_framework "Pods-jiangXunLu/AFNetworking.framework"
  install_framework "Pods-jiangXunLu/Alamofire.framework"
  install_framework "Pods-jiangXunLu/BSGridCollectionViewLayout.framework"
  install_framework "Pods-jiangXunLu/BSImagePicker.framework"
  install_framework "Pods-jiangXunLu/BlocksKit.framework"
  install_framework "Pods-jiangXunLu/CocoaLumberjack.framework"
  install_framework "Pods-jiangXunLu/Dollar.framework"
  install_framework "Pods-jiangXunLu/KVOController.framework"
  install_framework "Pods-jiangXunLu/Kingfisher.framework"
  install_framework "Pods-jiangXunLu/MJExtension.framework"
  install_framework "Pods-jiangXunLu/MJRefresh.framework"
  install_framework "Pods-jiangXunLu/Masonry.framework"
  install_framework "Pods-jiangXunLu/NYXImagesKit.framework"
  install_framework "Pods-jiangXunLu/ObjectMapper.framework"
  install_framework "Pods-jiangXunLu/RxBlocking.framework"
  install_framework "Pods-jiangXunLu/RxCocoa.framework"
  install_framework "Pods-jiangXunLu/RxSwift.framework"
  install_framework "Pods-jiangXunLu/SDWebImage.framework"
  install_framework "Pods-jiangXunLu/SVProgressHUD.framework"
  install_framework "Pods-jiangXunLu/SnapKit.framework"
  install_framework "Pods-jiangXunLu/Starscream.framework"
  install_framework "Pods-jiangXunLu/SwiftyJSON.framework"
  install_framework "Pods-jiangXunLu/SwiftyTimer.framework"
  install_framework "Pods-jiangXunLu/TPKeyboardAvoiding.framework"
  install_framework "Pods-jiangXunLu/TSVoiceConverter.framework"
  install_framework "Pods-jiangXunLu/UICollectionViewLeftAlignedLayout.framework"
  install_framework "Pods-jiangXunLu/UIColor_Hex_Swift.framework"
  install_framework "Pods-jiangXunLu/UIImageViewModeScaleAspect.framework"
  install_framework "Pods-jiangXunLu/XCGLogger.framework"
  install_framework "Pods-jiangXunLu/XXNibBridge.framework"
  install_framework "Pods-jiangXunLu/YYModel.framework"
  install_framework "Pods-jiangXunLu/YYText.framework"
  install_framework "Pods-jiangXunLu/iVersion.framework"
fi
if [[ "$CONFIGURATION" == "Release" ]]; then
  install_framework "Pods-jiangXunLu/AFNetworking.framework"
  install_framework "Pods-jiangXunLu/Alamofire.framework"
  install_framework "Pods-jiangXunLu/BSGridCollectionViewLayout.framework"
  install_framework "Pods-jiangXunLu/BSImagePicker.framework"
  install_framework "Pods-jiangXunLu/BlocksKit.framework"
  install_framework "Pods-jiangXunLu/CocoaLumberjack.framework"
  install_framework "Pods-jiangXunLu/Dollar.framework"
  install_framework "Pods-jiangXunLu/KVOController.framework"
  install_framework "Pods-jiangXunLu/Kingfisher.framework"
  install_framework "Pods-jiangXunLu/MJExtension.framework"
  install_framework "Pods-jiangXunLu/MJRefresh.framework"
  install_framework "Pods-jiangXunLu/Masonry.framework"
  install_framework "Pods-jiangXunLu/NYXImagesKit.framework"
  install_framework "Pods-jiangXunLu/ObjectMapper.framework"
  install_framework "Pods-jiangXunLu/RxBlocking.framework"
  install_framework "Pods-jiangXunLu/RxCocoa.framework"
  install_framework "Pods-jiangXunLu/RxSwift.framework"
  install_framework "Pods-jiangXunLu/SDWebImage.framework"
  install_framework "Pods-jiangXunLu/SVProgressHUD.framework"
  install_framework "Pods-jiangXunLu/SnapKit.framework"
  install_framework "Pods-jiangXunLu/Starscream.framework"
  install_framework "Pods-jiangXunLu/SwiftyJSON.framework"
  install_framework "Pods-jiangXunLu/SwiftyTimer.framework"
  install_framework "Pods-jiangXunLu/TPKeyboardAvoiding.framework"
  install_framework "Pods-jiangXunLu/TSVoiceConverter.framework"
  install_framework "Pods-jiangXunLu/UICollectionViewLeftAlignedLayout.framework"
  install_framework "Pods-jiangXunLu/UIColor_Hex_Swift.framework"
  install_framework "Pods-jiangXunLu/UIImageViewModeScaleAspect.framework"
  install_framework "Pods-jiangXunLu/XCGLogger.framework"
  install_framework "Pods-jiangXunLu/XXNibBridge.framework"
  install_framework "Pods-jiangXunLu/YYModel.framework"
  install_framework "Pods-jiangXunLu/YYText.framework"
  install_framework "Pods-jiangXunLu/iVersion.framework"
fi

#import <UIKit/UIKit.h>

#import "amrFileCodec.h"
#import "wav.h"
#import "interf_dec.h"
#import "interf_enc.h"
#import "dec_if.h"
#import "if_rom.h"
#import "VoiceConverterHeaders.h"

FOUNDATION_EXPORT double TSVoiceConverterVersionNumber;
FOUNDATION_EXPORT const unsigned char TSVoiceConverterVersionString[];


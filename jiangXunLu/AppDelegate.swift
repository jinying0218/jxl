//
//  AppDelegate.swift
//  jiangXunLu
//
//  Created by liuxb on 16/6/12.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit
import Starscream
import Alamofire
import SVProgressHUD

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var isActive: Bool = false
    var manager: NetworkReachabilityManager?
    
    lazy var webSocket: WebSocket = {
        let socket = WebSocket(url: NSURL(string: Globe.messageWS)!)
        socket.delegate = self
        socket.pongDelegate = self
        return socket
    }()
    
    var num_unreadmsg:Int = DataBase.shareDataBase.fetchUnreadMessageNum()              //未读消息
    var more_unreadmsg:Bool = true                                                      //判断未读消息是否一次性请求完毕
    var firstPing: Bool = true                                                          //是否首次ping
    var timer: NSTimer?                                                                 //每隔5秒执行ping
    var cmsg_code: Int = 1                                                              //消息交互的id，每次都不一样
    var smsg_code: NSNumber = -1                                                        //收到消息服务器返回的id
    var start_id: NSNumber = 0                                                          //请求未读消息，起始id
    var end_id: NSNumber = 0                                                            //请求未读消息，末端id
    
    var sendMessages = NSMutableDictionary()
    
    // 即时消息刷新
    var refreshMessageList:(()->Void)?
    var refreshChatList:((String?,String?)->Void)?
    var refreshMessageBadge:(()->Void)?
    // 刷新通往消息的个人页面
    var refreshUnreadMessage:(()->Void)?
    
    static let shareDelegate: AppDelegate = {
        let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
        return delegate
    }()
    
    func setupLogionViewController() {
        
        if self.webSocket.isConnected {
            self.firstPing = true
            //记录登录状态
            NSUserDefaults.standardUserDefaults().setObject(NSNumber(bool: false), forKey: kLoginStatus)
            if self.timer != nil {
                self.timer!.invalidate()
                self.timer = nil
            }
            //断开链接
            self.webSocket.disconnect()
            //登出
            Login.loginOut()
        }
        
        let navigationVC = UINavigationController(rootViewController: LoginViewController())
        window!.rootViewController = navigationVC
    }

    func setupMainViewController() {
        let rootVC = MainTabBarController()
        self.window!.rootViewController = rootVC
    }
    
    func initNavigationStyle() {
        //设置全局样式
        SVProgressHUD.setMinimumDismissTimeInterval(0.5)
        SVProgressHUD.setDefaultStyle(.Dark)
        
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        UIApplication.sharedApplication().statusBarHidden = false
        // 导航栏样式  #333333
        let customBlack = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
        UINavigationBar.appearance().barTintColor = customBlack
        UINavigationBar.appearance().translucent = false
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor()]
        // TabBar样式 #fecd08
        UITabBar.appearance().tintColor = UIColor(red: 0.996, green: 0.804, blue: 0.031, alpha: 1.00)
        UITabBar.appearance().barTintColor = customBlack
        UITabBar.appearance().translucent = false
    }
    
    func registerNofitication() {
        let category = UIMutableUserNotificationCategory()
        let settings = UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: NSSet(object: category) as? Set<UIUserNotificationCategory>)
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
        UIApplication.sharedApplication().registerForRemoteNotifications()
    }
    
    func LoadGuideViewController() {
        let infoDictionary = NSBundle.mainBundle().infoDictionary
        let currentAppVersion = infoDictionary!["CFBundleShortVersionString"] as! String

        let userDefaults = NSUserDefaults.standardUserDefaults()
        let appVersion = userDefaults.stringForKey("appVersion")
        
        if appVersion == nil || appVersion != currentAppVersion {
            userDefaults.setValue(currentAppVersion, forKey: "appVersion")
            JXLGuideView.guideView()
        }
    }
    
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        self.initNavigationStyle()
        
        window = UIWindow(frame: UIScreen.mainScreen().bounds)
        window?.backgroundColor = UIColor.whiteColor()
        
        //初始化登录入口
        Login.shareLogin.fromLoginPanel = false
        
        if Login.isLogin() {
            self.setupMainViewController()
        } else {
            application.applicationIconBadgeNumber = 0
            self.setupLogionViewController()
        }
        
        window!.makeKeyAndVisible()
        
        self.manager = NetworkReachabilityManager()
        self.manager?.listener = { status in
            if status == .NotReachable || status == .Unknown{
                print("似乎断开与互联网的连接!")
            }
            print(status)
        }
        self.manager?.startListening()
        
        self.LoadGuideViewController()
    
        let startView = JXLStartView.startView()
        startView.startAnimationWithCompletionBlock {
            self.completionStartAnimationWithOptions(launchOptions)
        }
        

        
        return true
    }
    
    
    func completionStartAnimationWithOptions(launchOptions: [NSObject: AnyObject]?) {
        
        if Login.isLogin() {
            UIApplication.sharedApplication().applicationIconBadgeNumber = 0
            if let remoteNotification = launchOptions?[UIApplicationLaunchOptionsRemoteNotificationKey] as? [String:AnyObject] {
                BaseViewController.handleNotificationInfo(remoteNotification, applicationState: .Inactive)
            }
        }
        
        //初始化友盟
        UMSocialData.setAppKey(Globe.umengAppKey)
        UMSocialWechatHandler.setWXAppId(Globe.weChatAppID, appSecret: Globe.weChatAppSecret, url: Globe.baseUrlStr)
        UMSocialQQHandler.setQQWithAppId(Globe.qqAppID, appKey: Globe.qqAppKey, url: Globe.baseUrlStr)
        UMSocialSinaSSOHandler.openNewSinaSSOWithAppKey(Globe.sinaAppKey, secret: Globe.sinaAppSecret, redirectURL: Globe.sinaRedirectURL)
        
        //初始化信鸽
        XGPush.startApp(Globe.XGPushID, appKey: Globe.XGPushAppKey)
        Login.setXGAccountWithCurUser()
        
        let successCallback:(()->Void) = {
            if !XGPush.isUnRegisterStatus() && Login.isLogin() {
                self.registerNofitication()
            }
        }
        XGPush.initForReregister(successCallback)
        
        //推送反馈(app不在前台运行时，点击推送激活时。统计而已)
        XGPush.handleLaunching(launchOptions)
    }
    
    
    func applicationWillResignActive(application: UIApplication) {
        self.isActive = false
        if Login.isLogin() {
            if self.timer != nil {
                self.timer!.invalidate()
                self.timer = nil
                self.firstPing = true
            }
            self.webSocket.disconnect()
            DataBase.shareDataBase.saveContext()
        }
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        
    }
    
    func applicationWillEnterForeground(application: UIApplication) {

    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        self.isActive = true
        if Login.isLogin() {
            self.webSocket.connect()
        }
    }
    
    func applicationWillTerminate(application: UIApplication) {
        if self.timer != nil {
            self.timer!.invalidate()
            self.timer = nil
            self.firstPing = true
        }
        self.webSocket.disconnect()
        DataBase.shareDataBase.closeMessageList()
    }
    
    
    //MARK: - 友盟回调
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        let result = UMSocialSnsService.handleOpenURL(url)
        if result == false {
            //调用其他sdk，例如支付宝、微信支付SDK等
        }
        return result
    }
    
    //MARK: - 信鸽回调
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        let deviceTokenStr = XGPush.registerDevice(deviceToken)
        if deviceTokenStr != nil {
            let params = [
                "action":"set_device_info",
                "uid":g_uid,
                "randcode":g_randcode,
                "data": [
                    "device_os": 1,
                    "device_token": deviceTokenStr
                ]
            ]
            //向服务发送deviceTokenStr
            JxlNetAPIManager.sharedManager.request_SetDeviceInfoWithBlock(Globe.baseUrlStr, Params: params as! [String : AnyObject], andBlock: { (data, error) in
                if let data = data {
                    let status = data["status"] as! NSNumber
                    if status != 1 {
                        SVProgressHUD.showInfoWithStatus(data["msg"] as! String)
                    }
                }
            })
        }
        print("deviceTokenStr: \(deviceTokenStr)")
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        print("didReceiveRemoteNotification-userInfo:--------------------\n \(userInfo)")
        XGPush.handleReceiveNotification(userInfo)
        BaseViewController.handleNotificationInfo(userInfo, applicationState: application.applicationState)
    }
    
}




























































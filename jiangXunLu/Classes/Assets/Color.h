//
//  Color.h
//  jiangXunLu
//
//  Created by lixu on 16/6/17.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#define DEFINE_COLOR_TRANSPARENT     0xFFFFFF00
#define DEFINE_COLOR_TRANSBLACK      0x00000088
#define DEFINE_COLOR_WHITE           0xFFFFFF
#define DEFINE_COLOR_BROWN           0x373635
#define DEFINE_COLOR_BROWN_LIGHT     0x606060
#define DEFINE_COLOR_HINT            0xAAAAAA
#define DEFINE_COLOR_BLACK           0x000000
#define DEFINE_COLOR_BLACK_LIGHT     0x333333
#define DEFINE_COLOR_GRAY_DARK       0x666666
#define DEFINE_COLOR_GRAY            0x999999
#define DEFINE_COLOR_BACK            0xEEEEEE
#define DEFINE_COLOR_DIVIDE_LING     0xCCCCCC
#define DEFINE_COLOR_RED             0xEA3D0D
#define DEFINE_COLOR_YELLOW          0xFECD08
#define DEFINE_COLOR_YELLOW_DARK     0xD0A90C
#define DEFINE_COLOR_YELLOW_LIGHT    0xEACB21

//
//  Dimen.h
//  jiangXunLu
//
//  Created by lixu on 16/6/17.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#define STATUS_BAR_HEIGHT                    20 // 状态栏高度
#define NAVIGATION_BAR_HEIGHT                44 // 导航条高度
#define STATUS_NAVIGATION_BAR_HEIGHT         64 // 状态栏和导航条总高度
#define BOTTOM_BAR_HEIGHT_MAIN               49 // tab bottombar高度
#define BOTTOM_BAR_HEIGHT_PROFILE            60 // 个人主页的bottombar高度

// 文字大小
#define FONTSIZE_MIN_EXTRA                   10.0f
#define FONTSIZE_MINI                        11.0f
#define FONTSIZE_SMALL                       12.0f
#define FONTSIZE_MIDDLE                      13.0f
#define FONTSIZE_MEDIA                       14.0f
#define FONTSIZE_BIG                         15.0f
#define FONTSIZE_EXTRA                       16.0f
#define FONTSIZE_MAX                         18.0f

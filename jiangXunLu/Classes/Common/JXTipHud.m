//
//  JXTipHud.m
//  jiangXunLu
//
//  Created by lixu06 on 16/6/17.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXTipHud.h"
#import "CommonTools.h"

@interface JXTipHud ()


@property (strong, nonatomic) UIView                *hudView;
@property (strong, nonatomic) UIView                *indicatorView;
@property (strong, nonatomic) CAKeyframeAnimation   *indicatorAnimation;

@property (strong, nonatomic) UIImageView           *hudImageView;
@property (strong, nonatomic) UILabel               *hudTipView;

@property (copy, nonatomic) void(^dismissHanlder)(void);

@property (nonatomic) BOOL showIndicator;
@property (nonatomic) BOOL canDismiss;
@property (nonatomic) BOOL bShowing;

@end

@implementation JXTipHud

- (instancetype)init {
    self = [super initWithFrame:[UIScreen mainScreen].bounds];
    
    return self;
}

- (void)dealloc {
    [JXTipHud dismiss];
}

#pragma mark - public method

+ (JXTipHud*)sharedHud {
    static dispatch_once_t once;
    static JXTipHud *sharedHud = nil;
    dispatch_once(&once, ^ { sharedHud = [[self alloc] init]; });
    
    return sharedHud;
}

+ (void)dismiss {
    [[JXTipHud sharedHud] hide];
}

+ (void)forceDismiss {
    [[JXTipHud sharedHud] dismissDirectly];
}

+ (void)showTip:(NSString *)tip dismissAfter:(NSTimeInterval)duration withFinishHandler:(void(^)(void))handler {
    [[JXTipHud sharedHud] showHudTip:tip image:nil indicator:NO duration:duration dismissHandler:handler];
}

+ (void)showSuccessTip:(NSString *)tip dismissAfter:(NSTimeInterval)duration withFinishHandler:(void(^)(void))handler {
    [[JXTipHud sharedHud] showHudTip:tip image:[JXTipHud successImage] indicator:NO duration:duration dismissHandler:handler];
}

+ (void)showFailTip:(NSString *)tip dismissAfter:(NSTimeInterval)duration withFinishHandler:(void(^)(void))handler {
    [[JXTipHud sharedHud] showHudTip:tip image:[JXTipHud errorImage] indicator:NO duration:duration dismissHandler:handler];
}

+ (void)showLoadingWithFinishHandler:(void(^)(void))handler {
    [[JXTipHud sharedHud] showHudTip:nil image:nil indicator:YES duration:0.0f dismissHandler:handler];
}

+ (void)showLoadingTip:(NSString *)tip withFinishHandler:(void(^)(void))handler {
    [[JXTipHud sharedHud] showHudTip:tip image:nil indicator:YES duration:0.0f dismissHandler:handler];
}

#pragma mark - private method

- (void)showHudTip:(NSString*)tip
             image:(UIImage*)image
         indicator:(BOOL)bIndicator
          duration:(NSTimeInterval)duration
    dismissHandler:(void(^)(void))handler {
    [self hide];
    self.bShowing = YES;
    
    self.dismissHanlder = handler;
    [self.hudTipView setText:tip];
    self.showIndicator = bIndicator;
    [self.hudImageView setImage:image];
    
    [self updatePosition];
    
    if (self.showIndicator){
        [self performSelector:@selector(showLoadingIndicator) withObject:self afterDelay:1.0f];
    }else{
        self.canDismiss = YES;
        [self showHudAutoHideAfter:duration];
    }
}

- (void)showHudAutoHideAfter:(NSTimeInterval)time {
    [self show];
    [self performSelector:@selector(hide) withObject:self afterDelay:time];
}

- (void)updatePosition {
    [self.indicatorView removeFromSuperview];
    [self.hudImageView removeFromSuperview];
    [self.hudTipView removeFromSuperview];
    
    UIView *topView = (self.showIndicator)? self.indicatorView: (self.hudImageView.image)? self.hudImageView: self.hudTipView;
    UIView *bottomView = (self.hudTipView.text.length > 0)? self.hudTipView: topView;
    
    if ([topView isEqual:bottomView]) {
        [self.hudView addSubview:topView];
        if ([topView isEqual:self.hudTipView]) {
            [self.hudView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-30-[topView]-30-|"
                                                                                 options:0
                                                                                 metrics:nil
                                                                                   views:@{@"topView":topView}]];
            [self.hudView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[topView]-20-|"
                                                                                 options:0
                                                                                 metrics:nil
                                                                                   views:@{@"topView":topView}]];
        } else {
            [self.hudView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-30@750-[topView]-30@750-|"
                                                                                 options:0
                                                                                 metrics:nil
                                                                                   views:@{@"topView":topView}]];
            [self.hudView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20@750-[topView]-20@750-|"
                                                                                 options:0
                                                                                 metrics:nil
                                                                                   views:@{@"topView":topView}]];
        }
        
        [self.hudView addConstraint:[NSLayoutConstraint constraintWithItem:topView
                                                                 attribute:NSLayoutAttributeCenterX
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.hudView
                                                                 attribute:NSLayoutAttributeCenterX
                                                                multiplier:1.0f
                                                                  constant:0.0f]];
        
        [self.hudView addConstraint:[NSLayoutConstraint constraintWithItem:topView
                                                                 attribute:NSLayoutAttributeCenterY
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.hudView
                                                                 attribute:NSLayoutAttributeCenterY
                                                                multiplier:1.0f
                                                                  constant:0.0f]];
    } else {
        [self.hudView addSubview:topView];
        [self.hudView addSubview:bottomView];
        // add horizontal constraints
        
        [self.hudView addConstraint:[NSLayoutConstraint constraintWithItem:topView
                                                                 attribute:NSLayoutAttributeCenterX
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.hudView
                                                                 attribute:NSLayoutAttributeCenterX
                                                                multiplier:1.0f
                                                                  constant:0.0f]];
        
        [self.hudView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[bottomView]-15-|"
                                                                             options:0
                                                                             metrics:nil
                                                                               views:@{@"bottomView":bottomView}]];
        // add vertical constraints
        [self.hudView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[topView]-10-[bottomView]-20-|"
                                                                             options:0
                                                                             metrics:nil
                                                                               views:@{@"bottomView":bottomView, @"topView":topView}]];
    }
}

- (void)showLoadingIndicator {
    self.canDismiss = NO;
    [self.indicatorView.layer addAnimation:self.indicatorAnimation forKey:@"transform"];
    [self show];
    [self performSelector:@selector(setHudCanBeDismissed) withObject:self afterDelay:1.0f];
}

- (void)setHudCanBeDismissed {
    self.canDismiss = YES;
    if (!self.showIndicator) {
        [self hide];
    }
}

- (void)show {
    UIView *superView = [[[UIApplication sharedApplication] windows] lastObject];
    [superView addSubview:self];
    [self setTranslatesAutoresizingMaskIntoConstraints:NO];
    [superView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[self]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:@{@"self":self}]];
    [superView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[self]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:@{@"self":self}]];
}

- (void)hide {
    if (!self.bShowing) return;
    
    self.bShowing = NO;
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    
    [self removeFromSuperview];
    
    self.hudImageView.image = nil;
    self.hudTipView.text = nil;
    
    if (self.showIndicator){
        [self.indicatorView.layer removeAnimationForKey:@"transform"];
    }
    
    if (self.dismissHanlder){
        self.dismissHanlder();
    }
    
    self.dismissHanlder = nil;
}

- (void)dismiss {
    if (!self.canDismiss) {
        self.showIndicator = NO;
    }else{
        [self hide];
    }
}

- (void)dismissDirectly {
    [self hide];
}

+ (UIImage *)successImage {
    static dispatch_once_t once;
    static UIImage *image = nil;
    dispatch_once(&once, ^ { image = [CommonTools imageNamed:@"sucess"]; });
    
    return image;
}

+ (UIImage *)errorImage {
    static dispatch_once_t once;
    static UIImage *image = nil;
    dispatch_once(&once, ^ { image = [CommonTools imageNamed:@"error"]; });
    
    return image;
}


#pragma mark - dynamic methods
- (UIView *)hudView {
    if(!_hudView) {
        _hudView = [[UIView alloc] init];
        _hudView.layer.cornerRadius = 8.0f;
        _hudView.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.8f];
        [self addSubview:_hudView];
        _hudView.translatesAutoresizingMaskIntoConstraints = NO;
        // Horizontal Constraints
        [_hudView addConstraint:[NSLayoutConstraint constraintWithItem:_hudView
                                                             attribute:NSLayoutAttributeWidth
                                                             relatedBy:NSLayoutRelationGreaterThanOrEqual
                                                                toItem:nil
                                                             attribute:NSLayoutAttributeNotAnAttribute
                                                            multiplier:0.0f
                                                              constant:120.0f]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:_hudView
                                                         attribute:NSLayoutAttributeWidth
                                                         relatedBy:NSLayoutRelationLessThanOrEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeWidth
                                                        multiplier:1.0f
                                                          constant:-100.0f]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:_hudView
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterX
                                                        multiplier:1.0f
                                                          constant:0.0f]];
        // Vertical Constraints
        [_hudView addConstraint:[NSLayoutConstraint constraintWithItem:_hudView
                                                             attribute:NSLayoutAttributeHeight
                                                             relatedBy:NSLayoutRelationGreaterThanOrEqual
                                                                toItem:nil
                                                             attribute:NSLayoutAttributeNotAnAttribute
                                                            multiplier:0.0f
                                                              constant:80.0f]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:_hudView
                                                         attribute:NSLayoutAttributeHeight
                                                         relatedBy:NSLayoutRelationLessThanOrEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeHeight
                                                        multiplier:0.3f
                                                          constant:0.0f]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:_hudView
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.0f
                                                          constant:0.0f]];
    }
    
    return _hudView;
}

- (UILabel *)hudTipView {
    if(!_hudTipView){
        _hudTipView = [[UILabel alloc] initWithFrame:CGRectZero];
        _hudTipView.adjustsFontSizeToFitWidth = YES;
        _hudTipView.textAlignment = NSTextAlignmentCenter;
        _hudTipView.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
        _hudTipView.numberOfLines = 0;
        _hudTipView.textColor = [UIColor whiteColor];
        _hudTipView.font = [UIFont systemFontOfSize:16.0f];
        _hudTipView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    
    return _hudTipView;
}

- (UIImageView *)hudImageView {
    if (!_hudImageView) {
        _hudImageView = [[UIImageView alloc] init];
        _hudImageView.translatesAutoresizingMaskIntoConstraints = NO;
        [_hudImageView addConstraint:[NSLayoutConstraint constraintWithItem:_hudImageView
                                                                  attribute:NSLayoutAttributeWidth
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:nil
                                                                  attribute:NSLayoutAttributeNotAnAttribute
                                                                 multiplier:0.0f
                                                                   constant:30.0f]];
        [_hudImageView addConstraint:[NSLayoutConstraint constraintWithItem:_hudImageView
                                                                  attribute:NSLayoutAttributeHeight
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:nil
                                                                  attribute:NSLayoutAttributeNotAnAttribute
                                                                 multiplier:0.0f
                                                                   constant:30.0f]];
    }
    
    return _hudImageView;
}

- (UIView *)indicatorView {
    if (!_indicatorView) {
        _indicatorView = [[UIView alloc] init];
        _indicatorView.layer.contents = (id)[[CommonTools imageNamed:@"loading"] CGImage];
        _indicatorView.translatesAutoresizingMaskIntoConstraints = NO;
        [_indicatorView addConstraint:[NSLayoutConstraint constraintWithItem:_indicatorView
                                                                   attribute:NSLayoutAttributeWidth
                                                                   relatedBy:NSLayoutRelationEqual
                                                                      toItem:nil
                                                                   attribute:NSLayoutAttributeNotAnAttribute
                                                                  multiplier:0.0f
                                                                    constant:30.0f]];
        [_indicatorView addConstraint:[NSLayoutConstraint constraintWithItem:_indicatorView
                                                                   attribute:NSLayoutAttributeHeight
                                                                   relatedBy:NSLayoutRelationEqual
                                                                      toItem:nil
                                                                   attribute:NSLayoutAttributeNotAnAttribute
                                                                  multiplier:0.0f
                                                                    constant:30.0f]];
    }
    
    return _indicatorView;
}

- (CAKeyframeAnimation *)indicatorAnimation {
    if (!_indicatorAnimation) {
        _indicatorAnimation = [CAKeyframeAnimation animation];
        _indicatorAnimation.values = [NSArray arrayWithObjects:
                                      [NSValue valueWithCATransform3D:CATransform3DMakeRotation(0, 0, 0, 1)],
                                      [NSValue valueWithCATransform3D:CATransform3DMakeRotation(3.13f, 0, 0, 1)],
                                      [NSValue valueWithCATransform3D:CATransform3DMakeRotation(6.26f, 0, 0,1)],
                                      nil];
        _indicatorAnimation.cumulative = YES;
        _indicatorAnimation.duration = 0.8;
        _indicatorAnimation.repeatCount = INFINITY;
        _indicatorAnimation.removedOnCompletion = YES;
    }
    
    return _indicatorAnimation;
}
@end


//
//  JXViewControlerRouter.h
//  jiangXunLu
//
//  Created by lixu06 on 16/6/17.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JXProfileDetailViewController.h"
#import "JXProfileEditViewController.h"
#import "JXAcquaitanceViewController.h"

@interface JXViewControlerRouter : NSObject

/*
 * 跳转个人主页-MAIN 自己的
 * @param data 数据从userdefault中读取，这里传入空值即可
 */
+ (void)pushToProfileSelfViewController:(UIViewController*)orgVC
                               animated:(BOOL)animated
                                   data:(id)data;
/*
 * 跳转个人主页-MAIN 别人的
 * @param data 数据请求，{"targertId":userId}
 */
+ (void)pushToProfileOtherViewController:(UIViewController*)orgVC
                                animated:(BOOL)animated
                                    data:(id)data;

/**
 *  跳转别人的主页
 *
 *  @param orgVC       源controller
 *  @param animated    是否动画
 *  @param data        数据请求，{"targertId":userId}
 *  @param finishBlock 回调
 */
+ (void)pushToProfileOtherViewController:(UIViewController*)orgVC
                                animated:(BOOL)animated
                                    data:(id)data
                             finishBlock:(JXViewControllerBlock)finishBlock;

/*
 * 跳转个人主页-基本信息
 * @param data 卡片的info节点
 */
+ (void)pushToProfileDetailEditableViewController:(UIViewController*)orgVC
                                         animated:(BOOL)animated
                                             data:(id)data
                                      finishBlock:(JXViewControllerBlock )finishBlock ;
+ (void)pushToProfileDetailDisabledViewController:(UIViewController*)orgVC
                                         animated:(BOOL)animated
                                             data:(id)data;
/*
 * 跳转个人主页-编辑页
 */
+ (void)pushToProfileEditViewController:(UIViewController*)orgVC
                               animated:(BOOL)animated
                                   data:(id)data
                            finishBlock:(JXViewControllerBlock)finishBlock;

/*
 * 跳转个人主页-人脉
 */
+ (void)pushToAcquaitanceViewController:(UIViewController*)orgVC
                               animated:(BOOL)animated
                                   data:(id)data;

/*
 * 退出 当前VC
 */
+ (BOOL)popCurrentViewController:(UIViewController*)orgVC animated:(BOOL)animated;
@end

//
//  JXTipHud.h
//  jiangXunLu
//
//  Created by lixu06 on 16/6/17.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JXTipHud : UIView

+ (JXTipHud *)sharedHud;
+ (void)dismiss;
+ (void)forceDismiss;

+ (void)showTip:(NSString *)tip dismissAfter:(NSTimeInterval)duration withFinishHandler:(void(^)(void))handler;
+ (void)showSuccessTip:(NSString *)tip dismissAfter:(NSTimeInterval)duration withFinishHandler:(void(^)(void))handler;
+ (void)showFailTip:(NSString *)tip dismissAfter:(NSTimeInterval)duration withFinishHandler:(void(^)(void))handler;
+ (void)showLoadingWithFinishHandler:(void(^)(void))handler;
+ (void)showLoadingTip:(NSString *)tip withFinishHandler:(void(^)(void))handler;

- (void)showHudTip:(NSString*)tip
             image:(UIImage*)image
         indicator:(BOOL)bIndicator
          duration:(NSTimeInterval)duration
    dismissHandler:(void(^)(void))handler;
@end


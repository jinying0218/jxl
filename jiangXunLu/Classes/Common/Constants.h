//
//  Constants.h
//  jiangXunLu
//
//  Created by 金莹 on 16/9/1.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#ifndef Constants_h
#define Constants_h


typedef enum JXLDisType {
    kDiscoverProject = 1,      //1.项目 2，品牌  3.其他
    kDiscoverBrand = 2,
    kDiscoverInfo = 3
    
}JXLDisType;

typedef enum JXLRealseType {
    kDiscoverRealse = 1,      //1.发布  2.编辑
    kDiscoverEdit = 2,
    
}JXLRealseType;


typedef void(^HeaderImageViewClickHandler)(id cellModel);


#endif /* Constants_h */

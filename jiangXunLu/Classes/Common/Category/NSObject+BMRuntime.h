//
//  NSObject+BMRuntime.h
//  ZXBaiduApp
//
//  Created by lixu06 on 16/7/8.
//  Copyright © 2016年 百度医生. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface NSObject (BMRuntime)

- (void)performSelectorSEL:(SEL)selector withObject:(id) object;
- (void)performSelectorNamed:(NSString *)selectorName withObject:(id) object;
- (void)performSelectorSEL:(SEL)selector withObject:(id) object1 withObject:(id) object2;
- (void)performSelectorNamed:(NSString *)selectorName withObject:(id) object1 withObject:(id) object2;

@end

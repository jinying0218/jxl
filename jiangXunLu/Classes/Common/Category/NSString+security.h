//
//  NSString+security.h
//  jiangXunLu
//
//  Created by 金莹 on 16/9/4.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (security)
+ (NSString *)md5:(NSString *)str;
@end

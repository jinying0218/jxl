//
//  UIScrollView+Refresh.h
//  jiangXunLu
//
//  Created by lixu06 on 16/7/20.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSObject (JXRefresh)

-(void)setUpRefreshContent:(UIScrollView *)scrollView;
-(void)setUpRefreshHeader:(UIView *)headerRefreshView onSuperView:(UIView *)superView;
-(void)setUpRefreshFooter:(UIView *)footerRefreshView onSuperView:(UIView *)superView;
-(void)setUpNoneFooter:(UIView *)footerNoneView onSuperView:(UIView *)superView;

- (void)headerRefresh;
- (BOOL)headerRefreshable;
- (void)headerRefreshAction;
- (void)headerRefreshBegin;
- (void)headerRefreshEnd;
- (void)footerRefresh;
- (BOOL)footerRefreshable;
- (void)footerRefreshAction;
- (void)footerRefreshBegin;
- (void)footerRefreshEnd;

@property (nonatomic, strong) UIScrollView * ex_scrollView;
@property (nonatomic, strong) UIView *headerRefreshView;
@property (nonatomic, strong) UIView *footerRefreshView;
@property (nonatomic, strong) UIView *footerNoneView;
@property (nonatomic, strong) NSValue *scrollViewFrame;
@property (nonatomic, strong) NSValue *headerRefreshFrame;
@property (nonatomic, strong) NSValue *footerRefreshFrame;
@property (nonatomic, strong) NSValue *footerNoneFrame;
@property (nonatomic, strong) NSNumber *isHeaderRefreshing;
@property (nonatomic, strong) NSNumber *isFooterRefreshing;
@property (nonatomic, strong) NSNumber *isFooterNone;
@property (nonatomic, strong) NSNumber *animationTime;

- (void)refresh_scrollViewDidScroll:(UIScrollView *)scrollView;
- (void)refresh_scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
- (void)refresh_scrollViewDidEndDecelerating:(UIScrollView *)scrollView;

@end

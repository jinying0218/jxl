//
//  UITableView+Common.swift
//  jiangXunLu
//
//  Created by liuxb on 16/6/12.
//  Copyright © 2016年 teamOne. All rights reserved.

import UIKit

extension UITableView {

    func addRadiusforCell(cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if cell.respondsToSelector(Selector("tintColor")) {
            let cornerRadius:CGFloat = 5.0
            cell.backgroundColor = UIColor.clearColor()

            let layer = CAShapeLayer()
            let pathRef = CGPathCreateMutable()
            let bounds = CGRectInset(cell.bounds, 0, 0)
            
            var addLine = false
            
            if indexPath.row == 0 && indexPath.row == self.numberOfRowsInSection(indexPath.section)-1 {
                CGPathAddRoundedRect(pathRef, nil, bounds, cornerRadius, cornerRadius)
            } else if indexPath.row == 0 {
                CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds))
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds), CGRectGetMidX(bounds), CGRectGetMinY(bounds), cornerRadius)
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius)
                CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds))
                addLine = true
            } else if indexPath.row == self.numberOfRowsInSection(indexPath.section)-1 {
                CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds))
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds), CGRectGetMidX(bounds), CGRectGetMaxY(bounds), cornerRadius)
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius)
                CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds))
            } else {
                CGPathAddRect(pathRef, nil, bounds)
                addLine = true
            }
            
            layer.path = pathRef
            
            if cell.backgroundColor != nil {
                layer.fillColor = cell.backgroundColor?.CGColor
            } else if cell.backgroundView != nil && cell.backgroundView?.backgroundColor != nil {
                layer.fillColor = cell.backgroundView?.backgroundColor?.CGColor
            } else {
                layer.fillColor = UIColor(white: 1.0, alpha: 0.8).CGColor
            }
            
            if addLine {
                let lineLayer = CALayer()
                let lineHeight = 1.0/UIScreen.mainScreen().scale
                lineLayer.frame = CGRect(x: CGRectGetMinX(bounds) + 2, y: bounds.size.height - lineHeight, width: bounds.size.width - 2, height: lineHeight)
                lineLayer.backgroundColor = self.separatorColor?.CGColor
                layer.addSublayer(lineLayer)
            }
            
            let view = UIView(frame: bounds)
            view.layer.insertSublayer(layer, atIndex: 0)
            view.backgroundColor = UIColor.clearColor()
            cell.backgroundView = view
        }
    }
    
    func addLineforPlainCell(cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath, withLeftSpaceAndSectionLine leftSpace:CGFloat) {
        let layer = CAShapeLayer()
        let pathRef = CGPathCreateMutable()
        let bounds = CGRectInset(cell.bounds, 0, 0)
        
        CGPathAddRect(pathRef, nil, bounds)
        layer.path = pathRef
        
        if cell.backgroundColor != nil {
            layer.fillColor = cell.backgroundColor?.CGColor
        } else if cell.backgroundView != nil && cell.backgroundView?.backgroundColor != nil {
            layer.fillColor = cell.backgroundView?.backgroundColor?.CGColor
        } else {
            layer.fillColor = UIColor(white: 1.0, alpha: 0.8).CGColor
        }
        
        let lineColor = UIColor(red: 0.867, green: 0.867, blue: 0.867, alpha: 1.0).CGColor
        
        // last row
        if self.numberOfSections == indexPath.section + 1 && indexPath.row == self.numberOfRowsInSection(indexPath.section) - 1 {
            self.layer(layer, addLineUp: false, andLong: true, andColor: lineColor, andBounds: bounds, withLeftSpace: 0)
        } else {
            self.layer(layer, addLineUp: false, andLong: false, andColor: lineColor, andBounds: bounds, withLeftSpace: leftSpace)
        }
        
        let view =  UIView(frame: bounds)
        view.layer.insertSublayer(layer, atIndex: 0)
        cell.backgroundView = view
    }
    
    func addLineforPlainCell(cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath, withLeftSpace leftSpace:CGFloat, hasSectionLine isHasSectionLine:Bool) {
        let layer = CAShapeLayer()
        let pathRef = CGPathCreateMutable()
        let bounds = CGRectInset(cell.bounds, 0, 0)
        
        CGPathAddRect(pathRef, nil, bounds)
        layer.path = pathRef
        
        if cell.backgroundColor != nil {
            layer.fillColor = cell.backgroundColor?.CGColor
        } else if cell.backgroundView != nil && cell.backgroundView?.backgroundColor != nil {
            layer.fillColor = cell.backgroundView?.backgroundColor?.CGColor;
        } else {
            layer.fillColor = UIColor(white: 1.0, alpha:0.8).CGColor
        }

        let sectionLineColor = UIColor(red: 0.867, green: 0.867, blue: 0.867, alpha: 1.0).CGColor
        
        if indexPath.row == 0 && indexPath.row == self.numberOfRowsInSection(indexPath.section) - 1 {
            //just only one
            if isHasSectionLine {
                self.layer(layer, addLineUp: true, andLong: true, andColor: sectionLineColor, andBounds: bounds, withLeftSpace: 0)
                self.layer(layer, addLineUp: false, andLong: true, andColor: sectionLineColor, andBounds: bounds, withLeftSpace: 0)
            }
        } else if indexPath.row == self.numberOfRowsInSection(indexPath.section) - 1 {
            //last
            if isHasSectionLine {
                self.layer(layer, addLineUp: false, andLong: true, andColor: sectionLineColor, andBounds: bounds, withLeftSpace: 0)
            }
        } else {
            //middle
            self.layer(layer, addLineUp: false, andLong: false, andColor: sectionLineColor, andBounds: bounds, withLeftSpace: 0)
        }

        let view =  UIView(frame: bounds)
        view.layer.insertSublayer(layer, atIndex: 0)
        cell.backgroundView = view
    }
    
    func addLineforPlainCell(cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath, withLeftSpace leftSpace:CGFloat) {
        self.addLineforPlainCell(cell, forRowAtIndexPath: indexPath, withLeftSpace: leftSpace, hasSectionLine: true)
    }

    func layer(layer:CALayer, addLineUp isUp: Bool, andLong isLong: Bool, andColor color: CGColor, andBounds bounds:CGRect, withLeftSpace leftSpace:CGFloat) {
        let lineLayer = CALayer()
        let lineHeight = 1.0/UIScreen.mainScreen().scale
        var left:CGFloat = 0, top: CGFloat = 0
        
        if isUp {
            top = 0
        } else {
            top = bounds.size.height - lineHeight
        }
        
        if isLong {
            left = 0
        } else {
            left = leftSpace
        }
        
        lineLayer.frame = CGRect(x: CGRectGetMinX(bounds) + left, y: top, width: bounds.size.width - left, height: lineHeight)
        lineLayer.backgroundColor = color
        layer.addSublayer(lineLayer)
    }
}


























































































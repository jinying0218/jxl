//
//  UIView+Initiation.h
//  jiangXunLu
//
//  Created by lixu06 on 16/6/24.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Initiation)

+ (id)defaultView;

@end
//
//  NSString+SizeCalculator.h
//  jiangXunLu
//
//  Created by lixu06 on 16/6/17.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSString (SizeCalculator)

- (CGSize)sizeWithFont:(UIFont *)font maxSize:(CGSize)maxSize;

@end

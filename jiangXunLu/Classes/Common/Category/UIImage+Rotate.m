//
//  UIImage+Rotate.m
//  jiangXunLu
//
//  Created by lixu06 on 16/8/9.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "UIImage+Rotate.h"

@implementation UIImage (Rotate)
+ (UIImage *)image:(UIImage *)image rotation:(UIImageOrientation)orientation
{
    long double rotate = 0.0;
    CGRect rect;
    float translateX = 0;
    float translateY = 0;
    float scaleX = 1.0;
    float scaleY = 1.0;
    
    switch (orientation) {
        case UIImageOrientationLeft:
            rotate = M_PI_2;
            rect = CGRectMake(0, 0, image.size.height, image.size.width);
            translateX = 0;
            translateY = -rect.size.width;
            scaleY = rect.size.width/rect.size.height;
            scaleX = rect.size.height/rect.size.width;
            break;
        case UIImageOrientationRight:
            rotate = 3 * M_PI_2;
            rect = CGRectMake(0, 0, image.size.height, image.size.width);
            translateX = -rect.size.height;
            translateY = 0;
            scaleY = rect.size.width/rect.size.height;
            scaleX = rect.size.height/rect.size.width;
            break;
        case UIImageOrientationDown:
            rotate = M_PI;
            rect = CGRectMake(0, 0, image.size.width, image.size.height);
            translateX = -rect.size.width;
            translateY = -rect.size.height;
            break;
        default:
            rotate = 0.0;
            rect = CGRectMake(0, 0, image.size.width, image.size.height);
            translateX = 0;
            translateY = 0;
            break;
    }
    
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    //做CTM变换
    CGContextTranslateCTM(context, 0.0, rect.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextRotateCTM(context, rotate);
    CGContextTranslateCTM(context, translateX, translateY);
    
    CGContextScaleCTM(context, scaleX, scaleY);
    //绘制图片
    CGContextDrawImage(context, CGRectMake(0, 0, rect.size.width, rect.size.height), image.CGImage);
    
    UIImage *newPic = UIGraphicsGetImageFromCurrentImageContext();
    
    return newPic;
}

+ (UIImage *)fixOrientation:(UIImage *)aImage {
    if (!aImage) {
        return nil;
    }
    switch (aImage.imageOrientation) {
        case UIImageOrientationUp:{
            return aImage;
        }
        case UIImageOrientationUpMirrored:{
            break;
        }
        case UIImageOrientationDown:{
            return [UIImage image:aImage rotation:UIImageOrientationDown];
        }
        case UIImageOrientationDownMirrored:{
            break;
        }
        case UIImageOrientationLeft: {
            return [UIImage image:aImage rotation:UIImageOrientationLeft];
        }
        case UIImageOrientationLeftMirrored: {
            break;
        }
        case UIImageOrientationRight: {
            return [UIImage image:aImage rotation:UIImageOrientationRight];
        }
        case UIImageOrientationRightMirrored:{
            break;
        }
        default: {
            break;
        }
    }
    return aImage;
}

@end

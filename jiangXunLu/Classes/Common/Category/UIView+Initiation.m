//
//  UIView+Initiation.m
//  jiangXunLu
//
//  Created by lixu06 on 16/6/24.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "UIView+Initiation.h"


@implementation UIView (Initiation)

+ (id)defaultView {
    NSString *nibName = NSStringFromClass([self class]);
    NSArray *cells = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    if ([cells count]<1) {
        return nil;
    }
    return [cells firstObject];
}


@end
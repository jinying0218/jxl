//
//  UITextView+LimitLength.h
//  jiangXunLu
//
//  Created by lixu06 on 16/6/17.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextView (LimitLength)

/** 
 * 限制最多字数
 * @param maxLength 最多字数
 * @return 是否设置成功
 */
- (BOOL)limitLengthTo:(NSInteger)maxLength;

@end

//
//  UIScrollView+Refresh.m
//  jiangXunLu
//
//  Created by lixu06 on 16/7/20.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "NSObject+JXRefresh.h"
#import <objc/runtime.h>
#import "EXTScope.h"
#import "GlobalDefine.h"

static const void *ScrollViewKey = &ScrollViewKey;
static const void *HeaderRefreshViewKey = &HeaderRefreshViewKey;
static const void *FooterRefreshViewKey = &FooterRefreshViewKey;
static const void *FooterNoneViewKey = &FooterNoneViewKey;
static const void *ScrollViewFrameKey = &ScrollViewFrameKey;
static const void *HeaderRefreshFrameKey = &HeaderRefreshFrameKey;
static const void *FooterRefreshFrameKey = &FooterRefreshFrameKey;
static const void *FooterNoneFrameKey = &FooterNoneFrameKey;
static const void *IsHeaderRefreshingKey = &IsHeaderRefreshingKey;
static const void *IsFooterRefreshingKey = &IsFooterRefreshingKey;
static const void *IsFooterNoneKey = &IsFooterNoneKey;
static const void *AnimationTimeKey = &AnimationTimeKey;

@implementation NSObject (JXRefresh)

@dynamic ex_scrollView;
@dynamic headerRefreshView;
@dynamic footerRefreshView;
@dynamic footerNoneView;
@dynamic scrollViewFrame;
@dynamic headerRefreshFrame;
@dynamic footerRefreshFrame;
@dynamic footerNoneFrame;
@dynamic isHeaderRefreshing;
@dynamic isFooterRefreshing;
@dynamic isFooterNone;
@dynamic animationTime;

#pragma mark - Getters & Setters

- (void)setEx_scrollView:(UIView *)scrollView {
    objc_setAssociatedObject(self, ScrollViewKey, scrollView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIView *)ex_scrollView {
    return objc_getAssociatedObject(self, ScrollViewKey);
}

- (void)setHeaderRefreshView:(UIView *)headerRefreshView {
    objc_setAssociatedObject(self, HeaderRefreshViewKey, headerRefreshView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIView *)headerRefreshView {
    return objc_getAssociatedObject(self, HeaderRefreshViewKey);
}

- (void)setFooterRefreshView:(UIView *)footerRefreshView {
    objc_setAssociatedObject(self, FooterRefreshViewKey, footerRefreshView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIView *)footerRefreshView {
    return objc_getAssociatedObject(self, FooterRefreshViewKey);
}

- (void)setFooterNoneView:(UIView *)footerNoneView {
    objc_setAssociatedObject(self, FooterNoneViewKey, footerNoneView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIView *)footerNoneView {
    return objc_getAssociatedObject(self, FooterNoneViewKey);
}

- (void)setScrollViewFrame:(NSValue *)scrollViewFrame {
    objc_setAssociatedObject(self, ScrollViewFrameKey, scrollViewFrame, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSValue *)scrollViewFrame {
    return objc_getAssociatedObject(self, ScrollViewFrameKey);
}

- (void)setHeaderRefreshFrame:(NSValue *)headerRefreshFrame {
    objc_setAssociatedObject(self, HeaderRefreshFrameKey, headerRefreshFrame, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSValue *)headerRefreshFrame {
    return objc_getAssociatedObject(self, HeaderRefreshFrameKey);
}

- (void)setFooterRefreshFrame:(NSValue *)footerRefreshFrame {
    objc_setAssociatedObject(self, FooterRefreshFrameKey, footerRefreshFrame, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSValue *)footerRefreshFrame {
    return objc_getAssociatedObject(self, FooterRefreshFrameKey);
}

- (void)setFooterNoneFrame:(NSValue *)footerNoneFrame {
    objc_setAssociatedObject(self, FooterNoneFrameKey, footerNoneFrame, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSValue *)footerNoneFrame {
    return objc_getAssociatedObject(self, FooterNoneFrameKey);
}

- (void)setIsHeaderRefreshing:(NSNumber *)isHeaderRefreshing {
    objc_setAssociatedObject(self, IsHeaderRefreshingKey, isHeaderRefreshing, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSNumber *)isHeaderRefreshing {
    return objc_getAssociatedObject(self, IsHeaderRefreshingKey);
}

- (void)setIsFooterRefreshing:(NSNumber *)isFooterRefreshing {
    objc_setAssociatedObject(self, IsFooterRefreshingKey, isFooterRefreshing, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSNumber *)isFooterRefreshing {
    return objc_getAssociatedObject(self, IsFooterRefreshingKey);
}

- (void)setIsFooterNone:(NSNumber *)isFooterNone {
    objc_setAssociatedObject(self, IsFooterNoneKey, isFooterNone, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSNumber *)isFooterNone {
    return objc_getAssociatedObject(self, IsFooterNoneKey);
}

- (void)setAnimationTime:(NSNumber *)animationTime {
    objc_setAssociatedObject(self, AnimationTimeKey, animationTime, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSNumber *)animationTime {
    return objc_getAssociatedObject(self, AnimationTimeKey);
}


#pragma mark - Header & Footer Control
-(void)setUpRefreshContent:(UIScrollView *)scrollView{
    self.ex_scrollView = scrollView;
    self.isHeaderRefreshing = [NSNumber numberWithBool:NO];
    self.isFooterRefreshing = [NSNumber numberWithBool:NO];
    self.isFooterNone = [NSNumber numberWithBool:NO];
    self.scrollViewFrame = [NSValue valueWithCGRect:scrollView.frame];
    self.animationTime = [NSNumber numberWithDouble:0.80f];
}
-(void)setUpRefreshHeader:(UIView *)view onSuperView:(UIView *)superView{
    
    self.headerRefreshView = view;
    self.headerRefreshFrame = [NSValue valueWithCGRect:RECT(view.frame.origin.x,
                                                            view.frame.origin.y,
                                                            view.frame.size.width,
                                                            view.frame.size.height)]; //view.frame
    [superView addSubview:self.headerRefreshView];
    self.headerRefreshView.hidden = YES;
    
}
-(void)setUpRefreshFooter:(UIView *)view onSuperView:(UIView *)superView{
    self.footerRefreshView = view;
    self.footerRefreshFrame = [NSValue valueWithCGRect:RECT(view.frame.origin.x,
                                                            view.frame.origin.y,
                                                            view.frame.size.width,
                                                            view.frame.size.height)]; //view.frame
    [superView addSubview:self.footerRefreshView];
    self.footerRefreshView.hidden = YES;
}

-(void)setUpNoneFooter:(UIView *)view onSuperView:(UIView *)superView{
    self.footerNoneView = view;
    self.footerNoneFrame = [NSValue valueWithCGRect:RECT(view.frame.origin.x,
                                                         view.frame.origin.y,
                                                         view.frame.size.width,
                                                         view.frame.size.height)]; //view.frame
}

- (BOOL)headerRefreshable {
    return NO;
}
- (BOOL)footerRefreshable {
    return NO;
}
- (void)headerRefreshAction{
}
- (void)footerRefreshAction {
}

- (void)headerRefresh {
    [self headerRefreshAction];
//    [self headerRefreshEnd];
}
- (void)footerRefresh {
    [self footerRefreshAction];
//    [self footerRefreshEnd];
}
- (void)headerRefreshBegin {
    self.isHeaderRefreshing = [NSNumber numberWithBool:YES];
    CGRect headerRefreshFrame = [self.headerRefreshFrame CGRectValue];
    CGRect scrollViewFrame = [self.scrollViewFrame CGRectValue];
    self.headerRefreshView.frame = RECT(0,
                                        scrollViewFrame.origin.y,
                                        headerRefreshFrame.size.width,
                                        headerRefreshFrame.size.height);
    self.ex_scrollView.contentInset = UIEdgeInsetsMake(headerRefreshFrame.size.height, 0, 0, 0);
}
- (void)footerRefreshBegin {
    self.isFooterRefreshing = [NSNumber numberWithBool:YES];
    CGRect footerRefreshFrame = [self.footerRefreshFrame CGRectValue];
    CGRect scrollViewFrame = [self.scrollViewFrame CGRectValue];
    self.footerRefreshView.frame = RECT(0,
                                        scrollViewFrame.origin.y + scrollViewFrame.size.height - footerRefreshFrame.size.height,
                                        footerRefreshFrame.size.width,
                                        footerRefreshFrame.size.height);
    self.ex_scrollView.contentInset = UIEdgeInsetsMake(0, 0, footerRefreshFrame.size.height, 0);
}
- (void)headerRefreshEnd {
    self.isHeaderRefreshing = [NSNumber numberWithBool:NO];
    @weakify(self)
    [UIView animateWithDuration:[self.animationTime doubleValue]
                     animations:^() {
                         @strongify(self)
                         self.headerRefreshView.frame = [self.headerRefreshFrame CGRectValue];
                         self.ex_scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
                     }
                     completion:^(BOOL finished){
                         @strongify(self)
                         self.headerRefreshView.hidden = YES;
                     }];
    
}
- (void)footerRefreshEnd {
    self.isFooterRefreshing = [NSNumber numberWithBool:NO];
    @weakify(self)
    [UIView animateWithDuration:[self.animationTime doubleValue]
                     animations:^(){
                         @strongify(self)
                         self.footerRefreshView.frame = [self.footerRefreshFrame CGRectValue];
                         self.ex_scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
                     }
                     completion:^(BOOL finished){
                         @strongify(self)
                         self.footerRefreshView.hidden = YES;
                     }];
    
}

#pragma mark -
#pragma mark UIScrollViewDelegate

- (void)refresh_scrollViewDidScroll:(UIScrollView *)scrollView{
    CGPoint offset = scrollView.contentOffset;
    CGRect scrollViewFrame = [self.scrollViewFrame CGRectValue];
    CGRect headerRefreshFrame = [self.headerRefreshFrame CGRectValue];
    if (offset.y < 0) { // down
        if(offset.y<-1){
            self.headerRefreshView.hidden = NO;
        }
        if (offset.y >= -headerRefreshFrame.size.height) {
            if ([self.isHeaderRefreshing boolValue]) {
                self.headerRefreshView.frame = RECT(0,
                                                    scrollViewFrame.origin.y,
                                                    headerRefreshFrame.size.width,
                                                    headerRefreshFrame.size.height);
            } else{
                self.headerRefreshView.frame = RECT(0,
                                                    headerRefreshFrame.origin.y - offset.y,
                                                    headerRefreshFrame.size.width,
                                                    headerRefreshFrame.size.height);
            }
        }
    } else if (offset.y == 0) {
    } else if (offset.y > 0) { // up
        CGRect footerRefreshFrame = [self.footerRefreshFrame CGRectValue];
        if (scrollView.contentSize.height <= scrollView.frame.size.height) {
            if(offset.y>1){
                self.footerRefreshView.hidden = NO;
            }
            if (offset.y <= footerRefreshFrame.size.height) {
                if (![self.isFooterRefreshing boolValue]) {
                    self.footerRefreshView.frame = RECT(0,
                                                        footerRefreshFrame.origin.y - offset.y,
                                                        footerRefreshFrame.size.width,
                                                        footerRefreshFrame.size.height);
                }
            }
        } else {
            CGFloat delta = scrollView.contentSize.height - scrollView.frame.size.height;
            if(offset.y> delta + 1){
                self.footerRefreshView.hidden = NO;
            }
            if (offset.y <= delta + footerRefreshFrame.size.height) {
                if (![self.isFooterRefreshing boolValue]) {
                    self.footerRefreshView.frame = RECT(0,
                                                        footerRefreshFrame.origin.y - offset.y + delta,
                                                        footerRefreshFrame.size.width,
                                                        footerRefreshFrame.size.height);
                }
            }
        }
    }
}

- (void)refresh_scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    CGPoint offset = scrollView.contentOffset;
    CGRect scrollViewFrame = [self.scrollViewFrame CGRectValue];
    CGRect headerRefreshFrame = [self.headerRefreshFrame CGRectValue];
    CGRect footerRefreshFrame = [self.footerRefreshFrame CGRectValue];
    if (offset.y < 0) { // down
        if (offset.y < - headerRefreshFrame.size.height) {
            if (![self.isHeaderRefreshing boolValue]) {
                self.headerRefreshView.frame = RECT(0,
                                                    scrollViewFrame.origin.y,
                                                    headerRefreshFrame.size.width,
                                                    headerRefreshFrame.size.height);
                [self headerRefreshBegin];
            }
        }
    } else if (offset.y == 0) {
    } else if (offset.y > 0) { // up
        if (scrollView.contentSize.height <= scrollView.frame.size.height) {
            if (offset.y > footerRefreshFrame.size.height) {
                if (![self.isFooterRefreshing boolValue]) {
                    [self footerRefreshBegin];
                }
            }
        } else{
            CGFloat delta = scrollView.contentSize.height - scrollView.frame.size.height;
            if (offset.y > delta + footerRefreshFrame.size.height) {
                if (![self.isFooterRefreshing boolValue]) {
                    [self footerRefreshBegin];
                }
            }
        }
        
    }
}

- (void)refresh_scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if([self.isHeaderRefreshing boolValue]){
        [self headerRefresh];
    }
    if([self.isFooterRefreshing boolValue]){
        [self footerRefresh];
    }
}

@end

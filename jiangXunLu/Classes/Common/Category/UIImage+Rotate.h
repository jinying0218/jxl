//
//  UIImage+UIImage_Rotate.h
//  jiangXunLu
//
//  Created by lixu06 on 16/8/9.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (UIImage_Rotate)

+ (UIImage *)image:(UIImage *)image rotation:(UIImageOrientation)orientation;
+ (UIImage *)fixOrientation:(UIImage *)aImage;

@end

//
//  NSObject+BMRuntime.m
//  ZXBaiduApp
//
//  Created by lixu06 on 16/7/8.
//  Copyright © 2016年 百度医生. All rights reserved.
//

#import "NSObject+BMRuntime.h"
#import <objc/runtime.h>
#import <objc/message.h>

@implementation NSObject (BMRuntime)

- (void)performSelectorSEL:(SEL)selector withObject:(id) object{
    void (*func)(id, SEL ,id) = (__typeof__(func))objc_msgSend;
    func(self, selector,object);
}

- (void)performSelectorNamed:(NSString *)selectorName withObject:(id) object{
    SEL faSelector = NSSelectorFromString(selectorName);
    if([self respondsToSelector:faSelector]){
        [self performSelectorSEL:faSelector withObject:object];
    }
}

- (void)performSelectorSEL:(SEL)selector withObject:(id) object1 withObject:(id) object2{
    void (*func)(id, SEL ,id,id) = (__typeof__(func))objc_msgSend;
    func(self, selector,object1,object2);
}

- (void)performSelectorNamed:(NSString *)selectorName withObject:(id) object1 withObject:(id) object2{
    SEL faSelector = NSSelectorFromString(selectorName);
    [self performSelectorSEL:faSelector withObject:object1 withObject:object2];
}
@end

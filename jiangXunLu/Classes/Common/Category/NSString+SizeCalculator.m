//
//  NSString+SizeCalculator.m
//  jiangXunLu
//
//  Created by lixu06 on 16/6/17.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "NSString+SizeCalculator.h"

@implementation NSString (SizeCalculator)

- (CGSize)sizeWithFont:(UIFont *)font maxSize:(CGSize)maxSize{
    if (!self.length) return CGSizeZero;
    NSDictionary *attrs = @{NSFontAttributeName : font};
    return [self boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attrs context:nil].size;
}

@end

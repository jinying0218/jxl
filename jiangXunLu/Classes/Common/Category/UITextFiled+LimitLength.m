//
//  UITextFiled+LimitLength.m
//  jiangXunLu
//
//  Created by lixu06 on 16/6/17.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "UITextFiled+LimitLength.h"
#import <objc/runtime.h>

@implementation UITextField (LimitLength)

static const void   *unlimitedTextKey   = &unlimitedTextKey;
static const void   *limitedTextKey     = &limitedTextKey;

@dynamic unlimitedText;

- (void)limitLength:(NSInteger)maxLength{
    if (![[[self textInputMode] primaryLanguage] isEqualToString: @"en-US"] &&// 中文输入法下
        self.markedTextRange){
        self.unlimitedText = [self textInRange:[self textRangeFromPosition:self.beginningOfDocument toPosition:self.markedTextRange.start]];
    }else{
        if ([self.text length] >= maxLength+1) {
            self.limitedText = [self.text substringToIndex:maxLength];
            [self goToLimitedText];
        }else{
            self.unlimitedText = self.text;
        }
    }

    return;
}

#pragma mark - undo redo methods

- (void)backToUnlimitedText{
    [self setText:self.unlimitedText];
}

- (void)goToLimitedText{
    [self setText:self.limitedText];

    [self.undoManager removeAllActions];
    [self.undoManager registerUndoWithTarget:self selector:@selector(backToUnlimitedText) object:nil];
}

#pragma mark - dynamic property methods

- (NSString *)unlimitedText{
    return objc_getAssociatedObject(self, unlimitedTextKey);
}

- (void)setUnlimitedText:(NSString *)unlimitedText{
    objc_setAssociatedObject(self, unlimitedTextKey, unlimitedText, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSString *)limitedText{
    return objc_getAssociatedObject(self, limitedTextKey);
}

- (void)setLimitedText:(NSString *)limitedText{
    objc_setAssociatedObject(self, limitedTextKey, limitedText, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end

//
//  UITextFiled+LimitLength.h
//  jiangXunLu
//
//  Created by lixu06 on 16/6/17.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (LimitLength)

@property (strong, nonatomic) NSString *unlimitedText;
@property (strong, nonatomic) NSString *limitedText;

- (void)limitLength:(NSInteger)maxLength;

@end

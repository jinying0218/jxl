//
//  JXViewControlerRouter.m
//  jiangXunLu
//
//  Created by lixu06 on 16/6/17.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXViewControlerRouter.h"
#import "String.h"

#import "JXProfileDetailViewController.h"
#import "JXProfileEditViewController.h"
#import "JXAcquaitanceViewController.h"
#import "JXProfileViewController.h"
#import "jiangXunLu-swift.h"

@implementation JXViewControlerRouter
/*
 * @description: 跳转个人主页：- 用户自己的个人主页
 * @params: (UIViewController*)orgVC, 调用代码的VC;
 *          (BOOL)animated, 是否使用动画;
 *          (id)data, 数据,传nil;
 */
+ (void)pushToProfileSelfViewController:(UIViewController*)orgVC
                               animated:(BOOL)animated
                                   data:(id)data {
//    JXProfileViewController *pushVC = [[JXProfileViewController alloc] init];
//    pushVC.vcMask = JXViewControllerProfileMaskSelf;
//    pushVC.hidesBottomBarWhenPushed = NO;
//    [orgVC.navigationController pushViewController:pushVC animated:YES];
    NSInteger profileIndex = 4;
    UIViewController *rootController = [[UIApplication sharedApplication] keyWindow].rootViewController;
    if ([rootController isKindOfClass:[MainTabBarController class]]) {
        MainTabBarController *tabController = (MainTabBarController *)rootController;
        tabController.selectedIndex = profileIndex;
        tabController.selectedViewController = tabController.childViewControllers[profileIndex];
    }
    
    
}
/*
 * @description: 跳转个人主页：- 非用户自己的个人主页
 * @params: (UIViewController*)orgVC, 调用代码的VC;
 *          (BOOL)animated, 是否使用动画;
 *          (id)data, 数据,传入字典{"targetId":uid};
 */
+ (void)pushToProfileOtherViewController:(UIViewController*)orgVC
                                animated:(BOOL)animated
                                    data:(id)data {
    JXProfileViewController *pushVC = [[JXProfileViewController alloc] init];
    pushVC.vcMask = JXViewControllerProfileMaskOther;
    pushVC.queryDic = data;
    pushVC.hidesBottomBarWhenPushed = YES;
    [orgVC.navigationController pushViewController:pushVC animated:YES];
}

+ (void)pushToProfileOtherViewController:(UIViewController*)orgVC
                                animated:(BOOL)animated
                                    data:(id)data
                             finishBlock:(JXViewControllerBlock)finishBlock{
    JXProfileViewController *pushVC = [[JXProfileViewController alloc] init];
    pushVC.vcMask = JXViewControllerProfileMaskOther;
    pushVC.queryDic = data;
    pushVC.finishBlock = finishBlock;
    pushVC.hidesBottomBarWhenPushed = YES;
    [orgVC.navigationController pushViewController:pushVC animated:YES];
}


+ (void)pushToProfileDetailEditableViewController:(UIViewController*)orgVC
                                         animated:(BOOL)animated
                                             data:(id)data
                                      finishBlock:(JXViewControllerBlock )finishBlock {
    JXProfileDetailViewController *pushVC = [[JXProfileDetailViewController alloc] init];
    pushVC.vcMask = JXViewControllerEditMaskEnable;
    pushVC.caseDic = data;
    pushVC.finishBlock = finishBlock;
    pushVC.hidesBottomBarWhenPushed = YES;
    [orgVC.navigationController pushViewController:pushVC animated:animated];
}

+ (void)pushToProfileDetailDisabledViewController:(UIViewController*)orgVC
                                         animated:(BOOL)animated
                                             data:(id)data {
    JXProfileDetailViewController *pushVC = [[JXProfileDetailViewController alloc] init];
    pushVC.vcMask = JXViewControllerEditMaskDisable;
    pushVC.caseDic = data;
    pushVC.hidesBottomBarWhenPushed = YES;
    [orgVC.navigationController pushViewController:pushVC animated:animated];
}

+ (void)pushToProfileEditViewController:(UIViewController*)orgVC
                               animated:(BOOL)animated
                                   data:(id)data
                            finishBlock:(JXViewControllerBlock )finishBlock {
    JXProfileEditViewController *pushVC = [[JXProfileEditViewController alloc] init];
    pushVC.caseDic = data;
    pushVC.finishBlock = finishBlock;
    pushVC.hidesBottomBarWhenPushed = YES;
    [orgVC.navigationController pushViewController:pushVC animated:animated];
}

+ (BOOL)popCurrentViewController:(UIViewController*)orgVC animated:(BOOL)animated{
    return [orgVC.navigationController popViewControllerAnimated:YES];
}

+ (void)pushToAcquaitanceViewController:(UIViewController*)orgVC
                               animated:(BOOL)animated
                                   data:(id)data {
    JXAcquaitanceViewController *pushVC = [[JXAcquaitanceViewController alloc] init];
    pushVC.queryDic = data;
    pushVC.hidesBottomBarWhenPushed = YES;
    [orgVC.navigationController pushViewController:pushVC animated:animated];
}

@end

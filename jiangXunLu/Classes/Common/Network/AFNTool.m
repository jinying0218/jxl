//
//  AFNTool.m
//  jiangXunLu
//
//  Created by 金莹 on 16/8/28.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "AFNTool.h"
#import "AFNetworking.h"
#import "MBProgressHUD+Add.h"
#import "RealReachability.h"
#import <YYModel/NSObject+YYModel.h>


//#define KBaseURL @"http://wx.jiangxunlu.com/action./forapp.php"
//#define KBaseURL @"https://wx.jiangxunlu.com"
#define KBaseURL @"https://wx2.jiangxunlu.com"
#define kPath @"action/forapp.php"

@implementation AFNTool


#pragma mark - get
+ (void)get:(NSString *)urlStr params:(NSDictionary *)params progress:(YYProgress)progress success:(void (^)(id responsObject))success failure:(void (^)(NSError *error))failure{
    
    if ([[self class] checkNetworkStatus]) {
        urlStr = [NSString stringWithFormat:@"%@%@",[urlStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"],kPath];
        NSURL *baseUrl = [NSURL URLWithString:urlStr];
        NSURLSessionConfiguration *configuration =
        [NSURLSessionConfiguration defaultSessionConfiguration];
        
        NSMutableDictionary *requestParms = [NSMutableDictionary dictionary];
        [requestParms addEntriesFromDictionary:params];
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseUrl sessionConfiguration:configuration];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        [manager.responseSerializer setAcceptableContentTypes:[NSSet setWithObjects:@"application/json",
                                                               @"text/json",
                                                               @"text/javascript",
                                                               @"text/html",
                                                               @"text/plain",
                                                               nil]];
        
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        [manager GET:urlStr parameters:requestParms progress:^(NSProgress * _Nonnull downloadProgress) {
            if (progress) {
                progress(downloadProgress);
            }
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            
            if (success) {
                NSDictionary *dict = nil;
                if (![responseObject isKindOfClass:[NSDictionary class]]) {
                    NSError *error = nil;
//

                    dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&error];
                    NSLog(@"%@",error.description);
                    
                }
                success(dict);
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if (failure) {
                [MBProgressHUD showError:error.description toView:[UIApplication sharedApplication].keyWindow];
                failure(nil);
            }
            
        }];
        
    }else {
        [MBProgressHUD showError:@"请检查网络连接" toView:[UIApplication sharedApplication].keyWindow];
        failure(nil);
    }
    
}

+ (void)get:(NSString *)urlStr params:(NSDictionary *)params success:(void (^)(id responsObject))success failure:(void (^)(NSError *error))failure{
    [[self class] get:urlStr params:params progress:nil success:success failure:failure];
}



#pragma mark - post
+ (void)post:(NSString *)urlStr params:(NSDictionary *)params progress:(YYProgress)progress success:(void (^)(id responsObject))success failure:(void (^)(NSError *error))failure
{
    if ([[self class] checkNetworkStatus]) {
        urlStr = [NSString stringWithFormat:@"%@%@",[urlStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"],kPath];
        NSURL *baseUrl = [NSURL URLWithString:urlStr];
        
        NSURLSessionConfiguration *configuration =
        [NSURLSessionConfiguration defaultSessionConfiguration];
        
        NSMutableDictionary *requestParms = [NSMutableDictionary dictionary];
        [requestParms addEntriesFromDictionary:params];
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseUrl sessionConfiguration:configuration];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        [manager.responseSerializer setAcceptableContentTypes:[NSSet setWithObjects:@"application/json",
                                                               @"text/json",
                                                               @"text/javascript",
                                                               @"text/html",
                                                               @"text/plain",
                                                               nil]];
        
//        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        [manager POST:urlStr parameters:requestParms progress:^(NSProgress * _Nonnull uploadProgress) {
            if (progress) {
                progress(uploadProgress);
            }
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            if (success) {
                NSDictionary *dict = nil;
                if (![responseObject isKindOfClass:[NSDictionary class]]) {
                    dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
                    NSLog(@"错误：%@",dict[@"msg"]);
                }
                success(dict);
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if (failure) {
                [MBProgressHUD showError:error.description toView:[UIApplication sharedApplication].keyWindow];
                failure(error);
            }
        }];
        
    }else {
        
        [MBProgressHUD showError:@"请检查网络连接" toView:[UIApplication sharedApplication].keyWindow];
        failure(nil);
    }
}

+ (void)post:(NSString *)urlStr params:(NSDictionary *)params success:(void (^)(id responsObject))success failure:(void (^)(NSError *error))failure
{
    [[self class] post:urlStr params:params progress:nil success:success failure:failure];
    
}


#pragma mark - upload
+ (void)uploadRequestURL:(NSString *)urlStr params:(id)params fileData:(NSData *)fileData success:(void (^)(id responsObject))success failure:(void (^)(NSError *error))failure{
    if ([[self class] checkNetworkStatus]) {
        urlStr = [NSString stringWithFormat:@"%@%@",[urlStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"],kPath];
        NSURL *baseUrl = [NSURL URLWithString:urlStr];
        
        NSURLSessionConfiguration *configuration =
        [NSURLSessionConfiguration defaultSessionConfiguration];
        
        NSMutableDictionary *requestParms = [NSMutableDictionary dictionary];
        [requestParms addEntriesFromDictionary:params];
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseUrl sessionConfiguration:configuration];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        [manager.responseSerializer setAcceptableContentTypes:[NSSet setWithObjects:@"application/json",
                                                               @"text/json",
                                                               @"text/javascript",
                                                               @"text/html",
                                                               @"text/plain",
                                                               nil]];

        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        [manager POST:urlStr parameters:requestParms constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
            //将得到的二进制数据拼接到表单中 /** data,指定上传的二进制流;name,服务器端所需参数名*/
            [formData appendPartWithFileData :fileData name:@"file" fileName:@"test.jpg" mimeType:@"image/jpg"];
        } progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            if (success) {
                NSDictionary *dict = nil;
                if (![responseObject isKindOfClass:[NSDictionary class]]) {
                    dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
                    NSLog(@"错误：%@",dict[@"msg"]);
                }else {
                    dict = responseObject;
                }
                success(dict);
            }

        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if (failure) {
                [MBProgressHUD showError:error.description toView:[UIApplication sharedApplication].keyWindow];
                failure(error);
            }
        }];
        
    }else {
        [MBProgressHUD showError:@"请检查网络连接" toView:[UIApplication sharedApplication].keyWindow];
        failure(nil);
    }}

#pragma mark -
+ (BOOL)checkNetworkStatus{
    __block BOOL isConnect = YES;
    [[RealReachability sharedInstance] startNotifier];
    
    [[RealReachability sharedInstance] reachabilityWithBlock:^(ReachabilityStatus status) {
        switch (status) {
            case RealStatusUnknown:{
                NSLog(@"未知");
                isConnect = NO;
            }
                break;
            case RealStatusNotReachable:{
                NSLog(@"无网络");
                isConnect = YES;
            }
                break;
            case RealStatusViaWWAN:{
                NSLog(@"3G、2G网络");
                isConnect = YES;
            }
                break;
            case RealStatusViaWiFi:{
                NSLog(@"WiFi网络");
                isConnect = YES;
            }
                break;
            default:
                break;
        }
    }];
    return isConnect;
}


@end

//
//  AFNTool.h
//  jiangXunLu
//
//  Created by 金莹 on 16/8/28.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^YYProgress)(NSProgress *progress);

@interface AFNTool : NSObject


+ (void)get:(NSString *)urlStr params:(NSDictionary *)params progress:(YYProgress)progress success:(void (^)(id responsObject))success failure:(void (^)(NSError *error))failure;
+ (void)get:(NSString *)urlStr params:(NSDictionary *)params success:(void (^)(id responsObject))success failure:(void (^)(NSError *error))failure;

+ (void)post:(NSString *)urlStr params:(NSDictionary *)params progress:(YYProgress)progress success:(void (^)(id responsObject))success failure:(void (^)(NSError *error))failure;
+ (void)post:(NSString *)urlStr params:(NSDictionary *)params success:(void (^)(id responsObject))success failure:(void (^)(NSError *error))failure;

+ (void)uploadRequestURL:(NSString *)urlStr params:(id)params fileData:(NSData *)fileData success:(void (^)(id responsObject))success failure:(void (^)(NSError *error))failure;
@end

//
//  JxlNetAPIClient.swift
//  jiangXunLu
//
//  Created by liuxb on 16/6/12.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD


class Globe: NSObject {
    class var baseUrlStr: String {
        return "https://wx.jiangxunlu.com/"
//        return "http://wx2.jiangxunlu.com/"

    }
    class var forappStr: String {
        return "action/forapp.php"
    }
    class var messageWS: String {
        return "ws://wx.jiangxunlu.com:1506"
//        return "ws://wx.jiangxunlu.com:1507"

    }
    
    // 友盟app key
    class var umengAppKey: String {
        return "57874a9c67e58eeca7000345"
    }
    
    //微信appid
    class var weChatAppID: String {
        return "wxd8e4313958fb0a5c"
    }
    //微信appSecret
    class var weChatAppSecret: String {
        return "05b652b736693e511c839a445eb0c400"
    }

    //QQ appid
    class var qqAppID: String {
        return "1105428895"
    }
    //QQ appkey
    class var qqAppKey: String {
        return "yGs8zOKjO1p9IhFJ"
    }
    
    //Sina AppKey
    class var sinaAppKey: String {
        return "526941346"
    }
    //Sina AppSecret
    class var sinaAppSecret: String {
        return "5549f097ec377fb3d09a5f05cbbc9233"
    }
    //Sina AppSecret
    class var sinaRedirectURL: String {
        return "http://sns.whalecloud.com/sina2/callback"
    }
    
    //XGPush id
    class var XGPushID: UInt32 {
        return 2200209172
    }
    //XGPush appKey
    class var XGPushAppKey: String {
        return "I49SG45TE2PJ"
    }
    
    
    static let ExpressionBundle = NSBundle(URL: NSBundle.mainBundle().URLForResource("Expression", withExtension: "bundle")!)
    static let ExpressionBundleName = "Expression.bundle"
    static let ExpressionPlist = NSBundle.mainBundle().pathForResource("Expression", ofType: "plist")
}


class Tips: NSObject {
    
    class func tipFromError(error: NSError?) -> String? {
        var tipStr: String?
        if error != nil && error?.userInfo != nil {
            if (error!.userInfo["NSLocalizedDescription"] != nil) {
                tipStr = error!.userInfo["NSLocalizedDescription"] as? String
                if tipStr!.containsString("The request timed out") {
                    tipStr = "网络请求超时"
                }
            } else {
                tipStr = "ErrorCode" + String(error!.code)
            }
        }
        return tipStr
    }
    
    class func showHudTipsStr(error: NSError) {
        let tipStr = Tips.tipFromError(error)
        if tipStr != nil && tipStr?.characters.count > 0 {
            SVProgressHUD.showInfoWithStatus(tipStr)
        }
    }
    
}


class JxlNetAPIClient: Manager {

    static let timeoutIntervalForRequest: NSTimeInterval = 10
    
    static let sharedClient: JxlNetAPIClient = {
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        configuration.timeoutIntervalForRequest = timeoutIntervalForRequest
        configuration.HTTPAdditionalHeaders = Manager.defaultHTTPHeaders
        let manager = JxlNetAPIClient(configuration: configuration)
        return manager
    }()
    
    func requestJsonDataWithPath(aPath: String, withParams params:[String:AnyObject], withMethodType method: Alamofire.Method, andBlock finished:(data: AnyObject?, error:NSError?)->Void){
        if aPath.characters.count <= 0 {
            return
        }
        let requsetPath = aPath + Globe.forappStr
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        self.request(method, requsetPath, parameters: params).responseJSON(completionHandler: { (response) in
            switch response.result{
            case .Success:
                print("\n===========response===========\n\(requsetPath)\n\(params)\n\(response.result.value)")
                
                if let dictJson = response.result.value as? NSDictionary {
                    let status = dictJson["status"] as! NSNumber
                    if status == -101 {
                        SVProgressHUD.showInfoWithStatus(dictJson["msg"] as! String)
                        AppDelegate.shareDelegate.setupLogionViewController()
                    } else {
                        finished(data: dictJson, error: nil)
                    }
                }
                
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            case .Failure(let error):
                print("\n===========response===========\n\(requsetPath)\n\(params)\n\(error)")
                finished(data: nil, error: error)
                Tips.showHudTipsStr(error)
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            }
        })
    }
    
}





















































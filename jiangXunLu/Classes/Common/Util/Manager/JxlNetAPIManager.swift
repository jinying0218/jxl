//
//  JxlNetAPIManager.swift
//  jiangXunLu
//
//  Created by liuxb on 16/6/20.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit

class JxlNetAPIManager: NSObject {
    
    static let sharedManager: JxlNetAPIManager = {
        let manager = JxlNetAPIManager()
        return manager
    }()
    

    /**
    1 注册
     - parameter path:   请求地址字符串
     - parameter params: ["action":"register","data":["phone":String,"password":String,"name":String,"company":String,"brand":String,"job":String,"party":Int]]
     - parameter block:  回调
     */
    func request_registerWithBlock(path: String, Params params:[String:AnyObject], andBlock block:((data: AnyObject?, error:NSError?) -> Void)) {
        JxlNetAPIClient.sharedClient.requestJsonDataWithPath(path, withParams: params, withMethodType: .POST) { (data, error) in
            
            if let data = data {
                block(data: data, error: nil)
            } else {
                block(data: nil, error: error)
            }
        }
    }
    
    /**
    2 登录
     - parameter path:   请求地址字符串
     - parameter params: ["action":"login","data":["username":String,"password":String]]
     - parameter block:  回调
     */
    func request_LoginWithBlock(path: String, Params params:[String:AnyObject], andBlock block:((data: AnyObject?, error:NSError?) -> Void)) {
        JxlNetAPIClient.sharedClient.requestJsonDataWithPath(path, withParams: params, withMethodType: .POST) { (data, error) in
            
            if let data = data {
                block(data: data, error: nil)
            } else {
                block(data: nil, error: error)
            }
        }
    }
    
    /**
    3 登出
     - parameter path:   请求地址字符串
     - parameter params: ["action":"loginout","uid":String,"randcode":String,"data":[]]
     - parameter block:  回调
     */
    func request_LoginOutWithBlock(path: String, Params params:[String:AnyObject], andBlock block:((data: AnyObject?, error:NSError?) -> Void)) {
        JxlNetAPIClient.sharedClient.requestJsonDataWithPath(path, withParams: params, withMethodType: .POST) { (data, error) in
            
            if let data = data {
                block(data: data, error: nil)
            } else {
                block(data: nil, error: error)
            }
        }
    }
    
    /**
    4 重置密码
     - parameter path:   请求地址字符串
     - parameter params: ["action":"reset_password","uid":String,"randcode":String,"data":["phone":String,"code":String,"password":String]]
     - parameter block:  回调
     */
    func request_ResetPwdWithBlock(path: String, Params params:[String:AnyObject], andBlock block:((data: AnyObject?, error:NSError?) -> Void)) {
        JxlNetAPIClient.sharedClient.requestJsonDataWithPath(path, withParams: params, withMethodType: .POST) { (data, error) in
            
            if let data = data {
                block(data: data, error: nil)
            } else {
                block(data: nil, error: error)
            }
        }
    }
    
    /**
    5 重置密码 短信验证
     - parameter path:   请求地址字符串
     - parameter params: ["action":"sms_send_resetpwd","uid":String,"randcode":String,"data":["phone":String]]
     - parameter block:  回调
     */
    func request_Send_Sms_ResetPwdWithBlock(path: String, Params params:[String:AnyObject], andBlock block:((data: AnyObject?, error:NSError?) -> Void)) {
        JxlNetAPIClient.sharedClient.requestJsonDataWithPath(path, withParams: params, withMethodType: .POST) { (data, error) in
            
            if let data = data {
                block(data: data, error: nil)
            } else {
                block(data: nil, error: error)
            }
        }
    }

    
    /**
    6 注册 短信验证 （暂未开通）
     - parameter path:   请求地址字符串
     - parameter params: ["action":"sms_send_register","uid":String(可选),"randcode":String(可选),"data":["phone":String]]
     - parameter block:  回调
     */
    func request_Send_Sms_RegisterWithBlock(path: String, Params params:[String:AnyObject], andBlock block:((data: AnyObject?, error:NSError?) -> Void)) {
        JxlNetAPIClient.sharedClient.requestJsonDataWithPath(path, withParams: params, withMethodType: .POST) { (data, error) in
            
            if let data = data {
                block(data: data, error: nil)
            } else {
                block(data: nil, error: error)
            }
        }
    }
    
    
    /**
    7 加载名片列表
     - parameter path:   请求地址字符串
     - parameter params: ["action":"card_list_load","uid":String,"randcode":String,"data":["party":Int,"start":Int,"count":Int]]
     - parameter block:  回调
     */
    func request_CardListWithBlock(path: String, Params params:[String:AnyObject], andBlock block:((data: AnyObject?, error:NSError?) -> Void)) {
        JxlNetAPIClient.sharedClient.requestJsonDataWithPath(path, withParams: params, withMethodType: .POST) { (data, error) in
            
            if let data = data {
                block(data: data, error: nil)
            } else {
                block(data: nil, error: error)
            }
        }
    }
    
    /**
    8 加载名片信息
     - parameter path:   请求地址字符串
     - parameter params: ["action":"card_info_load","uid":String,"randcode":String,"data":["uid":Sting]]
     - parameter block:  回调
     */
    func request_CardInfoWithBlock(path: String, Params params:[String:AnyObject], andBlock block:((data: AnyObject?, error:NSError?) -> Void)) {
        JxlNetAPIClient.sharedClient.requestJsonDataWithPath(path, withParams: params, withMethodType: .POST) { (data, error) in
            
            if let data = data {
                block(data: data, error: nil)
            } else {
                block(data: nil, error: error)
            }
        }
    }
    
    
    /**
    9 收藏／取消收藏
     - parameter path:   请求地址字符串
     - parameter params: ["action":"collection","uid":String,"randcode":String,"data":["uid":被收藏／取消收藏的用户id,"status":1收藏 0取消收藏]]
     - parameter block:  回调
     */
    func request_CollectionWithBlock(path: String, Params params:[String:AnyObject], andBlock block:((data: AnyObject?, error:NSError?) -> Void)) {
        JxlNetAPIClient.sharedClient.requestJsonDataWithPath(path, withParams: params, withMethodType: .POST) { (data, error) in
            
            if let data = data {
                block(data: data, error: nil)
            } else {
                block(data: nil, error: error)
            }
        }
    }
    
    /**
    10 加载收藏列表
     - parameter path:   请求地址字符串
     - parameter params: ["action":"collection_list_load","uid":String,"randcode":String,"data":["start":Int,"count":Int]]
     - parameter block:  回调
     */
    func request_CollectionListWithBlock(path: String, Params params:[String:AnyObject], andBlock block:((data: AnyObject?, error:NSError?) -> Void)) {
        JxlNetAPIClient.sharedClient.requestJsonDataWithPath(path, withParams: params, withMethodType: .POST) { (data, error) in
            
            if let data = data {
                block(data: data, error: nil)
            } else {
                block(data: nil, error: error)
            }
        }
    }
    
    /**
    11 搜索名片
     - parameter path:   请求地址字符串
     - parameter params: ["action":"search_card","uid":String,"randcode":String,"data":["keyword":String]]
     - parameter block:  回调
     */
    func request_SearchCardWithBlock(path: String, Params params:[String:AnyObject], andBlock block:((data: AnyObject?, error:NSError?) -> Void)) {
        JxlNetAPIClient.sharedClient.requestJsonDataWithPath(path, withParams: params, withMethodType: .POST) { (data, error) in
            
            if let data = data {
                block(data: data, error: nil)
            } else {
                block(data: nil, error: error)
            }
        }
    }
    
    /**
    12 搜索历史加载
     - parameter path:   请求地址字符串
     - parameter params: ["action":"search_history_load","uid":String,"randcode":String,"data":["type":(可选)搜索类型,1.搜索名片;更多类型待扩展;默认为1]]
     - parameter block:  回调
     */
    func request_SearchHistoryWithBlock(path: String, Params params:[String:AnyObject], andBlock block:((data: AnyObject?, error:NSError?) -> Void)) {
        JxlNetAPIClient.sharedClient.requestJsonDataWithPath(path, withParams: params, withMethodType: .POST) { (data, error) in
            
            if let data = data {
                block(data: data, error: nil)
            } else {
                block(data: nil, error: error)
            }
        }
    }
    
    /**
    13 清空搜索历史
     - parameter path:   请求地址字符串
     - parameter params: ["action":"search_history_clear","uid":String,"randcode":String,"data":["type":(可选)搜索类型,1.搜索名片;更多类型待扩展;默认为1]]
     - parameter block:  回调
     */
    func request_ClearSearchHistoryWithBlock(path: String, Params params:[String:AnyObject], andBlock block:((data: AnyObject?, error:NSError?) -> Void)) {
        JxlNetAPIClient.sharedClient.requestJsonDataWithPath(path, withParams: params, withMethodType: .POST) { (data, error) in
            
            if let data = data {
                block(data: data, error: nil)
            } else {
                block(data: nil, error: error)
            }
        }
    }
    
    /**
    14  热门搜索关键词
     - parameter path:   请求地址字符串
     - parameter params: ["action":"search_hot_load","uid":String,"randcode":String,"data":["type":(可选)搜索类型,1.搜索名片;更多类型待扩展;默认为1]]
     - parameter block:  回调
     */
    func request_SearchHotWithBlock(path: String, Params params:[String:AnyObject], andBlock block:((data: AnyObject?, error:NSError?) -> Void)) {
        JxlNetAPIClient.sharedClient.requestJsonDataWithPath(path, withParams: params, withMethodType: .POST) { (data, error) in
            
            if let data = data {
                block(data: data, error: nil)
            } else {
                block(data: nil, error: error)
            }
        }
    }

    /**
    15 城市定位
     - parameter path:   请求地址字符串
     - parameter params: ["action":"get_location","uid":String,"randcode":String,"data":[]]
     - parameter block:  回调
     */
    func request_GetLocationWithBlock(path: String, Params params:[String:AnyObject], andBlock block:((data: AnyObject?, error:NSError?) -> Void)) {
        JxlNetAPIClient.sharedClient.requestJsonDataWithPath(path, withParams: params, withMethodType: .POST) { (data, error) in
            
            if let data = data {
                block(data: data, error: nil)
            } else {
                block(data: nil, error: error)
            }
        }
    }
    
    /**
    16 搜索城市记录 （ 返回历史搜索城市 和 热门搜索城市）
     - parameter path:   请求地址字符串
     - parameter params: ["action":"search_region_record_load","uid":String,"randcode":String,"data":["type":(可选)搜索类型,1.搜索名片;更多类型待扩展;默认为1]]
     - parameter block:  回调
     */
    func request_SearchRegionRecordWithBlock(path: String, Params params:[String:AnyObject], andBlock block:((data: AnyObject?, error:NSError?) -> Void)) {
        JxlNetAPIClient.sharedClient.requestJsonDataWithPath(path, withParams: params, withMethodType: .POST) { (data, error) in
            
            if let data = data {
                block(data: data, error: nil)
            } else {
                block(data: nil, error: error)
            }
        }
    }
    
    
    
    /**
    17 获取项目信息
     - parameter path:   请求地址字符串
     - parameter params: ["action":"project_load","uid":String,"randcode":String,"data":["pid":项目id,"field":(可选)自定义返回数据字段(key),array数组格式]]
     - parameter block:  回调
     */
    func request_ProjectWithBlock(path: String, Params params:[String:AnyObject], andBlock block:((data: AnyObject?, error:NSError?) -> Void)) {
        JxlNetAPIClient.sharedClient.requestJsonDataWithPath(path, withParams: params, withMethodType: .POST) { (data, error) in
            
            if let data = data {
                block(data: data, error: nil)
            } else {
                block(data: nil, error: error)
            }
        }
    }
    
    /**
    18 更新用户信息
     - parameter path:   请求地址字符串
     - parameter params: ["action":"card_info_update","uid":String,"randcode":String,"data":["uid":用户id,"name":姓名(可选),"sex":1男 2女(可选),"phone":电话(可选),"company":公司(可选),"job":职位(可选),"region":地区(可选,格式“xx 省 xx 市”),"show_flag":是否显示手机号码(可选),"brand":品牌(可选),"brief":项目简介(可选),"detail":项目详情(可选)]]
     - parameter block:  回调
     */
    func request_UpdateCardInfoWithBlock(path: String, Params params:[String:AnyObject], andBlock block:((data: AnyObject?, error:NSError?) -> Void)) {
        JxlNetAPIClient.sharedClient.requestJsonDataWithPath(path, withParams: params, withMethodType: .POST) { (data, error) in
            
            if let data = data {
                block(data: data, error: nil)
            } else {
                block(data: nil, error: error)
            }
        }
    }
    
    /**
    19 更新用户头像
     - parameter path:   请求地址字符串
     - parameter params: ["action":"user_head_modify","uid":String,"randcode":String,"data":["uid":用户id,"image":base64图像数据]]
     - parameter block:  回调
     */
    func request_UpdateUserIconWithBlock(path: String, Params params:[String:AnyObject], andBlock block:((data: AnyObject?, error:NSError?) -> Void)) {
        JxlNetAPIClient.sharedClient.requestJsonDataWithPath(path, withParams: params, withMethodType: .POST) { (data, error) in
            
            if let data = data {
                block(data: data, error: nil)
            } else {
                block(data: nil, error: error)
            }
        }
    }
    
    /**
    20 新消息数
     - parameter path:   请求地址字符串
     - parameter params: ["action":"message_new_count","uid":String,"randcode":String,"data":["uid":用户id]]
     - parameter block:  回调
     */
    func request_NewMessageWithBlock(path: String, Params params:[String:AnyObject], andBlock block:((data: AnyObject?, error:NSError?) -> Void)) {
        JxlNetAPIClient.sharedClient.requestJsonDataWithPath(path, withParams: params, withMethodType: .POST) { (data, error) in
            
            if let data = data {
                block(data: data, error: nil)
            } else {
                block(data: nil, error: error)
            }
        }
    }
    
    /**
    21 聊天消息列表
     - parameter path:   请求地址字符串
     - parameter params: ["action":"message_list_load","uid":String,"randcode":String,"data":["start":Int,"count":Int]]
     - parameter block:  回调
     */
    func request_MessageListWithBlock(path: String, Params params:[String:AnyObject], andBlock block:((data: AnyObject?, error:NSError?) -> Void)) {
        JxlNetAPIClient.sharedClient.requestJsonDataWithPath(path, withParams: params, withMethodType: .POST) { (data, error) in
            
            if let data = data {
                block(data: data, error: nil)
            } else {
                block(data: nil, error: error)
            }
        }
    }
    
    /**
    22 删除一条消息列表
     - parameter path:   请求地址字符串
     - parameter params: ["action":"message_list_del","uid":String,"randcode":String,"data":["uid_to":聊天对象的用户id]]
     - parameter block:  回调
     */
    func request_DeleteMessageListWithBlock(path: String, Params params:[String:AnyObject], andBlock block:((data: AnyObject?, error:NSError?) -> Void)) {
        JxlNetAPIClient.sharedClient.requestJsonDataWithPath(path, withParams: params, withMethodType: .POST) { (data, error) in
            
            if let data = data {
                block(data: data, error: nil)
            } else {
                block(data: nil, error: error)
            }
        }
    }
    
    /**
    23 请求聊天数据
     - parameter path:   请求地址字符串
     - parameter params: ["action":"chat_data_load","uid":String,"randcode":String,"data":["chat_id":聊天id,"start_id":聊天记录的id,//可选，此参数值大于0有效，设置后将搜索id>start_id的聊天数据,"end_id":聊天记录的id,//可选，此参数值大于0有效，设置后将搜索id<end_id的聊天数据,"count":数据行数//可选，不设置时默认为20]]
     - parameter block:  回调
     */
    func request_ChatDataWithBlock(path: String, Params params:[String:AnyObject], andBlock block:((data: AnyObject?, error:NSError?) -> Void)) {
        JxlNetAPIClient.sharedClient.requestJsonDataWithPath(path, withParams: params, withMethodType: .POST) { (data, error) in
            
            if let data = data {
                block(data: data, error: nil)
            } else {
                block(data: nil, error: error)
            }
        }
    }
    
    /**
     24 请求聊天信息
     - parameter path:   请求地址字符串
     - parameter params: ["action":"chat_info_load","uid":String,"randcode":String,"data":["toid":聊天id]]
     - parameter block:  回调
     */
    func request_ChatInfoWithBlock(path: String, Params params:[String:AnyObject], andBlock block:((data: AnyObject?, error:NSError?) -> Void)) {
        JxlNetAPIClient.sharedClient.requestJsonDataWithPath(path, withParams: params, withMethodType: .POST) { (data, error) in
            
            if let data = data {
                block(data: data, error: nil)
            } else {
                block(data: nil, error: error)
            }
        }
    }
    
    /**
    25 获取人脉信息列表
     - parameter path:   请求地址字符串
     - parameter params: ["action":"connections_list_load","uid":String,"randcode":String,"data":["uid":聊天的用户id,"start":Int,"count":Int]]
     - parameter block:  回调
     */
    func request_ConnectionsListWithBlock(path: String, Params params:[String:AnyObject], andBlock block:((data: AnyObject?, error:NSError?) -> Void)) {
        JxlNetAPIClient.sharedClient.requestJsonDataWithPath(path, withParams: params, withMethodType: .POST) { (data, error) in
            
            if let data = data {
                block(data: data, error: nil)
            } else {
                block(data: nil, error: error)
            }
        }
    }
    
    /**
    26 访问记录
     - parameter path:   请求地址字符串
     - parameter params: ["action":"record_visit","uid":String,"randcode":String,"data":["type":1请求页面 2加载完成 3离开,"url":访问的页面路径]]
     - parameter block:  回调
     */
    func request_RecordVisitWithBlock(path: String, Params params:[String:AnyObject], andBlock block:((data: AnyObject?, error:NSError?) -> Void)) {
        JxlNetAPIClient.sharedClient.requestJsonDataWithPath(path, withParams: params, withMethodType: .POST) { (data, error) in
            
            if let data = data {
                block(data: data, error: nil)
            } else {
                block(data: nil, error: error)
            }
        }
    }
    
    /**
    27 帖子列表
     - parameter path:   请求地址字符串
     - parameter params: ["action":"post_list_load","uid":String,"randcode":String,"data":["party":帖子类别1项目 2品牌 3其他,"start":Int,"count":Int]]
     - parameter block:  回调
     */
    func request_PostListWithBlock(path: String, Params params:[String:AnyObject], andBlock block:((data: AnyObject?, error:NSError?) -> Void)) {
        JxlNetAPIClient.sharedClient.requestJsonDataWithPath(path, withParams: params, withMethodType: .POST) { (data, error) in
            
            if let data = data {
                block(data: data, error: nil)
            } else {
                block(data: nil, error: error)
            }
        }
    }
    
    /**
    28 加载帖子内容
     - parameter path:   请求地址字符串
     - parameter params: ["action":"post_detail_load","uid":String,"randcode":String,"data":["id":帖子id]]
     - parameter block:  回调
     */
    func request_PostDetailWithBlock(path: String, Params params:[String:AnyObject], andBlock block:((data: AnyObject?, error:NSError?) -> Void)) {
        JxlNetAPIClient.sharedClient.requestJsonDataWithPath(path, withParams: params, withMethodType: .POST) { (data, error) in
            
            if let data = data {
                block(data: data, error: nil)
            } else {
                block(data: nil, error: error)
            }
        }
    }
    
    /**
    29 加载帖子回复
     - parameter path:   请求地址字符串
     - parameter params: ["action":"post_reply_load","uid":String,"randcode":String,"data":["id":帖子id,"start":Int,"count":Int]]
     - parameter block:  回调
     */
    func request_PostReplyWithBlock(path: String, Params params:[String:AnyObject], andBlock block:((data: AnyObject?, error:NSError?) -> Void)) {
        JxlNetAPIClient.sharedClient.requestJsonDataWithPath(path, withParams: params, withMethodType: .POST) { (data, error) in
            
            if let data = data {
                block(data: data, error: nil)
            } else {
                block(data: nil, error: error)
            }
        }
    }
    
    /**
    30 回复帖子
     - parameter path:   请求地址字符串
     - parameter params: ["action":"post_reply","uid":String,"randcode":String,"data":["id":帖子id,"content":回复内容,"reply_to":(可选)对现有回复内容回复时赋值为现有回复的(数据项)id，值小于或等于0时将被忽略]]
     - parameter block:  回调
     */
    func request_ReplyPostWithBlock(path: String, Params params:[String:AnyObject], andBlock block:((data: AnyObject?, error:NSError?) -> Void)) {
        JxlNetAPIClient.sharedClient.requestJsonDataWithPath(path, withParams: params, withMethodType: .POST) { (data, error) in
            
            if let data = data {
                block(data: data, error: nil)
            } else {
                block(data: nil, error: error)
            }
        }
    }
    
    /**
    31 发布帖子
     - parameter path:   请求地址字符串
     - parameter params: ["action":"post_release","uid":String,"randcode":String,"data":["pid":帖子id,"party":帖子类别 1项目 2品牌 3其他,"title":标题,"content":内容,"summary":摘要(可选),"cover":封面(可选)]]
     - parameter block:  回调
     */
    func request_PostReleaseWithBlock(path: String, Params params:[String:AnyObject], andBlock block:((data: AnyObject?, error:NSError?) -> Void)) {
        JxlNetAPIClient.sharedClient.requestJsonDataWithPath(path, withParams: params, withMethodType: .POST) { (data, error) in
            
            if let data = data {
                block(data: data, error: nil)
            } else {
                block(data: nil, error: error)
            }
        }
    }

    /**
    32 帖子修改
     - parameter path:   请求地址字符串
     - parameter params: ["action":"post_modify","uid":String,"randcode":String,"data":["party":帖子类别 1项目 2品牌 3其他,"title":标题,"content":内容,"summary":摘要(可选),"cover":封面(可选)]]
     - parameter block:  回调
     */
    func request_PostModifyWithBlock(path: String, Params params:[String:AnyObject], andBlock block:((data: AnyObject?, error:NSError?) -> Void)) {
        JxlNetAPIClient.sharedClient.requestJsonDataWithPath(path, withParams: params, withMethodType: .POST) { (data, error) in
            
            if let data = data {
                block(data: data, error: nil)
            } else {
                block(data: nil, error: error)
            }
        }
    }

    /**
     33 上传头像
     - parameter path:   请求地址字符串
     - parameter params: ["action":"upload_img","uid":String,"randcode":String,"data":["type":"base64"/"stream","data":(可选)base64编码,图片数据,"place":(可选)(int)图片所在位置]]
     - parameter block:  回调
     */
    func request_UploadImgWithBlock(path: String, Params params:[String:AnyObject], andBlock block:((data: AnyObject?, error:NSError?) -> Void)) {
        JxlNetAPIClient.sharedClient.requestJsonDataWithPath(path, withParams: params, withMethodType: .POST) { (data, error) in
            
            if let data = data {
                block(data: data, error: nil)
            } else {
                block(data: nil, error: error)
            }
        }
    }

    
    /**
     34 App 设置device_info
     - parameter path:   请求地址字符串
     - parameter params: ["action":"set_device_info","uid":String,"randcode":String,"data":["device_os": (int)设备系统:1.ios 2.android,"device_token ": (string)消息推送的设备识别码]]
     - parameter block:  回调
     */
    func request_SetDeviceInfoWithBlock(path: String, Params params:[String:AnyObject], andBlock block:((data: AnyObject?, error:NSError?) -> Void)) {
        JxlNetAPIClient.sharedClient.requestJsonDataWithPath(path, withParams: params, withMethodType: .POST) { (data, error) in
            
            if let data = data {
                block(data: data, error: nil)
            } else {
                block(data: nil, error: error)
            }
        }
    }
    
    /**
     35 获取简洁的用户信息
     - parameter path:   请求地址字符串
     - parameter params: ["action":"get_userinfo_simple","uid":String,"data":["uid":要获取信息的用户id]]
     - parameter block:  回调
     */
    func request_SimpleUserInfoWithBlock(path: String, Params params:[String:AnyObject], andBlock block:((data: AnyObject?, error:NSError?) -> Void)) {
        JxlNetAPIClient.sharedClient.requestJsonDataWithPath(path, withParams: params, withMethodType: .POST) { (data, error) in
            
            if let data = data {
                block(data: data, error: nil)
            } else {
                block(data: nil, error: error)
            }
        }
    }
    
    /**
     36 检查应用更新
     - parameter path:   请求地址字符串
     - parameter params: ["action":"check_update","uid":String,"randcode":String,"data":["client_version":(int)客户端版本号,"pass_version":(int)客户端忽略的版本号,"client_channel":(string)客户端渠道号]]
     - parameter block:  回调
     */
    func request_CheckAppUpdateWithBlock(path: String, Params params:[String:AnyObject], andBlock block:((data: AnyObject?, error:NSError?) -> Void)) {
        JxlNetAPIClient.sharedClient.requestJsonDataWithPath(path, withParams: params, withMethodType: .POST) { (data, error) in
            
            if let data = data {
                block(data: data, error: nil)
            } else {
                block(data: nil, error: error)
            }
        }
    }
    

    /**
     37 发现页banner数据
     - parameter path:   请求地址字符串
     - parameter params: ["action":"find_banner_load","uid":String,"randcode":String,"data":["party":所属方 1甲方 2乙方 3其他(暂时未对party值做区分处理)]]
     - parameter block:  回调
     */
    func request_FindBannerDataWithBlock(path: String, Params params:[String:AnyObject], andBlock block:((data: AnyObject?, error:NSError?) -> Void)) {
        JxlNetAPIClient.sharedClient.requestJsonDataWithPath(path, withParams: params, withMethodType: .POST) { (data, error) in
            
            if let data = data {
                block(data: data, error: nil)
            } else {
                block(data: nil, error: error)
            }
        }
    }
}
















































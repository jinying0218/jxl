//
//  AddressBookViewController.swift
//  jiangXunLu
//
//  Created by liuxb on 16/6/12.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit
import SVProgressHUD
import MJRefresh

let kCellIdentifier_PersonCollectionCell = "PersonCollectionCell"
let kCellIdentifier_CustomHeaderView = "CustomHeaderView"

class AddressBookViewController: BaseViewController {
    
    // 甲方人脉数组
    var partyACards:[AddressCardModel] = []
    var partyBCards:[AddressCardModel] = []
    var partyTempCards:[AddressCardModel] = []
    private var party: Int = 1 //甲方
    private var start: Int = 0
    private var count: Int = 50
    
    private var collectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpNavigationBar()
        self.configViews()
        
        self.reloadData()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.setupNavigationStyle()
    }
        
    private func reloadData(){
        self.start = 0
        self.count = 50
        
        self.partyACards.removeAll()
        self.partyBCards.removeAll()
        self.collectionView.reloadData()
        self.loadCards()
        
        if Login.shareLogin.BannerData.count == 0 {
            self.loadBannerData()
        }
    }
    
    
    private func configViews(){
        if collectionView == nil {
            let frame = CGRect(x: 0, y: 0, width: kScreen_Bounds.width, height: kScreen_Bounds.height - self.navigationBarHeight - self.toolBarHeight)
            collectionView = UICollectionView(frame: frame, collectionViewLayout: UICollectionViewFlowLayout())
            collectionView.backgroundColor = UIColor(red: 0.933, green: 0.933, blue: 0.933, alpha: 1.00)
            collectionView.dataSource = self
            collectionView.delegate = self
            collectionView.registerClass(PersonCollectionCell.self, forCellWithReuseIdentifier: kCellIdentifier_PersonCollectionCell)
            collectionView.registerClass(CustomHeaderView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: kCellIdentifier_CustomHeaderView)
            self.view.addSubview(collectionView)
            
            // 添加下拉刷新
            collectionView.mj_header = MJRefreshNormalHeader(refreshingBlock: {
                self.reloadData()
            })
            
            // 添加上拉刷新
            collectionView.mj_footer = MJRefreshAutoNormalFooter(refreshingBlock: {
                self.start += self.partyTempCards.count
                self.loadCards()
            })
            
            // 改变颜色为#999999
            (collectionView.mj_footer as! MJRefreshAutoStateFooter).stateLabel.textColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1.0)
        }
    }
    
    private func setUpNavigationBar(){
        let leftBtn = UIBarButtonItem(image: UIImage(named: "nav_search_normal"), style: .Plain, target: self, action: #selector(self.clickSearchBtn))
        navigationItem.leftBarButtonItem = leftBtn
        
        let rightBtn = UIBarButtonItem(image: UIImage(named: "nav_share_normal"), style: .Plain, target: self, action: #selector(self.clickShareBtn))
        navigationItem.rightBarButtonItem = rightBtn
    }
    
    
    private func loadCards(){
        
        self.partyTempCards.removeAll()
        
        let params = [
            "action":"card_list_load",
            "uid":g_uid,
            "randcode":g_randcode,
            "data":[
                "party":self.party,
                "start":self.start,
                "count":self.count
            ]
        ]
        JxlNetAPIManager.sharedManager.request_CardListWithBlock(Globe.baseUrlStr, Params: params as! [String : AnyObject]) { (data, error) in
            if let data = data {
                let status = data["status"] as! NSNumber
                if status == 1 { //sucess 
                    let info = data["info"] as! [String:AnyObject]
                    for dict in info["list"] as! [[String:AnyObject]] {
                        let card = AddressCardModel(dict: dict)
                        self.partyTempCards.append(card)
                    }
                    if self.party == 1 {
                        self.partyACards.appendContentsOf(self.partyTempCards)
                    } else {
                        self.partyBCards.appendContentsOf(self.partyTempCards)
                    }
                } else {
                    SVProgressHUD.showInfoWithStatus(data["msg"] as! String)
                }
                self.collectionView.reloadData()
                if self.partyTempCards.count < self.count {
                    self.collectionView.mj_footer.endRefreshingWithNoMoreData()
                } else {
                    self.collectionView.mj_footer.endRefreshing()
                }
            } else {
                self.collectionView.reloadData()
                self.collectionView.mj_footer.endRefreshing()
            }
            self.collectionView.mj_header.endRefreshing()
        }
    }
    
    private func updateCollection(uid: NSNumber,status: NSNumber,indexPath:NSIndexPath) {
        let params = [
            "action":"collection",
            "uid":g_uid,
            "randcode":g_randcode,
            "data":[
                "uid": uid,
                "status": status,
            ]
        ]
        JxlNetAPIManager.sharedManager.request_CollectionWithBlock(Globe.baseUrlStr, Params: params as! [String : AnyObject]) { (data, error) in
            if let data = data {
                let requestStatus = data["status"] as! NSNumber
                if requestStatus == 1 { //sucess
                    let info = data["info"] as! [String:AnyObject]
                    let collected_num = info["collected_num"] as! NSNumber
                    
                    let card = self.party == 1 ? self.partyACards[indexPath.row]:self.partyBCards[indexPath.row]
                    card.collected_num = collected_num
                    card.coll_status = status
                    
                } else {
                    SVProgressHUD.showInfoWithStatus(data["msg"] as! String)
                }
                self.collectionView.reloadItemsAtIndexPaths([indexPath])
            } else {
                self.collectionView.reloadData()
            }
        }
    }
    
    func loadBannerData() {
        let params = [
            "action":"find_banner_load",
            "uid":g_uid,
            "randcode":g_randcode,
            "data": [
                "party": Login.curLoginUser()!.user_info!.party!
            ]
        ]
        
        JxlNetAPIManager.sharedManager.request_FindBannerDataWithBlock(Globe.baseUrlStr, Params: params as! [String : AnyObject]) { (data, error) in
            if let data = data {
                let requestStatus = data["status"] as! NSNumber
                if requestStatus == 1 {
                    let info = data["info"] as! [String:AnyObject]
                    for dict in info["list"] as! [[String:AnyObject]] {
                        let banner = BannerHeaderModel(dict: dict)
                        Login.shareLogin.BannerData.append(banner)
                    }
                    
                } else {
                    SVProgressHUD.showInfoWithStatus(data["msg"] as! String)
                }
            }
        }
    }
    
    // 搜索
    func clickSearchBtn() {
        print("点击搜索")
        let poiSearchVC = UINavigationController(rootViewController: POISearchViewController())
        self.navigationController?.presentViewController(poiSearchVC, animated: false, completion: nil)
    }
    
    // 分享
    func clickShareBtn() {
        UMSocialData.defaultData().extConfig.title = "将讯录"
        UMSocialData.defaultData().extConfig.qqData.url = "https://wx.jiangxunlu.com/"
        UMSocialSnsService.presentSnsIconSheetView(self, appKey: Globe.umengAppKey, shareText: "云集商业地产甲方乙方将才精英的通讯录，项目招商与品牌拓展人士扩展交际的必备通讯工具", shareImage: UIImage(named: "logo_default"), shareToSnsNames: [UMShareToWechatSession,UMShareToWechatTimeline,UMShareToSina,UMShareToQQ,UMShareToQzone], delegate: self)
    }
    
    func collectBtnClick(sender: HotIndexButton) {
        if sender.selected {
            print("取消收藏")
            var uid: NSNumber?
            let status = 0 //取消收藏
            let indexPath = NSIndexPath(forRow: sender.row, inSection: sender.section)
            if self.party == 1 {
                let card = partyACards[sender.row]
                uid = card.id
            } else {
                let card = partyBCards[sender.row]
                uid = card.id
            }
            self.updateCollection(uid!, status: status, indexPath: indexPath)
            
        } else {
            print("添加收藏")
            var uid: NSNumber?
            let status = 1 //添加收藏
            let indexPath = NSIndexPath(forRow: sender.row, inSection: sender.section)
            if self.party == 1 {
                let card = partyACards[sender.row]
                uid = card.id
            } else {
                let card = partyBCards[sender.row]
                uid = card.id
            }
            self.updateCollection(uid!, status: status, indexPath: indexPath)
        }
    }
}

//MARK: -- UICollectionViewDataSource, UICollectionViewDelegate
extension AddressBookViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return party == 1 ? partyACards.count : partyBCards.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(kCellIdentifier_PersonCollectionCell, forIndexPath: indexPath) as? PersonCollectionCell
        if party == 1 {
            if partyACards.count > 0 {
                cell!.person = partyACards[indexPath.row]
            }
        } else {
            if partyBCards.count > 0 {
                cell!.person = partyBCards[indexPath.row]
            }
        }
        
        cell?.collectBtn.row = indexPath.row
        cell?.collectBtn.section = indexPath.section
        cell?.collectBtn.addTarget(self, action: #selector(self.collectBtnClick(_:)), forControlEvents: .TouchUpInside)
        
        return cell!
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: kScreen_Bounds.width, height: CustomHeaderView.viewHeight())
    }
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        if kind == UICollectionElementKindSectionHeader {
            let view = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: kCellIdentifier_CustomHeaderView, forIndexPath: indexPath) as! CustomHeaderView
            view.tapPartyBlock = { tag in
                switch tag {
                case 0:
                    print("当前选择：甲方")
                    self.party = 1
                    self.partyACards.removeAll()
                default:
                    print("当前选择：乙方")
                    self.party = 2
                    self.partyBCards.removeAll()
                }
                
                self.start = 0
                self.loadCards()
            }
            return view
        }
        
        abort()
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        collectionView.deselectItemAtIndexPath(indexPath, animated: true)
        var cardModel: AddressCardModel?
        switch self.party {
        case 1:
            cardModel = self.partyACards[indexPath.row]
        default:
            cardModel = self.partyBCards[indexPath.row]
        }
        
        let transdata = ["targetId":cardModel!.id!]
        JXViewControlerRouter.pushToProfileOtherViewController(self, animated: true, data: transdata)
    }
}

//MARK: -- UICollectionViewDelegateFlowLayout
extension AddressBookViewController: UICollectionViewDelegateFlowLayout{
    //设定全局的Cell尺寸
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let screenW = UIScreen.mainScreen().bounds.size.width
        return CGSize(width: screenW * 0.5 - 15, height: 100)
    }
    //每组collectionView的上、下、左、右边距
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    //设定全局的行间距
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 10
    }
    //设定全局的Cell间距
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0
    }
}

//MARK: -- UMSocialUIDelegate
extension AddressBookViewController: UMSocialUIDelegate {
    func didSelectSocialPlatform(platformName: String!, withSocialData socialData: UMSocialData!) {
        print(platformName)
    }
}

















//
//  JXLSegmentView.swift
//  jiangXunLu
//
//  Created by liuxb on 16/7/16.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit


class SegmentButton: UIButton {
    
    convenience init(title:String, normalImage: UIImage, selectedImage: UIImage){
        self.init()
        self.setTitle(title, forState: .Normal)
        self.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        self.titleLabel?.font = UIFont.systemFontOfSize(16)
        self.setBackgroundImage(normalImage, forState: .Normal)
        self.setBackgroundImage(selectedImage, forState: .Selected)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}




class JXLSegmentView: UIView {
    
    var selectedBlock:((Int)->Void)?
    
    private var segmentItems: [SegmentButton]?
    private var selectedIndex: Int?
    
    private var selectedWidth: CGFloat?
    private var otherWidth: CGFloat?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    /**
     - parameter items:      按钮数组
     - parameter selectedBy: 选中的按钮，占整个view的比例，例如：0.5
     */
    convenience init(frame: CGRect, segmentItems:[SegmentButton], selectedBy: CGFloat,selectedIndex: Int) {
        self.init(frame: frame)
        
        self.segmentItems = segmentItems
        self.selectedIndex = selectedIndex
        
        let selectedWidth = frame.width * selectedBy
        let otherWidth = (frame.width - selectedWidth)/CGFloat(segmentItems.count - 1)
        
        self.selectedWidth = selectedWidth
        self.otherWidth = otherWidth
        
        for (index, segmentBtn) in segmentItems.enumerate() {
            
            segmentBtn.frame = CGRect(x: index > 0 ? CGRectGetMaxX(segmentItems[index-1].frame) : 0,
                                      y: 0,
                                      width: index == selectedIndex ? selectedWidth : otherWidth,
                                      height: frame.height)
            
            segmentBtn.selected = index == selectedIndex ? true:false
            segmentBtn.tag = index
            segmentBtn.addTarget(self, action: #selector(self.selectButton(_:)), forControlEvents: .TouchUpInside)
            self.addSubview(segmentBtn)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func selectButton(sender: SegmentButton) {
        if sender.tag != self.selectedIndex {

            for (index,segmentBtn) in self.segmentItems!.enumerate() {
                segmentBtn.frame = CGRect(x: index > 0 ? CGRectGetMaxX(self.segmentItems![index-1].frame) : 0,
                                          y: 0,
                                          width: index == sender.tag ? self.selectedWidth! : self.otherWidth!,
                                          height: segmentBtn.frame.height)
                segmentBtn.selected = sender.tag == index ? true : false
            }
            
            self.selectedIndex = sender.tag
            
            if self.selectedBlock != nil {
                self.selectedBlock!(sender.tag)
            }
        }
    }
}


























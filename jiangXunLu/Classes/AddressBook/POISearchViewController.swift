//
//  POISearchViewController.swift
//  jiangXunLu
//
//  Created by liuxb on 16/6/17.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit
import UICollectionViewLeftAlignedLayout
import SVProgressHUD

let kCollectionViewCellHeight: CGFloat                  = 25
let kCollectionViewCellsHorizonMargin: CGFloat          = 12

let kCollectionViewToLeftMargin: CGFloat                = 20
let kCollectionViewToTopMargin: CGFloat                 = 12
let kCollectionViewToRightMargin: CGFloat               = 20
let kCollectionViewToBottomtMargin: CGFloat             = 10

let kCellBtnCenterToBorderMargin: CGFloat               = 19


let kHotCellIdentifier = "HotCellIdentifier"
let kHotHeaderViewIdentifier = "HotHeaderViewIdentifier"
let kSearchCellIdentifier = "SearchCellIdentifier"
let kSearchHeaderViewIdentifier = "SearchHeaderViewIndentifier"
let kSearchFooterViewIdentifier = "SearchFooterViewIndentifier"

let kSearchResultTableViewCellIdentifier = "SearchResultTableViewCellIndentifier"

class POISearchViewController: BaseViewController {
    
    // hot words
    var hotWords:[String] = []
    // search history
    var searchHistory:[String] = []
    // search results
    var searchResults:[AddressCardModel] = []
    
    private var firstLoad:Bool = false
    // search history and hot view
    private var collectionView: UICollectionView!
    // search results view
    private var tableView:UITableView!
    // isShow results view
    private var showResult: Bool = false
        
    private lazy var customSearchBar: CustomSearchBar = {
        let customSearchBar = CustomSearchBar(frame: CGRect(x: 20, y: 7, width: kScreen_Bounds.width - 75, height: 31))
        customSearchBar.tapCategoryBtnBlock = {
            print("go to country search")
//            let cityVC = CitySearchViewController()
//            self.navigationController?.pushViewController(cityVC, animated: true)
        }
        customSearchBar.tapClearBtnBlock = {
            if self.collectionView.hidden {
                self.tableView.hidden = true
                self.collectionView.hidden = false
                self.reloadSearchHot()
            }
        }
        customSearchBar.delegate = self
        return customSearchBar
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpNavigationBar()
        self.setupCollectionView()
        self.setupTableView()
        self.firstLoad = true
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.setupNavigationStyle()
        self.navigationController?.navigationBar.addSubview(customSearchBar)
        if self.firstLoad {
            self.customSearchBar.becomeFirstResponder()
        }
        self.reloadSearchHot()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillAppear(animated)
        self.customSearchBar.resignFirstResponder()
        self.customSearchBar.removeFromSuperview()
        self.firstLoad = false
    }
    
    private func reloadSearchHot(){
        self.hotWords.removeAll()
        self.searchHistory.removeAll()
        self.loadSearchHot()
        self.collectionView.reloadData()
    }
    
    private func loadSearchHot(){
        let hotParams = [
            "action":"search_hot_load",
            "uid":g_uid,
            "randcode":g_randcode,
            "data":[
                "type": 1
            ]
        ]
        JxlNetAPIManager.sharedManager.request_SearchHotWithBlock(Globe.baseUrlStr, Params: hotParams as! [String : AnyObject]) { (data, error) in
            if let data = data {
                let requestStatus = data["status"] as! NSNumber
                if requestStatus == 1 { //sucess
                    let info = data["info"] as! [String:AnyObject]
                    for dict in info["list"] as! [[String:AnyObject]] {
                        let keyword = dict["keyword"] as! String
                        self.hotWords.append(keyword)
                    }
                } else {
                    SVProgressHUD.showInfoWithStatus(data["msg"] as! String)
                }
                self.collectionView.reloadData()
            } else {
                self.collectionView.reloadData()
            }
        }
        
        let historyParams = [
            "action":"search_history_load",
            "uid":g_uid,
            "randcode":g_randcode,
            "data":[
                "type": 1 //搜索类型,1.搜索名片;更多类型待扩展;默认为1

            ]
        ]
        JxlNetAPIManager.sharedManager.request_SearchHistoryWithBlock(Globe.baseUrlStr, Params: historyParams as! [String : AnyObject]) { (data, error) in
            if let data = data {
                let requestStatus = data["status"] as! NSNumber
                if requestStatus == 1 { //sucess
                    let info = data["info"] as! [String:AnyObject]
                    for dict in info["list"] as! [[String:AnyObject]] {
                        let keyword = dict["keyword"] as! String
                        self.searchHistory.append(keyword)
                    }
                } else {
                    SVProgressHUD.showInfoWithStatus(data["msg"] as! String)
                }
                self.collectionView.reloadData()
            } else {
                self.collectionView.reloadData()
            }
        }
    }
    
    private func searchCards(keyword: String){
        let params = [
            "action":"search_card",
            "uid":g_uid,
            "randcode":g_randcode,
            "data":[
                "keyword": keyword,
            ]
        ]
        
        JxlNetAPIManager.sharedManager.request_SearchCardWithBlock(Globe.baseUrlStr, Params: params as! [String : AnyObject]) { (data, error) in
            if let data = data {
                let requestStatus = data["status"] as! NSNumber
                if requestStatus == 1 { //sucess
                    let info = data["info"] as! [String:AnyObject]
                    for dict in info["list"] as! [[String:AnyObject]] {
                        let card = AddressCardModel(dict: dict)
                        self.searchResults.append(card)
                    }
                } else {
                    SVProgressHUD.showInfoWithStatus(data["msg"] as! String)
                }
                self.collectionView.hidden = true
                self.tableView.hidden = false
                self.tableView.reloadData()
            } else {
                self.tableView.reloadData()
            }
        }
    }
    
    private func setupCollectionView(){
        if collectionView == nil {
            let layout = UICollectionViewLeftAlignedLayout()
            let frame = CGRect(x: 0, y: 0, width: kScreen_Bounds.width, height: kScreen_Bounds.height - self.navigationBarHeight)
            collectionView = UICollectionView(frame: frame, collectionViewLayout: layout)
            collectionView.dataSource = self
            collectionView.delegate = self
            collectionView.alwaysBounceVertical = true
            collectionView.backgroundColor = UIColor(red: 0.933, green: 0.933, blue: 0.933, alpha: 1.00)
            collectionView.registerClass(SearchHistoryCell.self, forCellWithReuseIdentifier: kSearchCellIdentifier)
            collectionView.registerClass(HotCollectionViewCell.self, forCellWithReuseIdentifier: kHotCellIdentifier)
            collectionView.registerClass(HotHeaderView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: kHotHeaderViewIdentifier)
            collectionView.registerClass(SearchHeaderView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: kSearchHeaderViewIdentifier)
            collectionView.registerClass(SearchFooterView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: kSearchFooterViewIdentifier)
            self.view.addSubview(collectionView)
        }
    }
    
    private func setupTableView(){
        if tableView == nil {
            let frame = CGRect(x: 0, y: 0, width: kScreen_Bounds.width, height: kScreen_Bounds.height - self.navigationBarHeight)
            tableView = UITableView(frame: frame, style: .Plain)
            tableView.hidden = true
            tableView.separatorStyle = .None
            tableView.dataSource = self
            tableView.delegate = self
            tableView.backgroundColor = UIColor(red: 0.933, green: 0.933, blue: 0.933, alpha: 1.00)
            tableView.registerClass(SearchResultTableViewCell.self, forCellReuseIdentifier: kSearchResultTableViewCellIdentifier)
            self.view.addSubview(tableView)
        }
    }
    
    
    private func setUpNavigationBar(){
        let cancelBtn = UIBarButtonItem(title: "取消", style: .Plain, target: self, action: #selector(self.popToMainVCAction))
        self.navigationItem.rightBarButtonItem = cancelBtn
    }
    
    private func collectionCellWidthText(text: String) -> CGFloat{
        let size = (text as NSString).sizeWithAttributes([NSFontAttributeName : hotWordFont])
        var cellWidth: CGFloat = size.width + kCellBtnCenterToBorderMargin
        let limitWidth: CGFloat = CGRectGetWidth(self.collectionView.frame) - kCollectionViewToLeftMargin - kCollectionViewToRightMargin
        if cellWidth >= limitWidth {
            cellWidth = limitWidth
        }
        return cellWidth
    }
    
    func popToMainVCAction() {
        self.dismissViewControllerAnimated(false, completion: nil)
    }
    
    func hotButtonClick(sender: HotIndexButton) {
        let indexPath = NSIndexPath(forItem: sender.row, inSection: sender.section)
        self.collectionView(self.collectionView, didSelectItemAtIndexPath: indexPath)
    }
    
    func deleteAllHistory() {
        self.deleteHistoryForServer()
        self.searchHistory.removeAll()
        self.collectionView.reloadData()
    }

    func deleteHistoryForServer() {
        let params = [
            "action":"search_history_clear",
            "uid":g_uid,
            "randcode":g_randcode,
            "data":[
                "type": 1 //(可选)搜索类型,1.搜索名片;更多类型待扩展;默认为1
            ]
        ]
        
        JxlNetAPIManager.sharedManager.request_ClearSearchHistoryWithBlock(Globe.baseUrlStr, Params: params as! [String : AnyObject]) { (data, error) in
            if let data = data {
                let status = data["status"] as! NSNumber
                if status != 1 {
                     SVProgressHUD.showInfoWithStatus(data["msg"] as! String)
                }
            }
        }
    }
    
    
    func deleteHistory(sender: HotIndexButton) {
        let indexPath = NSIndexPath(forItem: sender.row, inSection: sender.section)
        self.searchHistory.removeAtIndex(sender.row)
        if self.searchHistory.count > 1 {
            self.collectionView.deleteItemsAtIndexPaths([indexPath])
        }
        sender.hidden = true
        self.collectionView.reloadData()
    }
    
    func handleLongPress(recognizer: UILongPressGestureRecognizer) {
        if recognizer.state == .Began {
            print("开始长按操作")
            let indexPath = NSIndexPath(forItem: recognizer.view!.tag, inSection: 1)
            (recognizer.view! as! SearchHistoryCell).deleteBtn.row = indexPath.row
            (recognizer.view! as! SearchHistoryCell).deleteBtn.section = indexPath.section
            (recognizer.view! as! SearchHistoryCell).deleteBtn.hidden = false
        } else if recognizer.state == .Ended {
            print("结束触发长按操作")
        }
    }
    
    
//    func collectBtnClick(sender: HotIndexButton) {
//        if sender.selected {
//            print("取消收藏")
//            sender.setImage(UIImage(named: "add_sc_normal"), forState: .Normal)
//        } else {
//            print("添加收藏")
//            sender.setImage(UIImage(named: "add_sc_light"), forState: .Normal)
//        }
//    }
    
    
    func collectBtnClick(sender: HotIndexButton) {
        var status = -1
        if sender.selected {
            print("取消收藏")
            status = 0 //取消收藏
        } else {
            print("添加收藏")
            status = 1 //添加收藏
        }
        let indexPath = NSIndexPath(forRow: sender.row, inSection: sender.section)
        let card = searchResults[sender.row]
        let uid:NSNumber = card.id!
        self.updateCollection(uid, status: status, indexPath: indexPath)
    }
    
    
    func searchWithKeyword(keyword: String?) {
        self.searchResults.removeAll()
        self.customSearchBar.resignFirstResponder()
        self.customSearchBar.text = keyword
        
        if let keyword = keyword {
            if keyword != "" {
                self.searchResults.removeAll()
                self.searchCards(keyword)
            }
        }
    }
    
    private func updateCollection(uid: NSNumber,status: NSNumber,indexPath:NSIndexPath) {
        let params = [
            "action":"collection",
            "uid":g_uid,
            "randcode":g_randcode,
            "data":[
                "uid": uid,
                "status": status,
            ]
        ]
        JxlNetAPIManager.sharedManager.request_CollectionWithBlock(Globe.baseUrlStr, Params: params as! [String : AnyObject]) { (data, error) in
            if let data = data {
                let requestStatus = data["status"] as! NSNumber
                if requestStatus == 1 { //sucess
                    let info = data["info"] as! [String:AnyObject]
                    let collected_num = info["collected_num"] as! NSNumber
                    
                    let card = self.searchResults[indexPath.row]
                    card.collected_num = collected_num
                    card.coll_status = status
                    
                } else {
                    SVProgressHUD.showInfoWithStatus(data["msg"] as! String)
                }
                self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .None)
            } else {
                self.tableView.reloadData()
            }
        }
    }
    
}

//MARK: -- UICollectionViewDataSource, UICollectionViewDelegate
extension POISearchViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    //MARK: -- UICollectionViewDataSource
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0:
            return hotWords.count
        default:
            return searchHistory.count
        }
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(kHotCellIdentifier, forIndexPath: indexPath) as! HotCollectionViewCell
            
            cell.button.frame = CGRect(x: 0, y: 0, width: CGRectGetWidth(cell.frame), height: CGRectGetHeight(cell.frame))
            if hotWords.count > 0 {
                cell.button.setTitle(hotWords[indexPath.row], forState: .Normal)
                cell.button.addTarget(self, action: #selector(self.hotButtonClick(_:)), forControlEvents: .TouchUpInside)
                cell.button.section = indexPath.section
                cell.button.row = indexPath.row
            }
            return cell
        default:
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(kSearchCellIdentifier, forIndexPath: indexPath) as! SearchHistoryCell
            cell.deleteBtn.addTarget(self, action: #selector(self.deleteHistory(_:)), forControlEvents: .TouchUpInside)
            let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress(_:)))
            longPressRecognizer.minimumPressDuration = 1.0
            longPressRecognizer.view?.tag = indexPath.row
            cell.addGestureRecognizer(longPressRecognizer)
            if searchHistory.count > 0 {
                cell.historyLabel.text = searchHistory[indexPath.row]
            }
            return cell
        }
        

    }
    
    //MARK: -- UICollectionViewDelegate
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        collectionView.deselectItemAtIndexPath(indexPath, animated: true)
        switch indexPath.section {
        case 0:
            let hotWord = hotWords[indexPath.row]
            print("热门搜查词：\(hotWord)")
            self.searchWithKeyword(hotWord)
        default:
            let history = searchHistory[indexPath.row]
            print("搜索历史：\(history)")
            self.searchWithKeyword(history)
        }

    }
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        if kind == UICollectionElementKindSectionHeader {
            switch indexPath.section {
            case 0:
                let hotHeaderview = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: kHotHeaderViewIdentifier, forIndexPath: indexPath) as! HotHeaderView
                return hotHeaderview
            default:
                let searchHeaderview = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: kSearchHeaderViewIdentifier, forIndexPath: indexPath) as! SearchHeaderView
                let hidden = searchHistory.count > 0 ? false : true
                searchHeaderview.seHeaderViewtHidden(hidden)
                return searchHeaderview
            }
        } else {
            let searchFooterView = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: kSearchFooterViewIdentifier, forIndexPath: indexPath) as! SearchFooterView
            switch indexPath.section {
            case 0:
                searchFooterView.hidden = true
            default:
                searchFooterView.hidden = searchHistory.count > 0 ? false : true
                searchFooterView.cleanBtn.addTarget(self, action: #selector(self.deleteAllHistory), forControlEvents: .TouchUpInside)
            }
            
            return searchFooterView
        }
    }
}


extension POISearchViewController: UICollectionViewDelegateLeftAlignedLayout {
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        switch indexPath.section {
        case 0:
            let text = hotWords[indexPath.row]
            let cellWidth = self.collectionCellWidthText(text)
            return CGSize(width: cellWidth, height: kCollectionViewCellHeight)
        default:
            return CGSize(width: kScreen_Bounds.width, height: SearchHistoryCell.cellHeight())
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        switch section {
        case 0:
            return kCollectionViewCellsHorizonMargin
        default:
            return 0
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return section == 0 ? kCollectionViewToTopMargin : 0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        switch section {
        case 0:
            return UIEdgeInsets(top: kCollectionViewToTopMargin, left: kCollectionViewToLeftMargin, bottom: kCollectionViewToBottomtMargin, right: kCollectionViewToRightMargin)
        default:
            return UIEdgeInsetsZero
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: kScreen_Bounds.width, height: section == 0 ? HotHeaderView.viewHeight():SearchHeaderView.viewHeight())
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: kScreen_Bounds.width, height: section == 0 ? 0 : SearchFooterView.viewHeight())
    }
}

//MARK: -- UITableViewDataSource, UITableViewDelegate
extension POISearchViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResults.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(kSearchResultTableViewCellIdentifier, forIndexPath: indexPath) as! SearchResultTableViewCell
        if searchResults.count > 0 {
            cell.person = searchResults[indexPath.row]
        }
        cell.collectBtn.row = indexPath.row
        cell.collectBtn.section = indexPath.section
        cell.collectBtn.addTarget(self, action: #selector(self.collectBtnClick(_:)), forControlEvents: .TouchUpInside)
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return SearchResultTableViewCell.cellHeight()
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        let cardModel = self.searchResults[indexPath.row]
        print("点击搜索卡片：\(cardModel)")
        let transdata = ["targetId":cardModel.id!]
        JXViewControlerRouter.pushToProfileOtherViewController(self, animated: true, data: transdata)
    }
}

//MARK: --  UIScrollViewDelegate
extension POISearchViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(scrollView: UIScrollView){
        self.customSearchBar.resignFirstResponder()
    }
}

//MARK: --  UISearchBarDelegate
extension POISearchViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        self.searchWithKeyword(searchBar.text)
    }
}
































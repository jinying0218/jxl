//
//  CitySearchViewController.swift
//  jiangXunLu
//
//  Created by liuxb on 16/6/17.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit

let kSectionCityHeaderViewIdentifier = "SectionCityHeaderView"
let kButtonCityTableViewCellIdentifier = "ButtonCityTableViewCell"
let kSectionCityTableViewCellIdentifier = "SectionCityTableViewCell"

class CitySearchViewController: BaseViewController {
    
    var currentCity: String = "上海"
    var recentCities: [String] = ["上海","北京"]
    var hotCities:[String] = ["上海","北京","杭州","黔西南布依族苗族自治州"]
    var allCtiies:[SectionCityModel] = []
    
    private var tableView: UITableView!
    
    override func viewDidLoad() {
        self.initData()
        super.viewDidLoad()
        self.setUpNavigationBar()
    }
    
    private func initData(){
        let filePath = NSBundle.mainBundle().pathForResource("EMcitydict", ofType: "plist")
        if let  cityDict = NSDictionary(contentsOfFile: filePath!) {
            for key in cityDict.allKeys as! [String]{
                let sectionCity = SectionCityModel()
                sectionCity.type = key
                sectionCity.cities = cityDict[key] as? [String]
                allCtiies.append(sectionCity)
            }
            //A->Z 排序
            allCtiies = allCtiies.sort { $0.type < $1.type }
        }

    }
    
    private func setupTableView(){
        if tableView == nil {
            tableView = UITableView(frame: kScreen_Bounds, style: .Grouped)
            tableView.dataSource = self
            tableView.delegate = self
            tableView.separatorStyle = .None
            tableView.backgroundColor = UIColor(red: 0.933, green: 0.933, blue: 0.933, alpha: 1.00)
            tableView.registerClass(ButtonCityTableViewCell.self, forCellReuseIdentifier: kButtonCityTableViewCellIdentifier)
            tableView.registerClass(SectionCityTableViewCell.self, forCellReuseIdentifier: kSectionCityTableViewCellIdentifier)
            tableView.registerClass(SectionCityHeaderView.self, forHeaderFooterViewReuseIdentifier: kSectionCityHeaderViewIdentifier)
            self.view.addSubview(tableView)
        }
    }
    
    private func setUpNavigationBar(){
        self.navigationItem.title = "选择城市"
        self.navigationItem.hidesBackButton = true
        let backBtn = UIBarButtonItem(image: UIImage(named: "back_normal"), style: .Plain, target: self, action: #selector(self.backToPreViewController))
        navigationItem.leftBarButtonItem = backBtn
    }

    // 返回
    func backToPreViewController() {
        self.navigationController?.popViewControllerAnimated(true)
    }
}

extension CitySearchViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return allCtiies.count + 3
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section < 3 {
            return 1
        }
        return allCtiies.count > 0 ? allCtiies[section - 3].cities!.count : 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if indexPath.section < 3 {
            let cell = tableView.dequeueReusableCellWithIdentifier(kButtonCityTableViewCellIdentifier, forIndexPath: indexPath) as! ButtonCityTableViewCell
            return cell
        }
        
        let cell = tableView.dequeueReusableCellWithIdentifier(kSectionCityTableViewCellIdentifier, forIndexPath: indexPath) as! SectionCityTableViewCell
        return cell
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterViewWithIdentifier(kSectionCityHeaderViewIdentifier) as! SectionCityHeaderView
        switch section {
        case 0:
            view.setTitleButton("定位城市", isCircleStyle: false)
        case 1:
            view.setTitleButton("最近访问城市", isCircleStyle: false)
        case 2:
            view.setTitleButton("热门城市", isCircleStyle: false)
        default:
            view.setTitleButton(allCtiies[section-3].type!, isCircleStyle: true)
        }
        return view
    }
}











































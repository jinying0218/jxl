//
//  SearchHeaderView.swift
//  jiangXunLu
//
//  Created by liuxb on 16/6/18.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit
import SnapKit

class SearchHeaderView: UICollectionReusableView {
    var titleButton: HotIndexButton!
    private var horizonLine: UIView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        titleButton = HotIndexButton()
        titleButton.sizeToFit()
        titleButton.setTitle("搜索历史", forState: .Normal)
        titleButton.titleLabel?.font = hotWordFont
        titleButton.titleLabel?.textAlignment = .Left

        // #eacb21
        titleButton.setTitleColor(UIColor(red: 0.4, green: 0.4, blue: 0.4, alpha: 1.0), forState: .Normal)
        titleButton.backgroundColor = UIColor.clearColor()
        titleButton.userInteractionEnabled = false
        self.addSubview(titleButton)
        titleButton.snp_makeConstraints { (make) in
            make.left.equalTo(self).offset(20)
            make.centerY.equalTo(self)
        }
        
        horizonLine = UIView()
        horizonLine.backgroundColor = UIColor(red: 0.871, green: 0.871, blue: 0.871, alpha: 1.0)
        self.addSubview(horizonLine)
        horizonLine.snp_makeConstraints { (make) in
            make.left.equalTo(self)
            make.right.equalTo(self)
            make.height.equalTo(1)
            make.top.equalTo(self)
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func seHeaderViewtHidden(hidden: Bool) {
        if hidden {
            self.titleButton.setTitle("", forState: .Normal)
            self.horizonLine.backgroundColor = UIColor.clearColor()
        } else {
            self.titleButton.setTitle("搜索历史", forState: .Normal)
            self.horizonLine.backgroundColor = UIColor(red: 0.871, green: 0.871, blue: 0.871, alpha: 1.0)
        }
    }
    
    class func viewHeight() -> CGFloat{
        return 50
    }
}

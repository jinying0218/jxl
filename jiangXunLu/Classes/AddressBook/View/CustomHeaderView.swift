//
//  CustomHeaderView.swift
//  jiangXunLu
//
//  Created by liuxb on 16/6/16.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit

class CustomHeaderView: UICollectionReusableView {
    
    var tapPartyBlock:((Int)->Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        // 375*120 top image
        let scale = kScreen_Bounds.width/375
        let topBannerView = UIImageView(frame: CGRect(x: 0, y: 0, width: 375*scale, height: 120*scale))
        topBannerView.image = UIImage(named: "add_top")
        self.addSubview(topBannerView)
        
        let segmentJia = SegmentButton(title: "甲方", normalImage: UIImage(named: "menu_yellow_normal")!, selectedImage: UIImage(named: "menu_yellow_light")!)
        let segmentYi = SegmentButton(title: "乙方", normalImage: UIImage(named: "menu_yellow_normal")!, selectedImage: UIImage(named: "menu_yellow_light")!)
        let segmentedControl = JXLSegmentView(frame: CGRect(x: 0.0, y: CGRectGetMaxY(topBannerView.frame), width: self.bounds.width, height: 35.0),segmentItems: [segmentJia,segmentYi], selectedBy: 0.5, selectedIndex: 0)
        segmentedControl.selectedBlock = { tag in
            switch tag {
            case 0:
                print("甲方")
                if self.tapPartyBlock != nil {
                    self.tapPartyBlock!(0)
                }
            default:
                print("乙方")
                if self.tapPartyBlock != nil {
                    self.tapPartyBlock!(1)
                }
            }
        }
        
        self.frame = CGRect(x: 0, y: 0, width: kScreen_Bounds.width, height: CGRectGetMaxY(segmentedControl.frame))
        self.addSubview(segmentedControl)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    class func viewHeight() -> CGFloat{
        return 120 * kScreen_Bounds.width/375 + 35
    }
}

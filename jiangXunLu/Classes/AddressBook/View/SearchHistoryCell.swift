//
//  SearchHistoryCell.swift
//  jiangXunLu
//
//  Created by liuxb on 16/6/18.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit
import SnapKit

class SearchHistoryCell: UICollectionViewCell {
    
    var historyLabel: UILabel!
    var deleteBtn: HotIndexButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        // #eeeee
        self.backgroundColor = UIColor(red: 0.933, green: 0.933, blue: 0.933, alpha: 1.00)
        
        let iconView = UIImageView(image: UIImage(named: "poi_search_normal"))
        self.contentView.addSubview(iconView)
        iconView.snp_makeConstraints { (make) in
            make.left.equalTo(self).offset(kCollectionViewToLeftMargin)
            make.centerY.equalTo(self)
            make.size.equalTo(25)
        }
        
        historyLabel = UILabel()
        historyLabel.sizeToFit()
        historyLabel.textColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1.0)
        historyLabel.font = hotWordFont
        self.contentView.addSubview(historyLabel)
        historyLabel.snp_makeConstraints { (make) in
            make.left.equalTo(iconView.snp_right).offset(5)
            make.centerY.equalTo(self)
            make.width.lessThanOrEqualTo(self).offset(-kCollectionViewToRightMargin)
        }
        
        deleteBtn = HotIndexButton()
        deleteBtn.hidden = true
        deleteBtn.setTitle("删除", forState: .Normal)
        deleteBtn.titleLabel?.font = hotWordFont
        deleteBtn.backgroundColor = UIColor.redColor()
        self.contentView.addSubview(deleteBtn)
        deleteBtn.snp_makeConstraints { (make) in
            make.right.equalTo(self).offset(-kCollectionViewToRightMargin)
            make.centerY.equalTo(self)
            make.size.equalTo(CGSize(width: SearchHistoryCell.cellHeight()*1.5, height: SearchHistoryCell.cellHeight()*0.6))
        }
        
        let horizonLine = UIView()
        horizonLine.backgroundColor = UIColor(red: 0.871, green: 0.871, blue: 0.871, alpha: 1.0)
        self.contentView.addSubview(horizonLine)
        horizonLine.snp_makeConstraints { (make) in
            make.left.equalTo(self)
            make.right.equalTo(self)
            make.height.equalTo(1)
            make.bottom.equalTo(self)
        }
        
        self.selectedBackgroundView = CustomBackgroundView(frame: CGRectZero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    class func cellHeight() -> CGFloat{
        return 44
    }
}

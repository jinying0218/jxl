//
//  ButtonCityTableViewCell.swift
//  jiangXunLu
//
//  Created by liuxb on 16/6/19.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit

class ButtonCityTableViewCell: UITableViewCell {

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .None
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

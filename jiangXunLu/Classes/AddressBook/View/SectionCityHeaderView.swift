//
//  SectionCityHeaderView.swift
//  jiangXunLu
//
//  Created by liuxb on 16/6/19.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit

class SectionCityHeaderView: UITableViewHeaderFooterView {
    
    var titleButton: HotIndexButton!
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        titleButton = HotIndexButton()
        titleButton.sizeToFit()
        titleButton.userInteractionEnabled = false
        titleButton.titleLabel?.font = hotWordFont
        titleButton.titleLabel?.textAlignment = .Left
        titleButton.snp_makeConstraints { (make) in
            make.left.equalTo(self).offset(20)
            make.centerY.equalTo(self)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setTitleButton(title: String, isCircleStyle: Bool) {
        self.titleButton.setTitle(title, forState: .Normal)
        if isCircleStyle {
            self.titleButton.layer.cornerRadius = self.titleButton.frame.height
            self.titleButton.layer.masksToBounds = true
            self.titleButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            self.titleButton.backgroundColor = UIColor(red: 0.918, green: 0.796, blue: 0.129, alpha: 1.0)
        } else {
            self.titleButton.setTitleColor(UIColor(red: 0.4, green: 0.4, blue: 0.4, alpha: 1.0), forState: .Normal)
            self.titleButton.backgroundColor = UIColor.clearColor()
        }
    }
}

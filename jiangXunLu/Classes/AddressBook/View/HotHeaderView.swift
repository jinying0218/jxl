//
//  HotHeaderView.swift
//  jiangXunLu
//
//  Created by liuxb on 16/6/17.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit

class HotHeaderView: UICollectionReusableView {
    var titleButton: HotIndexButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        titleButton = HotIndexButton(frame: CGRect(x: 20, y: 10, width: 50, height: 22))
        titleButton.setTitle("HOT", forState: .Normal)
        titleButton.titleLabel?.font = UIFont.systemFontOfSize(14)
        titleButton.layer.cornerRadius = 8
        titleButton.layer.masksToBounds = true
        // #eacb21
        titleButton.backgroundColor = UIColor(red: 0.918, green: 0.796, blue: 0.129, alpha: 1.0)
        titleButton.userInteractionEnabled = false
        self.addSubview(titleButton)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    class func viewHeight() -> CGFloat{
        return 30
    }
}

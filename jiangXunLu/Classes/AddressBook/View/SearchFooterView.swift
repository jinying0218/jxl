//
//  SearchFooterView.swift
//  jiangXunLu
//
//  Created by liuxb on 16/6/18.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit
import SnapKit

class SearchFooterView: UICollectionReusableView {
    
    var cleanBtn: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        cleanBtn = UIButton()
        cleanBtn.setTitle("清除搜索历史", forState: .Normal)
        cleanBtn.titleLabel?.font = hotWordFont
        cleanBtn.setTitleColor(UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1.0), forState: .Normal)
        cleanBtn.setTitleColor(UIColor.blackColor(), forState: .Highlighted)
        self.addSubview(cleanBtn)
        cleanBtn.snp_makeConstraints { (make) in
            make.center.equalTo(self)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    class func viewHeight() -> CGFloat {
        return 44
    }
}

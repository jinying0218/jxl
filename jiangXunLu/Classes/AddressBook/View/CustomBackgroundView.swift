//
//  CustomBackgroundView.swift
//  jiangXunLu
//
//  Created by liuxb on 16/6/18.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit

class CustomBackgroundView: UIView {
    override func drawRect(rect: CGRect) {
        // draw a rounded rect bezier path filled with blue
        let aRef = UIGraphicsGetCurrentContext()
        CGContextSaveGState(aRef!);
        let bezierPath = UIBezierPath(roundedRect: rect, cornerRadius: 0.0)
        
        bezierPath.lineWidth = 5.0
        UIColor.blackColor().setStroke()
        
        let fillColor = UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1.0)// color equivalent is #666666
        fillColor.setFill()
        
        bezierPath.stroke()
        bezierPath.fill()
        
        CGContextRestoreGState(aRef!)
    }
}

//
//  SearchResultTableViewCell.swift
//  jiangXunLu
//
//  Created by liuxb on 16/6/18.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit
import SnapKit

class SearchResultTableViewCell: UITableViewCell {
    
    var person: AddressCardModel? {
        didSet{
            self.companyLabel.text = person?.company
            self.positionLabel.text = person?.region
            if let head = person?.head {
                let url = NSURL(string: Globe.baseUrlStr + head)!
                self.iconView.sd_setImageWithURL(url)
            }
            self.userNameLabel.text = person?.name
            self.departmentLabel.text = person?.job
            self.collectBtn.selected = person?.coll_status == 1 ? true : false
            if  let num = person?.collected_num {
                self.personsCollect.text = String(num)
            }
            
        }
    }
    
    var viewWrapper: UIView!
    private var companyLabel: UILabel!
    private var positionLabel: UILabel!
    private var iconView: UIImageView!
    private var userNameLabel: UILabel!
    private var departmentLabel: UILabel!
    var collectBtn: HotIndexButton!
    private var personsCollect: UILabel!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        self.selectionStyle = .None
        self.backgroundColor = UIColor.clearColor()

        let margin:CGFloat = 10
        let largeFont = UIFont.systemFontOfSize(16)
        let smallFont = UIFont.systemFontOfSize(14)
        let gpsWidth: CGFloat = 120

        viewWrapper = UIView()
        viewWrapper.backgroundColor = UIColor.whiteColor()
        viewWrapper.layer.cornerRadius = 8.0
        viewWrapper.layer.masksToBounds = true
        self.contentView.addSubview(viewWrapper)
        viewWrapper.snp_makeConstraints { (make) in
            make.edges.equalTo(self.contentView).offset(EdgeInsets(top: margin*1.5, left: margin*1.5, bottom: 0, right: -margin*1.5))
        }
        
        companyLabel = UILabel()
        companyLabel.sizeToFit()
        companyLabel.font = largeFont
        companyLabel.textColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1.0)
        viewWrapper.addSubview(companyLabel)
        companyLabel.snp_makeConstraints { (make) in
            make.left.equalTo(viewWrapper).offset(margin)
            make.top.equalTo(viewWrapper).offset(margin)
            make.right.lessThanOrEqualTo(viewWrapper).offset(-gpsWidth-margin)
        }
        
        positionLabel = UILabel()
        positionLabel.sizeToFit()
        positionLabel.font = smallFont
        positionLabel.textColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1.0)
        viewWrapper.addSubview(positionLabel)
        positionLabel.snp_makeConstraints { (make) in
            make.right.equalTo(viewWrapper).offset(-margin*1.5)
            make.centerY.equalTo(companyLabel)
            make.width.lessThanOrEqualTo(80)
        }
        
        let gpsIconView = UIImageView()
        gpsIconView.image = UIImage(named: "search_position_normal")
        viewWrapper.addSubview(gpsIconView)
        gpsIconView.snp_makeConstraints { (make) in
            make.right.equalTo(positionLabel.snp_left)
            make.centerY.equalTo(positionLabel)
            make.size.equalTo(22)
        }
        
        let horizonLine = UIView()
        horizonLine.backgroundColor = UIColor(red: 0.871, green: 0.871, blue: 0.871, alpha: 1.0)
        viewWrapper.addSubview(horizonLine)
        horizonLine.snp_makeConstraints { (make) in
            make.centerX.equalTo(viewWrapper)
            make.top.equalTo(companyLabel.snp_bottom).offset(margin)
            make.left.equalTo(viewWrapper).offset(margin*0.5)
            make.right.equalTo(viewWrapper).offset(-margin*0.5)
            make.height.equalTo(1)
        }
        
        iconView = UIImageView()
        iconView.backgroundColor = UIColor.lightGrayColor()
        viewWrapper.addSubview(iconView)
        iconView.snp_makeConstraints { (make) in
            make.left.equalTo(companyLabel)
            make.top.equalTo(horizonLine.snp_bottom).offset(margin)
            make.size.equalTo(60)
        }
        
        userNameLabel = UILabel()
        userNameLabel.font = largeFont
        userNameLabel.sizeToFit()
        userNameLabel.textColor = UIColor(red: 0.4, green: 0.4, blue: 0.4, alpha: 1.0)
        viewWrapper.addSubview(userNameLabel)
        userNameLabel.snp_makeConstraints { (make) in
            make.left.equalTo(iconView.snp_right).offset(margin)
            make.top.equalTo(iconView).offset(margin*0.4)
            make.width.lessThanOrEqualTo(80)
        }
        
        let verticalLine = UIView()
        verticalLine.backgroundColor = UIColor(red: 0.871, green: 0.871, blue: 0.871, alpha: 1.0)
        viewWrapper.addSubview(verticalLine)
        verticalLine.snp_makeConstraints { (make) in
            make.left.equalTo(userNameLabel.snp_right).offset(margin)
            make.top.equalTo(userNameLabel)
            make.width.equalTo(1)
            make.height.equalTo(userNameLabel.snp_height)
        }
        
        departmentLabel = UILabel()
        departmentLabel.font = largeFont
        departmentLabel.sizeToFit()
        departmentLabel.textColor = UIColor(red: 0.4, green: 0.4, blue: 0.4, alpha: 1.0)
        viewWrapper.addSubview(departmentLabel)
        departmentLabel.snp_makeConstraints { (make) in
            make.left.equalTo(verticalLine.snp_right).offset(margin)
            make.top.equalTo(userNameLabel)
            make.right.lessThanOrEqualTo(viewWrapper).offset(-30-margin*1.5)
        }
        
        // collectBtn
        collectBtn = HotIndexButton()
        collectBtn.setImage(UIImage(named: "add_sc_normal"), forState: .Normal)
        collectBtn.setImage(UIImage(named: "add_sc_light"), forState: .Selected)
        viewWrapper.addSubview(collectBtn)
        collectBtn.snp_makeConstraints { (make) in
            make.right.equalTo(viewWrapper).offset(-margin*1.5)
            make.top.equalTo(departmentLabel)
            make.size.equalTo(20)
        }
        
        let personsImgView = UIImageView()
        personsImgView.image = UIImage(named: "add_sc_persons")
        viewWrapper.addSubview(personsImgView)
        personsImgView.snp_makeConstraints { (make) in
            make.left.equalTo(userNameLabel)
            make.bottom.equalTo(iconView).offset(-margin*0.4)
            make.size.equalTo(14)
        }

        personsCollect = UILabel()
        personsCollect.text = "965"
        personsCollect.sizeToFit()
        personsCollect.font = smallFont
        personsCollect.textColor = UIColor(red: 0.918, green: 0.239, blue: 0.051, alpha: 1)
        viewWrapper.addSubview(personsCollect)
        personsCollect.snp_makeConstraints { (make) in
            make.left.equalTo(personsImgView.snp_right)
            make.centerY.equalTo(personsImgView)
            make.width.lessThanOrEqualTo(viewWrapper).offset(margin)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    class func cellHeight() -> CGFloat {
        return 140
    }
}






































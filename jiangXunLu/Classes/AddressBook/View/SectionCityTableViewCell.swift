//
//  SectionCityTableViewCell.swift
//  jiangXunLu
//
//  Created by liuxb on 16/6/19.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit

class SectionCityTableViewCell: UITableViewCell {
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .None
        self.accessoryType = .DisclosureIndicator
        self.selectedBackgroundView = CustomBackgroundView(frame: CGRectZero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

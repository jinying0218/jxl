//
//  CustomSearchBar.swift
//  jiangXunLu
//
//  Created by liuxb on 16/6/17.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit

class CustomSearchBar: UISearchBar {
    
    private var categoryBtn: UIButton!
    private var iconBtn: UIButton!
    private var textField: UITextField!
    var tapCategoryBtnBlock:(()->Void)?
    var tapClearBtnBlock:(()->Void)?


    override init(frame: CGRect) {
        super.init(frame: frame)
        // #999999
        self.tintColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1.0)
        self.barTintColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1.0)
        
        self.layer.cornerRadius = 15
        self.layer.masksToBounds = true
        
        //categoryBtn
        categoryBtn = UIButton(frame: CGRect(x: 5, y: 0, width: 40, height: 31))
        categoryBtn.titleLabel?.font = UIFont.systemFontOfSize(14)
        categoryBtn.setTitle("全国", forState: .Normal)
        categoryBtn.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        categoryBtn.addTarget(self, action: #selector(self.selectCategoryAction), forControlEvents: .TouchUpInside)
        self.addSubview(categoryBtn)
        
        //iconBtn
        iconBtn = UIButton(frame: CGRect(x: 40, y: 10, width: 12, height: 12))
        iconBtn.setBackgroundImage(UIImage(named: "search_arrow_down"), forState: .Normal)
        iconBtn.addTarget(self, action: #selector(self.selectCategoryAction), forControlEvents: .TouchUpInside)
        self.addSubview(iconBtn)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        self.autoresizesSubviews = true
        let finalPredicate = NSPredicate { (candidateView, dict) -> Bool in
            if candidateView.isMemberOfClass(NSClassFromString("UISearchBarTextField")!) {
                return true
            } else {
                return false
            }
        }
        
        // find textField
        if let searchField = NSArray(array: self.subviews.first!.subviews).filteredArrayUsingPredicate(finalPredicate).last as? UITextField{
            searchField.textAlignment = .Left
            searchField.frame = CGRect(x: 48, y: 4.8, width: self.frame.size.width - 50, height: 22)
            searchField.backgroundColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1.0)
            searchField.tintColor = UIColor.whiteColor()
            searchField.textColor = UIColor.whiteColor()
            searchField.leftView?.frame = CGRectZero
            textField = searchField
            let deleteBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            deleteBtn.setBackgroundImage(UIImage(named: "search_delete"), forState: .Normal)
            deleteBtn.addTarget(self, action: #selector(self.deleteBtnClick), forControlEvents: .TouchUpInside)
            searchField.rightView = deleteBtn
            searchField.rightViewMode = .WhileEditing
            searchField.clearButtonMode = .Never
        }
    }
    
    
    func selectCategoryAction() {
        if self.tapCategoryBtnBlock != nil {
            self.tapCategoryBtnBlock!()
        }
    }
    
    func setCategoryBtntTitle(title: String) {
        self.categoryBtn.setTitle(title, forState: .Normal)
    }
    
    func deleteBtnClick() {
        self.textField.text = ""
        self.textField.resignFirstResponder()
        if self.tapClearBtnBlock != nil {
            self.tapClearBtnBlock!()
        }
    }
    
}





























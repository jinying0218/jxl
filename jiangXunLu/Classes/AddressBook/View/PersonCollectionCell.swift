//
//  PersonCollectionCell.swift
//  jiangXunLu
//
//  Created by liuxb on 16/6/15.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit
import SnapKit
import SDWebImage

class PersonCollectionCell: UICollectionViewCell {
    var person: AddressCardModel? {
        didSet{
            if let head = person?.head {
                let url = NSURL(string: Globe.baseUrlStr + head)!
                self.iconImgView.sd_setImageWithURL(url)
            }
            self.nameLabel.text = person?.name
            if  let num = person?.collected_num {
                self.collectNumLabel.text = String(num)
            }
            self.collectBtn.selected = person?.coll_status == 1 ? true : false
            self.departmentLabel.text = person?.job
            if let phone = person?.phone {
                let phoneStr = String(phone)
                let text = "Tel:  "
                if phoneStr == "" {
                    self.teleLabel.text = text
                    return
                }
                /*
                let  preNum = phoneStr.substringToIndex(phoneStr.startIndex.advancedBy(2))
                let  sufNum = phoneStr.substringFromIndex(phoneStr.endIndex.advancedBy(-3))
                text = text + preNum + "*****" + sufNum
                */
                self.teleLabel.text = text + phoneStr
            }
            self.companyLabel.text = person?.company
        }
    }
    
    private var iconImgView: UIImageView!
    private var nameLabel: UILabel!
    var collectNumLabel: UILabel!
    var collectBtn: HotIndexButton!
    private var departmentLabel: UILabel!
    private var teleLabel: UILabel!
    private var companyLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.whiteColor()
        self.layer.cornerRadius = 8.0
        self.layer.masksToBounds = true
        
        let margin:CGFloat = 10
        // iconImgView
        iconImgView = UIImageView()
        iconImgView.layer.cornerRadius = 40/2
        iconImgView.layer.masksToBounds = true
        iconImgView.image = UIImage(named: "logo_default_woman")
        self.contentView.addSubview(iconImgView)
        iconImgView.snp_makeConstraints { (make) in
            make.top.equalTo(self).offset(margin)
            make.left.equalTo(self).offset(margin)
            make.size.equalTo(40)
        }
        
        // nameLabel
        nameLabel = UILabel()
        nameLabel.font = UIFont.systemFontOfSize(15)
        nameLabel.sizeToFit()
        self.contentView.addSubview(nameLabel)
        nameLabel.snp_makeConstraints { (make) in
            make.left.equalTo(iconImgView.snp_right).offset(margin*0.4)
            make.top.equalTo(iconImgView)
            make.width.equalTo(48)
        }
        
        // collectBtn
        collectBtn = HotIndexButton()
        collectBtn.setImage(UIImage(named: "add_sc_normal"), forState: .Normal)
        collectBtn.setImage(UIImage(named: "add_sc_light"), forState: .Selected)
        self.contentView.addSubview(collectBtn)
        collectBtn.snp_makeConstraints { (make) in
            make.centerY.equalTo(nameLabel)
            make.right.equalTo(self).offset(-margin)
            make.size.equalTo(18)
        }
        
        // personsImg + collectNumLabel
        
        collectNumLabel = UILabel()
        collectNumLabel.sizeToFit()
        collectNumLabel.textColor = UIColor(red: 0.918, green: 0.239, blue: 0.051, alpha: 1)
        collectNumLabel.font = UIFont.systemFontOfSize(10)
        self.contentView.addSubview(collectNumLabel)
        collectNumLabel.snp_makeConstraints { (make) in
            make.centerY.equalTo(nameLabel)
            make.right.equalTo(collectBtn.snp_right).offset( -margin * 2)
        }
        
        let personsImgView = UIImageView()
        personsImgView.image = UIImage(named: "add_sc_persons")
        self.contentView.addSubview(personsImgView)
        personsImgView.snp_makeConstraints { (make) in
            make.right.equalTo(collectNumLabel.snp_left)
            make.centerY.equalTo(collectBtn)
            make.size.equalTo(10)
        }

        
        // departmentLabel
        departmentLabel = UILabel()
        departmentLabel.font = UIFont.systemFontOfSize(14)
        departmentLabel.sizeToFit()
        departmentLabel.textColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1.0)
        self.contentView.addSubview(departmentLabel)
        departmentLabel.snp_makeConstraints { (make) in
            make.left.equalTo(nameLabel)
            make.bottom.equalTo(iconImgView)
            make.right.lessThanOrEqualTo(self).offset(-margin*2)
        }
        
        // teleLabel
        teleLabel = UILabel()
        teleLabel.font = UIFont.systemFontOfSize(14)
        teleLabel.sizeToFit()
        teleLabel.textColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1.0)
        self.contentView.addSubview(teleLabel)
        teleLabel.snp_makeConstraints { (make) in
            make.left.equalTo(iconImgView)
            make.top.equalTo(iconImgView.snp_bottom).offset(margin*0.4)
            make.right.equalTo(self).offset(-margin)
        }
        
        // companyLabel
        companyLabel = UILabel()
        companyLabel.font = UIFont.systemFontOfSize(14)
        companyLabel.sizeToFit()
        companyLabel.textColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1.0)
        self.contentView.addSubview(companyLabel)
        companyLabel.snp_makeConstraints { (make) in
            make.left.equalTo(iconImgView)
            make.top.equalTo(teleLabel.snp_bottom).offset(margin*0.4)
            make.right.equalTo(self).offset(-margin)
        }
        
        self.selectedBackgroundView = CustomBackgroundView(frame: CGRectZero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}


















































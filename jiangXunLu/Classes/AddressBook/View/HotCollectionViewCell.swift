//
//  HotCollectionViewCell.swift
//  jiangXunLu
//
//  Created by liuxb on 16/6/17.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit

let hotWordFont = UIFont.systemFontOfSize(15)

class HotCollectionViewCell: UICollectionViewCell {
    var button: HotIndexButton!
    var section: Int = 0
    var row: Int = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup(){
        self.button = HotIndexButton(type: .Custom)
        self.button?.frame = CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height)
        self.contentView.addSubview(self.button)
        self.button.titleLabel?.font = hotWordFont
        self.button.layer.cornerRadius = 8
        self.button.layer.masksToBounds = true
        self.button.setTitleColor(UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1.0), forState: .Normal)
        self.button.setTitleColor(UIColor.whiteColor(), forState: .Highlighted)
        let imageNormal = self.imageWithColor(UIColor.whiteColor())
        self.button.setBackgroundImage(imageNormal, forState: .Normal)
        let imageHighlighted = self.imageWithColor(UIColor(red: 0.376, green: 0.376, blue: 0.376, alpha: 1.0))
        self.button.setBackgroundImage(imageHighlighted, forState: .Highlighted)
        
    }

    private func imageWithColor(color: UIColor) -> UIImage{
        let rect = CGRect(x: 0, y: 0, width: 1.0, height: 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        
        CGContextSetFillColorWithColor(context!, color.CGColor)
        CGContextFillRect(context!, rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
}










































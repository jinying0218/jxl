//
//  BannerHeaderModel.swift
//  jiangXunLu
//
//  Created by liuxb on 16/7/23.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit

class BannerHeaderModel: NSObject {
    var id: NSNumber?
    var src: String?
    
    init(dict:[String:AnyObject]){
        super.init()
        setValuesForKeysWithDictionary(dict)
    }
    
    override func setValue(value: AnyObject?, forKey key: String) {
        if key == "src" {
            self.src = Globe.baseUrlStr + (value! as! String)
        } else {
            super.setValue(value, forKey: key)
        }
    }
    
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {
        print("提示：BannerHeaderModel中\(key)无对应数据")
    }
    
    override var description: String{
        let properties = ["id","src"]
        let dict = dictionaryWithValuesForKeys(properties)
        return "\(dict)"
    }
}

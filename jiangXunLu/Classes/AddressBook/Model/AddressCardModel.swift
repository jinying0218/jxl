//
//  AddressCardModel.swift
//  jiangXunLu
//
//  Created by liuxb on 16/6/21.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit

class AddressCardModel: NSObject {
    var coll_status: NSNumber?
    var collected_num: NSNumber?
    var company:String?
    var head: String?
    var id: NSNumber?
    var job: String?
    var name: String?
    var party: NSNumber?
    var phone: NSNumber?
    var show_flag: NSNumber?
    var updated_time:String?
    var region: String?
    
    init(dict:[String:AnyObject]){
        super.init()
        setValuesForKeysWithDictionary(dict)
    }
    
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {
        print("提示：AddressCardModel中\(key)无对应数据")
    }
    
    override var description: String{
        let properties = ["coll_status","collected_num","company","head","id","job","name","party","phone","show_flag","updated_time","region"]
        let dict = dictionaryWithValuesForKeys(properties)
        return "\(dict)"
    }
    
}

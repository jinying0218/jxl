//
//  GlobalDefine.h
//  jiangXunLu
//
//  Created by lixu on 16/6/17.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "jiangXunLu-swift.h"

// debug编译，打Log的宏定义开关
#define JXDEBUG

// debug编译，打包时去掉JXDEBUG，去掉工程中的log输出
#ifdef JXDEBUG
#define JXLog(s, ...) NSLog( @"%s - %@ - %@",__func__, NSStringFromSelector(_cmd),[NSString stringWithFormat:(s), ##__VA_ARGS__] )
#else
#define JXLog(s, ...)
#endif

// 屏幕高度
#define DEVICE_HEIGHT [UIScreen mainScreen].bounds.size.height
// 屏幕宽度
#define DEVICE_WIDTH [UIScreen mainScreen].bounds.size.width

// 获取RGB格式的24位颜色
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
// 获取RGBA格式的32位颜色
#define UIColorFromRGBA(rgbaValue) [UIColor colorWithRed:((float)((rgbaValue & 0xFF000000) >> 24))/255.0 green:((float)((rgbaValue & 0xFF0000) >> 16))/255.0 blue:((float)((rgbaValue & 0xFF00) >> 8))/255.0 alpha:((float)(rgbaValue & 0xFF))/255.0]

// CGRectMake 简写
#define RECT(x, y, w, h) CGRectMake(x, y, w, h)

// 简写：获取ImageAssets中的图片
#define IMGName(name) [CommonTools imageNamed:name]
// 简写：获取ImageAssets中的图片
#define UIIMGName(name) [UIImage imageNamed:name]
// 简写：指定大小字体字体
#define FONTS(size) [UIFont systemFontOfSize:size]
// 简写：拼接图片url字符串，并获取图片url
#define IMGURL(name) [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",Globe.baseUrlStr,name]];

// 简写：计算文字的高度和宽度
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= 70000
#define BD_MULTILINE_TEXTSIZE(text, font, maxSize, mode) [text length] > 0 ? [text \
boundingRectWithSize:maxSize options:(NSStringDrawingUsesLineFragmentOrigin) \
attributes:@{NSFontAttributeName:font} context:nil].size : CGSizeZero
#else
#define BD_MULTILINE_TEXTSIZE(text, font, maxSize, mode) [text length] > 0 ? [text \
sizeWithFont:font constrainedToSize:maxSize lineBreakMode:mode] : CGSizeZero
#endif

#define BD_SINGLELINE_TEXTSIZE(text, font, size) [text length] > 0? [text sizeWithFont:font maxSize:size] : CGSizeZero

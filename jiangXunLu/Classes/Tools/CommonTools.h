//
//  CommonTools.h
//  jiangXunLu
//
//  Created by lixu on 16/6/17.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CommonTools : NSObject

/*
 * imageNamed:
 * @description: 读取ImageAsset的图片
 * @params: (NSString *)name, 图片名字，没有后缀
 * @return: (UIImage *) 图片obj
 */
+ (UIImage *)imageNamed:(NSString *)name;

/* 
 * imageNamed:inBundle:
 * @description: 读取bundle内的的图片
 * @params: (NSString *)name, 图片名字，没有后缀
 * @params: (NSBundle *)bundle
 * @return: (UIImage *) 图片obj
 */
+ (UIImage *)imageNamed:(NSString *)name inBundle:(NSBundle *)bundle;

/*
 * isValidProperty:
 * @description: 判断参数是否合法
 * @params: (id)value 判断标的;
 * @return: (BOOL) 是：非NSNULL；否，反之
 */
+ (BOOL)isValidProperty:(id)value;

/*
 * isValidProperty:
 * @description: 获取字符串类型的合法参数
 * @params: (id)value 判断标的;
 * @params: (id)placeHolder 否时的默认值;
 * @return: (NSString *) 非NSNULL，非nil，返回value；否，返回placeHolder
 */
+ (NSString *)getValidText:(id)value withPlaceHolder:(id)placeHolder;

@end

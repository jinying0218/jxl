//
//  YYDateFormatterTool.m
//  jiangXunLu
//
//  Created by 金莹 on 16/8/31.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "YYDateFormatterTool.h"

static YYDateFormatterTool *shareInstance;

@implementation YYDateFormatterTool
+ (instancetype)shareInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shareInstance = [[self alloc] init];
//        NSTimeZone *zone = [NSTimeZone timeZoneForSecondsFromGMT:8*60*60];
//        shareInstance.timeZone = zone;
        [shareInstance setDateFormat:@"yyyy-MM-dd"];
    });
    return shareInstance;
}

+ (instancetype)allocWithZone:(struct _NSZone *)zone{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shareInstance = [super allocWithZone:zone];
    });
    return shareInstance;
}

- (id)copyWithZone:(NSZone *)zone{
    return shareInstance;
}
@end

//
//  YYDateFormatterTool.h
//  jiangXunLu
//
//  Created by 金莹 on 16/8/31.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YYDateFormatterTool : NSDateFormatter

+ (instancetype)shareInstance;
@end

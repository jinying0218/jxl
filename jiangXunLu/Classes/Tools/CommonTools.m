//
//  CommonTools.m
//  jiangXunLu
//
//  Created by lixu on 16/6/17.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "CommonTools.h"
#import "GlobalDefine.h"
#import "NSString+SizeCalculator.h"

@implementation CommonTools

+ (UIImage *)imageNamed:(NSString *)name {
    return [UIImage imageNamed:name];
}

+ (UIImage *)imageNamed:(NSString *)name inBundle:(NSBundle *)bundle{
    NSString *imageName = [bundle.resourcePath stringByAppendingPathComponent:name];
    return [UIImage imageNamed:imageName];
}

+ (BOOL)isValidProperty:(id)value {
    return ![value isKindOfClass:[NSNull class]];
}
+ (NSString *)getValidText:(id)value withPlaceHolder:(id)placeHolder {
    id result;
    if(value){
        if([CommonTools isValidProperty:value]){
            result = value;
        }else{
            result = placeHolder;
        }
    }else{
        result = placeHolder;
    }
    return [NSString stringWithFormat:@"%@",result];
}

@end

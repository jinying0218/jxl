//
//  UserIconView.swift
//  jiangXunLu
//
//  Created by liuxb on 16/6/14.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit
import SDWebImage

class UserIconView: UIView {
    
    private var iconView: UIImageView!
    private var textLabelOne: UILabel!
    private var textLabelTwo: UILabel!
    private var textLabelThree: UILabel!

    
    init(frame: CGRect, iconName: String, textOne: String, textTwo: String, textThree: String){
        super.init(frame: frame)
        
        let radius: CGFloat = 120.0
        // LOGO
        iconView = UIImageView(frame: CGRect(x: (frame.size.width - radius)/2, y: (frame.size.height - radius)/3, width: radius, height: radius))
        iconView.layer.borderColor = UIColor.whiteColor().CGColor
        iconView.layer.borderWidth = 0.0
        iconView.layer.cornerRadius = 10//iconView.frame.width/2
        iconView.layer.masksToBounds = true
        
        iconView.image = UIImage(named: iconName)
        self.addSubview(iconView)
        
        let frameOne = CGRect(x: 0, y: CGRectGetMaxY(iconView.frame) + 25, width: self.frame.width, height: 30)
        textLabelOne = self.createLabel(frameOne, text: textOne)
        self.addSubview(textLabelOne)
        
        let frameTwo = CGRect(x: 0, y: CGRectGetMaxY(textLabelOne.frame), width: self.frame.width, height: 30)
        textLabelTwo = self.createLabel(frameTwo, text: textTwo)
        self.addSubview(textLabelTwo)
        
        let frameThree = CGRect(x: 0, y: CGRectGetMaxY(textLabelTwo.frame), width: self.frame.width, height: 30)
        textLabelThree = self.createLabel(frameThree, text: textThree)
        self.addSubview(textLabelThree)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func createLabel(frame: CGRect, text: String) -> UILabel{
        let label = UILabel(frame: frame)
        label.text = text
        label.font = UIFont.systemFontOfSize(14)
        label.textColor = UIColor.whiteColor()
        label.textAlignment = .Center
        label.preferredMaxLayoutWidth = self.frame.width
        return label
    }
}

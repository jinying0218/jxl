//
//  UserTextField.swift
//  jiangXunLu
//
//  Created by liuxb on 16/6/15.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit

class UserTextField: UITextField {
    override func textRectForBounds(bounds: CGRect) -> CGRect {
        var textRect = super.textRectForBounds(bounds)
        textRect.origin.x += 5
        return textRect
    }
    
    override func editingRectForBounds(bounds: CGRect) -> CGRect {
        var editRect = super.editingRectForBounds(bounds)
        editRect.origin.x += 5
        return editRect
    }
}

//
//  RegisterOrForgotPsdCell.swift
//  jiangXunLu
//
//  Created by liuxb on 16/6/15.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit

class RegisterOrForgotPsdCell: UITableViewCell {
    
    var tapRegisterBlock:(()->Void)?
    var tapForgotPswd:(()->Void)?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .None
        self.backgroundColor = UIColor.clearColor()
        
        let viewWrapper = UIView(frame: CGRect(x: kLogin_Margin, y: 0, width: kScreen_Bounds.width - kLogin_Margin*2, height: 28))
        self.contentView.addSubview(viewWrapper)
        
        let frameR = CGRect(x: 0, y: 0, width: 30, height: 28)
        let registerBtn = self.createButton(frameR, title: "注册", tag: 0)
        let horizonLineR = self.createHorizonLine(CGRect(x: 0, y: 21, width: 30, height: 0.8))
        viewWrapper.addSubview(registerBtn)
        viewWrapper.addSubview(horizonLineR)

        
        let frameF = CGRect(x: viewWrapper.frame.width - 70, y: 0, width: 70, height: 28)
        let forgotBtn = self.createButton(frameF, title: "忘记密码?", tag: 1)
        let horizonLineF = self.createHorizonLine(CGRect(x: viewWrapper.frame.width - 70, y: 21, width: 70, height: 0.8))
        viewWrapper.addSubview(forgotBtn)
        viewWrapper.addSubview(horizonLineF)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private func createButton(frame: CGRect,title: String,tag: Int) -> UIButton{
        let button = UIButton(frame: frame)
        button.setTitle(title, forState: .Normal)
        button.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        button.titleLabel?.font = UIFont.systemFontOfSize(14)
        button.tag = tag
        button.addTarget(self, action: #selector(self.click(_:)), forControlEvents: .TouchUpInside)
        return button
    }
    
    private func createHorizonLine(frame: CGRect) -> UIView {
        let horizonLineR = UIView(frame: frame)
        horizonLineR.backgroundColor = UIColor.whiteColor()
        return horizonLineR
    }
    
    func click(sender: UIButton) {
        switch sender.tag {
        case 0:
            //register
            if self.tapRegisterBlock != nil {
                self.tapRegisterBlock!()
            }
            
        default:
            //forgot pswd
            if self.tapForgotPswd != nil {
                self.tapForgotPswd!()
            }
        }
    }
}

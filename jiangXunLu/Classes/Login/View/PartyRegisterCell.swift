//
//  PartyARegisterCell.swift
//  jiangXunLu
//
//  Created by liuxb on 16/6/15.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit

class PartyRegisterCell: UITableViewCell {
    

    private var telephoneField: UITextField!
    private var pswdField: UITextField!
    private var pswdAgainField: UITextField!
    private var nameField: UITextField!
    private var companyField: UITextField!
    private var projectNameField: UITextField!
    private var departmentField: UITextField!
    
    var checkChangedBlock:((Int)->Void)?
    var textValueChangedBlock:((Int,String)->Void)?
    
    private var partyABtn: UIButton!
    private var partyBBtn: UIButton!
    private var partyCBtn: UIButton!
    
    private var currnetPartyBtn: UIButton!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .None
        self.backgroundColor = UIColor.clearColor()
        
        let viewWrapper = UIView(frame: CGRect(x: kLogin_Margin, y: 20, width: kScreen_Bounds.width - kLogin_Margin*2, height: kRegister_Height*8+7))
        viewWrapper.backgroundColor = UIColor.whiteColor()
        viewWrapper.layer.borderWidth = 1
        viewWrapper.layer.borderColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.7).CGColor
        viewWrapper.layer.cornerRadius = 8.0
        viewWrapper.layer.masksToBounds = true
        self.contentView.addSubview(viewWrapper)
        
        let typeView = self.createType(CGRect(x: 5, y: 0, width: viewWrapper.frame.width-10, height: kRegister_Height), leftTitle: "行业类型")
        let hLine1 = self.createHorizonlLine(CGRect(x: 5, y: CGRectGetMaxY(typeView.frame), width: viewWrapper.frame.width-10, height: 1))
        
        telephoneField = self.createTextField(CGRect(x: 5, y: CGRectGetMaxY(hLine1.frame), width: viewWrapper.frame.width-10, height: kRegister_Height), placeholder: "手机号码为登录依据", leftTitle: "手机号码", tag: 0)
        let hLine2 = self.createHorizonlLine(CGRect(x: 5, y: CGRectGetMaxY(telephoneField.frame), width: viewWrapper.frame.width-10, height: 1))
        
        pswdField = self.createTextField(CGRect(x: 5, y: CGRectGetMaxY(hLine2.frame), width: viewWrapper.frame.width-10, height: kRegister_Height), placeholder: "请输入密码", leftTitle: "密       码", tag: 1)
        let hLine3 = self.createHorizonlLine(CGRect(x: 5, y: CGRectGetMaxY(pswdField.frame), width: viewWrapper.frame.width-10, height: 1))
        
        pswdAgainField = self.createTextField(CGRect(x: 5, y: CGRectGetMaxY(hLine3.frame), width: viewWrapper.frame.width-10, height: kRegister_Height), placeholder: "请再次输入密码", leftTitle: "确认密码", tag: 2)
        let hLine4 = self.createHorizonlLine(CGRect(x: 5, y: CGRectGetMaxY(pswdAgainField.frame), width: viewWrapper.frame.width-10, height: 1))
        
        nameField = self.createTextField(CGRect(x: 5, y: CGRectGetMaxY(hLine4.frame), width: viewWrapper.frame.width-10, height: kRegister_Height), placeholder: "请输入您的姓名", leftTitle: "姓       名", tag: 3)
        let hLine5 = self.createHorizonlLine(CGRect(x: 5, y: CGRectGetMaxY(nameField.frame), width: viewWrapper.frame.width-10, height: 1))
        
        companyField = self.createTextField(CGRect(x: 5, y: CGRectGetMaxY(hLine5.frame), width: viewWrapper.frame.width-10, height: kRegister_Height), placeholder: "请输入您所在的企业名称", leftTitle: "公       司", tag: 4)
        let hLine6 = self.createHorizonlLine(CGRect(x: 5, y: CGRectGetMaxY(companyField.frame), width: viewWrapper.frame.width-10, height: 1))
        
        projectNameField = self.createTextField(CGRect(x: 5, y: CGRectGetMaxY(hLine6.frame), width: viewWrapper.frame.width-10, height: kRegister_Height), placeholder: "请输入地产项目名称", leftTitle: "项目名称", tag: 5)
        let hLine7 = self.createHorizonlLine(CGRect(x: 5, y: CGRectGetMaxY(projectNameField.frame), width: viewWrapper.frame.width-10, height: 1))
        
        departmentField = self.createTextField(CGRect(x: 5, y: CGRectGetMaxY(hLine7.frame), width: viewWrapper.frame.width-10, height: kRegister_Height), placeholder: "请输入您的部门和职位", leftTitle: "部门职位", tag: 6)
        viewWrapper.addSubview(typeView)
        viewWrapper.addSubview(hLine1)
        viewWrapper.addSubview(telephoneField)
        viewWrapper.addSubview(hLine2)
        viewWrapper.addSubview(pswdField)
        viewWrapper.addSubview(hLine3)
        viewWrapper.addSubview(pswdAgainField)
        viewWrapper.addSubview(hLine4)
        viewWrapper.addSubview(nameField)
        viewWrapper.addSubview(hLine5)
        viewWrapper.addSubview(companyField)
        viewWrapper.addSubview(hLine6)
        viewWrapper.addSubview(projectNameField)
        viewWrapper.addSubview(hLine7)
        viewWrapper.addSubview(departmentField)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private func createTextField(frame:CGRect, placeholder:String, leftTitle: String, tag: Int) -> UITextField{
        let textField = UserTextField(frame: frame)
        textField.tag = tag
        textField.placeholder = placeholder
        
        textField.borderStyle = .None
        textField.font = UIFont.systemFontOfSize(16)
        textField.keyboardType = tag == 0 ? .NumberPad: .EmailAddress
        
        let leftView = UIView(frame: CGRect(x: 0, y: 0, width: 80, height: frame.height))
        let textLabel = UILabel(frame: CGRect(x: 0, y: (frame.height-20)/2, width: 64, height: 20))
        textLabel.text = leftTitle
        textLabel.textColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1)
        textLabel.font = UIFont.systemFontOfSize(16)
        textLabel.sizeToFit()
        leftView.addSubview(textLabel)
        let verticalLine = UIView(frame: CGRect(x: 72, y: (frame.height-20)/2, width: 1, height: textLabel.frame.height))
        verticalLine.backgroundColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.7)
        leftView.addSubview(verticalLine)
        textField.leftView = leftView
        textField.leftViewMode = .Always
        
        textField.clearButtonMode = .WhileEditing
        textField.addTarget(self, action: #selector(self.textValueChanged(_:)), forControlEvents: .EditingChanged)
        return textField
    }
    
    private func createType(frame:CGRect,leftTitle: String) -> UIView{
        let view = UIView(frame: frame)
        
        //左侧行业类型 ＋ 分割线
        let leftView = UIView(frame: CGRect(x: 0, y: 0, width: 80, height: frame.height))
        let textLabel = UILabel(frame: CGRect(x: 0, y: (frame.height-20)/2, width: 64, height: 20))
        textLabel.text = leftTitle
        textLabel.textColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1)
        textLabel.font = UIFont.systemFontOfSize(16)
        textLabel.sizeToFit()
        leftView.addSubview(textLabel)
        let verticalLine = UIView(frame: CGRect(x: 72, y: (frame.height-20)/2, width: 1, height: textLabel.frame.height))
        verticalLine.backgroundColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.7)
        leftView.addSubview(verticalLine)
        view.addSubview(leftView)
        
        let uncheckImg = UIImage(named: "register_uncheck_normal")!.scaleToSize(CGSize(width: 18, height: 18))
        let checkImg = UIImage(named: "register_check_normal")!.scaleToSize(CGSize(width: 18, height: 18))
        
        let rightView = UIView(frame: CGRect(x: 80, y: 0, width: frame.width - 80, height: frame.height))
  
        partyABtn = self.createPartyButton(CGRect(x: 0, y: 0, width: rightView.frame.width/3, height: frame.height), title: "  甲方", normal: uncheckImg, highlighted: checkImg, tag: 0)
        partyABtn.selected = true
        currnetPartyBtn = partyABtn
        rightView.addSubview(partyABtn)

        partyBBtn = self.createPartyButton(CGRect(x: rightView.frame.width/3, y: 0, width: rightView.frame.width/3, height: frame.height), title: "  乙方", normal: uncheckImg, highlighted: checkImg, tag: 1)
        rightView.addSubview(partyBBtn)

        partyCBtn = self.createPartyButton(CGRect(x: rightView.frame.width*2/3, y: 0, width: rightView.frame.width/3, height: frame.height), title: "  其他", normal: uncheckImg, highlighted: checkImg, tag: 2)
        rightView.addSubview(partyCBtn)
    
        view.addSubview(rightView)
        return view
    }
    
    
    private func createPartyButton(frame: CGRect, title: String, normal normalImg: UIImage, highlighted highImg: UIImage, tag: Int)-> UIButton{
        let btn = UIButton(frame: frame)
        btn.tag = tag
        btn.setImage(normalImg, forState: .Normal)
        btn.setImage(highImg, forState: .Selected)
        btn.setTitle(title, forState: .Normal)
        btn.setTitleColor(UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1), forState: .Normal)
        btn.titleLabel?.font = UIFont.systemFontOfSize(14)
        btn.addTarget(self, action: #selector(self.selectType(_:)), forControlEvents: .TouchUpInside)
        return btn
    }
    
    
    private func createHorizonlLine(frame: CGRect) -> UIView {
        let hLine = UIView(frame: frame)
        hLine.backgroundColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.7)
        return hLine
    }

    func textValueChanged(textFeild: UITextField?) {
        
        if textFeild == nil {
            return
        }
        
        if self.textValueChangedBlock != nil {
            self.textValueChangedBlock!(textFeild!.tag,textFeild!.text!)
        }
    }
    
    func selectType(sender: UIButton) {
        if currnetPartyBtn != sender {
            currnetPartyBtn.selected = false
            sender.selected = true
            currnetPartyBtn = sender
            
            if self.checkChangedBlock != nil {
                self.checkChangedBlock!(sender.tag)
            }
        }
    }
}

//
//  UserPsdInputCell.swift
//  jiangXunLu
//
//  Created by liuxb on 16/6/14.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit

class UserPsdInputCell: UITableViewCell {
    
    private var userNameTextField: UITextField!
    private var userPswdTextField: UITextField!
    
    var userNameTextValueChangedBlock:((String)->Void)?
    var userPswdTextValueChangedBlock:((String)->Void)?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .None
        self.backgroundColor = UIColor.clearColor()
        
        let viewWrapper = UIView(frame: CGRect(x: kLogin_Margin, y: 0, width: kScreen_Bounds.width - kLogin_Margin*2, height: 90))
        viewWrapper.backgroundColor = UIColor.whiteColor()
        viewWrapper.layer.cornerRadius = 8.0
        viewWrapper.layer.masksToBounds = true
        self.contentView.addSubview(viewWrapper)
        
        if userNameTextField == nil {
            let frame = CGRect(x: 20, y: 0, width: viewWrapper.frame.width - 40, height: kLogin_Height)
            userNameTextField = self.createTextField(frame, placeholder: "手机号码", leftImgName: "login_user", rightImgName: "login_clear", tag: 0)
            viewWrapper.addSubview(userNameTextField)
        }
        
        let horizonLine = UIView(frame: CGRect(x: 20, y: viewWrapper.frame.height/2, width: viewWrapper.frame.width - 40, height: 1))
        horizonLine.backgroundColor = UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1.00)
        viewWrapper.addSubview(horizonLine)
        
        if userPswdTextField == nil {
            let frame = CGRect(x: 20, y: 47, width: viewWrapper.frame.width - 40, height: kLogin_Height)
            userPswdTextField = self.createTextField(frame, placeholder: "输入密码", leftImgName: "login_pswd", rightImgName: "login_hide", tag: 1)
            userPswdTextField.secureTextEntry = true
            viewWrapper.addSubview(userPswdTextField)
        }
        
        //self.setUserLoginInfo()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func createTextField(frame:CGRect, placeholder: String, leftImgName: String, rightImgName: String, tag: Int) -> UITextField{
        let textField = UserTextField(frame: frame)
        textField.tag = tag
        textField.keyboardType = tag == 0 ? .NumberPad : .EmailAddress
        
        textField.placeholder = placeholder
        textField.borderStyle = .None
        textField.font = UIFont.systemFontOfSize(16)
        
        let leftImg = UIImage(named: leftImgName)
        let leftImgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        leftImgView.image = leftImg
        textField.leftView = leftImgView
        textField.leftViewMode = .Always
        
        let rightImg = UIImage(named: rightImgName)
        let rightBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        rightBtn.setImage(rightImg, forState: .Normal)
        rightBtn.tag = tag
        rightBtn.addTarget(self, action: #selector(self.rightBtnClick(_:)), forControlEvents: .TouchUpInside)
        textField.rightView = rightBtn
        textField.rightViewMode = .WhileEditing
        
        textField.addTarget(self, action: #selector(self.textValueChanged(_:)), forControlEvents: .EditingChanged)
        return textField
    }
    
    func rightBtnClick(sender: UIButton) {
        switch sender.tag {
        case 0:
            self.userNameTextField.text = ""
            self.textValueChanged(nil)
        default:
            self.userPswdTextField.secureTextEntry = !self.userPswdTextField.secureTextEntry
            let text = self.userPswdTextField.text
            self.userPswdTextField.text = ""
            self.userPswdTextField.text = text
            
            let image = UIImage(named: self.userPswdTextField.secureTextEntry ? "login_hide":"login_look")
            sender.setImage(image, forState: .Normal)
        }
    }
    
    
    func textValueChanged(textFeild: UITextField?) {
        if textFeild == nil {
            return
        }
        switch textFeild!.tag {
        case 0:
            if self.userNameTextValueChangedBlock != nil {
                self.userNameTextValueChangedBlock!(textFeild!.text!)
            }
        default:
            if self.userPswdTextValueChangedBlock != nil {
                self.userPswdTextValueChangedBlock!(textFeild!.text!)
            }
        }
    }
    
    func setUserLoginInfo() {
        let username = NSUserDefaults.standardUserDefaults().objectForKey(kLogin_userName) as? String
        let pswd = NSUserDefaults.standardUserDefaults().objectForKey(kLogin_userpwsd) as? String
        if username != nil {
            self.userNameTextField.text = username
        }
        if pswd != nil {
            self.userPswdTextField.text = pswd
        }
    }
}




































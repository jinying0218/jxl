//
//  PartyARegisterCell.swift
//  jiangXunLu
//
//  Created by liuxb on 16/6/15.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit

class ForgotPswdCell: UITableViewCell {
    

    private var telephoneField: UITextField!
    private var validateField: UITextField!
    private var newpswdField: UITextField!
    
    var textValueChangedBlock:((Int,String)->Void)?
    var tapValidateBtnBlock:(()->Bool)?
    
    var sendMsgBtn: UIButton!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .None
        self.backgroundColor = UIColor.clearColor()
        
        let viewWrapper = UIView(frame: CGRect(x: kLogin_Margin, y: 20, width: kScreen_Bounds.width - kLogin_Margin*2, height: kRegister_Height*3+2))
        viewWrapper.backgroundColor = UIColor.whiteColor()
        viewWrapper.layer.borderWidth = 1
        viewWrapper.layer.borderColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.7).CGColor
        viewWrapper.layer.cornerRadius = 8.0
        viewWrapper.layer.masksToBounds = true
        self.contentView.addSubview(viewWrapper)
        
        
        telephoneField = self.createTextField(CGRect(x: 5, y: 0, width: viewWrapper.frame.width-10, height: kRegister_Height), placeholder: "手机号码为登录依据", leftTitle: "手机号码", tag: 0)
        let hLine1 = self.createHorizonlLine(CGRect(x: 5, y: CGRectGetMaxY(telephoneField.frame), width: viewWrapper.frame.width-10, height: 1))
        
        validateField = self.createTextField(CGRect(x: 5, y: CGRectGetMaxY(hLine1.frame), width: viewWrapper.frame.width-10, height: kRegister_Height), placeholder: "请输入验证码", leftTitle: "验  证  码", tag: 1)
        let hLine2 = self.createHorizonlLine(CGRect(x: 5, y: CGRectGetMaxY(validateField.frame), width: viewWrapper.frame.width-10, height: 1))
        
        newpswdField = self.createTextField(CGRect(x: 5, y: CGRectGetMaxY(hLine2.frame), width: viewWrapper.frame.width-10, height: kRegister_Height), placeholder: "请输入新的密码", leftTitle: "新的密码", tag: 2)
        
        viewWrapper.addSubview(telephoneField)
        viewWrapper.addSubview(hLine1)
        viewWrapper.addSubview(validateField)
        viewWrapper.addSubview(hLine2)
        viewWrapper.addSubview(newpswdField)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private func createTextField(frame:CGRect, placeholder:String, leftTitle: String, tag: Int) -> UITextField{
        let textField = UserTextField(frame: frame)
        textField.tag = tag
        textField.placeholder = placeholder
        
        textField.borderStyle = .None
        textField.font = UIFont.systemFontOfSize(16)
        textField.keyboardType = tag != 2 ? .NumberPad: .EmailAddress
        
        let leftView = UIView(frame: CGRect(x: 0, y: 0, width: 80, height: frame.height))
        let textLabel = UILabel(frame: CGRect(x: 0, y: (frame.height-20)/2, width: 64, height: 20))
        textLabel.text = leftTitle
        textLabel.textColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1)
        textLabel.font = UIFont.systemFontOfSize(16)
        textLabel.sizeToFit()
        leftView.addSubview(textLabel)
        let verticalLine = UIView(frame: CGRect(x: 72, y: (frame.height-20)/2, width: 1, height: textLabel.frame.height))
        verticalLine.backgroundColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.7)
        leftView.addSubview(verticalLine)
        textField.leftView = leftView
        textField.leftViewMode = .Always
        
        if tag == 1 {
            self.sendMsgBtn = createValidateButton(CGRect(x: 0, y: 0, width: 85, height: frame.height - 16))
            textField.rightView = self.sendMsgBtn
            textField.rightViewMode = .Always
        } else {
            textField.clearButtonMode = .WhileEditing
        }
        
        textField.addTarget(self, action: #selector(self.textValueChanged(_:)), forControlEvents: .EditingChanged)
        return textField
    }

    private func createHorizonlLine(frame: CGRect) -> UIView {
        let hLine = UIView(frame: frame)
        hLine.backgroundColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.7)
        return hLine
    }

    
    private func createValidateButton(frame: CGRect) -> UIButton{
        let button = UIButton(frame: frame)
        button.setTitle("发送短信验证码", forState: .Normal)
        button.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        button.titleLabel?.font = UIFont.systemFontOfSize(11)
        button.backgroundColor = UIColor(red: 0.918, green: 0.796, blue: 0.129, alpha: 1)
        button.layer.borderColor = UIColor(red: 0.918, green: 0.796, blue: 0.129, alpha: 1).CGColor
        button.layer.cornerRadius = 4.0
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(self.tapValidateButton), forControlEvents: .TouchUpInside)
        return button
    }
    
    func countOver(timeOver: Int) {
        var timeOut = timeOver
        let queue: dispatch_queue_t = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
        let timer: dispatch_source_t = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue)
        dispatch_source_set_timer(timer, dispatch_walltime(nil, 0), 1*NSEC_PER_SEC, 0)
        dispatch_source_set_event_handler(timer) { 
            if timeOut <= 0 {
                dispatch_source_cancel(timer)
                dispatch_sync(dispatch_get_main_queue(), {
                    self.sendMsgBtn.backgroundColor = UIColor(red: 0.918, green: 0.796, blue: 0.129, alpha: 1)
                    self.sendMsgBtn.layer.borderColor = UIColor(red: 0.918, green: 0.796, blue: 0.129, alpha: 1).CGColor
                    self.sendMsgBtn.setTitle("发送短信验证码", forState: .Normal)
                    self.sendMsgBtn.userInteractionEnabled = true
                })
            } else {
                let seconds = timeOut % 60
                let strTime = String.localizedStringWithFormat("%.2d", seconds)
                dispatch_sync(dispatch_get_main_queue(), {
                    UIView.animateWithDuration(1.0, animations: {
                        self.sendMsgBtn.setTitle(String.localizedStringWithFormat("%@秒后再获取", strTime), forState: .Normal)
                        self.sendMsgBtn.userInteractionEnabled = false
                    })
                    self.sendMsgBtn.backgroundColor = UIColor.grayColor()
                    self.sendMsgBtn.layer.borderColor = UIColor.grayColor().CGColor
                })
                timeOut -= 1
            }
        }
        dispatch_resume(timer)
    }
    
    
    
    func tapValidateButton() {
        if self.tapValidateBtnBlock != nil {
            if self.tapValidateBtnBlock!() {
                self.countOver(20)
            }
        }
    }
    
    func textValueChanged(textFeild: UITextField?) {
        
        if textFeild == nil {
            return
        }
        
        if self.textValueChangedBlock != nil {
            self.textValueChangedBlock!(textFeild!.tag,textFeild!.text!)
        }
    }
}






























//
//  LoginViewController.swift
//  jiangXunLu
//
//  Created by liuxb on 16/6/12.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit
import TPKeyboardAvoiding
import NYXImagesKit
import SVProgressHUD
import MJExtension


let kCellIdentifier_RegisterOrForgotPsdCell = "RegisterOrForgotPsdCell"
let kCellIdentifier_UserPsdInputCell = "UserPsdInputCell"

let kScreen_Bounds = UIScreen.mainScreen().bounds
let kLogin_Margin:CGFloat = 45
let kLogin_Height: CGFloat = 50
let kRegister_Height: CGFloat = 40

let kLogin_userName = "userName"
let kLogin_userpwsd = "password"
let kLogin_userid = "userid"
let kLogin_randcode = "randcode"
let kLogin_status = "login_tatus"

var g_uid = NSUserDefaults.standardUserDefaults().objectForKey(kLogin_userid) as? String ?? ""
var g_randcode = NSUserDefaults.standardUserDefaults().objectForKey(kLogin_randcode) as? String ?? ""

class LoginViewController: BaseViewController {
    
    private var myTableView: TPKeyboardAvoidingTableView!
    private var userName: String = NSUserDefaults.standardUserDefaults().objectForKey(kLogin_userName) as? String ?? ""
    private var userPswd: String = NSUserDefaults.standardUserDefaults().objectForKey(kLogin_userpwsd) as? String ?? ""
    private var loginBtn: UIButton!
    private var loginActivityView: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configViews()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.refreshUserInfoForInput()
    }
    
    private func refreshUserInfoForInput(){
        let username = NSUserDefaults.standardUserDefaults().objectForKey(kLogin_userName) as? String
        let pswd = NSUserDefaults.standardUserDefaults().objectForKey(kLogin_userpwsd) as? String
        if username != nil {
            self.userName = username!
        }
        if pswd != nil {
            self.userPswd = pswd!
        }
        self.myTableView.reloadData()
    }
    
    
    private func configViews(){
        if myTableView == nil {
            myTableView = TPKeyboardAvoidingTableView(frame: self.view.bounds, style: .Plain)
            
            myTableView.registerClass(RegisterOrForgotPsdCell.self, forCellReuseIdentifier: kCellIdentifier_RegisterOrForgotPsdCell)
            myTableView.registerClass(UserPsdInputCell.self, forCellReuseIdentifier: kCellIdentifier_UserPsdInputCell)
            
            myTableView.backgroundView = self.bgView
            myTableView.dataSource = self
            myTableView.delegate = self
            myTableView.separatorStyle = .None
            
            myTableView.tableHeaderView = self.headView()
            myTableView.tableFooterView = self.footerView()
            
            self.view.addSubview(myTableView)
            self.view.addSubview(bottomView)
        }
        
    }
    
    private func headView() -> UIView{
        let frame = CGRect(x: 0, y: 0, width: kScreen_Bounds.width, height: kScreen_Bounds.height/2)
        let iconName = "logo_default"
        let textOne = "地产与商家集结完毕，唯独差了一个你"
        let textTwo = "作为地产和商家的媒人"
        let textThree = "我们能为你做的远远比你想象的多"
        let view = UserIconView(frame: frame, iconName: iconName, textOne: textOne, textTwo: textTwo,textThree: textThree)
        return view
    }
    
    
    private func footerView() -> UIView{
        let footerV = UIView(frame: CGRect(x: 0, y: 0, width: kScreen_Bounds.width, height: 50))
        if loginBtn == nil {
            loginBtn = UIButton(frame: CGRect(x: kLogin_Margin, y: 50, width: footerV.frame.width - 90, height: 40))
            loginBtn.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            loginBtn.titleLabel?.font = UIFont(name: "FontAwesome", size: loginBtn.titleLabel!.font.pointSize)
            loginBtn.layer.borderWidth = 1
            loginBtn.layer.cornerRadius = 8
            loginBtn.layer.masksToBounds = true
            // #eacb21
            loginBtn.backgroundColor = UIColor(red: 0.918, green: 0.796, blue: 0.129, alpha: 1)
            loginBtn.layer.borderColor = UIColor(red: 0.918, green: 0.796, blue: 0.129, alpha: 1).CGColor
            loginBtn.setTitle("登 录", forState: .Normal)
            loginBtn.addTarget(self, action: #selector(self.login), forControlEvents: .TouchUpInside)
            loginBtn.center = footerV.center
        }
        if loginActivityView == nil {
            loginActivityView = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
            loginActivityView.frame = loginBtn.frame
            loginActivityView.hidden = true
        }
        
        footerV.addSubview(loginBtn)
        footerV.addSubview(loginActivityView)
        
        return footerV
    }
    
    private lazy var bgView: UIImageView = {
        let bgView = UIImageView(frame: kScreen_Bounds)
        bgView.contentMode = .ScaleAspectFill
        var bgImage = UIImage(named: "login_bg")
        
        let bgImageSize = bgImage?.size
        let bgViewSize = bgView.frame.size
        
        if bgImageSize?.width > bgViewSize.width && bgImageSize?.height > bgViewSize.height {
            bgImage = bgImage?.scaleToSize(bgViewSize, usingMode: NYXResizeModeAspectFill)
        }
        
        bgView.image = bgImage
        return bgView
    }()
    
    private lazy var bottomView: UIView = {
        let view = UIView(frame: CGRect(x: 0, y: kScreen_Bounds.height-49, width: kScreen_Bounds.width, height: 49.0))

        let horizonLine = UIView(frame: CGRect(x: 40, y: 0, width: kScreen_Bounds.width-80, height: 0.7))
        horizonLine.backgroundColor = UIColor.whiteColor()
        view.addSubview(horizonLine)
        
        let applyBtn = UIButton(frame: CGRect(x: (kScreen_Bounds.width-56)/2, y: (49-29)/2, width: 56, height: 29))
        applyBtn.setTitle("申请加入", forState: .Normal)
        applyBtn.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        applyBtn.titleLabel?.font = UIFont.systemFontOfSize(14)
        applyBtn.sizeToFit()
        applyBtn.addTarget(self, action: #selector(self.applyJoin), forControlEvents: .TouchUpInside)
        view.addSubview(applyBtn)
        return view
    }()
    
    private func changeLoginStatus(actionEnabled enabled: Bool){
        self.loginBtn.enabled = enabled
        if enabled {
            self.loginActivityView.hidden = enabled
            self.loginBtn.setTitle("登 录", forState: .Normal)
        } else {
            self.loginActivityView.startAnimating()
            self.loginActivityView.hidden = enabled
            self.loginBtn.setTitle("正在登录中...", forState: .Normal)
        }
    }
    // 申请加入
    func applyJoin() {
        let registerVC = RegisterViewController()
        self.navigationController?.presentViewController(registerVC, animated: true, completion: nil)
    }
    // 忘记密码
    func forgotPswd() {
        let forgotVC = ForgetPswdViewController()
        self.navigationController?.presentViewController(forgotVC, animated: true, completion: nil)
    }
    
    // 登录
    func login() {
        
        if self.userName == ""{
            SVProgressHUD.showInfoWithStatus("帐号不能为空！")
            return
        } else if self.userPswd == ""{
            SVProgressHUD.showInfoWithStatus("密码不能为空！")
            return
        }
        
        self.changeLoginStatus(actionEnabled: false)
        
        let params = [
            "action":"login",
            "data": [
                "username":self.userName,
                "password":self.userPswd.MD5String
            ]
        ]
        JxlNetAPIManager.sharedManager.request_LoginWithBlock(Globe.baseUrlStr, Params: params) { (data, error) in
            if let data = data {
                let status = data["status"] as! NSNumber
                if status == 1 { //sucess
                    let dict = data["info"] as! [String:AnyObject]
                    let uid = (dict["login_id"] as! String).componentsSeparatedByString("_").first
                    let randcode = (dict["login_id"] as! String).componentsSeparatedByString("_").last
                    //初始化用户数据
                    let user = User(dict: dict)
                    NSUserDefaults.standardUserDefaults().setObject(user.mj_keyValues(), forKey: kLoginUserDict)
                    //记录登录状态
                    NSUserDefaults.standardUserDefaults().setObject(NSNumber(bool: true), forKey: kLoginStatus)
                    
                    //清除原来帐户的消息数据
                    if uid != g_uid {
                        DataBase.shareDataBase.clearMessageData()
                    }
                    
                    g_uid = uid!
                    g_randcode = randcode!
                    
                    //注册信鸽用户
                    Login.setXGAccountWithCurUser()
                    
                    NSUserDefaults.standardUserDefaults().setObject(self.userName, forKey: kLogin_userName)
                    NSUserDefaults.standardUserDefaults().setObject(self.userPswd, forKey: kLogin_userpwsd)
                    NSUserDefaults.standardUserDefaults().setObject(uid, forKey: kLogin_userid)
                    NSUserDefaults.standardUserDefaults().setObject(randcode, forKey: kLogin_randcode)
                    //确认登录入口
                    Login.shareLogin.fromLoginPanel = true
                    //连接消息
                    AppDelegate.shareDelegate.webSocket.connect()
                    
                } else {
                    SVProgressHUD.showInfoWithStatus(data["msg"] as! String)
                    self.changeLoginStatus(actionEnabled: true)
                }
            } else {
                self.changeLoginStatus(actionEnabled: true)
            }
            
        }
    }
}


//MARK: --  UITableViewDataSource, UITableViewDelegate
extension LoginViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCellWithIdentifier(kCellIdentifier_RegisterOrForgotPsdCell, forIndexPath: indexPath) as! RegisterOrForgotPsdCell
            cell.tapRegisterBlock = {
                self.applyJoin()
            }
            cell.tapForgotPswd = {
                self.forgotPswd()
            }
            return cell
        default:
            let cell = tableView.dequeueReusableCellWithIdentifier(kCellIdentifier_UserPsdInputCell, forIndexPath: indexPath) as! UserPsdInputCell
            cell.setUserLoginInfo()
            cell.userNameTextValueChangedBlock = { name in
                self.userName = name
            }
            cell.userPswdTextValueChangedBlock = { pswd in
                self.userPswd = pswd
            }
            return cell
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 30
        default:
            return 100
        }
    }
}



















//
//  RegisterViewController.swift
//  jiangXunLu
//
//  Created by liuxb on 16/6/15.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit
import TPKeyboardAvoiding
import SVProgressHUD


let kCellIdentifier_PartyRegisterCell = "PartyRegisterCell"

class RegisterViewController: BaseViewController {
    
    private var joinTxlBtn: UIButton!
    private var myTableView: TPKeyboardAvoidingTableView!
    private var registerBtn: UIButton!
    private var registerActivityView: UIActivityIndicatorView!
    
    private var party: Int = 1 // 1甲方 2乙方 3其他
    private var phone: String = ""
    private var password: String = ""
    private var passwordAgain: String = ""
    private var name: String = ""
    private var company: String = ""
    private var brand: String = ""
    private var job: String = ""
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configViews()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    private func configViews(){

        if myTableView == nil {
            myTableView = TPKeyboardAvoidingTableView(frame: self.view.bounds, style: .Plain)
            
            myTableView.registerClass(PartyRegisterCell.self, forCellReuseIdentifier: kCellIdentifier_PartyRegisterCell)
     
            myTableView.backgroundColor = UIColor.whiteColor()
            myTableView.dataSource = self
            myTableView.delegate = self
            myTableView.separatorStyle = .None
            
            myTableView.tableHeaderView = self.headView()
            myTableView.tableFooterView = self.footerView()
            
            self.view.addSubview(myTableView)
        }
    }
    
    private func headView() -> UIView{
        let headerView = UIView()
        // 375*200 top image
        let scale = kScreen_Bounds.width/375
        let topBannerView = UIImageView(frame: CGRect(x: 0, y: 0, width: 375*scale, height: 200*scale))
        topBannerView.image = UIImage(named: "register_top")
        headerView.addSubview(topBannerView)
        headerView.frame = CGRect(x: 0, y: 0, width: kScreen_Bounds.width, height: CGRectGetMaxY(topBannerView.frame))
        return headerView
    }
    
    private func footerView() -> UIView{
        let footerV = UIView(frame: CGRect(x: 0, y: 0, width: kScreen_Bounds.width, height: 100))
        
        if registerBtn == nil {
            registerBtn = UIButton(frame: CGRect(x: kLogin_Margin, y: 10, width: footerV.frame.width - kLogin_Margin*2, height: kLogin_Margin))
            registerBtn.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            registerBtn.titleLabel?.font = UIFont(name: "FontAwesome", size: registerBtn.titleLabel!.font.pointSize)
            registerBtn.layer.borderWidth = 1
            registerBtn.layer.cornerRadius = 8
            registerBtn.layer.masksToBounds = true
            // #333333
            registerBtn.backgroundColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
            registerBtn.layer.borderColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1).CGColor
            registerBtn.setTitle("加入通讯录", forState: .Normal)
            registerBtn.addTarget(self, action: #selector(self.joinTongXunLu), forControlEvents: .TouchUpInside)
            footerV.addSubview(registerBtn)
        }
        
        if registerActivityView == nil {
            registerActivityView = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
            registerActivityView.frame = registerBtn.frame
            registerActivityView.hidden = true
            footerV.addSubview(registerActivityView)
        }

        let backLogin = UIButton(frame: CGRect(x: footerV.frame.width - 130 - kLogin_Margin, y: CGRectGetMaxY(registerBtn.frame) + 10, width: 130, height: 18))
        backLogin.setTitle("已有帐号？返回登录", forState: .Normal)
        backLogin.setTitleColor(UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1), forState: .Normal)
        backLogin.titleLabel?.font = UIFont.systemFontOfSize(14)
        backLogin.addTarget(self, action: #selector(self.backToPreViewController), forControlEvents: .TouchUpInside)
        footerV.addSubview(backLogin)
        let horizonLineR = UIView(frame: CGRect(x: backLogin.frame.origin.x, y: CGRectGetMaxY(backLogin.frame), width: backLogin.frame.width, height: 1))
        horizonLineR.backgroundColor = UIColor.grayColor()
        footerV.addSubview(horizonLineR)
        
        return footerV
    }
    
    private func setUpNavigationBar(){
        self.navigationItem.title = "注册"
        self.navigationItem.hidesBackButton = true
        let backBtn = UIBarButtonItem(image: UIImage(named: "back_normal"), style: .Plain, target: self, action: #selector(self.backToPreViewController))
        navigationItem.leftBarButtonItem = backBtn
    }
    
    private func changeLoginStatus(actionEnabled enabled: Bool){
        self.registerBtn.enabled = enabled
        if enabled {
            self.registerActivityView.hidden = enabled
            self.registerBtn.setTitle("加入通讯录", forState: .Normal)
        } else {
            self.registerActivityView.startAnimating()
            self.registerActivityView.hidden = enabled
            self.registerBtn.setTitle("正在注册中...", forState: .Normal)
        }
    }
    
    // 返回
    func backToPreViewController() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    //加入通讯录
    func joinTongXunLu() {
        print("加入通讯录")
        
        if self.phone == "" {
            SVProgressHUD.showInfoWithStatus("手机号码不能为空")
        } else if self.password == "" {
            SVProgressHUD.showInfoWithStatus("密码不能为空")
        } else if self.passwordAgain != self.passwordAgain {
            SVProgressHUD.showInfoWithStatus("两次密码输入不一致")
        } else if self.name == "" {
            SVProgressHUD.showInfoWithStatus("请填写您的姓名")
        } else if self.company == "" {
            SVProgressHUD.showInfoWithStatus("请填写您的公司名称")
        }
        
        self.changeLoginStatus(actionEnabled: false)
        
        let params = [
            "action":"register",
            "data":[
                "phone":self.phone,
                "password":self.password.MD5String,
                "name":self.name,
                "company":self.company,
                "brand":self.brand,
                "job":self.job,
                "party":self.party
            ]
        ]
        
        JxlNetAPIManager.sharedManager.request_registerWithBlock(Globe.baseUrlStr, Params: params) { (data, error) in
            if let data = data {
                let status = data["status"] as! NSNumber
                if status == 1 { //sucess
                    let dict = data["info"] as! [String:AnyObject]
                    let uid = (dict["login_id"] as! String).componentsSeparatedByString("_").first
                    let randcode = (dict["login_id"] as! String).componentsSeparatedByString("_").last
                    
                    //初始化用户数据
                    let user = User(dict: dict)
                    NSUserDefaults.standardUserDefaults().setObject(user.mj_keyValues(), forKey: kLoginUserDict)
                    //记录登录状态
                    NSUserDefaults.standardUserDefaults().setObject(NSNumber(bool: true), forKey: kLoginStatus)

                    //清除原来帐户的消息数据
                    if uid != g_uid {
                        DataBase.shareDataBase.clearMessageData()
                    }
                    
                    g_uid = uid!
                    g_randcode = randcode!
                    
                    //注册信鸽用户
                    Login.setXGAccountWithCurUser()
                    
                    NSUserDefaults.standardUserDefaults().setObject(self.phone, forKey: kLogin_userName)
                    NSUserDefaults.standardUserDefaults().setObject(self.password, forKey: kLogin_userpwsd)
                    NSUserDefaults.standardUserDefaults().setObject(uid, forKey: kLogin_userid)
                    NSUserDefaults.standardUserDefaults().setObject(randcode, forKey: kLogin_randcode)
                    //确认登录入口
                    Login.shareLogin.fromLoginPanel = true
                    //连接消息
                    AppDelegate.shareDelegate.webSocket.connect()

                } else {
                    SVProgressHUD.showInfoWithStatus(data["msg"] as! String)
                    self.changeLoginStatus(actionEnabled: true)
                }
            } else {
                self.changeLoginStatus(actionEnabled: true)
            }
        }
    }
}



extension RegisterViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(kCellIdentifier_PartyRegisterCell, forIndexPath: indexPath) as! PartyRegisterCell
        cell.textValueChangedBlock = { (tag,text) in
            switch tag {
            case 0:
                self.phone = text
            case 1:
                self.password = text
            case 2:
                self.passwordAgain = text
            case 3:
                self.name = text
            case 4:
                self.company = text
            case 5:
                self.brand = text
            default:
                self.job = text
            }
        }

        cell.checkChangedBlock = { tag in
            self.party = tag
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return kRegister_Height*8 + 43
    }
}













































//
//  LoginUser.swift
//  jiangXunLu
//
//  Created by liuxb on 16/7/10.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit
import MJExtension
import SVProgressHUD

let kLoginStatus = "login_status"
let kLoginUserDict = "user_dict"

class User: NSObject {
    var collection_status: NSNumber?
    var connections_list: [UserCollection]=[]
    var uid: String?
    var randcode: String?
    var project_info: UserProjectInfo?
    var user_info: UserInfo?
    
    internal init(dict:[String:AnyObject]){
        super.init()
        setValuesForKeysWithDictionary(dict)
    }
    
    override func setValue(value: AnyObject?, forKey key: String) {
        if key == "login_id" {
            self.uid = (value as! String).componentsSeparatedByString("_").first
            self.randcode = (value as! String).componentsSeparatedByString("_").last
        } else if key == "user_info" {
            self.user_info = UserInfo(dict: value as! [String : AnyObject])
        } else if key == "connections_list" {
            if let con_arr = value as? [[String:AnyObject]] {
                for dict in con_arr {
                    let userCollection = UserCollection(dict: dict)
                    self.connections_list.append(userCollection)
                }
            }
        } else if key == "project_info" {
            self.project_info = UserProjectInfo(dict: value as! [String : AnyObject])
        } else {
            super.setValue(value, forKey: key)
        }
    }
    
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {
        print("提示：User中\(key)无对应数据")
    }
    
    override var description: String{
        let properties = ["collection_status","connections_list","uid","randcode","project_info","user_info"]
        let dict = dictionaryWithValuesForKeys(properties)
        return "\(dict)"
    }
}


class UserInfo: NSObject {
    var brand: String?
    var collected_num: NSNumber?
    var company:String?
    var head: String?
    var id: NSNumber?
    var job: String?
    var name: String?
    var party: NSNumber?
    var phone: NSNumber?
    var region: String?
    var sex: NSNumber?
    var show_flag: NSNumber?
    
    init(dict:[String:AnyObject]){
        super.init()
        setValuesForKeysWithDictionary(dict)
    }
    
    override func setValue(value: AnyObject?, forKey key: String) {
        super.setValue(value, forKey: key)
    }
    
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {
        print("提示：UserInfo中\(key)无对应数据")
    }
    
    override var description: String{
        let properties = ["brand","collected_num","company","head","id","job","name","party","phone","region","sex","show_flag"]
        let dict = dictionaryWithValuesForKeys(properties)
        return "\(dict)"
    }
    
}


class UserCollection: NSObject {
    var head: String?
    var id: NSNumber?
    var name: String?
    
    init(dict:[String:AnyObject]){
        super.init()
        setValuesForKeysWithDictionary(dict)
    }

    override func setValue(value: AnyObject?, forUndefinedKey key: String) {
        print("提示：UserCollection中\(key)无对应数据")
    }
    
    override var description: String{
        let properties = ["head","id","name"]
        let dict = dictionaryWithValuesForKeys(properties)
        return "\(dict)"
    }
}

class UserProjectInfo: NSObject {
    var brief: String?
    var detail: String?
    var id: NSNumber?
    var name: String?
    
    init(dict:[String:AnyObject]){
        super.init()
        setValuesForKeysWithDictionary(dict)
    }
    

    override func setValue(value: AnyObject?, forUndefinedKey key: String) {
        print("提示：UserProjectInfo中\(key)无对应数据")
    }
    
    override var description: String{
        let properties = ["brief","detail","id","name"]
        let dict = dictionaryWithValuesForKeys(properties)
        return "\(dict)"
    }
}


class Login: NSObject{
    
    
    var BannerData:[BannerHeaderModel] = []
    
    var fromLoginPanel: Bool = false
    
    static let shareLogin: Login = {
        let login = Login()
        return login
    }()
    
    class func curLoginUser() -> User?{
        if let userDict = NSUserDefaults.standardUserDefaults().objectForKey(kLoginUserDict) as? [String:AnyObject]{
            return User(dict: userDict)
        }
        return nil
    }
    
    class func isLogin() -> Bool {
        if let loginStatus = NSUserDefaults.standardUserDefaults().objectForKey(kLoginStatus) as? NSNumber {
            if loginStatus.boolValue && Login.curLoginUser() != nil {
                return true
            }
            return false
        } else {
            return false
        }
    }

    class func loginOut(){
        let params = [
            "action":"loginout",
            "uid": g_uid,
            "randcode":g_randcode,
            "data": ""
        ]
        
        JxlNetAPIManager.sharedManager.request_LoginOutWithBlock(Globe.baseUrlStr, Params: params) { (data, error) in
            if let data = data {
                SVProgressHUD.showInfoWithStatus(data["msg"] as! String)
            }
        }
    }
    
    class func setXGAccountWithCurUser() {
        if self.isLogin() {
            let user = Login.curLoginUser()!
            XGPush.setAccount(String(user.user_info!.phone!))
            AppDelegate.shareDelegate.registerNofitication()
        } else {
            XGPush.setAccount(nil)
            XGPush.unRegisterDevice()
        }
    }
}























































//
//  ForgetPswdViewController.swift
//  jiangXunLu
//
//  Created by liuxb on 16/6/25.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit
import TPKeyboardAvoiding
import SVProgressHUD

let kCellIdentifier_ForgotPswdCell = "ForgotPswdCell"

class ForgetPswdViewController: BaseViewController {

    private var modifyPswdBtn: UIButton!
    private var myTableView: TPKeyboardAvoidingTableView!
    
    private var phone: String = ""
    private var validateCode: String = ""
    private var newPassword: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
    }

    private func setupTableView(){
        
        if myTableView == nil {
            myTableView = TPKeyboardAvoidingTableView(frame: self.view.bounds, style: .Plain)
            
            myTableView.registerClass(ForgotPswdCell.self, forCellReuseIdentifier: kCellIdentifier_ForgotPswdCell)
            
            myTableView.backgroundColor = UIColor.whiteColor()
            myTableView.dataSource = self
            myTableView.delegate = self
            myTableView.separatorStyle = .None
            
            myTableView.tableHeaderView = self.headView()
            myTableView.tableFooterView = self.footerView()
            
            self.view.addSubview(myTableView)
        }
    }
    
    private func headView() -> UIView{
        let headerView = UIView()
        // 375*200 top image
        let scale = kScreen_Bounds.width/375
        let topBannerView = UIImageView(frame: CGRect(x: 0, y: 0, width: 375*scale, height: 200*scale))
        topBannerView.image = UIImage(named: "register_top")
        headerView.addSubview(topBannerView)
        headerView.frame = CGRect(x: 0, y: 0, width: kScreen_Bounds.width, height: CGRectGetMaxY(topBannerView.frame))
        return headerView
    }
    
    private func footerView() -> UIView{
        let footerV = UIView(frame: CGRect(x: 0, y: 0, width: kScreen_Bounds.width, height: 100))
        let btn = UIButton(frame: CGRect(x: kLogin_Margin, y: 10, width: footerV.frame.width - kLogin_Margin*2, height: kLogin_Margin))
        btn.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        btn.titleLabel?.font = UIFont(name: "FontAwesome", size: btn.titleLabel!.font.pointSize)
        btn.layer.borderWidth = 1
        btn.layer.cornerRadius = 8
        btn.layer.masksToBounds = true
        // #333333
        btn.backgroundColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
        btn.layer.borderColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1).CGColor
        btn.setTitle("修改密码", forState: .Normal)
        btn.addTarget(self, action: #selector(self.modifyPwsd), forControlEvents: .TouchUpInside)
        footerV.addSubview(btn)
        
        let backLogin = UIButton(frame: CGRect(x: footerV.frame.width - 126 - kLogin_Margin, y: CGRectGetMaxY(btn.frame) + 10, width: 126, height: 18))
        backLogin.setTitle("无需修改？直接登录", forState: .Normal)
        backLogin.setTitleColor(UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1), forState: .Normal)
        backLogin.titleLabel?.font = UIFont.systemFontOfSize(14)
        backLogin.addTarget(self, action: #selector(self.backToPreViewController), forControlEvents: .TouchUpInside)
        footerV.addSubview(backLogin)
        let horizonLineR = UIView(frame: CGRect(x: backLogin.frame.origin.x, y: CGRectGetMaxY(backLogin.frame), width: backLogin.frame.width, height: 1))
        horizonLineR.backgroundColor = UIColor.grayColor()
        footerV.addSubview(horizonLineR)
        
        return footerV
    }
    
    // 返回
    func backToPreViewController() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // 发送验证短信
    func sendMsgValidateCode() -> Bool {
        
        if self.phone == "" {
            SVProgressHUD.showInfoWithStatus("请输入手机号码")
            return false
        }
        
        let params = [
            "action":"sms_send_resetpwd",
            "uid":g_uid,
            "randcode":g_randcode,
            "data": [
                "phone":self.phone
            ]
        ]
        
        JxlNetAPIManager.sharedManager.request_Send_Sms_ResetPwdWithBlock(Globe.baseUrlStr, Params: params as! [String : AnyObject]) { (data, error) in
            if let data = data {
                let status = data["status"] as! NSNumber
                if status != 1 {
                    SVProgressHUD.showInfoWithStatus(data["msg"] as! String)
                }
            }
        }
        
        return true
    }
    
    // 修改密码
    func modifyPwsd() {
        if self.phone == "" {
            SVProgressHUD.showInfoWithStatus("请输入手机号码")
            return
        } else if self.validateCode == "" {
            SVProgressHUD.showInfoWithStatus("请正确填写验证码")
            return
        } else if self.newPassword == "" {
            SVProgressHUD.showInfoWithStatus("请输入新的密码")
            return
        }
        
        let params = [
            "action":"reset_password",
            "uid":g_uid,
            "randcode":g_randcode,
            "data": [
                "phone":self.phone,
                "code":self.validateCode,
                "password":self.newPassword.MD5String
            ]
        ]
        
        JxlNetAPIManager.sharedManager.request_ResetPwdWithBlock(Globe.baseUrlStr, Params: params as! [String : AnyObject]) { (data, error) in
            if let data =  data {
                let status = data["status"] as! NSNumber
                if status != 1 {
                    SVProgressHUD.showInfoWithStatus(data["msg"] as! String)
                } else {
                    SVProgressHUD.showInfoWithStatus(data["msg"] as! String)
                    NSUserDefaults.standardUserDefaults().setObject("", forKey: kLogin_userpwsd)
                    self.dismissViewControllerAnimated(true, completion: nil)
                }
            }
        }
    }
}


extension ForgetPswdViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(kCellIdentifier_ForgotPswdCell, forIndexPath: indexPath) as! ForgotPswdCell
        cell.textValueChangedBlock = { (tag,text) in
            switch tag {
            case 0:
                self.phone = text
            case 1:
                self.validateCode = text
            default:
                self.newPassword = text
            }
        }
        cell.tapValidateBtnBlock = {
            return self.sendMsgValidateCode()
        }
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return kRegister_Height*3 + 43
    }
}

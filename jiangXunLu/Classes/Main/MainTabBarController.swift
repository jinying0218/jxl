//
//  MainTabBarController.swift
//  jiangXunLu
//
//  Created by liuxb on 16/6/12.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController{
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addChildViewControllers()
        AppDelegate.shareDelegate.refreshMessageBadge = {
            let delayInSeconds:Int64 =  Int64(Double(NSEC_PER_SEC) * 0.2)
            let popTime:dispatch_time_t = dispatch_time(DISPATCH_TIME_NOW,delayInSeconds)
            dispatch_after(popTime, dispatch_get_main_queue(), {
                self.refreshMessageBadgeValue()
            })
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.refreshMessageBadgeValue()
    }
    
    private func refreshMessageBadgeValue(){
        if AppDelegate.shareDelegate.num_unreadmsg > 0 {
            if AppDelegate.shareDelegate.num_unreadmsg >= 100 {
                self.tabBar.items![2].badgeValue = "99+"
            } else {
                self.tabBar.items![2].badgeValue = String(AppDelegate.shareDelegate.num_unreadmsg)
            }
        }
    }
    
    private  func addChildViewControllers() {
        addChildViewController("AddressBookViewController", title: "通讯录", imageName: "tab_txl_normal", tag:0)
        addChildViewController("CollectionViewController", title: "收藏", imageName: "tab_sc_normal", tag:1)
        addChildViewController("TSMessageViewController", title: "消息", imageName: "tab_xx_normal", tag:2)
        addChildViewController("DiscoverViewController", title: "发现", imageName: "tab_fx_normal", tag:3)
        addChildViewController("JXLMyCenterViewController", title: "我", imageName: "tab_me_normal", tag:4)
    }

    private func addChildViewController(childControllerName: String, title:String, imageName:String, tag:Int) {
        
        let nameSpace = NSBundle.mainBundle().infoDictionary!["CFBundleExecutable"] as! String

        var childController:UIViewController?
        switch tag {
//        case 1:
//            let classType = NSClassFromString(childControllerName) as! UIViewController.Type
//            childController = classType.init()
        case 2:
            let classType = NSClassFromString(nameSpace + "." + childControllerName) as! UIViewController.Type
            childController = classType.initFromNib()
        case 3:
            let classType = NSClassFromString(childControllerName) as! UIViewController.Type
            childController = classType.init()
        case 4:
            let classType = NSClassFromString(childControllerName) as! UIViewController.Type
            childController = classType.init()
        default:
            let classType = NSClassFromString(nameSpace + "." + childControllerName) as! UIViewController.Type
            childController = classType.init()
        }
        
        if !imageName.isEmpty {
            childController!.tabBarItem.tag = tag;
            childController!.tabBarItem.image = UIImage(named: imageName)
            childController!.tabBarItem.selectedImage = UIImage(named: imageName + "_highlighted")
            childController!.title = title
        }
        
//        if tag ==  4 {
//            (childController as! JXProfileViewController).vcMask = .Self
//        }
        
        let nav = UINavigationController(rootViewController: childController!)
        addChildViewController(nav)
    }
}

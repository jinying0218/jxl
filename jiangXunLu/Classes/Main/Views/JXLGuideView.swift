//
//  JXLGuideView.swift
//  jiangXunLu
//
//  Created by liuxb on 16/7/16.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit
import SnapKit

let numOfPages = 4

class JXLGuideView: UIView {

    private var pageControl: UIPageControl!
    private var startButton: UIButton!
    private var scrollView: UIScrollView!
    
    class func guideView(){
        let sView = JXLGuideView(frame: kScreen_Bounds)
        sView.setupView()
        AppDelegate.shareDelegate.window?.addSubview(sView)
    }

    private func setupView(){
        if scrollView == nil {
            scrollView = UIScrollView(frame: self.frame)
            scrollView.pagingEnabled = true
            scrollView.showsHorizontalScrollIndicator = false
            scrollView.showsVerticalScrollIndicator = false
            scrollView.scrollsToTop = false
            scrollView.bounces = false
            scrollView.contentOffset = CGPointZero
            scrollView.delegate = self
            // 将 scrollView 的 contentSize 设为屏幕宽度的4倍(根据实际情况改变)
            scrollView.contentSize = CGSize(width: self.frame.size.width * CGFloat(numOfPages), height: self.frame.size.height)
            for index  in 0..<numOfPages {
                let imageView = UIImageView(image: UIImage(named: "guide_\(index + 1)"))
                imageView.frame = CGRect(x: frame.size.width * CGFloat(index), y: 0, width: frame.size.width, height: frame.size.height)
                scrollView.addSubview(imageView)

            }
            self.insertSubview(scrollView, atIndex: 0)
        }
        
        if pageControl == nil {
            pageControl = UIPageControl()
            pageControl.currentPage = 0
            pageControl.numberOfPages = numOfPages
            pageControl.currentPageIndicatorTintColor = UIColor(red: 0.992, green: 0.212, blue: 0.424, alpha: 1)
            pageControl.pageIndicatorTintColor = UIColor.darkGrayColor()
            self.addSubview(pageControl)
            pageControl.snp_makeConstraints(closure: { (make) in
                make.centerX.equalTo(self)
                make.bottom.equalTo(self).offset(-20)
            })
        }
        
        if startButton == nil {
            startButton = UIButton()
            startButton.alpha = 0
            startButton.layer.cornerRadius = 15
            startButton.layer.masksToBounds = true
            startButton.setTitle("开始体验", forState: .Normal)
            startButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            startButton.titleLabel?.font = UIFont.systemFontOfSize(18)
            startButton.backgroundColor = UIColor(red: 0.918, green: 0.796, blue: 0.129, alpha: 1)
            startButton.addTarget(self, action: #selector(self.startJoin(_:)), forControlEvents: .TouchUpInside)
            self.addSubview(startButton)
            startButton.snp_makeConstraints(closure: { (make) in
                make.centerX.equalTo(self)
                make.size.equalTo(CGSize(width: 200, height: 40))
                make.bottom.equalTo(pageControl.snp_top).offset(-20)
            })
        }
        
    }

    func startJoin(sender: UIButton) {
        let frame = self.frame
        UIView.animateWithDuration(0.5, animations: {
            self.frame = CGRect(x: -frame.width, y: 0, width: frame.width, height: frame.height)
        }) { (finished) in
            if finished {
                self.removeFromSuperview()
                //AppDelegate.shareDelegate.setupLogionViewController()
            }
        }
    }
}

// MARK: - UIScrollViewDelegate
extension JXLGuideView: UIScrollViewDelegate {
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let offset = scrollView.contentOffset
        // 随着滑动改变pageControl的状态
        pageControl.currentPage = Int(offset.x / self.frame.width)
        
        // 因为currentPage是从0开始，所以numOfPages减1
        if pageControl.currentPage == numOfPages - 1 {
            UIView.animateWithDuration(0.5) {
                self.startButton.alpha = 1.0
            }
        } else {
            UIView.animateWithDuration(0.2) {
                self.startButton.alpha = 0.0
            }
        }
    }
}








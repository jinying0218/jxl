//
//  JXLStartView.swift
//  jiangXunLu
//
//  Created by liuxb on 16/7/13.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit

class JXLStartView: UIView {
    class func startView() -> JXLStartView{
        let sView = JXLStartView(frame: kScreen_Bounds)
        sView.setLaunchScreenImage()
        return sView
    }
    
    private func setLaunchScreenImage(){
        var launchImageName: String?
        if let imagesArray = (NSBundle.mainBundle().infoDictionary! as NSDictionary).valueForKey("UILaunchImages") {
            for dict in imagesArray as! [[String:String]] {
                let imgSize = CGSizeFromString(dict["UILaunchImageSize"]!)
                if CGSizeEqualToSize(imgSize, kScreen_Bounds.size) {
                    launchImageName = dict["UILaunchImageName"]
                    break
                }
            }
        }
        if launchImageName != nil {
            let imgView = UIImageView(image: UIImage(named: launchImageName!))
            imgView.frame = kScreen_Bounds
            self.addSubview(imgView)
        }
    }
    
    
    func startAnimationWithCompletionBlock(completionHandler:(()->Void)?) {
        AppDelegate.shareDelegate.window?.addSubview(self)
        AppDelegate.shareDelegate.window?.bringSubviewToFront(self)
        
        if Login.isLogin() {
            //确认登录入口
            Login.shareLogin.fromLoginPanel = false
            //连接消息
            AppDelegate.shareDelegate.webSocket.connect()
        }
        
        UIView.animateWithDuration(0.6, delay: 0.3, options: .CurveEaseInOut, animations: {
            self.frame = CGRect(x: -self.frame.width, y: 0, width: self.frame.width, height: self.frame.height)
            }) { (finished) in
                self.removeFromSuperview()
                if completionHandler != nil {
                    completionHandler!()
                }
        }
        
    } 
}































































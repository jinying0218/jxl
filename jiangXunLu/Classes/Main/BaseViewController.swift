//
//  BaseViewController.swift
//  jiangXunLu
//
//  Created by liuxb on 16/6/12.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        // #eeeeee
        self.view.backgroundColor = UIColor(red: 0.933, green: 0.933, blue: 0.933, alpha: 1.00)
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    
    internal lazy var navigationBarHeight: CGFloat = {
        var height: CGFloat = 0
        //导航栏高度
        if let navigationBar = self.navigationController?.navigationBar {
            height += navigationBar.bounds.height
        }
        //状态栏高度
        if !UIApplication.sharedApplication().statusBarHidden {
            height += UIApplication.sharedApplication().statusBarFrame.height
        }
        return height
    }()
    
    internal lazy var toolBarHeight: CGFloat = {
        return 44
    }()
    
    internal func setupNavigationStyle(){
        let customBlack = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
        self.navigationController?.navigationBar.barTintColor = customBlack
        self.navigationController?.navigationBar.translucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor()]
    }
    
    class func handleNotificationInfo(userInfo: NSDictionary, applicationState state: UIApplicationState) {
        if state == .Inactive {
            print("-------------userInfo Inactive-----------\n\(userInfo)")
        } else if state == .Active {
            print("-------------userInfo active-----------\n\(userInfo)")
        }
    }
    
    class func presentingVC() -> UIViewController {
        var window = UIApplication.sharedApplication().keyWindow
        if window?.windowLevel != UIWindowLevelNormal {
            let windows = UIApplication.sharedApplication().windows
            for tmpWin in windows {
                if tmpWin.windowLevel == UIWindowLevelNormal {
                    window = tmpWin
                    break
                }
            }
        }
        var result = window?.rootViewController
        while result?.presentedViewController != nil {
            result = result!.presentedViewController
        }
        if result!.isKindOfClass(MainTabBarController) {
            result = (result as! MainTabBarController).selectedViewController
        }
        if result!.isKindOfClass(UINavigationController) {
            result = (result as! UINavigationController).topViewController
        }
        return result!
    }
}




































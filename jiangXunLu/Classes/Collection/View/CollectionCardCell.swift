//
//  CollectionCardCell.swift
//  jiangXunLu
//
//  Created by liuxb on 16/8/7.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit

class CollectionCardCell: UITableViewCell {

    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var telephoneLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var collectionLabel: UILabel!
    
    var cardModel: AddressCardModel? {
        didSet{
            if let head = cardModel?.head {
                let url = NSURL(string: Globe.baseUrlStr + head)!
                self.iconView.sd_setImageWithURL(url)
            }
            self.nameLabel.text = cardModel?.name
            self.telephoneLabel.text = String(cardModel!.phone!)
            self.addressLabel.text = cardModel?.company
            if  let num = cardModel?.collected_num {
                self.collectionLabel.text = String(num)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

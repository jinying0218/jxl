//
//  JXCollectionViewController.h
//  jiangXunLu
//
//  Created by lixu06 on 16/6/30.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXViewController.h"

@interface JXCollectionViewController : JXViewController

@property (nonatomic,strong) NSDictionary *queryDic; // 传入post参数
@property (nonatomic,strong) NSDictionary *caseDic; // 传入控制参数

@end

//
//  JXCollectionViewController.m
//  jiangXunLu
//
//  Created by lixu06 on 16/6/30.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXCollectionViewController.h"
#import "JXCollectionTableManager.h"
#import "GlobalDefine.h"
#import "CommonTools.h"
#import "Color.h"
#import "String.h"
#import "Dimen.h"
#import "EXTScope.h"
#import "JXListTableManager.h"
#import "JXTipHud.h"
#import "JXViewControlerRouter.h"
#import "JXCustomRefreshView.h"

#define COLLECT_LIST_REFRESH_TYPE_HEAD    0
#define COLLECT_LIST_REFRESH_TYPE_FOOT    1
#define COLLECT_LIST_REFRESH_TYPE_REFRESH 3

@interface JXCollectionViewController ()

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) JXListTableManager *tableManager;
@property (nonatomic, strong) NSMutableArray *cellInfoList;
@property (nonatomic, strong) NSMutableArray *photoArray;
@property (nonatomic, strong) NSMutableDictionary *resultDic;
@property (nonatomic, assign) NSInteger pageSize;
@property (nonatomic, assign) NSInteger pageIndex;
@property (nonatomic, assign) CGFloat pageOffset;
@property (nonatomic, strong) NSMutableArray *dataArray;

@end

@implementation JXCollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(DEFINE_COLOR_BACK);
    self.title = STRING_COLLECT_CONTROLLER_TITLE;
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.backgroundColor = UIColorFromRGB(DEFINE_COLOR_BACK);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.contentView = self.tableView;
    [self setUpContentView];
    self.pageSize = 10;
    self.pageIndex = 0;
    if (self.resultDic) {
    } else {
        if (self.caseDic) {
            self.resultDic = [NSMutableDictionary dictionaryWithDictionary:self.caseDic];
        }else {
            self.resultDic = [NSMutableDictionary dictionaryWithCapacity:0];
        }
    }
    NSDictionary * allDic =@{@"resultDic":self.resultDic,
                             @"settingDic":@{}};
    JXCollectionTableManager *tableManager = [[JXCollectionTableManager alloc] initWithDictionary:allDic
                                                                                   viewController:self
                                                                                        tableView:self.tableView
                                                                                    finishHanlder:nil];
    self.cellInfoList = [tableManager getCellInfoList];
    self.dataArray = [NSMutableArray arrayWithCapacity:0];
    [self.cellInfoList addObject:self.dataArray];
    self.tableView.delegate = tableManager;
    self.tableView.dataSource = tableManager;
    [self.tableView reloadData];
    JXCustomRefreshView *headerRefreshView = [JXCustomRefreshView defaultView];
    headerRefreshView.styleDic = [@{@"textValue":@"下拉刷新"} mutableCopy] ;
    CGFloat headerHeight = [JXCustomRefreshView viewHeight:headerRefreshView.styleDic];
    headerRefreshView.frame = CGRectMake(0,
                                         self.contentView.frame.origin.y - headerHeight,
                                         DEVICE_WIDTH,
                                         headerHeight);
    [headerRefreshView updateViewInfo:headerRefreshView.styleDic];
    [tableManager setUpRefreshHeader:headerRefreshView onSuperView:self.view];
    
    JXCustomRefreshView *footerRefreshView = [JXCustomRefreshView defaultView];
    footerRefreshView.styleDic = [@{@"textValue":@"上拉加载更多"} mutableCopy] ;
    CGFloat footerHeight = [JXCustomRefreshView viewHeight:footerRefreshView.styleDic];
    footerRefreshView.frame = CGRectMake(0,
                                         self.contentView.frame.origin.y + self.contentView.frame.size.height,
                                         DEVICE_WIDTH,
                                         footerHeight);
    [footerRefreshView updateViewInfo:footerRefreshView.styleDic];
    [tableManager setUpRefreshFooter:footerRefreshView onSuperView:self.view];
    
    self.tableManager = tableManager;
    
    @weakify(self)
    tableManager.headerRefreshBlock = ^(id data){
        @strongify(self)
        [self requestData:COLLECT_LIST_REFRESH_TYPE_HEAD];
    };
    tableManager.footerRefreshBlock = ^(id data){
        @strongify(self)
        [self requestData:COLLECT_LIST_REFRESH_TYPE_FOOT];
    };
    
}
//-(void)setNavigationBarStyleCustom{
//    [super setNavigationBarStyleCustom];
//}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self requestData:COLLECT_LIST_REFRESH_TYPE_REFRESH];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)requestData:(NSInteger)type{
    /*10 加载收藏列表
     - parameter path:   请求地址字符串
     - parameter params: ["action":"collection_list_load","uid":String,"randcode":String,"data":["start":Int,"count":Int]]
     - parameter block:  回调
     */
    
    [JXTipHud showLoadingWithFinishHandler:nil];
    
    NSString *userId = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
    NSString *randcode = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"randcode"]];
    NSInteger start;
    NSInteger count;
    self.pageOffset = self.tableView.contentOffset.y;
    if (type == COLLECT_LIST_REFRESH_TYPE_HEAD) {
        start = 0;
        count = self.pageSize;
    } else if (type == COLLECT_LIST_REFRESH_TYPE_FOOT){
        start = [self.dataArray count];
        count = self.pageSize;
    } else if (type == COLLECT_LIST_REFRESH_TYPE_REFRESH){
        if ([self.dataArray count] <= self.pageSize) {
            start = 0;
            count = self.pageSize;
        } else {
            start = 0;
            count = [self.dataArray count];
        }
        
    }
    JxlNetAPIManager *netmanager = [JxlNetAPIManager sharedManager];
    NSDictionary *params = @{@"action":@"collection_list_load",
                             @"uid":userId,
                             @"randcode":randcode,
                             @"data":@{
                                     @"start":@(start),
                                     @"count":@(count),
                                     }
                             };
    @weakify(self);
    [netmanager request_CollectionListWithBlock:Globe.baseUrlStr
                                         Params:params
                                       andBlock:^(id _Nullable data, NSError * _Nullable error){
                                           @strongify(self);
                                           JXLog(@"%@",data);
                                           if (type == COLLECT_LIST_REFRESH_TYPE_HEAD) {
                                               [self.tableManager headerRefreshEnd];
                                           } else {
                                               [self.tableManager footerRefreshEnd];
                                           }
                                           [JXTipHud dismiss];
                                           if (error) {
                                               //NSString *errorMsg = [error domain];
                                               //[JXTipHud showTip:errorMsg dismissAfter:0.8 withFinishHandler:nil];
                                               return;
                                           }
                                           if (!data) {
                                               [JXTipHud showTip:@"访问失败" dismissAfter:0.8 withFinishHandler:nil];
                                               return;
                                           }
                                           NSInteger status = [data[@"status"] integerValue];
                                           if (status != 1) {
                                               NSString *errorMsg = data[@"msg"];
                                               [JXTipHud showTip:errorMsg dismissAfter:0.8 withFinishHandler:nil];
                                               return;
                                           }
                                           id result = data[@"info"];
                                           if (!result) {
                                               [JXTipHud showTip:@"访问失败" dismissAfter:0.8 withFinishHandler:nil];
                                               return;
                                           }
                                           id list = result[@"list"];
                                           if ( list && [list isKindOfClass:[NSArray class]]) {
                                               [self updateListData:list withType:type];
                                           } else{
                                               [JXTipHud showTip:@"数据错误" dismissAfter:0.8 withFinishHandler:nil];
                                               return;
                                           }
                                       }];
}

-(void)updateListData:(NSArray *)list withType:(NSInteger)type{
    if (type == COLLECT_LIST_REFRESH_TYPE_HEAD || type == COLLECT_LIST_REFRESH_TYPE_REFRESH) {
        [self.dataArray removeAllObjects];
    }
    if ([list count] == 0) {
        [JXTipHud showTip:@"没有更多数据" dismissAfter:0.8 withFinishHandler:nil];
    }
    for (int i =0; i<list.count; i++) {
        NSMutableDictionary *cellData = [@{@"CellIdentifier":@"JXCollectListTableViewCell",
                                           @"clickBlock":@"pushToProfileViewController:",
                                           @"infoData":list[i]} mutableCopy];
        [self.dataArray addObject:cellData];
    }
    
    [self.tableView reloadData];
    if (type == COLLECT_LIST_REFRESH_TYPE_REFRESH) {
        [self.tableView setContentOffset:CGPointMake(0, self.pageOffset)];
    }
}

@end

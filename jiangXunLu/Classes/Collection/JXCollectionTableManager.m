//
//  JXCollectionTableManager.m
//  jiangXunLu
//
//  Created by lixu on 16/7/1.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXCollectionTableManager.h"
#import "EXTScope.h"
#import "GlobalDefine.h"
#import "Color.h"
#import "String.h"
#import "Dimen.h"
#import "JXTipHud.h"
#import "JXViewControlerRouter.h"

@interface JXCollectionTableManager ()

@property (nonatomic,strong) NSDictionary *settingDic; // 传入控制

@end

@implementation JXCollectionTableManager

- (instancetype)initWithDictionary:(NSDictionary*)dic
                    viewController:(UIViewController *)controller
                         tableView:(UITableView *)tableView
                     finishHanlder:(CommomEditBlock)finishBlock {
    if (self = [super initWithDictionary:dic
                          viewController:controller
                               tableView:tableView
                           finishHanlder:finishBlock]) {
        self.settingDic = dic[@"settingDic"]?dic[@"settingDic"]:@{};
        return self;
    }
    return nil;
}

- (NSMutableArray *)getCellInfoList {
    self.cellInfoList = [super getCellInfoList];
    
    NSMutableArray *firstSectionList = [NSMutableArray arrayWithCapacity:0];

    [firstSectionList addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                 @"JXCustomSectionVariableTableViewCell", @"CellIdentifier",
                                 @(10),@"height",
                                 @(NO),@"isEnd",
                                 UIColorFromRGB(DEFINE_COLOR_BACK),@"backgroundColor",
                                 nil]];
    
    [self.cellInfoList addObject:firstSectionList];
    return self.cellInfoList;
}

- (void)registerTableCell {
    [super registerTableCell];
    [self registerNibCell:@"JXCollectListTableViewCell"];
}
- (BOOL)headerRefreshable {
    return YES;
}
- (BOOL)footerRefreshable {
    return YES;
}

- (void)pushToProfileViewController:(id)data{
    NSDictionary *infoData = data[@"infoData"];
    NSString *targetId = infoData[@"id"];
    NSDictionary *transdata = @{@"targetId":targetId};
    [JXViewControlerRouter pushToProfileOtherViewController:self.controller
                                                   animated:YES
                                                       data:transdata];
    
}

@end



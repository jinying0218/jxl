//
//  AddressCardModelGroup.swift
//  jiangXunLu
//
//  Created by liuxb on 16/8/7.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit

class AddressCardModelGroup: NSObject {
    var title: String?
    var cardModels: [AddressCardModel] = []
}

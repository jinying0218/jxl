//
//  CollectionViewController.swift
//  jiangXunLu
//
//  Created by liuxb on 16/8/7.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit
import SVProgressHUD
import MJRefresh

class CollectionViewController: BaseViewController {

    var cardModelGroups: [AddressCardModelGroup] = []
    
    private lazy var indexTitles: [String] = {
        return ["#","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
    }()
    
    private var tableView: UITableView!
    private var start: Int = 0
    private var count: Int = 100
    private var onceRequstNum: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.setupNavigationStyle()
        self.reloadData()
    }
    
    private func reloadData(){
        self.start = 0
        self.count = 100
        self.cardModelGroups.removeAll()
        self.tableView.reloadData()
        self.loadCollectionCards()
    }

    private func setupTableView(){
        if tableView == nil {
            let frame = CGRect(x: 0, y: 0, width: kScreen_Bounds.width, height: kScreen_Bounds.height - self.toolBarHeight - self.navigationBarHeight)
            tableView = UITableView(frame: frame, style: .Grouped)
            tableView.dataSource = self
            tableView.delegate = self
            tableView.backgroundColor = UIColor(red: 0.933, green: 0.933, blue: 0.933, alpha: 1.00)
            tableView.registerNib(UINib(nibName: "CollectionCardCell", bundle:nil), forCellReuseIdentifier: "CollectionCardCell")
            tableView.tableHeaderView = UIView()
            tableView.tableFooterView = UIView()
            self.view.addSubview(tableView)
        }
        
        tableView.mj_header = MJRefreshNormalHeader(refreshingBlock: {
            self.reloadData()
        })
        
        tableView.mj_footer = MJRefreshAutoNormalFooter(refreshingBlock: {
            if self.onceRequstNum == self.count {
                self.start += self.count
                self.loadCollectionCards()
            } else {
                self.tableView.mj_footer.endRefreshingWithNoMoreData()
                self.tableView.mj_footer.hidden = true
            }
        })
        
        //改变颜色为#999999
        (tableView.mj_footer as! MJRefreshAutoStateFooter).stateLabel.textColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1.0)
        //改变颜色为#999999
        (tableView.mj_header as! MJRefreshStateHeader).lastUpdatedTimeLabel.textColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1.0)
        (tableView.mj_header as! MJRefreshStateHeader).stateLabel.textColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1.0)
    }
    
    //获取字符串的大写首字母
    private func firstUppercaseString(name: String) -> String{
        if name.characters.count < 1 {
            return ""
        } else {
            let index = (name as NSString).characterAtIndex(0)
            if index > 0x4e00  && index < 0x9fff{
                return self.firstUppercaseStringForChinese(name)
            } else {
                let index = name.startIndex.advancedBy(0)
                let firstString = name[index]
                return firstString.debugDescription.uppercaseString
            }
        }
    }
    //获取中文字符串的大写首字母
    private func firstUppercaseStringForChinese(name: String) -> String {
        let transformContents = CFStringCreateMutableCopy(nil, 0, name)
        
        CFStringTransform(transformContents, nil, kCFStringTransformMandarinLatin, false)
        CFStringTransform(transformContents, nil, kCFStringTransformStripDiacritics, false)
        
        let ztransformContents = transformContents as String
        let index = ztransformContents.startIndex.advancedBy(0)
        
        let firstString = ztransformContents[index]
        return firstString.debugDescription.uppercaseString
    }
    
    private func loadCollectionCards() {
        
        let params = [
            "action":"collection_list_load",
            "uid":g_uid,
            "randcode":g_randcode,
            "data":[
                "start": self.start,
                "count": self.count
            ]
        ]
        
        JxlNetAPIManager.sharedManager.request_CollectionListWithBlock(Globe.baseUrlStr, Params: params as! [String : AnyObject]) { (data, error) in
            if let data = data {
                let status = data["status"] as! NSNumber
                if status == 1 { //sucess
                    let info = data["info"] as! [String:AnyObject]
                    let dictArray = info["list"] as! [[String:AnyObject]]
                    self.onceRequstNum = dictArray.count
                    self.reclassifyCardModel(dictArray)
                } else {
                    SVProgressHUD.showInfoWithStatus(data["msg"] as! String)
                }
                self.tableView.reloadData()
            } else {
                self.tableView.reloadData()
            }
            
            //结束头刷新
            self.tableView.mj_header.endRefreshing()
            
            if self.onceRequstNum < self.count {
                self.tableView.mj_footer.endRefreshingWithNoMoreData()
                self.tableView.mj_footer.hidden = true
            } else {
                self.tableView.mj_footer.endRefreshing()
            }
        }
    }
    
    private func reclassifyCardModel(array: [[String:AnyObject]]){
        for dict in array  {
            let card = AddressCardModel(dict: dict)
            let firstLetter = self.firstUppercaseString(card.name!).stringByReplacingOccurrencesOfString("\"", withString: "")
            if self.indexTitles.contains(firstLetter) {
                var haveBeen = false
                for cardModelGroup in self.cardModelGroups {
                    if cardModelGroup.title == firstLetter {
                        cardModelGroup.cardModels.append(card)
                        haveBeen = true
                        break
                    }
                }
                if !haveBeen {
                    let cardModelGroup = AddressCardModelGroup()
                    cardModelGroup.title = firstLetter
                    cardModelGroup.cardModels.append(card)
                    self.cardModelGroups.append(cardModelGroup)
                }
            } else {
                for cardModelGroup in self.cardModelGroups {
                    if cardModelGroup.title == "#" {
                        cardModelGroup.cardModels.append(card)
                        break
                    }
                }
            }
        }
        
        self.cardModelGroups.sortInPlace({ $0.title < $1.title})
    }
}


//MARK: -- UITableViewDataSource, UITableViewDelegate
extension CollectionViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return cardModelGroups.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cardModelGroups[section].cardModels.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CollectionCardCell", forIndexPath: indexPath) as! CollectionCardCell
        cell.cardModel = self.cardModelGroups[indexPath.section].cardModels[indexPath.row]
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 0.1 : 30
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.cardModelGroups[section].title
    }
    
    func sectionIndexTitlesForTableView(tableView: UITableView) -> [String]? {
        return self.indexTitles
    }
    
    func tableView(tableView: UITableView, sectionForSectionIndexTitle title: String, atIndex index: Int) -> Int {

        for cardModelGroup in self.cardModelGroups {
            if cardModelGroup.title == title {
                return self.cardModelGroups.indexOf(cardModelGroup)!
            }
        }
        return 0
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        let cardModel = self.cardModelGroups[indexPath.section].cardModels[indexPath.row]
        let uid = String(cardModel.id!)
        let transdata = ["targetId":uid]
        JXViewControlerRouter.pushToProfileOtherViewController(self, animated: true, data: transdata)
    }
}


































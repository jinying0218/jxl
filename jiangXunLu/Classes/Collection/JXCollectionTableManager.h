//
//  JXCollectionTableManager.h
//  jiangXunLu
//
//  Created by lixu on 16/7/1.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXListTableManager.h"

/*
 * JXCollectionTableManager
 * @description: 收藏页TableView视图管理类
 */
@interface JXCollectionTableManager : JXListTableManager

@end

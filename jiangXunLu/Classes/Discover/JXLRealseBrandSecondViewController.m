//
//  JXLRealseBrandSecondViewController.m
//  jiangXunLu
//
//  Created by 金莹 on 16/9/8.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXLRealseBrandSecondViewController.h"
#import "jiangXunLu-swift.h"
#import "GlobalDefine.h"
#import "AFNTool.h"
#import "YYModel.h"
#import "UIImageView+WebCache.h"
#import "Masonry.h"
#import "EXTScope.h"
#import "MBProgressHUD+Add.h"
#import "KVOController.h"
#import "BlocksKit+UIKit.h"
#import "Hexcolor.h"
#import "YYText.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "JXLRealseCell.h"
#import "JXLReaseModel.h"
#import "YYThumbImagesView.h"
#import "JXLRealseBrandModel.h"
#import "JXLRealseBrandViewController.h"
#import "JXLPickerViewController.h"

static NSString *const kRealseCellIdentifier = @"kRealseCellIdentifier";
static NSString *const kRealseInfoSectionHeaderIdentifier = @"kRealseInfoSectionHeaderIdentifier";

@interface JXLRealseBrandSecondViewController ()<UITableViewDelegate,UITableViewDataSource,YYThumbImagesViewDelegate>
@property (strong, nonatomic) JXLRealseBrandModel *brandModel;

@property (strong, nonatomic) UIImagePickerController *imagePickerController;
@property (strong, nonatomic) UIAlertController *actionSheet;
@property (strong, nonatomic) YYThumbImagesView *thumbImageView;
@property (strong, nonatomic) NSArray *tableDatasArray;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) JXLPickerViewController *dataPickerViewController;
@property (strong, nonatomic) NSArray *pickerDatas;
@property (strong, nonatomic) NSDictionary *realsePlist;
@property (strong, nonatomic) UIView *activeCell;
@end

@implementation JXLRealseBrandSecondViewController
- (instancetype)initWithBrandModel:(JXLRealseBrandModel *)brandModel
{
    self = [super init];
    if (self) {
        _brandModel = brandModel;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"品牌";

    [self setUpLeftbarButtonWithImage:UIIMGName(@"back_normal")];
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setTitle:@"发布" forState:UIControlStateNormal];
    [rightButton setTitleColor:[UIColor colorWithHexString:@"FECD08"] forState:UIControlStateNormal];
    [rightButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [self setUpRightbarButtonWithCustomButton:rightButton];
    
    [self prepareData];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"JXLRealseCell" bundle:nil] forCellReuseIdentifier:kRealseCellIdentifier];
    [self.tableView registerClass:[UITableViewHeaderFooterView class] forHeaderFooterViewReuseIdentifier:kRealseInfoSectionHeaderIdentifier];
    self.tableView.backgroundColor = [UIColor colorWithHexString:@"eeeeee"];
    self.tableView.tableFooterView = self.thumbImageView;
    
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self addKeyboardNotification];
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self removeKeyboardNotification];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)leftBarItemTouchUpInside:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
//发布
- (void)rightBarItemTouchUpInside:(id)sender{
    [self.view endEditing:YES];
    NSUInteger section = 0;
    for (NSArray *subArray in self.tableDatasArray) {
        for (JXLReaseModel *realseModel in subArray) {
            if (!realseModel.content || realseModel.content.length == 0) {
                [MBProgressHUD showError:@"请将信息填写完整" toView:self.view];
                NSUInteger location = [subArray indexOfObject:realseModel];
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:location inSection:section];
                JXLRealseCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
                cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"find_remind"]];
                return;
            }
        }
        section ++;
    }
    
    
    NSString *g_uid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
    NSString *g_randcode = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"randcode"]];
    
    NSDictionary *data = @{@"title" :self.brandModel.title,
                           @"content" : self.brandModel.content,
                           @"cover" : self.brandModel.cover,
                           @"pics" : [self.brandModel.pics yy_modelToJSONString],
                           @"company" : self.brandModel.company,
                           @"addr_prov" : self.brandModel.addr_prov,
                           @"addr_city" : self.brandModel.addr_city,
                           @"addr_detail" : self.brandModel.addr_detail,
                           @"expand_area" : self.brandModel.expand_area,
                           @"industry" : self.brandModel.industry,
                           @"position" : self.brandModel.position,
                           @"project_type" : self.brandModel.project_type,
                           @"store_area" : self.brandModel.store_area,
                           @"property_usage" : self.brandModel.property_usage,
                           @"operation_mode" : self.brandModel.operation_mode,
                           @"store_count" : self.brandModel.store_count};
    
    
    NSDictionary *params = @{@"action" : @"find_brand_release",
                             @"uid" : g_uid,
                             @"randcode" : g_randcode,
                             @"data":[data yy_modelToJSONString]};
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    @weakify(self);
    [AFNTool post:Globe.baseUrlStr params:params success:^(id responsObject) {
        @strongify(self);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        //        NSLog(@"find_project_detail_load:%@",responsObject);
        if (responsObject && [responsObject isKindOfClass:[NSDictionary class]]) {
            NSInteger status = [responsObject[@"status"] integerValue];
            if (status == 1) {
                for (UIViewController *childVC in self.navigationController.viewControllers) {
                    if ([childVC isKindOfClass:[DiscoverViewController class]]) {
                        [self.navigationController popToViewController:childVC animated:YES];
                        break;
                    }
                }
            }
        }
    } failure:^(NSError *error) {
        @strongify(self);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:error.description toView:self.view];
        
        NSLog(@"%@",error.description);
    }];
}

#pragma mark - loadData
- (void)prepareData{
    
    if (self.brandModel.pics) {
        @weakify(self);
        for (NSString *imageString in self.brandModel.pics) {
            NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",Globe.baseUrlStr,imageString]];
            [[SDWebImageManager sharedManager] downloadImageWithURL:imageURL options:SDWebImageRetryFailed | SDWebImageAvoidAutoSetImage progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                @strongify(self);
                [self.thumbImageView insertOneImage:image refresh:^(YYThumbImagesView *thumbView) {
                    @strongify(self);
                    self.tableView.tableFooterView = thumbView;
                }];
            }];
        }
    }
    
    JXLReaseModel *cellModel1 = [[JXLReaseModel alloc] initWithTitleString:@"可进驻类型" placeholder:@"如：综合体" content:(self.brandModel.project_type ? self.brandModel.project_type : nil) keyString:@"project_type"];
    JXLReaseModel *cellModel2 = [[JXLReaseModel alloc] initWithTitleString:@"商业面积" placeholder:nil content:(self.brandModel.store_area ? self.brandModel.store_area : nil) keyString:@"store_area"];
    JXLReaseModel *cellModel3 = [[JXLReaseModel alloc] initWithTitleString:@"门店数" placeholder:nil content:(self.brandModel.store_count ? self.brandModel.store_count : nil) keyString:@"store_count"];
    JXLReaseModel *cellModel4 = [[JXLReaseModel alloc] initWithTitleString:@"开店方式" placeholder:@"如：独立经营" content:(self.brandModel.operation_mode ? self.brandModel.operation_mode : nil) keyString:@"operation_mode"];

    JXLReaseModel *cellModel5 = [[JXLReaseModel alloc] initWithTitleString:@"物业使用方式" placeholder:@"如：租赁" content:(self.brandModel.property_usage ? self.brandModel.property_usage : nil) keyString:@"property_usage"];

    JXLReaseModel *cellModel6 = [[JXLReaseModel alloc] initWithTitleString:nil placeholder:nil content:(self.brandModel.content ? self.brandModel.content : nil) keyString:@"content"];
    
    self.tableDatasArray = @[@[cellModel1,cellModel2,cellModel3,cellModel4,cellModel5],@[cellModel6]];
}
//上传二进制 图片
- (void)uploadImage:(UIImage *)uploadImage{
    NSString *g_uid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
    NSString *g_randcode = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"randcode"]];
    
    NSData *imageData = UIImageJPEGRepresentation(uploadImage, 0.8f);
    
    NSDictionary *data = @{@"type" : @"stream",
                           @"data" : imageData,
                           @"place" : @""};
    NSDictionary *params = @{@"action" : @"upload_img",
                             @"uid" : g_uid,
                             @"randcode" : g_randcode,
                             @"data":[data yy_modelToJSONString]};
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    @weakify(self);
    [AFNTool uploadRequestURL:Globe.baseUrlStr params:params fileData:imageData success:^(id responsObject) {
        //        NSLog(@"%@",responsObject);
        @strongify(self);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if ([responsObject[@"status"] intValue] == 1) {
            NSString *urlString = [responsObject[@"info"] objectForKey:@"url"];
            
            [self.brandModel.pics addObject:urlString];
            [self.thumbImageView insertOneImage:uploadImage refresh:^(YYThumbImagesView *thumbView) {
                @strongify(self);
                self.tableView.tableFooterView = thumbView;
            }];
        }else {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [MBProgressHUD showError:responsObject[@"msg"] toView:self.view];
        }
    } failure:^(NSError *error) {
        //        NSLog(@"%@",error.description);
        @strongify(self);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:error.description toView:self.view];
    }];
}



#pragma mark - YYThumbImagesViewDelegate
- (void)addImageButtonClick:(UIButton *)button{
    [self.view endEditing:YES];
    [self presentViewController:self.actionSheet animated:YES completion:nil];
}
- (void)deleteImageButton:(NSUInteger)deleteIndex{
    [self.brandModel.pics removeObjectAtIndex:deleteIndex];
}

#pragma mark - tableView delegate && tableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.tableDatasArray.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.tableDatasArray[section] count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 0;
    }
    return 52;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 0) {
        return 0;
    }
    return 18;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return [[UIView alloc] init];
    }
    UITableViewHeaderFooterView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:kRealseInfoSectionHeaderIdentifier];
    headerView.contentView.backgroundColor = [UIColor colorWithHexString:@"eeeeee"];
    headerView.textLabel.font = [UIFont systemFontOfSize:14];
    headerView.textLabel.textColor = [UIColor colorWithHexString:@"333333"];
    headerView.textLabel.text = @"品牌介绍";
    return headerView;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section == 0) {
        return [[UIView alloc] init];
    }
    UITableViewHeaderFooterView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:kRealseInfoSectionHeaderIdentifier];
    headerView.contentView.backgroundColor = [UIColor colorWithHexString:@"eeeeee"];
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    JXLReaseModel *cellModel = self.tableDatasArray[indexPath.section][indexPath.row];
    if (!cellModel.content || cellModel.content.length == 0) {
        if (indexPath.section == 1) {
            return 100;
        }
        return 50;
    }else {
        if (indexPath.section == 1) {
            return (cellModel.cellHeight + 30) < 100 ? 100 : (cellModel.cellHeight + 30);
        }
        return (cellModel.cellHeight + 30) < 50 ? 50 : (cellModel.cellHeight + 30);
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    JXLRealseCell *cell = [tableView dequeueReusableCellWithIdentifier:kRealseCellIdentifier];
    
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}
- (void)configureCell:(JXLRealseCell *)cell atIndexPath:(NSIndexPath *)indexPath{
    
    JXLReaseModel *cellModel = self.tableDatasArray[indexPath.section][indexPath.row];
    @weakify(self);
    [cell configureTableView:self.tableView CellData:cellModel indexPath:indexPath handler:^(NSString *inputString) {
        @strongify(self);
        if ([inputString isEqualToString:@""] || !inputString) {
            return ;
        }
        cellModel.content =  inputString;
        [self.brandModel setValue:inputString forKey:cellModel.keyString];
    } activeBlock:^(UIView *activeView) {
        @strongify(self);
        self.activeCell = activeView;
    }];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.view endEditing:YES];
    JXLRealseCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    JXLReaseModel *cellModel = cell.cellModel;
    
//    //设置是否多选
//    if ([cellModel.keyString isEqualToString:@"brand_industry"] || [cellModel.keyString isEqualToString:@"position"] || [cellModel.keyString isEqualToString:@"operation_mode"] || [cellModel.keyString isEqualToString:@"type"]) {
//        [self.dataPickerViewController setIsAllowMultableSelection:YES];
//    }
    [self.dataPickerViewController setIsAllowMultableSelection:YES];

    
    NSArray *array = self.realsePlist[cellModel.keyString];
    @weakify(self);
    [self.dataPickerViewController setTableDatas:array chooseCompletion:^(NSString *selectedString) {
        @strongify(self);
        cellModel.content = selectedString;
        [self.brandModel setValue:selectedString forKey:cellModel.keyString];
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
    self.dataPickerViewController.titleLabel.text = cellModel.title;
    [self addChildViewController:self.dataPickerViewController];
    [self.view addSubview:self.dataPickerViewController.view];
}

#pragma mark - getter
- (UIImagePickerController *)imagePickerController{
    if (!_imagePickerController) {
        _imagePickerController = [[UIImagePickerController alloc] init];
        _imagePickerController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        _imagePickerController.allowsEditing = NO;
        @weakify(self);
        [_imagePickerController setBk_didCancelBlock:^(UIImagePickerController *imagePickerController) {
            [imagePickerController dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [_imagePickerController setBk_didFinishPickingMediaBlock:^(UIImagePickerController *imagePickerController, NSDictionary <NSString *,id>*info) {
            @strongify(self);
            [imagePickerController dismissViewControllerAnimated:YES completion:^{
                UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
                if (!image) {
                    image = [info objectForKey:UIImagePickerControllerOriginalImage];
                }
                [self uploadImage:image];
            }];
        }];
    }
    return _imagePickerController;
}
- (UIAlertController *)actionSheet{
    if (!_actionSheet){
        _actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        @weakify(self);
        [_actionSheet addAction:[UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            @strongify(self);
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
                self.imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                [self presentViewController:self.imagePickerController animated:YES completion:nil];
            }
            [_actionSheet dismissViewControllerAnimated:YES completion:nil];
        }]];
        [_actionSheet addAction:[UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            @strongify(self);
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                self.imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:self.imagePickerController animated:YES completion:nil];
            }else {
                [MBProgressHUD showError:@"没有安装摄像头" toView:self.view];
            }
            
            [_actionSheet dismissViewControllerAnimated:YES completion:nil];
        }]];
        [_actionSheet addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [_actionSheet dismissViewControllerAnimated:YES completion:nil];
        }]];
        
    }
    return _actionSheet;
}

- (YYThumbImagesView *)thumbImageView{
    if (!_thumbImageView) {
        _thumbImageView = [[YYThumbImagesView alloc] init];
        _thumbImageView.delegate = self;
        _thumbImageView.height = 101;
    }
    return _thumbImageView;
}
- (JXLPickerViewController *)dataPickerViewController{
    if (!_dataPickerViewController) {
        _dataPickerViewController = [[JXLPickerViewController alloc] init];
        _dataPickerViewController.view.frame = CGRectMake( 0, 0, self.view.width, self.view.height);

    }
    return _dataPickerViewController;
}

- (NSDictionary *)realsePlist{
    if (!_realsePlist) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"JXLRealseProperty" ofType:@"plist"];
        _realsePlist = [[NSDictionary alloc] initWithContentsOfFile:path];
        
    }
    return _realsePlist;
}
#pragma mark - keyboardNotification

//注册键盘通知
- (void)addKeyboardNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
}

//移除键盘通知
- (void)removeKeyboardNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

//键盘显示
- (void)keyboardWillShow:(NSNotification*)notification
{
    CGRect keyboardBounds = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    float duringTime = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    CGSize kbSize = keyboardBounds.size;
    
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    
    if (!CGRectContainsPoint(aRect, CGPointMake(self.activeCell.origin.x, self.activeCell.origin.y + self.activeCell.height))) {
        
        CGPoint scrollPoint = CGPointMake(0.0, (self.activeCell.frame.origin.y + self.activeCell.height - aRect.size.height) + 44);
        
        [UIView animateWithDuration:duringTime
                              delay:0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             [self.view layoutIfNeeded];
                             [self.tableView setContentOffset:scrollPoint animated:YES];
                         } completion:^(BOOL finished) {
                         }];
    }
}
//键盘隐藏
- (void)keyboardWillHide:(NSNotification*)notification
{
    float duringTime = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    
    [UIView animateWithDuration:duringTime
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self.view layoutIfNeeded];
                     } completion:^(BOOL finished) {
                         
                     }];
}
@end

//
//  JXLRealseBrandSecondViewController.h
//  jiangXunLu
//
//  Created by 金莹 on 16/9/8.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXViewController.h"
@class JXLRealseBrandModel;
@interface JXLRealseBrandSecondViewController : JXViewController


- (instancetype)initWithBrandModel:(JXLRealseBrandModel *)brandModel;
@end

//
//  JXLRealseInfoViewController.h
//  jiangXunLu
//
//  Created by 金莹 on 16/9/4.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXViewController.h"
#import "Constants.h"

@interface JXLRealseInfoViewController : JXViewController

- (instancetype)initWithEditStatus:(JXLRealseType)type identifier:(NSString *)identifier;
@end

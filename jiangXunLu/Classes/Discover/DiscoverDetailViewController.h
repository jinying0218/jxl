//
//  DiscoverDetailViewController.h
//  jiangXunLu
//
//  Created by 金莹 on 16/9/1.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXViewController.h"
#import "Constants.h"


@protocol DiscoverDetailViewControllerDelegate <NSObject>

- (void)finishEidtDiscoverDetail;

@end

@interface DiscoverDetailViewController : JXViewController
@property (weak, nonatomic) id<DiscoverDetailViewControllerDelegate>delegate;
@property (strong, nonatomic) NSString *identifier;
- (instancetype)initWithDiscoverType:(JXLDisType)disType;
@end

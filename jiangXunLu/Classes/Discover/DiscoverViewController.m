 //
//  DiscoverViewController.m
//  jiangXunLu
//
//  Created by 金莹 on 16/8/30.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "DiscoverViewController.h"
#import "DYMRollingBannerVC.h"
#import "AFNTool.h"
#import "YYModel.h"
#import "GlobalDefine.h"

#import "jiangXunLu-swift.h"
#import "UIImageView+WebCache.h"
#import "Masonry.h"
#import "EXTScope.h"
#import "MJRefresh.h"
#import "MJRefreshBackStateFooter.h"

#import "Constants.h"
#import "Hexcolor.h"
#import "DiscoverInfoTableViewCell.h"
#import "DiscoverProjectTableViewCell.h"
#import "DiscoverBrandTableViewCell.h"
#import "JXLChooseRealseItemView.h"


#import "JXLBannerModel.h"
#import "JXLDiscoverInfoModel.h"
#import "JXLDiscoverProjectModel.h"
#import "JXLDiscoverBrandModel.h"

#import "DiscoverDetailViewController.h"
#import "JXLRealseInfoViewController.h"
#import "JXLRealseProjectViewController.h"
#import "JXLRealseBrandViewController.h"
#import "JXLSearchDiscoverViewController.h"

#import "MBProgressHUD+Add.h"

static NSString *kDiscoverInfoCellIdentifier = @"DiscoverInfoTableViewCellIdentifier";
static NSString *kDiscoverProjectCellIdentifier = @"DiscoverProjectTableViewCellIdentifier";
static NSString *kDiscoverBrandCellIdentifier = @"DiscoverBrandCellIdentifier";


@interface DiscoverViewController ()<JXLChooseRealseItemViewDelegate,DiscoverDetailViewControllerDelegate>
@property (strong, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet UIView *bannerView;
@property (weak, nonatomic) IBOutlet UIView *segmentBox;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) DYMRollingBannerVC *rollingBannerVC;
@property (strong, nonatomic) IBOutlet JXLChooseRealseItemView *realseItemView;


@property (assign, nonatomic) JXLDisType currentDisType;
@property (strong, nonatomic) JXLSegmentView *segmentView;
@property (strong, nonatomic) NSMutableArray *bannersArray;
@property (strong, nonatomic) NSMutableArray *infoDatas;
@property (strong, nonatomic) NSMutableArray *projectDatas;
@property (strong, nonatomic) NSMutableArray *brandDatas;

@property (strong, nonatomic) NSMutableArray *tempDatas;

@property (assign, nonatomic) CGFloat startNumber;
@property (assign, nonatomic) CGFloat dataCount;
@end

@implementation DiscoverViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadBannerData];
    
    self.infoDatas = [[NSMutableArray alloc] initWithCapacity:2];
    self.projectDatas = [[NSMutableArray alloc] initWithCapacity:2];
    self.brandDatas = [[NSMutableArray alloc] initWithCapacity:2];

    self.tempDatas = [[NSMutableArray alloc] initWithCapacity:2];
    
    self.currentDisType = kDiscoverInfo;
    
    UIImage *image1 = [UIImage imageNamed:@"find_btn_release_normal"];
    UIImage *image2 = [UIImage imageNamed:@"find_btn_search_normal"];
    NSArray *images = @[image1,image2];

    UIImage *image3 = [UIImage imageNamed:@"find_btn_release_light"];
    UIImage *image4 = [UIImage imageNamed:@"find_btn_search_light"];
    NSArray *lightImages = @[image3,image4];
    
    [self setUpRightbarButtonWithImages:images highLightImages:lightImages];

    [[UIApplication sharedApplication].keyWindow addSubview:self.realseItemView];
    [self.realseItemView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo([UIApplication sharedApplication].keyWindow);
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    
    self.realseItemView.hidden = YES;
    self.realseItemView.delegate = self;
    [self setupSegmentView];
    
    [self setupRollBanner];
    
    [self setupTableView];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshUINofitifation) name:@"refreshUINofitifation" object:nil];

}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupSegmentView{
    
    SegmentButton *infoButton = [[SegmentButton alloc] initWithTitle:@"资讯" normalImage:[UIImage imageNamed:@"menu_yellow_normal"] selectedImage:[UIImage imageNamed:@"menu_yellow_unselect_normal"]];
    
    SegmentButton *projectButton = [[SegmentButton alloc] initWithTitle:@"项目" normalImage:[UIImage imageNamed:@"menu_yellow_normal"] selectedImage:[UIImage imageNamed:@"menu_yellow_unselect_normal"]];
    SegmentButton *brandButton = [[SegmentButton alloc] initWithTitle:@"品牌" normalImage:[UIImage imageNamed:@"menu_yellow_normal"] selectedImage:[UIImage imageNamed:@"menu_yellow_unselect_normal"]];
    
    self.segmentView = [[JXLSegmentView alloc] initWithFrame:CGRectMake( 0, 0, [UIScreen mainScreen].bounds.size.width, 35) segmentItems:@[infoButton,projectButton,brandButton] selectedBy:0.5 selectedIndex:0];
    [self.segmentBox addSubview:self.segmentView];
    @weakify(self);
    [self.segmentView setSelectedBlock:^(NSInteger index) {
        @strongify(self);
        switch (index) {
            case 0:{
                self.currentDisType = kDiscoverInfo;
                [self.infoDatas removeAllObjects];
            }
                break;
            case 1:{
                self.currentDisType = kDiscoverProject;
                [self.projectDatas removeAllObjects];
            }
                break;
            default:
            {
                self.currentDisType = kDiscoverBrand;
                [self.brandDatas removeAllObjects];

            }
                break;
        }
        [self loadtableViewData];
//        [self.tableView reloadData];
    }];
}

- (void)setupTableView{
    [self.tableView registerNib:[UINib nibWithNibName:@"DiscoverInfoTableViewCell" bundle:nil] forCellReuseIdentifier:kDiscoverInfoCellIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:@"DiscoverProjectTableViewCell" bundle:nil] forCellReuseIdentifier:kDiscoverProjectCellIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:@"DiscoverBrandTableViewCell" bundle:nil] forCellReuseIdentifier:kDiscoverBrandCellIdentifier];
    
    self.tableView.tableHeaderView = self.headerView;

    self.tableView.backgroundColor = [UIColor colorWithHexString:@"eeeeee"];

    
    @weakify(self);

    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        self.startNumber = 0;
        self.dataCount = 20;
        
        [self.infoDatas removeAllObjects];
        [self.projectDatas removeAllObjects];
        [self.brandDatas removeAllObjects];

        [self loadtableViewData];
    }];
    
    self.tableView.mj_footer = [MJRefreshBackStateFooter footerWithRefreshingBlock:^{
        @strongify(self);
        self.startNumber += 20;
        [self loadtableViewData];
    }];
    
    
    [self.tableView.mj_header beginRefreshing];

}

- (void)setupRollBanner{
    [self addChildViewController:self.rollingBannerVC];
    [self.bannerView addSubview:self.rollingBannerVC.view];
    
    @weakify(self);
    [self.rollingBannerVC.view mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.leading.top.and.right.bottom.equalTo(self.bannerView);
    }];
    
    [self.rollingBannerVC didMoveToParentViewController:self];
    
    [self reloadBannerUI];
}

- (void)reloadBannerUI{
    NSMutableArray *imageURLs = [[NSMutableArray alloc] initWithCapacity:2];
    for (JXLBannerModel *bannerModel in self.bannersArray) {
        [imageURLs addObject:bannerModel.banner_src];
    }
    self.rollingBannerVC.rollingImages = imageURLs;
    [self.rollingBannerVC startRolling];
    
    [self.rollingBannerVC setRemoteImageLoadingBlock:^(UIImageView *imageView, NSString *imageUrlStr, UIImage *placeHolderImage) {
        [imageView sd_cancelCurrentImageLoad];
        [imageView sd_setImageWithURL:[NSURL URLWithString:imageUrlStr] placeholderImage:placeHolderImage];
    }];
    
    @weakify(self);
    [self.rollingBannerVC addBannerTapHandler:^(NSInteger whichIndex) {
        @strongify(self);
        JXLBannerModel *bannerModel = self.bannersArray[whichIndex];
        JXLDisType ftype = kDiscoverInfo;
        if ([bannerModel.banner_ftype isEqualToString:@"info"]) {
            ftype = kDiscoverInfo;
        }else if ([bannerModel.banner_ftype isEqualToString:@"project"]){
            ftype = kDiscoverProject;
        }else {
            ftype = kDiscoverBrand;
        }

        DiscoverDetailViewController *detailVC = [[DiscoverDetailViewController alloc] initWithDiscoverType:ftype];
        detailVC.identifier = bannerModel.banner_Id;
        detailVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:detailVC animated:YES];
        
    }];
}

#pragma mark - action
- (void)rightBarItemTouchUpInside:(UIButton *)item{
    if(item.tag == 1000) {
        self.realseItemView.hidden = NO;

    }else {
        JXLSearchDiscoverViewController *searchVC = [[JXLSearchDiscoverViewController alloc] init];
        searchVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:searchVC animated:YES];
    }
}

- (void)refreshUINofitifation{
    NSLog(@"收到了通知");
}

#pragma mark -

- (void)loadBannerData{
    
    NSString *userId = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
    NSString *randcode = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"randcode"]];
    NSDictionary *subDict = @{@"party" : @(3)};
    
    NSString *dataString = [subDict yy_modelToJSONString];
    NSDictionary *params = @{@"action" : @"find_banner_load",
                             @"uid" : userId,
                             @"randcode" : randcode,
                             @"data" : dataString
                                    };

    [AFNTool post:Globe.baseUrlStr params:params success:^(id responsObject) {
        NSLog(@"%@",responsObject);
        if (responsObject && [responsObject isKindOfClass:[NSDictionary class]]) {
            NSInteger status = [responsObject[@"status"] integerValue];
            NSInteger count = [[responsObject[@"info"] objectForKey:@"count"] integerValue];
            if (status == 1 && count > 0) {
                self.bannersArray = [[NSMutableArray alloc] initWithCapacity:2];
                NSArray *list = [NSArray yy_modelArrayWithClass:[JXLBannerModel class] json:[responsObject[@"info"] objectForKey:@"list"]];
                for (JXLBannerModel *banner in list) {
                    banner.banner_src = [NSString stringWithFormat:@"%@%@",Globe.baseUrlStr,banner.banner_src];
                    [self.bannersArray addObject:banner];
                }
                [self reloadBannerUI];
            }
        }
    } failure:^(NSError *error) {
        NSLog(@"%@",error.description);
    }];
}

- (void)loadtableViewData{
    [self.tempDatas removeAllObjects];
    NSDictionary *params = nil;
    
    NSString *g_uid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
    NSString *g_randcode = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"randcode"]];
    
    NSDictionary *data = @{@"start" : @(self.startNumber),
                           @"count" : @(self.dataCount),
                           @"party" : @(self.currentDisType)};

    switch (self.currentDisType) {
        case 3:{
            params = @{@"action" : @"find_info_list_load",
                       @"uid" : g_uid,
                       @"randcode" : g_randcode,
                       @"data":[data yy_modelToJSONString]};
        }
            break;
        case 1:{
            params = @{@"action" : @"find_project_list_load",
                       @"uid" : g_uid,
                       @"randcode" : g_randcode,
                       @"data":[data yy_modelToJSONString]};
        }
            break;
        case 2:{
            params = @{@"action" : @"find_brand_list_load",
                       @"uid" : g_uid,
                       @"randcode" : g_randcode,
                       @"data":[data yy_modelToJSONString]};
        }
            break;
            
        default:
            break;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [AFNTool post:Globe.baseUrlStr params:params success:^(id responsObject) {
//        NSLog(@"%@",responsObject);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (responsObject && [responsObject isKindOfClass:[NSDictionary class]]) {
            NSInteger status = [responsObject[@"status"] integerValue];
            if (status == 1) {
                NSDictionary *infoDict = responsObject[@"info"];
                switch (self.currentDisType) {
                    case 1:{
                        NSArray *list = [NSArray yy_modelArrayWithClass:[JXLDiscoverProjectModel class] json:infoDict[@"list"]];
                        [self.tempDatas addObjectsFromArray:list];
                        [self.projectDatas addObjectsFromArray:self.tempDatas];
                    }
                        break;
                    case 2:{
                        NSArray *list = [NSArray yy_modelArrayWithClass:[JXLDiscoverBrandModel class] json:infoDict[@"list"]];
                        [self.tempDatas addObjectsFromArray:list];
                        [self.brandDatas addObjectsFromArray:self.tempDatas];
                    }
                        break;
                    default:{
                        NSArray *list = [NSArray yy_modelArrayWithClass:[JXLDiscoverInfoModel class] json:infoDict[@"list"]];
                        [self.tempDatas addObjectsFromArray:list];
                        [self.infoDatas addObjectsFromArray:self.tempDatas];
                    }
                        break;
                }

                if (self.tempDatas.count < self.dataCount) {
                    [self.tableView.mj_footer endRefreshingWithNoMoreData];
                }else {
                    [self.tableView.mj_footer endRefreshing];
                }
                [self.tableView.mj_header endRefreshing];
                [self.tableView reloadData];
            }
        }
    } failure:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSLog(@"%@",error.description);
    }];

}

#pragma mark - DiscoverDetailViewControllerDelegate
- (void)finishEidtDiscoverDetail{
    [self.tableView.mj_header beginRefreshing];
}
#pragma mark -  JXLChooseRealseItemViewDelegate
/*
 kDiscoverProject = 1,      //1.项目 2，品牌  3.其他
 kDiscoverBrand = 2,
 kDiscoverInfo = 3
*/
- (void)chooseRealseItemType:(JXLDisType)ftype{
    self.realseItemView.hidden = YES;
    UIViewController *realseVC = nil;
    switch (ftype) {
        case kDiscoverInfo:{
            realseVC = [[JXLRealseInfoViewController alloc] initWithEditStatus:kDiscoverRealse identifier:nil];
        }
            break;
        case kDiscoverProject:{
            realseVC = [[JXLRealseProjectViewController alloc] initWithEditStatus:kDiscoverRealse identifier:nil];
        }
            break;
        case kDiscoverBrand:{
            realseVC = [[JXLRealseBrandViewController alloc]  initWithEditStatus:kDiscoverRealse identifier:nil];
        }
            break;
        default:
            break;
    }
    realseVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:realseVC animated:YES];

}

#pragma mark - tableView delegate & dataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (self.currentDisType) {
        case 1:{
            return self.projectDatas.count;
        }
            break;
        case 2:{
            return self.brandDatas.count;
        }
            break;
            
        default:
            return self.infoDatas.count;
            break;
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (self.currentDisType) {
        case 1:{
            return [DiscoverProjectTableViewCell cellHeight];
        }
            break;
        case 2:{
            return [DiscoverBrandTableViewCell cellHeight];
        }
            break;
        default:{
            return [DiscoverInfoTableViewCell cellHeight];
        }
            break;
    }

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (self.currentDisType) {
        case 1:{
            DiscoverProjectTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:kDiscoverProjectCellIdentifier];
            [cell configureCell:self.projectDatas[indexPath.row]];
            return cell;
        }
            break;
        case 2:{
            DiscoverBrandTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:kDiscoverBrandCellIdentifier];
            [cell configureCell:self.brandDatas[indexPath.row]];
            return cell;
        }
            break;
        default:{
            DiscoverInfoTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:kDiscoverInfoCellIdentifier];
            [cell configureCell:self.infoDatas[indexPath.row]];
            return cell;
        }
            break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    DiscoverDetailViewController *detailVC = [[DiscoverDetailViewController alloc] initWithDiscoverType:self.currentDisType];
    switch (self.currentDisType) {
        case 1:{
            JXLDiscoverProjectModel *project = self.projectDatas[indexPath.row];
            detailVC.identifier = project.project_id;
        }
            break;
        case 2:{
            JXLDiscoverBrandModel *brand = self.brandDatas[indexPath.row];
            detailVC.identifier = brand.brand_id;
        }
            break;
        default:{
            JXLDiscoverInfoModel *info = self.infoDatas[indexPath.row];
            detailVC.identifier = info.discoverInfo_id;
        }
            break;
    }
    detailVC.delegate = self;
    detailVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:detailVC animated:YES];
}

#pragma mark - getter
- (DYMRollingBannerVC *)rollingBannerVC{
    if (!_rollingBannerVC) {
        _rollingBannerVC = [DYMRollingBannerVC new];
        _rollingBannerVC.rollingInterval = 2;
        _rollingBannerVC.placeHolderImage = [UIImage imageNamed:@"find_default-image"];
    }
    
    return _rollingBannerVC;
}
@end

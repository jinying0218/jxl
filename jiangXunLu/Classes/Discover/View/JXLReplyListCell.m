//
//  JXLReplyListCell.m
//  jiangXunLu
//
//  Created by 金莹 on 16/9/2.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXLReplyListCell.h"
#import "JXLFindReplyModel.h"
#import "HexColor.h"
#import "UIImageView+WebCache.h"
#import "jiangXunLu-swift.h"
#import "YYText.h"
#import "NSAttributedString+YYText.h"

@interface JXLReplyListCell()
@property (weak, nonatomic) IBOutlet UIImageView *headerImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *replyTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *replyContentLabel;
@property (copy, nonatomic) HeaderImageViewClickHandler handler;
@end
@implementation JXLReplyListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.headerImageView.layer.cornerRadius = self.headerImageView.width/2;
    self.headerImageView.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - 
- (IBAction)headerImageViewClick:(UIButton *)sender {
    self.handler(_cellModel);
}


#pragma mark -

- (void)setCellModel:(JXLFindReplyModel *)cellModel headerImageClick:(HeaderImageViewClickHandler)handler{
    _handler = handler;
    if (_cellModel != cellModel) {
        _cellModel = cellModel;
        
        NSRange range = [_cellModel.content rangeOfString:@"@"];
        NSRange range1 = [_cellModel.content rangeOfString:@":"];
        NSUInteger location = range.location;
        NSUInteger length = range1.location - range.location;
        
        
        if (location != NSNotFound) {
            NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:_cellModel.content];
            NSDictionary *dict = @{NSFontAttributeName : [UIFont systemFontOfSize:13],
                                   NSForegroundColorAttributeName : [UIColor colorWithHexString:@"fecd08"]};
            [attributeString addAttributes:dict range:NSMakeRange(location, length)];
            self.replyContentLabel.attributedText = attributeString;
        }else {
            self.replyContentLabel.text = _cellModel.content;
        }
        
        
        self.nameLabel.text = _cellModel.author;
        self.replyTimeLabel.text = _cellModel.created_time;
        
        [self.headerImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",Globe.baseUrlStr,_cellModel.userhead]] placeholderImage:nil];
    }

}

@end

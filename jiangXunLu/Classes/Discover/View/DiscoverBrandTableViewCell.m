//
//  DiscoverBrandTableViewCell.m
//  jiangXunLu
//
//  Created by 金莹 on 16/8/30.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "DiscoverBrandTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "jiangXunLu-swift.h"
#import "JXLDiscoverBrandModel.h"
#import "YYDateFormatterTool.h"

@interface DiscoverBrandTableViewCell()
@property (strong, nonatomic) JXLDiscoverBrandModel *cellModel;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *createdTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UILabel *operation_modeLabel;
@property (weak, nonatomic) IBOutlet UILabel *areaLabel;
@property (weak, nonatomic) IBOutlet UILabel *postion;
@property (weak, nonatomic) IBOutlet UILabel *industryLabel;
@property (weak, nonatomic) IBOutlet UILabel *readCountLabel;
@property (weak, nonatomic) IBOutlet UIImageView *coverImageView;

@end

@implementation DiscoverBrandTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (CGFloat)cellHeight{
    return 152;
}

- (void)configureCell:(id)cellModel{
    if (_cellModel != cellModel) {
        _cellModel = cellModel;
        
//        [[YYDateFormatterTool shareInstance] setDateFormat:@"yyy-MM-dd HH:mm:ss"];
//        NSDate *date = [[YYDateFormatterTool shareInstance] dateFromString:_cellModel.created_time];
//        [[YYDateFormatterTool shareInstance]  setDateFormat:@"yyyy-MM-dd"];
//        NSString *createDate = [[YYDateFormatterTool shareInstance] stringFromDate:date];
        
        self.titleLabel.text = _cellModel.title;
        self.createdTimeLabel.text = _cellModel.created_time;
        self.locationLabel.text = _cellModel.expand_area;
        self.operation_modeLabel.text = _cellModel.operation_mode;
        self.areaLabel.text = _cellModel.store_area;
        self.postion.text = _cellModel.position;
        self.industryLabel.text = _cellModel.industry;
        self.readCountLabel.text = [NSString stringWithFormat:@"%@",_cellModel.read_count];
        
        [self.coverImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",Globe.baseUrlStr,_cellModel.cover]]];
        
    }

}
@end

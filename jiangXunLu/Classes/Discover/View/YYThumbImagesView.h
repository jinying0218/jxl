//
//  YYThumbImagesView.h
//  AddImageScrollView
//
//  Created by 金莹 on 16/9/5.
//  Copyright © 2016年 YY. All rights reserved.
//

#import <UIKit/UIKit.h>

@class YYThumbImagesView;
typedef void(^ReFreshThumbViewBlock)(YYThumbImagesView *thumbView);

@protocol YYThumbImagesViewDelegate <NSObject>

- (void)addImageButtonClick:(UIButton *)button;
- (void)deleteImageButton:(NSUInteger)deleteIndex;
@end

@interface YYThumbImagesView : UIView
@property (assign, nonatomic) id<YYThumbImagesViewDelegate>delegate;


- (void)insertOneImage:(UIImage *)insertImage refresh:(ReFreshThumbViewBlock)refreshBlock;
- (void)layoutAllButtons;
@end

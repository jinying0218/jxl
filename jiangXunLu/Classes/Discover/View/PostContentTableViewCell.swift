//
//  PostContentTableViewCell.swift
//  jiangXunLu
//
//  Created by liuxb on 16/7/17.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit
import SnapKit

class PostContentTableViewCell: UITableViewCell {

    var discoverPost: DiscoverPostModel? {
        didSet{
            self.titleLabel.text = discoverPost?.title
            if let head = discoverPost?.userhead {
                let url = NSURL(string: Globe.baseUrlStr + head)!
                self.iconView.sd_setImageWithURL(url)
            }
            self.authorDate.text = (discoverPost?.author)! + " " + (discoverPost?.show_time)!
            if let postContent = discoverPost?.postContent {
                //内容类型1.html代码段;2.纯文字3图片4声音5影音
                let divString = "<div style='width:100%;height:100%;color:#666666'>" + postContent.content! + "</div>"
                self.webView.loadHTMLString(divString, baseURL: nil)
            }
            
        }
    }
    var refreshWebViewBlock:(()->Void)?
    var tapHeaderBlock:((String)->Void)?
    
    private var viewWrapper: UIView!
    private var titleLabel: UILabel!
    var iconView: UIImageView!
    private var authorDate: UILabel!
    private var webView: UIWebView!
    
    static var cellUpdateHeight:CGFloat = 0
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .None
        self.backgroundColor = UIColor.clearColor()
        
        let margin:CGFloat = 10
        let largeFont = UIFont.boldSystemFontOfSize(18)
        let smallFont = UIFont.systemFontOfSize(14)
        
        viewWrapper = UIView()
        viewWrapper.backgroundColor = UIColor.whiteColor()
        self.contentView.addSubview(viewWrapper)
        viewWrapper.snp_makeConstraints { (make) in
            make.edges.equalTo(self.contentView).offset(EdgeInsets(top: margin*0.5, left: margin*0.5, bottom: -margin*6.5, right: -margin*0.5))
        }
        
        titleLabel = UILabel()
        titleLabel.sizeToFit()
        titleLabel.font = largeFont
        titleLabel.textColor = UIColor.blackColor()
        viewWrapper.addSubview(titleLabel)
        titleLabel.snp_makeConstraints { (make) in
            make.left.equalTo(viewWrapper).offset(margin)
            make.top.equalTo(viewWrapper).offset(margin)
            make.right.equalTo(viewWrapper).offset(-margin)
            make.height.equalTo(17)
        }
        
        iconView = UIImageView()
        iconView.backgroundColor = UIColor.lightGrayColor()
        iconView.layer.cornerRadius = 35/2
        iconView.layer.masksToBounds = true
        iconView.userInteractionEnabled = true
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(self.singleTapAction(_:)))
        iconView.addGestureRecognizer(singleTap)
        viewWrapper.addSubview(iconView)
        iconView.snp_makeConstraints { (make) in
            make.left.equalTo(titleLabel)
            make.top.equalTo(titleLabel.snp_bottom).offset(margin)
            make.size.equalTo(CGSize(width: 35, height: 35))
        }
        
        authorDate = UILabel()
        authorDate.font = smallFont
        authorDate.sizeToFit()
        authorDate.textColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1.0)
        viewWrapper.addSubview(authorDate)
        authorDate.snp_makeConstraints { (make) in
            make.left.equalTo(iconView.snp_right).offset(margin)
            make.centerY.equalTo(iconView)
            make.right.lessThanOrEqualTo(viewWrapper).offset(-margin)
        }
        
        webView = UIWebView()
        webView.scalesPageToFit = false
        webView.scrollView.bounces = false
        webView.backgroundColor = UIColor.whiteColor()
        webView.delegate = self
        viewWrapper.addSubview(webView)
        webView.snp_makeConstraints { (make) in
            make.left.equalTo(viewWrapper).offset(margin)
            make.top.equalTo(authorDate.snp_bottom).offset(margin*2)
            make.right.equalTo(viewWrapper).offset(-margin)
            make.height.equalTo(10)
        }
        
        // 分割线
        let horizonLine = UIView()
        horizonLine.backgroundColor = UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
        self.contentView.addSubview(horizonLine)
        horizonLine.snp_makeConstraints { (make) in
            make.left.right.equalTo(self.contentView)
            make.top.equalTo(viewWrapper.snp_bottom).offset(margin*0.5)
            make.height.equalTo(0.3)
        }
        
        // 评论
        let commentView = UIView()
        commentView.backgroundColor = UIColor.whiteColor()
        self.contentView.addSubview(commentView)
        commentView.snp_makeConstraints { (make) in
            make.top.equalTo(horizonLine.snp_bottom).offset(margin)
            make.left.equalTo(self.contentView).offset(margin*0.5)
            make.right.equalTo(self.contentView).offset(-margin*0.5)
            make.height.equalTo(40)
        }
        
        let commentLabel = self.createTitle("评论")
        commentView.addSubview(commentLabel)
        commentLabel.snp_makeConstraints { (make) in
            make.left.equalTo(commentView).offset(margin)
            make.centerY.equalTo(commentView)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func createTitle(title: String) -> UILabel{
        let label = UILabel()
        label.text = title
        label.textColor = UIColor(red: 0.996, green: 0.804, blue: 0.031, alpha: 1.00)
        label.font = UIFont.boldSystemFontOfSize(18)
        label.sizeToFit()
        return label
    }
    
    func singleTapAction(recognizer: UIGestureRecognizer) {
        if self.tapHeaderBlock != nil {
            let uid = String(self.discoverPost!.created_by!)
            self.tapHeaderBlock!(uid)
        }
    }
}

extension PostContentTableViewCell: UIWebViewDelegate {
    func webViewDidFinishLoad(webView: UIWebView) {
        let fittingSize = webView.sizeThatFits(CGSizeZero)
        PostContentTableViewCell.cellUpdateHeight = CGRectGetMaxY(webView.frame) + fittingSize.height + 70
        webView.frame = CGRect(x: webView.frame.origin.x, y: webView.frame.origin.y, width: fittingSize.width, height: fittingSize.height)
        //刷新页面
        if self.refreshWebViewBlock != nil {
            self.refreshWebViewBlock!()
        }
    }
}




























































//
//  DiscoverProjectTableViewCell.h
//  jiangXunLu
//
//  Created by 金莹 on 16/8/30.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DiscoverProjectTableViewCell : UITableViewCell

+ (CGFloat)cellHeight;
- (void)configureCell:(id)cellModel;
@end

//
//  DiscoverInfoTableViewCell.m
//  jiangXunLu
//
//  Created by 金莹 on 16/8/30.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "DiscoverInfoTableViewCell.h"
#import "JXLDiscoverInfoModel.h"
#import "UIImageView+WebCache.h"
#import "jiangXunLu-swift.h"
#import "YYDateFormatterTool.h"

@interface DiscoverInfoTableViewCell()
@property (strong, nonatomic) JXLDiscoverInfoModel *cellModel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *createdTimeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *coverImageView;
@property (weak, nonatomic) IBOutlet UILabel *summerLabel;
@property (weak, nonatomic) IBOutlet UILabel *readCountLabel;

//@property (strong, nonatomic) 

@end

@implementation DiscoverInfoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (CGFloat)cellHeight{
    return 152;
}

- (void)configureCell:(id)cellModel{
    NSLog(@"%@",cellModel);
    if (_cellModel != cellModel) {
        _cellModel = cellModel;
        
//        [[YYDateFormatterTool shareInstance] setDateFormat:@"yyy-MM-dd HH:mm:ss"];
//        NSDate *date = [[YYDateFormatterTool shareInstance] dateFromString:_cellModel.created_time];
//        [[YYDateFormatterTool shareInstance]  setDateFormat:@"yyyy-MM-dd"];
//        NSString *createDate = [[YYDateFormatterTool shareInstance] stringFromDate:date];
        
        self.titleLabel.text = _cellModel.title;
        self.createdTimeLabel.text = _cellModel.created_time;
        self.summerLabel.text = _cellModel.summary;
        self.readCountLabel.text = [NSString stringWithFormat:@"%@",_cellModel.read_count];

        [self.coverImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",Globe.baseUrlStr,_cellModel.cover]]];
        
        
    }
}


@end

//
//  YYThumbImagesView.m
//  AddImageScrollView
//
//  Created by 金莹 on 16/9/5.
//  Copyright © 2016年 YY. All rights reserved.
//

#import "YYThumbImagesView.h"
#import "Masonry.h"
#import "EXTScope.h"
#import "BlocksKit+UIKit.h"

#define LD_OptionTag 1000
#define MarginX 15
#define Button_Width 108
#define Button_Height 81
#define Div_padding 10


@interface YYThumbImagesView ()
@property (strong, nonatomic) UIButton *addImageButton;
@property (strong, nonatomic) UIButton *lastButton;
@property (strong, nonatomic) NSMutableArray *buttonArray;
@property (assign, nonatomic) CGFloat buttonWidth;
@property (copy, nonatomic) ReFreshThumbViewBlock refreshBlock;

@end

@implementation YYThumbImagesView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self configureBaseView];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self configureBaseView];
    }
    return self;
}

- (void)configureBaseView{
    
    self.buttonWidth = ([UIScreen mainScreen].bounds.size.width - Div_padding * 2 - MarginX * 2)/3;
    
    self.backgroundColor = [UIColor clearColor];
    self.buttonArray = [[NSMutableArray alloc] init];
    self.addImageButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.addImageButton setImage:[UIImage imageNamed:@"find_picture_added"] forState:UIControlStateNormal];
    [self addSubview:self.addImageButton];
    [self.addImageButton addTarget:self action:@selector(addImageButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    @weakify(self);
    [self.addImageButton mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.size.mas_equalTo(CGSizeMake(self.buttonWidth, Button_Height));
        make.left.equalTo(self.mas_left).offset(MarginX);
        make.top.equalTo(self.mas_top);
    }];

}

- (void)awakeFromNib{
    [super awakeFromNib];
}


- (void)addImageButtonClick:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(addImageButtonClick:)]) {
        [self.delegate addImageButtonClick:sender];
    }
}

- (void)layoutSubviews{
    [super layoutSubviews];

}
//删除
- (void)optionButtonClick:(UIButton *)button{
    
    [UIAlertView bk_showAlertViewWithTitle:nil message:@"删除后将无法恢复，确定删除？" cancelButtonTitle:@"取消" otherButtonTitles:@[@"删除"] handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
        switch (buttonIndex) {
            case 1:{
                NSUInteger location = [self.buttonArray indexOfObject:button];
                
                [button removeFromSuperview];
                [self.buttonArray removeObject:button];
                
                [self layoutAllButtons];
                [self layoutIfNeeded];
                
                if (self.delegate && [self.delegate respondsToSelector:@selector(deleteImageButton:)]) {
                    [self.delegate deleteImageButton:location];
                }

            }
                break;
            default:
                break;
        }
    }];
    
}


- (void)insertOneImage:(UIImage *)insertImage refresh:(ReFreshThumbViewBlock)refreshBlock{

    _refreshBlock = refreshBlock;
    self.addImageButton.hidden = NO;
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.tag = LD_OptionTag + 1;
    [button setImage:insertImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(optionButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:button];
    
    [self.buttonArray addObject:button];
    @weakify(self);
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.size.mas_equalTo(CGSizeMake(self.buttonWidth, Button_Height));
        if (self.lastButton) {
            if (self.buttonArray.count <= 3) {
                //第一行
                make.top.equalTo(self.lastButton.mas_top);
                make.left.equalTo(self.lastButton.mas_right).with.offset(Div_padding);
                
            }else {
                if (self.buttonArray.count == 4) {
                    //第二行，第一张
                    make.left.equalTo(self.mas_left).with.offset(MarginX);
                    make.top.equalTo(self.mas_top).with.offset(Button_Height + Div_padding);
                }else {
                    //第二行
                    make.top.equalTo(self.lastButton.mas_top);
                    make.left.equalTo(self.lastButton.mas_right).with.offset(Div_padding);
                }
            }
        }else {
            //第一行，第一张
            make.top.equalTo(self.mas_top);
            make.left.equalTo(self.mas_left).with.offset(MarginX);
        }
    }];
    
    UILabel *deleteLabel = [[UILabel alloc] init];
    deleteLabel.text = @"删除";
    deleteLabel.textAlignment = NSTextAlignmentCenter;
    deleteLabel.textColor = [UIColor whiteColor];
    deleteLabel.font = [UIFont systemFontOfSize:16];
    
    [button addSubview:deleteLabel];
    
    [deleteLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    self.lastButton = button;
    
    [self.addImageButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.size.mas_equalTo(CGSizeMake(self.buttonWidth, Button_Height));
        make.left.equalTo(self.lastButton.mas_right).with.offset(Div_padding);
        make.top.equalTo(self.lastButton.mas_top);
    }];
    [self layoutIfNeeded];
    
    if (self.buttonWidth + self.addImageButton.frame.origin.x > [UIScreen mainScreen].bounds.size.width) {
        //如果超出屏幕宽度 --> 更新thumbView的大小
        [self updateViewHeight:(self.buttonWidth * 2 + Div_padding)];
        
        //更新添加按钮位置
        [self.addImageButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.size.mas_equalTo(CGSizeMake(self.buttonWidth, Button_Height));
            make.left.equalTo(self.mas_left).with.offset(MarginX);
            make.top.equalTo(self.mas_top).with.offset(Button_Height + Div_padding);
        }];

    }else {
        [self.addImageButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.size.mas_equalTo(CGSizeMake(self.buttonWidth, Button_Height));
            make.left.equalTo(self.lastButton.mas_right).with.offset(Div_padding);
            make.top.equalTo(self.lastButton.mas_top);
        }];
    }

    if (self.buttonArray.count >= 6) {
        self.addImageButton.hidden = YES;
    }
    
}

- (void)layoutAllButtons{
    
    self.lastButton = nil;
    self.addImageButton.hidden = NO;
    
    [self updateViewHeight:Button_Height];
    @weakify(self);
    if (self.buttonArray.count == 0) {
        [self.addImageButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.size.mas_equalTo(CGSizeMake(self.buttonWidth, Button_Height));
            make.left.equalTo(self.mas_left).with.offset(MarginX);
            make.top.equalTo(self.mas_top);
        }];
        return;
    }
    int index = 1;
    for (UIButton *button in self.buttonArray) {
        
        [button mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.size.mas_equalTo(CGSizeMake(self.buttonWidth, Button_Height));
            if (self.lastButton) {
                if (index <= 3) {
                    //第一行
                    make.top.equalTo(self.lastButton.mas_top);
                    make.left.equalTo(self.lastButton.mas_right).with.offset(Div_padding);
                    
                }else {
                    if (index == 4) {
                        //第二行，第一张
                        make.left.equalTo(self.mas_left).with.offset(MarginX);
                        make.top.equalTo(self.mas_top).with.offset(Button_Height + Div_padding);
                    }else {
                        //第二行
                        make.top.equalTo(self.lastButton.mas_top);
                        make.left.equalTo(self.lastButton.mas_right).with.offset(Div_padding);
                    }
                }
            }else {
                //第一行，第一张
                make.top.equalTo(self.mas_top);
                make.left.equalTo(self.mas_left).with.offset(MarginX);
            }
        }];

        
        self.lastButton = button;
        
        [self.addImageButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.size.mas_equalTo(CGSizeMake(self.buttonWidth, Button_Height));
            if (self.lastButton) {
                make.left.equalTo(self.lastButton.mas_right).with.offset(Div_padding);
            }else {
                make.left.equalTo(self.mas_left).with.offset(MarginX);
            }
            make.top.equalTo(self.mas_top);
        }];
        [self layoutIfNeeded];
        
        if (self.buttonWidth + self.addImageButton.frame.origin.x > [UIScreen mainScreen].bounds.size.width) {
            //如果超出屏幕宽度 --> 更新thumbView的大小
            [self updateViewHeight:(self.buttonWidth * 2 + Div_padding)];
            //更新添加按钮位置
            
            [self.addImageButton mas_remakeConstraints:^(MASConstraintMaker *make) {
                @strongify(self);
                make.size.mas_equalTo(CGSizeMake(self.buttonWidth, Button_Height));
                make.left.equalTo(self.mas_left).with.offset(MarginX);
                make.top.equalTo(self.mas_top).with.offset(Button_Height + Div_padding);
            }];
        }else {
            [self.addImageButton mas_remakeConstraints:^(MASConstraintMaker *make) {
                @strongify(self);
                make.size.mas_equalTo(CGSizeMake(self.buttonWidth, Button_Height));
                make.left.equalTo(self.lastButton.mas_right).with.offset(Div_padding);
                make.top.equalTo(self.lastButton.mas_top);
            }];
        }
        index ++;
    }
    
    if (self.buttonArray.count >= 6) {
        self.addImageButton.hidden = YES;
    }
}

- (void)updateViewHeight:(CGFloat)newHeight{
    CGRect newBounds = [UIScreen mainScreen].bounds;
    newBounds.size.height = newHeight;
    self.bounds = newBounds;
    for (NSLayoutConstraint *oneConstraint in self.constraints) {
        if (oneConstraint.firstAttribute == NSLayoutAttributeHeight) {
            oneConstraint.constant = newHeight;
        }
    }
    if (self.refreshBlock) {
        self.refreshBlock(self);
    }
}

@end

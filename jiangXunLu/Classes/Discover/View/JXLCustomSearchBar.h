//
//  JXLCustomSearchBar.h
//  jiangXunLu
//
//  Created by 金莹 on 16/9/11.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <UIKit/UIKit.h>

//typedef void(^CategoryButtonHandler)(void);
//typedef void(^CancelButtonHandler)(void);



@interface JXLCustomSearchBar : UIView
@property (copy, nonatomic) void(^categoryButtonHandler)(UIButton *);
@property (copy, nonatomic) void(^cancelButtonHandler)(void);
@property (copy, nonatomic) void(^searchButtonHandler)(NSString *);


@property (weak, nonatomic) IBOutlet UIImageView *indicatorImageView;
@property (strong, nonatomic) NSString *ftype;
@property (strong, nonatomic) NSString *textString;

- (void)setFtype:(NSString *)ftype completionBlock:(void (^)(NSString *ftypeString))chooseFtypeCompletion;
@end

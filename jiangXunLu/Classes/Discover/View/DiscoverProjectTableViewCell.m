//
//  DiscoverProjectTableViewCell.m
//  jiangXunLu
//
//  Created by 金莹 on 16/8/30.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "DiscoverProjectTableViewCell.h"
#import "JXLDiscoverProjectModel.h"
#import "UIImageView+WebCache.h"
#import "jiangXunLu-swift.h"
#import "YYDateFormatterTool.h"

@interface DiscoverProjectTableViewCell ()
@property (strong, nonatomic) JXLDiscoverProjectModel *cellModel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *createdTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UILabel *projectTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *areaLabel;
@property (weak, nonatomic) IBOutlet UILabel *showDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *authorLabel;
@property (weak, nonatomic) IBOutlet UILabel *readCountLabel;
@property (weak, nonatomic) IBOutlet UIImageView *coverImageView;

@end

@implementation DiscoverProjectTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


+ (CGFloat)cellHeight{
    return 152;
}

- (void)configureCell:(id)cellModel{
    if (_cellModel != cellModel) {
        _cellModel = cellModel;
        
        [[YYDateFormatterTool shareInstance] setDateFormat:@"yyy-MM-dd"];
        NSDate *opening_time = [[YYDateFormatterTool shareInstance] dateFromString:_cellModel.opening_time];
        [[YYDateFormatterTool shareInstance]  setDateFormat:@"yyyy-MM"];
        NSString *opening_timeString = [[YYDateFormatterTool shareInstance] stringFromDate:opening_time];

        self.titleLabel.text = _cellModel.title;
        self.createdTimeLabel.text = _cellModel.created_time;
        self.locationLabel.text = [NSString stringWithFormat:@"%@ %@",_cellModel.addr_prov,_cellModel.addr_city];
        self.projectTypeLabel.text = _cellModel.type;
        self.areaLabel.text = _cellModel.area;
        self.showDateLabel.text = opening_timeString;
        self.authorLabel.text = _cellModel.company;
        self.readCountLabel.text = [NSString stringWithFormat:@"%@",_cellModel.read_count];
        
        [self.coverImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",Globe.baseUrlStr,_cellModel.cover]]];

    }
}
@end

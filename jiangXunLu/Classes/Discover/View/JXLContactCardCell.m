//
//  JXLContactCardCell.m
//  jiangXunLu
//
//  Created by 金莹 on 16/9/2.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXLContactCardCell.h"
#import "HexColor.h"
#import "UIImageView+WebCache.h"
#import "jiangXunLu-swift.h"
#import "JXLPersonCardModel.h"

@interface JXLContactCardCell ()
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *sexImageView;
@property (weak, nonatomic) IBOutlet UILabel *jobLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyLabel;
@property (weak, nonatomic) IBOutlet UILabel *placeLabel;
@property (copy, nonatomic) HeaderImageViewClickHandler handler;

@end

@implementation JXLContactCardCell

- (void)awakeFromNib {
    [super awakeFromNib];

    self.containerView.layer.cornerRadius = 1;
    self.containerView.layer.borderColor = [UIColor colorWithHexString:@"cccccc"].CGColor;
    self.containerView.layer.borderWidth = 0.5;
    
    self.headImageView.layer.cornerRadius = self.headImageView.width/2;
    self.headImageView.layer.masksToBounds = YES;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - action

- (IBAction)headerImageClick:(UIButton *)sender {
    self.handler(_cellData);

}

#pragma mark -

- (void)setCellData:(JXLPersonCardModel *)cellData headerClickHandler:(HeaderImageViewClickHandler)handler{
    
    _handler = handler;
    if (_cellData != cellData) {
        _cellData = cellData;
        
        [self.headImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",Globe.baseUrlStr,_cellData.head]] placeholderImage:nil];
        
        self.nameLabel.text = _cellData.name;
        self.jobLabel.text = _cellData.job;
        self.companyLabel.text = _cellData.company;
        self.placeLabel.text = _cellData.region;
        
        if ([_cellData.sex isEqualToString:@"1"]) {
            [self.sexImageView setImage:[UIImage imageNamed:@"find_sex_man"]];
        }else if ([_cellData.sex isEqualToString:@"2"]){
            [self.sexImageView setImage:[UIImage imageNamed:@"find_sex_woman"]];
        }else {
            self.sexImageView.hidden = YES;
        }
    }

}

- (void)setCellData:(JXLPersonCardModel *)cellData{
    if (_cellData != cellData) {
        _cellData = cellData;
        
        [self.headImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",Globe.baseUrlStr,_cellData.head]] placeholderImage:nil];
        
        self.nameLabel.text = _cellData.name;
        self.jobLabel.text = _cellData.job;
        self.companyLabel.text = _cellData.company;
        self.placeLabel.text = _cellData.region;
        
        if ([_cellData.sex isEqualToString:@"1"]) {
            [self.sexImageView setImage:[UIImage imageNamed:@"find_sex_man"]];
        }else {
            [self.sexImageView setImage:[UIImage imageNamed:@"find_sex_woman"]];
        }


    }
}

@end

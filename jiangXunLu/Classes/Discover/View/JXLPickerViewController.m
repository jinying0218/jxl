//
//  JXLPickerViewController.m
//  jiangXunLu
//
//  Created by 金莹 on 16/9/9.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXLPickerViewController.h"
#import "JXLPickerTableViewCell.h"
#import "YYDateFormatterTool.h"

static NSString *const PickerControllerCellIdentifier = @"PickerControllerCellIdentifier";

@interface JXLPickerViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightConstraint;
@property (strong, nonatomic) NSArray *tableDatas;
@property (copy, nonatomic) JXLChooseCompetionBlock completionBlock;
@property (strong, nonatomic) NSString *selectedString;
@property (strong, nonatomic) NSMutableArray *selectedArray;

@property (weak, nonatomic) IBOutlet UIView *pickerBackView;
@property (weak, nonatomic) IBOutlet UIDatePicker *datepicker;

@property (assign, nonatomic) BOOL isAllowMultableSelection;

@end

@implementation JXLPickerViewController

- (instancetype)initWithTableDatas:(NSArray *)tableDatas
{
    self = [super init];
    if (self) {
        _tableDatas = tableDatas;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    
    [self.tableView registerNib:[UINib nibWithNibName:@"JXLPickerTableViewCell" bundle:nil] forCellReuseIdentifier:PickerControllerCellIdentifier];
    self.tableView.tableFooterView = [[UIView alloc] init];
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (self.pickerType == JXLDatePickerType) {
        self.datepicker.hidden = NO;
        self.pickerBackView.hidden = NO;
        self.tableView.hidden = YES;
        self.headerView.hidden = YES;
    }else {
        self.tableView.hidden = NO;
        self.headerView.hidden = NO;
        self.datepicker.hidden = YES;
        self.pickerBackView.hidden = YES;
        
        if (self.pickerType == JXLMultableChooseType) {
            self.tableView.allowsMultipleSelection = YES;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - setter

- (void)setIsAllowMultableSelection:(BOOL)isAllowMultableSelection{
    _isAllowMultableSelection = isAllowMultableSelection;
    self.tableView.allowsMultipleSelection = isAllowMultableSelection;
    if (isAllowMultableSelection) {
        _selectedArray = [[NSMutableArray alloc] initWithCapacity:2];
    }
}

- (void)setTableDatas:(NSArray *)tableDatas chooseCompletion:(JXLChooseCompetionBlock)completion{
    
    _completionBlock = completion;
    _tableDatas = tableDatas;
    self.pickerType = JXLTableViewType;

    if (_tableDatas.count < 7) {
        self.tableViewHeightConstraint.constant = 62 * (_tableDatas.count);
    }else {
        self.tableViewHeightConstraint.constant = 372;
    }
    [self.tableView reloadData];
}

- (void)setDatePickerViewChooseCompletion:(JXLChooseCompetionBlock)completion{
    _completionBlock = completion;
    self.pickerType = JXLDatePickerType;

}

#pragma mark - action

- (IBAction)confirmButtonClick:(UIButton *)sender {
    if (self.isAllowMultableSelection) {
        if (self.selectedArray.count > 0) {
            NSString *string = [self.selectedArray componentsJoinedByString:@"  "];
            self.completionBlock(string);
        }
    }else {
        if (_selectedString) {
            self.completionBlock(_selectedString);
        }
    }

    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}
- (IBAction)backViewTap:(UITapGestureRecognizer *)sender {
    if (self.pickerType == JXLDatePickerType) {
        
        [[YYDateFormatterTool shareInstance] setDateFormat:@"yyyy-MM-dd"];
        NSString *dateString = [[YYDateFormatterTool shareInstance] stringFromDate:self.datepicker.date];
        self.completionBlock(dateString);
    }
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

#pragma mark - tableView dataSource && delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.tableDatas.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    JXLPickerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:PickerControllerCellIdentifier];
    
    NSString *string = self.tableDatas[indexPath.row];
    [cell configureCell:string];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.isAllowMultableSelection) {
        [self.selectedArray addObject:self.tableDatas[indexPath.row]];
    }else {
        self.selectedString = self.tableDatas[indexPath.row];
    }
    JXLPickerTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell setSelected:YES animated:YES];
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView.allowsMultipleSelection) {
        [self.selectedArray removeObject:self.tableDatas[indexPath.row]];
    }else {
        JXLPickerTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        [cell setSelected:NO animated:YES];
    }

}

#pragma mark - 

@end

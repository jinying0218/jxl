//
//  JXLRealseCell.m
//  jiangXunLu
//
//  Created by 金莹 on 16/9/5.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXLRealseCell.h"
#import "JXLReaseModel.h"
#import "jiangXunLu-swift.h"

@interface JXLRealseCell()<YYTextViewDelegate>
@property (copy, nonatomic) ActionHandler handler;
@property (copy, nonatomic) ActiveBlock activeBlock;
@property (weak, nonatomic) IBOutlet YYTextView *contentInputView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *inputViewLeadingConstraint;
@property (strong, nonatomic) NSArray *accesoryArray;
@end

@implementation JXLRealseCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.contentInputView.delegate = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureTableView:(UITableView *)tableView CellData:(id)cellData indexPath:(NSIndexPath *)indexPath handler:(ActionHandler)handler activeBlock:(ActiveBlock)activeBlock{
    _handler = handler;
    _activeBlock = activeBlock;
    self.accessoryView = nil;
    self.accessoryType = UITableViewCellAccessoryNone;
    if ([cellData isKindOfClass:[JXLReaseModel class]]) {
        _cellModel = cellData;
    }
    
    switch (indexPath.section) {
        case 0:{
            self.titleLabel.hidden = NO;
            self.separatorInset = UIEdgeInsetsMake( 0, 100, 0, 15);
            self.inputViewLeadingConstraint.constant = 100;
        }
            break;
        default:{
            self.separatorInset = UIEdgeInsetsZero;
            self.inputViewLeadingConstraint.constant = 15;
            self.titleLabel.hidden = YES;
        }
            break;
    }
    //计算内容高度
    if (_cellModel.content) {
        self.contentInputView.text = _cellModel.content;
        
        YYTextContainer *container = [YYTextContainer containerWithSize:CGSizeMake(self.contentInputView.width, MAXFLOAT)];
        YYTextLayout *textLayout = [YYTextLayout layoutWithContainer:container text:[[NSAttributedString alloc] initWithString:_cellModel.content]];
        _cellModel.textLayout = textLayout;
        _cellModel.cellHeight = textLayout.textBoundingSize.height;
        
        self.contentInputView.size = textLayout.textBoundingSize;
    }
    
    self.contentInputView.placeholderText = _cellModel.placeholder;;
    self.titleLabel.text = _cellModel.title;
    
    if ([self.accesoryArray containsObject:_cellModel.keyString]) {
        self.contentInputView.userInteractionEnabled = NO;
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        if ([_cellModel.keyString isEqualToString:@"area"] || [_cellModel.keyString isEqualToString:@"store_area"]) {
            UILabel *label = [[UILabel alloc] init];
            label.text = @"㎡";
            label.font = [UIFont systemFontOfSize:12];
            label.frame = CGRectMake(0, 0, 20, 20);
            self.accessoryView = label;
            self.contentInputView.userInteractionEnabled = YES;
        }
    }else {
        self.contentInputView.userInteractionEnabled = YES;
    }
    
    if ([_cellModel.keyString isEqualToString:@"store_count"] || [_cellModel.keyString isEqualToString:@"area"] || [_cellModel.keyString isEqualToString:@"store_area"]) {
        self.contentInputView.keyboardType = UIKeyboardTypeNumberPad;
    }else {
        self.contentInputView.keyboardType = UIKeyboardTypeDefault;
    }
}

- (void)setInputView:(UIView *)view{
    self.contentInputView.inputView = view;
}

- (BOOL)textViewShouldBeginEditing:(YYTextView *)textView{
    self.activeBlock(self);
    return YES;
}

- (void)textViewDidEndEditing:(YYTextView *)textView{
    if (!textView.text || textView.text.length == 0) {
        return;
    }
//    self.accessoryView = nil;
    self.cellModel.content = textView.text;
    self.handler(textView.text);
}
- (BOOL)textView:(YYTextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"]) {
        NSLog(@"回车");
        [textView resignFirstResponder];
    }
    
    if ([self.cellModel.keyString isEqualToString:@"summary"]) {
        if (textView.text.length >= 100 && text.length > range.length){
            return NO;
        }
        return YES;
    }
    
    return YES;
}

- (void)textViewDidChange:(YYTextView *)textView{
    if ([self.cellModel.keyString isEqualToString:@"summary"]) {
        if (textView.markedTextRange == nil && textView.text.length >= 100) {
            textView.text = [textView.text substringWithRange:NSMakeRange(0, 100)];
        }
    }
}
/*
 type：项目类型 brand_industry~~industry~：招商需求/所属业态  position:项目定位   operation_mode：开店方式     property_usage：物业使用方式
 */
- (NSArray *)accesoryArray{
    if (!_accesoryArray) {
        _accesoryArray = @[@"type",@"project_type",@"brand_industry",@"industry",@"position",@"operation_mode",@"property_usage",@"opening_time",@"address",@"expand_area",@"area",@"store_area"];
    }
    return _accesoryArray;
}

@end

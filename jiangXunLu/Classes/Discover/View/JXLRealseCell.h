//
//  JXLRealseCell.h
//  jiangXunLu
//
//  Created by 金莹 on 16/9/5.
//  Copyright © 2016年 teamOne. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "YYText.h"

@class JXLReaseModel;
typedef void(^ActionHandler)(NSString *inputString);
typedef void(^ActiveBlock)(UIView *activeView);

@interface JXLRealseCell : UITableViewCell
@property (strong, nonatomic) JXLReaseModel *cellModel;

- (void)configureTableView:(UITableView *)tableView CellData:(id)cellData indexPath:(NSIndexPath *)indexPath handler:(ActionHandler)handler activeBlock:(ActiveBlock)activeBlock;
- (void)setInputView:(UIView *)view;
@end

//
//  JXLReplyListCell.h
//  jiangXunLu
//
//  Created by 金莹 on 16/9/2.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@class JXLFindReplyModel;
@interface JXLReplyListCell : UITableViewCell
@property (strong, nonatomic) JXLFindReplyModel *cellModel;


- (void)setCellModel:(JXLFindReplyModel *)cellModel headerImageClick:(HeaderImageViewClickHandler)handler;
@end

//
//  JXLChooseRealseItemView.h
//  jiangXunLu
//
//  Created by 金莹 on 16/9/4.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@protocol JXLChooseRealseItemViewDelegate <NSObject>

- (void)chooseRealseItemType:(JXLDisType)ftype;

@end

@interface JXLChooseRealseItemView : UIView
@property (assign, nonatomic) id<JXLChooseRealseItemViewDelegate>delegate;
@end

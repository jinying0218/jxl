//
//  JXLPickerTableViewCell.h
//  jiangXunLu
//
//  Created by 金莹 on 16/9/9.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JXLPickerTableViewCell : UITableViewCell

- (void)configureCell:(NSString *)string;
@end

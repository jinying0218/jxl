//
//  JXLPickerViewController.h
//  jiangXunLu
//
//  Created by 金莹 on 16/9/9.
//  Copyright © 2016年 teamOne. All rights reserved.
//


#import <UIKit/UIKit.h>

typedef enum JXLPickerType {
    JXLTableViewType = 0,
    JXLDatePickerType,
    JXLMultableChooseType
} JXLPickerType;


typedef void(^JXLChooseCompetionBlock)(NSString *selectedString);


@interface JXLPickerViewController : UIViewController
@property (assign, nonatomic) JXLPickerType pickerType;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;


- (void)setTableDatas:(NSArray *)tableDatas chooseCompletion:(JXLChooseCompetionBlock)completion;

- (void)setDatePickerViewChooseCompletion:(JXLChooseCompetionBlock)completion;

- (void)setIsAllowMultableSelection:(BOOL)isAllowMultableSelection;
@end

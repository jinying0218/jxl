//
//  JXLChooseRealseItemView.m
//  jiangXunLu
//
//  Created by 金莹 on 16/9/4.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXLChooseRealseItemView.h"
#import "XXNibBridge.h"

@interface JXLChooseRealseItemView()<XXNibBridge>
@property (weak, nonatomic) IBOutlet UIButton *inforButton;

@end

@implementation JXLChooseRealseItemView
- (void)awakeFromNib{
    [super awakeFromNib];
    
    
}

- (IBAction)tapBackView:(UITapGestureRecognizer *)sender {
    self.hidden = YES;
}


- (IBAction)infoButtonClick:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(chooseRealseItemType:)]) {
        [self.delegate chooseRealseItemType:kDiscoverInfo];
    }
}

- (IBAction)projectButtonClick:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(chooseRealseItemType:)]) {
        [self.delegate chooseRealseItemType:kDiscoverProject];
    }
}

- (IBAction)brandButtonClick:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(chooseRealseItemType:)]) {
        [self.delegate chooseRealseItemType:kDiscoverBrand];
    }
}


@end

//
//  ReplyPersonTableViewCell.swift
//  jiangXunLu
//
//  Created by liuxb on 16/7/17.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit
import SnapKit

let replyPersonCommentLargeFont = UIFont.systemFontOfSize(16)
let replyPersonCommentSmallFont = UIFont.systemFontOfSize(14)
let replyPersonBaseMargin: CGFloat = 10
let commentConstrainedSize = CGSize(width: kScreen_Bounds.width - replyPersonBaseMargin * 4 - 60, height: CGFloat.max)

class ReplyPersonTableViewCell: UITableViewCell {

    var replyPost: ReplyPostModel?{
        didSet{
            if let head = replyPost?.userhead {
                let url = NSURL(string: Globe.baseUrlStr + head)!
                self.iconView.sd_setImageWithURL(url)
            }
            self.nameLabel.text = replyPost?.author
            self.dateLabel.text = replyPost?.created_time
            self.commentLabel.text = replyPost?.content
        }
    }
    var tapHeaderBlock:((String)->Void)?
    private var iconView: UIImageView!
    private var nameLabel: UILabel!
    private var dateLabel: UILabel!
    private var commentLabel: UILabel!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .None
        self.backgroundColor = UIColor.clearColor()
        
        let viewWrapper = UIView()
        viewWrapper.backgroundColor = UIColor.whiteColor()
        self.contentView.addSubview(viewWrapper)
        viewWrapper.snp_makeConstraints { (make) in
            make.edges.equalTo(self.contentView).offset(EdgeInsets(top: replyPersonBaseMargin*0.5, left: replyPersonBaseMargin*0.5, bottom: -replyPersonBaseMargin*0.5, right: -replyPersonBaseMargin*0.5))
        }
        
        iconView = UIImageView()
        iconView.layer.cornerRadius = 25
        iconView.layer.masksToBounds = true
        iconView.userInteractionEnabled = true
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(self.singleTapAction(_:)))
        iconView.addGestureRecognizer(singleTap)
        iconView.backgroundColor = UIColor.lightGrayColor()
        viewWrapper.addSubview(iconView)
        iconView.snp_makeConstraints { (make) in
            make.left.equalTo(viewWrapper).offset(replyPersonBaseMargin)
            make.top.equalTo(viewWrapper).offset(replyPersonBaseMargin)
            make.size.equalTo(50)
        }
        
        nameLabel = UILabel()
        nameLabel.sizeToFit()
        nameLabel.font = replyPersonCommentLargeFont
        nameLabel.textColor = UIColor.darkGrayColor()
        viewWrapper.addSubview(nameLabel)
        nameLabel.snp_makeConstraints { (make) in
            make.left.equalTo(iconView.snp_right).offset(replyPersonBaseMargin)
            make.top.equalTo(iconView)
            make.right.equalTo(viewWrapper).offset(-replyPersonBaseMargin)
        }
        
        dateLabel = UILabel()
        dateLabel.font = replyPersonCommentSmallFont
        dateLabel.sizeToFit()
        dateLabel.textColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1.0)
        viewWrapper.addSubview(dateLabel)
        dateLabel.snp_makeConstraints { (make) in
            make.left.equalTo(nameLabel)
            make.top.equalTo(nameLabel.snp_bottom).offset(replyPersonBaseMargin*0.5)
            make.right.lessThanOrEqualTo(viewWrapper).offset(-replyPersonBaseMargin)
        }
        
        commentLabel = UILabel()
        commentLabel.font = replyPersonCommentLargeFont
        commentLabel.sizeToFit()
        commentLabel.textColor = UIColor.blackColor()
        commentLabel.numberOfLines = 0
        viewWrapper.addSubview(commentLabel)
        commentLabel.snp_makeConstraints { (make) in
            make.left.equalTo(nameLabel)
            make.top.equalTo(iconView.snp_bottom)
            make.right.lessThanOrEqualTo(viewWrapper).offset(-replyPersonBaseMargin)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func singleTapAction(recognizer: UIGestureRecognizer) {
        if self.tapHeaderBlock != nil {
            let uid = String(self.replyPost!.created_by!)
            self.tapHeaderBlock!(uid)
        }
    }

}

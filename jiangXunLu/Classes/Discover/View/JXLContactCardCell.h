//
//  JXLContactCardCell.h
//  jiangXunLu
//
//  Created by 金莹 on 16/9/2.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@class JXLPersonCardModel;
@interface JXLContactCardCell : UITableViewCell
@property (strong, nonatomic) JXLPersonCardModel *cellData;


- (void)setCellData:(JXLPersonCardModel *)cellData headerClickHandler:(HeaderImageViewClickHandler)handler;
@end

//
//  JXLPickerTableViewCell.m
//  jiangXunLu
//
//  Created by 金莹 on 16/9/9.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXLPickerTableViewCell.h"

@interface JXLPickerTableViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *selectedIndicator;

@end

@implementation JXLPickerTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    if (selected) {
        self.selectedIndicator.hidden = NO;
    }else {
        self.selectedIndicator.hidden = YES;
    }
    // Configure the view for the selected state
}

- (void)configureCell:(NSString *)string{
    self.titleLabel.text = string;
}

@end

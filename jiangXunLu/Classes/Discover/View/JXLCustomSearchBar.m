//
//  JXLCustomSearchBar.m
//  jiangXunLu
//
//  Created by 金莹 on 16/9/11.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXLCustomSearchBar.h"
#import "HexColor.h"
#import "XXNibBridge.h"

@interface JXLCustomSearchBar ()<XXNibBridge,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *searchBackView;
@property (weak, nonatomic) IBOutlet UIButton *categoryButton;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

@end

@implementation JXLCustomSearchBar

- (void)layoutSubviews{
    [super layoutSubviews];
    [self commit];
}

- (void)commit{
    self.ftype = self.categoryButton.titleLabel.text;
    self.textField.delegate = self;
    self.searchBackView.layer.cornerRadius = self.searchBackView.bounds.size.height/2;
    self.searchBackView.layer.masksToBounds = YES;
    
    NSAttributedString *attributeString = [[NSAttributedString alloc] initWithString:@"搜索" attributes:@{NSForegroundColorAttributeName : [UIColor colorWithHexString:@"eeeeee"],
                                                                                                        NSFontAttributeName : [UIFont systemFontOfSize:14]}];
    self.textField.attributedPlaceholder = attributeString;
}

- (IBAction)categoryButtonClick:(UIButton *)sender {
    self.categoryButtonHandler(sender);
//    NSLog(@"%s",__FUNCTION__);
}

- (IBAction)cancelButtonClick:(UIButton *)sender {
    self.cancelButtonHandler();
    self.textField.text = nil;
    [self endEditing:YES];
}

#pragma mark - setterx5

- (void)setFtype:(NSString *)ftype completionBlock:(void (^)(NSString *ftypeString))chooseFtypeCompletion{
    [self.categoryButton setTitle:ftype forState:UIControlStateNormal];
    if ([ftype isEqualToString:@"资讯"]) {
        _ftype = @"info";
        
    }else if ([ftype isEqualToString:@"项目"]){
        _ftype = @"project";
        
    }else {
        _ftype = @"brand";
    }

    chooseFtypeCompletion(_ftype);
}
- (void)setTextString:(NSString *)textString{
    _textString = textString;
    self.textField.text = textString;
}
- (void)setCategoryButtonHandler:(void (^)(UIButton *))categoryButtonHandler{
    _categoryButtonHandler = categoryButtonHandler;
//    NSLog(@"%s",__FUNCTION__);

}

- (void)setCancelButtonHandler:(void (^)(void))cancelButtonHandler{
    _cancelButtonHandler = cancelButtonHandler;
//    NSLog(@"%s",__FUNCTION__);

}

- (void)setSearchButtonHandler:(void (^)(NSString *))searchButtonHandler{
    _searchButtonHandler = searchButtonHandler;
}

#pragma mark - textfieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.textField resignFirstResponder];
    self.textString = self.textField.text;
    self.searchButtonHandler(textField.text);
    return YES;
}
@end

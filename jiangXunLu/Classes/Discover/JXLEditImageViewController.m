//
//  JXLEditImageViewController.m
//  jiangXunLu
//
//  Created by 金莹 on 16/9/24.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXLEditImageViewController.h"
#import "HexColor.h"
#import "UIView+Sizes.h"
#import "UIImage+Resizing.h"

@interface JXLEditImageViewController ()<UIScrollViewDelegate>
//@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;
@property (weak, nonatomic) IBOutlet UIView *targetSizeView;
//@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIView *topBackView;
@property (weak, nonatomic) IBOutlet UIView *bottomBackView;

@property (strong, nonatomic) UIImage *editImage;
@property (strong, nonatomic) UIImage *sourceImage;
@property (copy, nonatomic) EditImageCompletion completion;
@end

@implementation JXLEditImageViewController
- (instancetype)initWithImage:(UIImage *)editImage completion:(EditImageCompletion)completion
{
    self = [super init];
    if (self) {
        _sourceImage = editImage;
        _editImage = editImage;
        _imageView.image = editImage;
        _completion = completion;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
//    self.scrollView.maximumZoomScale = 2.0;
//    self.scrollView.minimumZoomScale = 0.5;
    
    self.imageView.image = self.editImage;
    
    UIPinchGestureRecognizer *pinchGestureRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchView:)];
    [self.imageView addGestureRecognizer:pinchGestureRecognizer];
    
    // 移动手势
    UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panView:)];
    [self.imageView addGestureRecognizer:panGestureRecognizer];
    
}
// 处理缩放手势
- (void) pinchView:(UIPinchGestureRecognizer *)pinchGestureRecognizer
{
    UIView *view = pinchGestureRecognizer.view;
    if (pinchGestureRecognizer.state == UIGestureRecognizerStateBegan || pinchGestureRecognizer.state == UIGestureRecognizerStateChanged) {
        view.transform = CGAffineTransformScale(view.transform, pinchGestureRecognizer.scale, pinchGestureRecognizer.scale);
        pinchGestureRecognizer.scale = 1;
    }
}

// 处理拖拉手势
- (void) panView:(UIPanGestureRecognizer *)panGestureRecognizer
{
    UIView *view = panGestureRecognizer.view;
    if (panGestureRecognizer.state == UIGestureRecognizerStateBegan || panGestureRecognizer.state == UIGestureRecognizerStateChanged) {
        CGPoint translation = [panGestureRecognizer translationInView:view.superview];
        [view setCenter:(CGPoint){view.center.x + translation.x, view.center.y + translation.y}];
        [panGestureRecognizer setTranslation:CGPointZero inView:view.superview];
    }
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
//    self.containerView.bounds = CGRectMake( 0, 0, self.view.width , self.containerView.height * 2);
//    self.scrollView.contentSize = CGSizeMake(self.view.width + 1, self.containerView.height * 2);
//    NSLog(@"%@",self.scrollView);
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ACTION
- (IBAction)cancelButtonClick:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)confirmButtonClick:(UIButton *)sender {

    UIGraphicsBeginImageContextWithOptions(self.view.size, NO, 1.0);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *screenImage = UIGraphicsGetImageFromCurrentImageContext();
    
    CGPoint point = [self.imageView convertPoint:self.targetSizeView.origin toView:self.imageView];
    self.editImage = [UIImage imageWithCGImage:CGImageCreateWithImageInRect(screenImage.CGImage, CGRectMake( 0, point.y, self.targetSizeView.width, self.targetSizeView.height))];
    
    UIGraphicsEndImageContext();
    
    self.completion(self.editImage);
    [self dismissViewControllerAnimated:YES completion:nil];

}

//- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    NSLog(@"%@",scrollView);
//}
//
//- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
////    return self.containerView;
//}
//
//- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(nullable UIView *)view atScale:(CGFloat)scale{
////    self.scrollView.contentSize = CGSizeMake(self.containerView.width, self.containerView.height * 2);
////    view.center = CGPointMake( self.view.centerX, self.view.centerY);
////    [self.scrollView setZoomScale:scale animated:YES];
////    [self centerScrollViewContents];
//    
////    NSLog(@"%f---:%@",scale,scrollView);
//}

//- (void)centerScrollViewContents {
//    
//    
//    CGSize boundsSize = self.scrollView.bounds.size;
//    CGRect contentsFrame = self.containerView.frame;
//    
//    if (contentsFrame.size.width < boundsSize.width) {
//        contentsFrame.origin.x = floorf((boundsSize.width - contentsFrame.size.width) / 2.0f);
//    } else {
//        contentsFrame.origin.x = 0.0f;
//    }
//    
//    if (contentsFrame.size.height < boundsSize.height) {
//        contentsFrame.origin.y = floorf((boundsSize.height - contentsFrame.size.height) / 2.0f);
//    } else {
//        contentsFrame.origin.y = 0.0f;
//    }
//    if (!CGRectEqualToRect(self.containerView.frame, contentsFrame))
//        self.containerView.frame = contentsFrame;
//}
@end

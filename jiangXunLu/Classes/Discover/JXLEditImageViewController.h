//
//  JXLEditImageViewController.h
//  jiangXunLu
//
//  Created by 金莹 on 16/9/24.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^EditImageCompletion)(UIImage *editedImage);

@interface JXLEditImageViewController : UIViewController

- (instancetype)initWithImage:(UIImage *)editImage completion:(EditImageCompletion)completion;
@end

//
//  JXLDiscoverDetailModel.h
//  jiangXunLu
//
//  Created by 金莹 on 16/9/1.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <Foundation/Foundation.h>
@class JXLPersonCardModel;
@interface JXLDiscoverDetailModel : NSObject

@property (strong, nonatomic) NSString *collect_count;
@property (strong, nonatomic) NSString *collect_status;
@property (strong, nonatomic) NSArray *connected;
@property (strong, nonatomic) NSString *content_url;
@property (strong, nonatomic) NSString *cover;
@property (strong, nonatomic) JXLPersonCardModel *created_by;
@property (strong, nonatomic) NSString *identifier;
@property (strong, nonatomic) NSString *read_count;
@property (strong, nonatomic) NSString *reply_count;
@property (strong, nonatomic) NSString *share_url;
@property (strong, nonatomic) NSString *share_title;
@property (strong, nonatomic) NSString *share_desc;
@property (strong, nonatomic) NSString *share_img;
@property (strong, nonatomic) NSString *show_time;
@property (strong, nonatomic) NSString *status;
@property (assign, nonatomic) NSUInteger thumbsup;   //点赞数
@property (strong, nonatomic) NSString *thumbsup_status;    //点赞状态
@property (strong, nonatomic) NSString *title;


@end

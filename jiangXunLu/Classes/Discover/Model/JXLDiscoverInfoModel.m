//
//  JXLDiscoverInfoModel.m
//  jiangXunLu
//
//  Created by 金莹 on 16/8/31.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXLDiscoverInfoModel.h"

@implementation JXLDiscoverInfoModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"author" : @"author",
             @"cover" : @"cover",
             @"created_by" : @"created_by",
             @"created_time" : @"created_time",
             @"discoverInfo_id" : @"id",
             @"read_count" : @"read_count",
             @"show_time" : @"show_time",
             @"summary" : @"summary",
             @"title" : @"title",
             @"reply_content" : @"reply_content"};
}

@end

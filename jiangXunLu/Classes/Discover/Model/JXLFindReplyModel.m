//
//  JXLFindReplyModel.m
//  jiangXunLu
//
//  Created by 金莹 on 16/9/2.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXLFindReplyModel.h"

@implementation JXLFindReplyModel

- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dic {
    
    NSString *identifier = dic[@"id"];
    if (!identifier) return NO;
    _identifier = identifier;
    return YES;
}
@end

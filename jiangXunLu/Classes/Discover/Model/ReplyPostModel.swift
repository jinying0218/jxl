//
//  ReplyPostModel.swift
//  jiangXunLu
//
//  Created by liuxb on 16/7/17.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit

class ReplyPostModel: NSObject {
    var author:String?
    var content:String? {
        didSet{
            //计算评论内容的高度
            let contentSize = content!.textSizeWithFont(replyPersonCommentLargeFont, constrainedToSize: commentConstrainedSize)
            self.cellHeight = contentSize.height + 50 + replyPersonBaseMargin * 3
        }
    }
    var created_by: NSNumber?
    var created_time: String?
    var id: NSNumber?
    var ip: String?
    var parent_id: NSNumber?
    var post_id: NSNumber?
    var status: NSNumber?
    var thumbsdown: NSNumber?
    var thumbsup: NSNumber?
    var type: NSNumber?
    var uid_to:String?
    var updated_by: String?
    var updated_time: String?
    var userhead: String?
    
    var cellHeight: CGFloat?
    
    override init() {
        super.init()
    }
    
    init(dict:[String:AnyObject]){
        super.init()
        setValuesForKeysWithDictionary(dict)
    }
    
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {
        print("提示：ReplyPostModel中\(key)无对应数据")
    }
    
    override var description: String{
        let properties = ["author","content","created_by","created_time","id","ip","parent_id","status","thumbsdown","thumbsup","type","uid_to","updated_by","updated_time","userhead"]
        let dict = dictionaryWithValuesForKeys(properties)
        return "\(dict)"
    }
}






























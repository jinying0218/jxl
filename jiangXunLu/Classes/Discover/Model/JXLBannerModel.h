//
//  JXLBannerModel.h
//  jiangXunLu
//
//  Created by 金莹 on 16/8/30.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JXLBannerModel : NSObject
@property (strong, nonatomic) NSString *banner_Id;
@property (strong, nonatomic) NSString *banner_src;
@property (strong, nonatomic) NSString *banner_ftype;
@end

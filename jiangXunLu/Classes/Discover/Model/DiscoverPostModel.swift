//
//  DiscoverPostModel.swift
//  jiangXunLu
//
//  Created by liuxb on 16/7/17.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit

class PostContent: NSObject {
    var content: String?
    var type: NSNumber? //内容类型1.html代码段;2.纯文字3图片4声音5影音
    init(dict:[String:AnyObject]){
        super.init()
        setValuesForKeysWithDictionary(dict)
    }
    
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {
        print("提示：PostContent中\(key)无对应数据")
    }
    
    override var description: String{
        let properties = ["content","type"]
        let dict = dictionaryWithValuesForKeys(properties)
        return "\(dict)"
    }
}

class DiscoverPostModel: NSObject {
    var author: String?
    var content_id: NSNumber?
    var cover: String?
    var created_by: NSNumber?
    var created_time: String?
    var id: NSNumber?
    var party: NSNumber?
    var readcount: NSNumber?
    var replycount: NSNumber?
    var show_time: String?
    var status: NSNumber?
    var summary: String?
    var title: String?
    var updated_by: String?
    var updated_time: String?
    var weight: NSNumber?
    var userhead: String?
    var postContent: PostContent?
    
    
    init(dict:[String:AnyObject]){
        super.init()
        setValuesForKeysWithDictionary(dict)
    }
    
    override func setValue(value: AnyObject?, forKey key: String) {
        if key == "content" {
            self.postContent = PostContent(dict: value as! [String:AnyObject])
        } else {
            super.setValue(value, forKey: key)
        }
    }
    
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {
        print("提示：DiscoverPostModel中\(key)无对应数据")
    }
    
    override var description: String{
        let properties = ["author","content_id","cover","created_by","created_time","id","party","readcount","replycount","show_time","status","summary","title","updated_by","updated_time","weight","postContent","userhead"]
        let dict = dictionaryWithValuesForKeys(properties)
        return "\(dict)"
    }
    
}
























































































//
//  JXLPersonCardModel.m
//  jiangXunLu
//
//  Created by 金莹 on 16/9/1.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXLPersonCardModel.h"

@implementation JXLPersonCardModel

- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dic {
    NSString *identifier = dic[@"id"];
    if (!identifier) return NO;
    _identifier = identifier;
    return YES;
}
@end

//
//  JXLRealseBrandModel.h
//  jiangXunLu
//
//  Created by 金莹 on 16/9/6.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JXLRealseBrandModel : NSObject
@property (strong, nonatomic) NSString *addr_city;
@property (strong, nonatomic) NSString *addr_detail;
@property (strong, nonatomic) NSString *addr_prov;
@property (strong, nonatomic) NSString *collect_count;
@property (strong, nonatomic) NSString *company;            //开发商
@property (strong, nonatomic) NSString *connected;
@property (strong, nonatomic) NSString *content;            //内容
@property (strong, nonatomic) NSString *content_id;
@property (strong, nonatomic) NSString *cover;
@property (strong, nonatomic) NSString *created_by;
@property (strong, nonatomic) NSString *created_time;
@property (strong, nonatomic) NSString *expand_area;
@property (strong, nonatomic) NSString *identifier;
@property (strong, nonatomic) NSString *industry;     //招商需求
@property (strong, nonatomic) NSString *operation_mode;        //开店方式
@property (strong, nonatomic) NSMutableArray *pics;
@property (strong, nonatomic) NSString *position;           //项目定位
@property (strong, nonatomic) NSString *project_type;       //可进驻类型
@property (strong, nonatomic) NSString *property_usage;     //物业使用方式
@property (strong, nonatomic) NSString *read_count;
@property (strong, nonatomic) NSString *reply_count;
@property (strong, nonatomic) NSString *show_time;        //开业时间
@property (strong, nonatomic) NSString *status;
@property (strong, nonatomic) NSString *store_area;       //商业面积
@property (strong, nonatomic) NSString *store_count;      //门店数量
@property (strong, nonatomic) NSString *thumbsup;
@property (strong, nonatomic) NSString *title;


@end

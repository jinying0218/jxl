//
//  JXLDiscoverInfoModel.h
//  jiangXunLu
//
//  Created by 金莹 on 16/8/31.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JXLDiscoverInfoModel : NSObject
@property (strong, nonatomic) NSString *author;
@property (strong, nonatomic) NSString *cover;
@property (strong, nonatomic) NSString *created_by;
@property (strong, nonatomic) NSString *created_time;
@property (strong, nonatomic) NSString *discoverInfo_id;
@property (strong, nonatomic) NSNumber *read_count;
@property (strong, nonatomic) NSString *show_time;
@property (strong, nonatomic) NSString *summary;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *reply_content;

@end

//
//  JXLSearchHotModel.h
//  jiangXunLu
//
//  Created by 金莹 on 16/9/11.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JXLSearchHotModel : NSObject
@property (strong, nonatomic) NSString *keyword;
@property (strong, nonatomic) NSString *scount;

@end


//
//  JXLDiscoverBrandModel.m
//  jiangXunLu
//
//  Created by 金莹 on 16/8/31.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXLDiscoverBrandModel.h"

@implementation JXLDiscoverBrandModel
+ (NSDictionary *)modelCustomPropertyMapper{
    return @{@"addr_city" : @"addr_city",
             @"addr_detail" : @"addr_detail",
             @"addr_prov" : @"addr_prov",
             @"author" : @"author",
             @"cover" : @"cover",
             @"created_by" : @"created_by",
             @"created_time" : @"created_time",
             @"expand_area" : @"expand_area",
             @"brand_id" : @"id",
             @"industry" : @"industry",
             @"operation_mode" : @"operation_mode",
             @"position" : @"position",
             @"read_count" : @"read_count",
             @"show_time" : @"show_time",
             @"store_area" : @"store_area",
             @"title" : @"title",
             @"reply_content" : @"reply_content"
             };
}

@end

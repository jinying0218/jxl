//
//  JXLDiscoverDetailModel.m
//  jiangXunLu
//
//  Created by 金莹 on 16/9/1.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXLDiscoverDetailModel.h"
#import "JXLPersonCardModel.h"
#import "YYModel.h"

@implementation JXLDiscoverDetailModel


- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dic {
    NSString *identifier = dic[@"id"];
    if (!identifier){
        return NO;
    }

    NSArray *connected = dic[@"connected"];
    NSDictionary *created_by = dic[@"created_by"];

    _connected = [NSArray yy_modelArrayWithClass:[JXLPersonCardModel class] json:connected];
    _created_by = [JXLPersonCardModel yy_modelWithDictionary:created_by];
    _identifier = identifier;
    return YES;
}
@end

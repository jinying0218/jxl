//
//  JXLRealseProjectModel.h
//  jiangXunLu
//
//  Created by 金莹 on 16/9/6.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JXLRealseProjectModel : NSObject

@property (strong, nonatomic) NSString *addr_city;
@property (strong, nonatomic) NSString *addr_detail;
@property (strong, nonatomic) NSString *addr_prov;
@property (strong, nonatomic) NSString *area;
@property (strong, nonatomic) NSString *brand_industry;     //招商需求
@property (strong, nonatomic) NSString *collect_count;
@property (strong, nonatomic) NSString *company;            //开发商
@property (strong, nonatomic) NSString *company_manage;     //
@property (strong, nonatomic) NSString *connected;
@property (strong, nonatomic) NSString *content;            //内容
@property (strong, nonatomic) NSString *content_id;
@property (strong, nonatomic) NSString *cover;
@property (strong, nonatomic) NSString *created_by;
@property (strong, nonatomic) NSString *created_time;
@property (strong, nonatomic) NSString *identifier;
@property (strong, nonatomic) NSString *opening_time;       //开业时间
@property (strong, nonatomic) NSMutableArray *pics;
@property (strong, nonatomic) NSString *position;           //项目定位
@property (strong, nonatomic) NSString *read_count;
@property (strong, nonatomic) NSString *reply_count;
@property (strong, nonatomic) NSString *thumbsup;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *updated_time;

@end

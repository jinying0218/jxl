//
//  JXLDiscoverProjectModel.h
//  jiangXunLu
//
//  Created by 金莹 on 16/8/31.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JXLDiscoverProjectModel : NSObject
@property (strong, nonatomic) NSString *addr_city;
@property (strong, nonatomic) NSString *addr_detail;
@property (strong, nonatomic) NSString *addr_prov;
@property (strong, nonatomic) NSString *area;
@property (strong, nonatomic) NSString *author;
@property (strong, nonatomic) NSString *company;
@property (strong, nonatomic) NSString *cover;
@property (strong, nonatomic) NSString *created_by;
@property (strong, nonatomic) NSString *created_time;
@property (strong, nonatomic) NSString *project_id;
@property (strong, nonatomic) NSString *opening_time;
@property (strong, nonatomic) NSString *position;
@property (strong, nonatomic) NSString *read_count;
@property (strong, nonatomic) NSString *show_time;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *type;

@property (strong, nonatomic) NSString *reply_content;

@end

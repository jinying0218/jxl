//
//  JXLReaseModel.h
//  jiangXunLu
//
//  Created by 金莹 on 16/9/5.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "YYText.h"

@interface JXLReaseModel : NSObject
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *placeholder;
@property (strong, nonatomic) NSString *content;
@property (strong, nonatomic) NSString *keyString;

@property (strong, nonatomic) YYTextLayout *textLayout;
@property (assign, nonatomic) CGFloat cellHeight;

- (instancetype)initWithTitleString:(NSString *)title placeholder:(NSString *)placeholder content:(NSString *)content keyString:(NSString *)keyString;
@end

//
//  JXLRealseInfoModel.m
//  jiangXunLu
//
//  Created by 金莹 on 16/9/5.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXLRealseInfoModel.h"
#import "YYModel.h"

@implementation JXLRealseInfoModel

- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dic {
    
    NSString *identifier = dic[@"id"];
    if (!identifier) return NO;
    _identifier = identifier;
    
    NSString *pics = dic[@"pics"];
    if (![pics isEqual:[NSNull null]]) {
        NSData *data = [pics dataUsingEncoding:NSASCIIStringEncoding];
        NSError *error = nil;
        id jsonObject = [NSJSONSerialization JSONObjectWithData:data
                                                        options:NSJSONReadingAllowFragments
                                                          error:&error];
        if (error) {
            NSLog(@"%@",error.description);
        }else{
            if ([jsonObject isKindOfClass:[NSArray class]]) {
                self.pics = [[NSMutableArray alloc] initWithArray:jsonObject];
            }
        }
    }

    
    return YES;
}

@end

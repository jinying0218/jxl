//
//  JXLBannerModel.m
//  jiangXunLu
//
//  Created by 金莹 on 16/8/30.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXLBannerModel.h"

@implementation JXLBannerModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"banner_Id" : @"id",
             @"banner_src" : @"src",
             @"banner_ftype" : @"ftype"};
}
@end

//
//  JXLRealseInfoModel.h
//  jiangXunLu
//
//  Created by 金莹 on 16/9/5.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JXLRealseInfoModel : NSObject
@property (strong, nonatomic) NSString *connected;
@property (strong, nonatomic) NSString *content;
@property (strong, nonatomic) NSString *content_id;
@property (strong, nonatomic) NSString *cover;
@property (strong, nonatomic) NSString *created_by;
@property (strong, nonatomic) NSString *created_time;
@property (strong, nonatomic) NSString *identifier;
@property (strong, nonatomic) NSMutableArray *pics;
@property (strong, nonatomic) NSString *show_time;
@property (strong, nonatomic) NSString *summary;
@property (strong, nonatomic) NSString *title;

@end

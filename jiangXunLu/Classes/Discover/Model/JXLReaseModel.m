//
//  JXLReaseModel.m
//  jiangXunLu
//
//  Created by 金莹 on 16/9/5.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXLReaseModel.h"

@implementation JXLReaseModel

- (instancetype)initWithTitleString:(NSString *)title placeholder:(NSString *)placeholder content:(NSString *)content keyString:(NSString *)keyString
{
    self = [super init];
    if (self) {
        _title = title;
        _placeholder = placeholder;
        _content = content;
        _keyString = keyString;
    }
    return self;
}

@end

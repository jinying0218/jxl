//
//  JXLDiscoverProjectModel.m
//  jiangXunLu
//
//  Created by 金莹 on 16/8/31.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXLDiscoverProjectModel.h"

@implementation JXLDiscoverProjectModel
+ (NSDictionary *)modelCustomPropertyMapper{
    return @{@"addr_city" : @"addr_city",
             @"addr_detail" : @"addr_detail",
             @"addr_prov" : @"addr_prov",
             @"area" : @"area",
             @"author" : @"author",
             @"company" : @"company",
             @"cover" : @"cover",
             @"created_by" : @"created_by",
             @"created_time" : @"created_time",
             @"project_id" : @"id",
             @"opening_time" : @"opening_time",
             @"position" : @"position",
             @"read_count" : @"read_count",
             @"show_time" : @"show_time",
             @"title" : @"title",
             @"type" : @"type",
             @"reply_content" : @"reply_content"
             };
}

@end

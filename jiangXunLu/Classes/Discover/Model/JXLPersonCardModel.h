//
//  JXLPersonCardModel.h
//  jiangXunLu
//
//  Created by 金莹 on 16/9/1.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JXLPersonCardModel : NSObject
@property (strong, nonatomic) NSString *identifier;
@property (strong, nonatomic) NSString *company;
@property (strong, nonatomic) NSString *head;
@property (strong, nonatomic) NSString *job;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *region;
@property (strong, nonatomic) NSString *sex;

@end

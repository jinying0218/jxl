//
//  JXLFindReplyModel.h
//  jiangXunLu
//
//  Created by 金莹 on 16/9/2.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JXLFindReplyModel : NSObject
@property (strong, nonatomic) NSString *author;
@property (strong, nonatomic) NSString *content;
@property (strong, nonatomic) NSString *created_by;
@property (strong, nonatomic) NSString *created_time;
@property (strong, nonatomic) NSString *identifier;
@property (strong, nonatomic) NSString *ip;
@property (strong, nonatomic) NSString *list_id;
@property (strong, nonatomic) NSString *parent_id;
@property (strong, nonatomic) NSString *status;
@property (strong, nonatomic) NSString *thumbsdown;
@property (strong, nonatomic) NSString *thumbsup;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *uid_to;
@property (strong, nonatomic) NSString *userhead;

@end

//
//  JXLRealseInfoViewController.m
//  jiangXunLu
//
//  Created by 金莹 on 16/9/4.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXLRealseInfoViewController.h"
#import "jiangXunLu-swift.h"
#import "GlobalDefine.h"
#import "AFNTool.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageManager.h"

#import "YYModel.h"
#import "Masonry.h"
#import "EXTScope.h"
#import "MBProgressHUD+Add.h"
#import "KVOController.h"
#import "Hexcolor.h"
#import "BlocksKit+UIKit.h"
#import "UITableView+FDTemplateLayoutCell.h"

#import "JXLRealseCell.h"
#import "JXLRealseInfoModel.h"
#import "YYText.h"
#import "JXLReaseModel.h"

#import <MobileCoreServices/MobileCoreServices.h>
#import "YYThumbImagesView.h"
#import "UIImage+Resizing.h"
#import "JXLEditImageViewController.h"


static NSString *const kRealseCellIdentifier = @"kRealseCellIdentifier";
static NSString *const kRealseInfoSectionHeaderIdentifier = @"kRealseInfoSectionHeaderIdentifier";


@interface JXLRealseInfoViewController ()<UITableViewDelegate,UITableViewDataSource,YYThumbImagesViewDelegate>
@property (strong, nonatomic) UIImagePickerController *imagePickerController;
@property (strong, nonatomic) JXLEditImageViewController *editImageViewController;
@property (strong, nonatomic) UIAlertController *actionSheet;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *addCoverImageButton;
@property (weak, nonatomic) IBOutlet UIImageView *coverImageView;
@property (weak, nonatomic) IBOutlet UIView *chooseCoverBtnBackView;
@property (weak, nonatomic) IBOutlet UIButton *chooseCoverButton;

@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (strong, nonatomic) YYThumbImagesView *thumbImageView;
@property (strong, nonatomic) JXLRealseInfoModel *infoModel;
@property (strong, nonatomic) NSArray *tableDatasArray;

@property (assign, nonatomic) JXLRealseType realseType;
@property (strong, nonatomic) NSString *identifier;
@property (assign, nonatomic) BOOL isGetCover;       //是获取封面照片还是底部照片
@property (strong, nonatomic) UIView *activeCell;

@end

@implementation JXLRealseInfoViewController

- (instancetype)initWithEditStatus:(JXLRealseType)type identifier:(NSString *)identifier
{
    self = [super init];
    if (self) {
        _realseType = type;
        _identifier = identifier;
        if (type == kDiscoverRealse) {
            _infoModel = [[JXLRealseInfoModel alloc] init];
            _infoModel.pics = [[NSMutableArray alloc] initWithCapacity:2];
        }
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"资讯";
    [self setUpLeftbarButtonWithImage:UIIMGName(@"back_normal")];
    
    //发布
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setTitle:@"发布" forState:UIControlStateNormal];
    [rightButton setTitleColor:[UIColor colorWithHexString:@"FECD08"] forState:UIControlStateNormal];
    [rightButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [self setUpRightbarButtonWithCustomButton:rightButton];
    
    if (self.realseType == kDiscoverEdit) {
        //编辑
        [self loadEditDataFromNet];
    }
    
    [self prepareData];

    [self.tableView registerNib:[UINib nibWithNibName:@"JXLRealseCell" bundle:nil] forCellReuseIdentifier:kRealseCellIdentifier];
    [self.tableView registerClass:[UITableViewHeaderFooterView class] forHeaderFooterViewReuseIdentifier:kRealseInfoSectionHeaderIdentifier];
    self.tableView.backgroundColor = [UIColor colorWithHexString:@"eeeeee"];
    self.tableView.tableHeaderView = self.headerView;
    self.tableView.tableFooterView = self.thumbImageView;
    
    [self blindActionHandler];
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self addKeyboardNotification];
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self removeKeyboardNotification];
}

- (void)refreshUI{
    self.chooseCoverButton.hidden = self.infoModel.cover.length > 0 ? NO : YES;
    self.chooseCoverBtnBackView.hidden = self.chooseCoverButton.hidden;
    
    self.addCoverImageButton.hidden = !self.chooseCoverButton.hidden;
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - loadData
- (void)prepareData{
    
    if (self.infoModel.cover) {
        [self.coverImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",Globe.baseUrlStr,self.infoModel.cover]]];
        self.chooseCoverButton.hidden = self.infoModel.cover.length > 0 ? NO : YES;
        self.chooseCoverBtnBackView.hidden = self.chooseCoverButton.hidden;
        self.addCoverImageButton.hidden = !self.chooseCoverButton.hidden;
    }
    if (self.infoModel.pics) {
        @weakify(self);
        for (NSString *imageString in self.infoModel.pics) {
            NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",Globe.baseUrlStr,imageString]];
            [[SDWebImageManager sharedManager] downloadImageWithURL:imageURL options:SDWebImageRetryFailed | SDWebImageAvoidAutoSetImage progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                @strongify(self);
                [self.thumbImageView insertOneImage:image refresh:^(YYThumbImagesView *thumbView) {
                    @strongify(self);
                    self.tableView.tableFooterView = thumbView;
                }];
            }];
        }
    }
    
    JXLReaseModel *cellModel1 = [[JXLReaseModel alloc] initWithTitleString:@"标题名称" placeholder:@"如：西安火车站E米国际" content:(self.infoModel.title ? self.infoModel.title : nil) keyString:@"title"];
    JXLReaseModel *cellModel2 = [[JXLReaseModel alloc] initWithTitleString:@"摘要" placeholder:@"字数不超过100字" content:(self.infoModel.summary ? self.infoModel.summary : nil) keyString:@"summary"];
    JXLReaseModel *cellModel3 = [[JXLReaseModel alloc] initWithTitleString:nil placeholder:nil content:(self.infoModel.content ? self.infoModel.content : nil) keyString:@"content"];
    
    self.tableDatasArray = @[@[cellModel1,cellModel2],@[cellModel3]];
}

//下载编辑数据
- (void)loadEditDataFromNet{
    
    NSString *g_uid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
    NSString *g_randcode = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"randcode"]];
    
    NSDictionary *data = @{@"id" : self.identifier};

    NSDictionary *params = @{@"action" : @"find_info_edit_load",
                             @"uid" : g_uid,
                             @"randcode" : g_randcode,
                             @"data":[data yy_modelToJSONString]};
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    @weakify(self);
    [AFNTool post:Globe.baseUrlStr params:params success:^(id responsObject) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        //        NSLog(@"find_project_detail_load:%@",responsObject);
        if (responsObject && [responsObject isKindOfClass:[NSDictionary class]]) {
            NSInteger status = [responsObject[@"status"] integerValue];
            if (status == 1) {
                NSDictionary *info = [responsObject[@"info"] objectForKey:@"info"];
                self.infoModel = [JXLRealseInfoModel yy_modelWithDictionary:info];
                if (!self.infoModel.pics) {
                    self.infoModel.pics = [[NSMutableArray alloc] initWithCapacity:2];
                }
                [self prepareData];
                [self.tableView reloadData];
            }
        }
    } failure:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:error.description toView:self.view];
        
        NSLog(@"%@",error.description);
    }];
}

//上传二进制 图片
- (void)uploadImage:(UIImage *)uploadImage{
    NSString *g_uid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
    NSString *g_randcode = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"randcode"]];
    
    NSData *imageData = UIImageJPEGRepresentation(uploadImage, 0.8f);
    
    NSDictionary *data = @{@"type" : @"stream",
                           @"data" : imageData,
                           @"place" : @""};
    NSDictionary *params = @{@"action" : @"upload_img",
                             @"uid" : g_uid,
                             @"randcode" : g_randcode,
                             @"data":[data yy_modelToJSONString]};
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    @weakify(self);
    [AFNTool uploadRequestURL:Globe.baseUrlStr params:params fileData:imageData success:^(id responsObject) {
//        NSLog(@"%@",responsObject);
        @strongify(self);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if ([responsObject[@"status"] intValue] == 1) {
            NSString *urlString = [responsObject[@"info"] objectForKey:@"url"];
            
            if (self.isGetCover) {
                self.infoModel.cover = urlString;
                self.coverImageView.image = uploadImage;
                [self refreshUI];
            }else {
                [self.infoModel.pics addObject:urlString];
                [self.thumbImageView insertOneImage:uploadImage refresh:^(YYThumbImagesView *thumbView) {
                    @strongify(self);
                    self.tableView.tableFooterView = thumbView;
                }];
            }
        }else {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [MBProgressHUD showError:responsObject[@"msg"] toView:self.view];
        }
    } failure:^(NSError *error) {
//        NSLog(@"%@",error.description);
        @strongify(self);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:error.description toView:self.view];
    }];
}

//上传修改
- (void)uploadModifyInfoData{
    
    NSString *g_uid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
    NSString *g_randcode = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"randcode"]];
    
    
    NSDictionary *data = @{@"id" : self.identifier,
                           @"title" : self.infoModel.title ? self.infoModel.title : @"",
                           @"summary" : self.infoModel.summary ? self.infoModel.summary : @"",
                           @"content" : self.infoModel.content ? self.infoModel.content : @"",
                           @"cover" : self.infoModel.cover ? self.infoModel.cover : @"",
                           @"pics" : self.infoModel.pics ? [self.infoModel.pics yy_modelToJSONString] : @""};
    
    NSDictionary *params = @{@"action" : @"find_info_modify",
                             @"uid" : g_uid,
                             @"randcode" : g_randcode,
                             @"data":[data yy_modelToJSONString]};
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    @weakify(self);
    [AFNTool post:Globe.baseUrlStr params:params success:^(id responsObject) {
        @strongify(self);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (responsObject && [responsObject isKindOfClass:[NSDictionary class]]) {
            NSInteger status = [responsObject[@"status"] integerValue];
            if (status == 1) {
                [MBProgressHUD showSuccess:@"发布成功" toView:[UIApplication sharedApplication].keyWindow];
                [self.navigationController popViewControllerAnimated:YES];
            }else {
                [MBProgressHUD showError:responsObject[@"msg"] toView:self.view];
            }
        }
    } failure:^(NSError *error) {
        @strongify(self);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:error.description toView:self.view];
        //        NSLog(@"%@",error.description);
    }];

}

#pragma mark - action
- (void)blindActionHandler{
    @weakify(self);
    [self.addCoverImageButton bk_addEventHandler:^(id sender) {
        @strongify(self);
        self.isGetCover = YES;
        [self presentViewController:self.actionSheet animated:YES completion:nil];
    } forControlEvents:UIControlEventTouchUpInside];
    
    [self.chooseCoverButton bk_addEventHandler:^(id sender) {
        @strongify(self);
        self.isGetCover = YES;
        [self presentViewController:self.actionSheet animated:YES completion:nil];
    } forControlEvents:UIControlEventTouchUpInside];

}

- (void)leftBarItemTouchUpInside:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

/*
 发布
 */
- (void)rightBarItemTouchUpInside:(id)sender{
    
    
    if (!self.infoModel.cover || self.infoModel.cover.length == 0) {
        [MBProgressHUD showError:@"请选择封面图片" toView:self.view];
        return;
    }

    [self.view endEditing:YES];
    
    NSUInteger section = 0;
    for (NSArray *subArray in self.tableDatasArray) {
        for (JXLReaseModel *realseModel in subArray) {
            if ([realseModel.keyString isEqualToString:@"summary"]) {
                continue;
            }
            if (!realseModel.content || realseModel.content.length == 0) {
                [MBProgressHUD showError:@"请将信息填写完整" toView:self.view];
                NSUInteger location = [subArray indexOfObject:realseModel];
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:location inSection:section];
                NSLog(@"位置~~:%@~~location:%d",indexPath,(int)location);
                JXLRealseCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
                cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"find_remind"]];
                return;
            }
        }
        section ++;
    }
    
    [self.tableView reloadData];
    if (self.realseType == kDiscoverRealse) {
        NSString *g_uid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
        NSString *g_randcode = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"randcode"]];
        
        NSDictionary *data = @{@"title" : self.infoModel.title,
                               @"summary" : self.infoModel.summary ? self.infoModel.summary : @"",
                               @"content" : self.infoModel.content,
                               @"cover" : self.infoModel.cover,
                               @"pics" : [self.infoModel.pics yy_modelToJSONString]};
        
        NSDictionary *params = @{@"action" : @"find_info_release",
                                 @"uid" : g_uid,
                                 @"randcode" : g_randcode,
                                 @"data":[data yy_modelToJSONString]};
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        @weakify(self);
        [AFNTool post:Globe.baseUrlStr params:params success:^(id responsObject) {
            @strongify(self);
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (responsObject && [responsObject isKindOfClass:[NSDictionary class]]) {
                NSInteger status = [responsObject[@"status"] integerValue];
                if (status == 1) {
                    [MBProgressHUD showSuccess:@"发布成功" toView:[UIApplication sharedApplication].keyWindow];
                    [self.navigationController popViewControllerAnimated:YES];
                }else {
                    [MBProgressHUD showError:responsObject[@"msg"] toView:self.view];
                }
            }
        } failure:^(NSError *error) {
            @strongify(self);
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [MBProgressHUD showError:error.description toView:self.view];
            //        NSLog(@"%@",error.description);
        }];

    }else {
        [self uploadModifyInfoData];
    }
    
}

#pragma mark - YYThumbImagesViewDelegate
- (void)addImageButtonClick:(UIButton *)button{
    self.isGetCover = NO;
    [self presentViewController:self.actionSheet animated:YES completion:nil];
}

- (void)deleteImageButton:(NSUInteger)deleteIndex{
    [self.infoModel.pics removeObjectAtIndex:deleteIndex];
}

#pragma mark - tableView delegate && tableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.tableDatasArray.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.tableDatasArray[section] count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 0;
    }
    return 52;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 0) {
        return 0;
    }
    return 18;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return [[UIView alloc] init];
    }
    UITableViewHeaderFooterView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:kRealseInfoSectionHeaderIdentifier];
    headerView.contentView.backgroundColor = [UIColor colorWithHexString:@"eeeeee"];
    headerView.textLabel.font = [UIFont systemFontOfSize:14];
    headerView.textLabel.textColor = [UIColor colorWithHexString:@"333333"];
    headerView.textLabel.text = @"正文";
    return headerView;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section == 0) {
        return [[UIView alloc] init];
    }
    UITableViewHeaderFooterView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:kRealseInfoSectionHeaderIdentifier];
    headerView.contentView.backgroundColor = [UIColor colorWithHexString:@"eeeeee"];
    return headerView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    JXLReaseModel *cellModel = self.tableDatasArray[indexPath.section][indexPath.row];
    if (!cellModel.content || cellModel.content.length == 0) {
        if (indexPath.section == 1) {
            return 100;
        }
        return 50;
    }else {
        if (indexPath.section == 1) {
            return (cellModel.cellHeight + 30) < 100 ? 100 : (cellModel.cellHeight + 30);
        }
        return cellModel.cellHeight + 30;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    JXLRealseCell *cell = [tableView dequeueReusableCellWithIdentifier:kRealseCellIdentifier];
    
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}
- (void)configureCell:(JXLRealseCell *)cell atIndexPath:(NSIndexPath *)indexPath{
    cell.fd_isTemplateLayoutCell = YES;
    JXLReaseModel *cellModel = self.tableDatasArray[indexPath.section][indexPath.row];
    @weakify(self);
    [cell configureTableView:self.tableView CellData:cellModel indexPath:indexPath handler:^(NSString *inputString) {
        @strongify(self);
        if ([inputString isEqualToString:@""] || !inputString) {
            return ;
        }
        [self.infoModel setValue:inputString forKey:cellModel.keyString];
    } activeBlock:^(UIView *activeView) {
        @strongify(self);
        self.activeCell = activeView;
    }];
}
#pragma mark - getter
//- (JXLEditImageViewController *)editImageViewController{
//    if (!_editImageViewController) {
//        @weakify(self);
//        _editImageViewController = [[JXLEditImageViewController alloc] initWithImage:nil completion:^(UIImage *editedImage) {
//            @strongify(self);
//            self.coverImageView.image = editedImage;
//            [self refreshUI];
//        }];
//    }
//    return _editImageViewController;
//}
- (UIImagePickerController *)imagePickerController{
    if (!_imagePickerController) {
        _imagePickerController = [[UIImagePickerController alloc] init];
        _imagePickerController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        _imagePickerController.allowsEditing = NO;
        @weakify(self);
        [_imagePickerController setBk_didCancelBlock:^(UIImagePickerController *imagePickerController) {
            [imagePickerController dismissViewControllerAnimated:NO completion:nil];
        }];
        
        [_imagePickerController setBk_didFinishPickingMediaBlock:^(UIImagePickerController *imagePickerController, NSDictionary <NSString *,id>*info) {
            @strongify(self);

            UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
            if (!image) {
                image = [info objectForKey:UIImagePickerControllerOriginalImage];
            }

            
            [imagePickerController dismissViewControllerAnimated:YES completion:^{

                if (self.isGetCover) {
                    self.editImageViewController = [[JXLEditImageViewController alloc] initWithImage:image completion:^(UIImage *editedImage) {
                        @strongify(self);
                        [self uploadImage:editedImage];
                    }];

                    [self presentViewController:self.editImageViewController animated:NO completion:nil];
//                                        image = [image cropToSize:CGSizeMake(image.size.width, image.size.width * 3/5) usingMode:NYXCropModeCenter];
                }else {
                    [self uploadImage:image];
                }
            }];
        }];
    }
    return _imagePickerController;
}
- (UIAlertController *)actionSheet{
    if (!_actionSheet){
        _actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        @weakify(self);
        [_actionSheet addAction:[UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            @strongify(self);
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
                self.imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                [self presentViewController:self.imagePickerController animated:YES completion:nil];
            }
            [_actionSheet dismissViewControllerAnimated:YES completion:nil];
        }]];
        [_actionSheet addAction:[UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            @strongify(self);
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                self.imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:self.imagePickerController animated:YES completion:nil];
            }else {
                [MBProgressHUD showError:@"没有安装摄像头" toView:self.view];
            }

            [_actionSheet dismissViewControllerAnimated:YES completion:nil];
        }]];
        [_actionSheet addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [_actionSheet dismissViewControllerAnimated:YES completion:nil];
        }]];

    }
    return _actionSheet;
}

- (YYThumbImagesView *)thumbImageView{
    if (!_thumbImageView) {
        _thumbImageView = [[YYThumbImagesView alloc] init];
        _thumbImageView.delegate = self;
        _thumbImageView.height = 101;
    }
    return _thumbImageView;
}


#pragma mark - keyboardNotification

//注册键盘通知
- (void)addKeyboardNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
}

//移除键盘通知
- (void)removeKeyboardNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

//键盘显示
- (void)keyboardWillShow:(NSNotification*)notification
{
    CGRect keyboardBounds = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
//    float height =  keyboardBounds.size.height;
    float duringTime = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    CGSize kbSize = keyboardBounds.size;
    
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    NSLog(@"keyboardWasShown");
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    
    if (!CGRectContainsPoint(aRect, CGPointMake(self.activeCell.origin.x, self.activeCell.origin.y + self.activeCell.height))) {
        
        CGPoint scrollPoint = CGPointMake(0.0, (self.activeCell.frame.origin.y + self.activeCell.height - aRect.size.height) + 44);
        
        [UIView animateWithDuration:duringTime
                              delay:0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             [self.view layoutIfNeeded];
                             [self.tableView setContentOffset:scrollPoint animated:YES];
                         } completion:^(BOOL finished) {
                         }];
    }
    
    

}


//键盘隐藏
- (void)keyboardWillHide:(NSNotification*)notification
{
    float duringTime = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    
    [UIView animateWithDuration:duringTime
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self.view layoutIfNeeded];
                     } completion:^(BOOL finished) {
                         
                     }];
}


@end

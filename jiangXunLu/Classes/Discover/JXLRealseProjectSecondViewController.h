//
//  JXLRealseProjectSecondViewController.h
//  jiangXunLu
//
//  Created by 金莹 on 16/9/8.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXViewController.h"

@class JXLRealseProjectModel;
@interface JXLRealseProjectSecondViewController : JXViewController


- (instancetype)initWithProjectModel:(JXLRealseProjectModel *)projectModel;
@end

//
//  JXLSearchDiscoverViewController.m
//  jiangXunLu
//
//  Created by 金莹 on 16/9/10.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXLSearchDiscoverViewController.h"
#import "jiangXunLu-swift.h"
#import "YYDateFormatterTool.h"
#import "AFNTool.h"
#import "YYModel.h"
#import "GlobalDefine.h"
#import "UIImageView+WebCache.h"
#import "Masonry.h"
#import "EXTScope.h"
#import "MBProgressHUD+Add.h"
#import "HexColor.h"
#import "BlocksKit+UIKit.h"
#import "FTPopOverMenu.h"
#import "JXLCustomSearchBar.h"
#import "FTPopOverMenu.h"
#import "JXLSearchHotModel.h"

#import "JXLDiscoverInfoModel.h"
#import "JXLDiscoverProjectModel.h"
#import "JXLDiscoverBrandModel.h"
#import "DiscoverInfoTableViewCell.h"
#import "DiscoverProjectTableViewCell.h"
#import "DiscoverBrandTableViewCell.h"
#import "DiscoverDetailViewController.h"


CGFloat kCollectionViewCellHeight                   = 25;
CGFloat kCollectionViewCellsHorizonMargin           = 12;
CGFloat kCollectionViewToLeftMargin                 = 20;
CGFloat kCollectionViewToTopMargin                  = 12;
CGFloat kCollectionViewToRightMargin                = 20;
CGFloat kCollectionViewToBottomtMargin              = 10;
CGFloat kCellBtnCenterToBorderMargin                = 19;

//搜索相关
static NSString *const kHotCellIdentifier = @"HotCellIdentifier";
static NSString *const kHotHeaderViewIdentifier = @"HotHeaderViewIdentifier";
static NSString *const kSearchCellIdentifier = @"SearchCellIdentifier";
static NSString *const kSearchHeaderViewIdentifier = @"SearchHeaderViewIndentifier";
static NSString *const kSearchFooterViewIdentifier = @"SearchFooterViewIndentifier";
static NSString *const kSearchResultTableViewCellIdentifier = @"SearchResultTableViewCellIndentifier";

//搜索结果
static NSString *kDiscoverInfoCellIdentifier = @"DiscoverInfoTableViewCellIdentifier";
static NSString *kDiscoverProjectCellIdentifier = @"DiscoverProjectTableViewCellIdentifier";
static NSString *kDiscoverBrandCellIdentifier = @"DiscoverBrandCellIdentifier";

@interface JXLSearchDiscoverViewController ()<UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (strong, nonatomic) IBOutlet JXLCustomSearchBar *customSearchBar;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *hotArray;
@property (strong, nonatomic) NSMutableArray *historyArray;
@property (strong, nonatomic) NSMutableArray *resultArray;
@property (strong, nonatomic) NSString *ftype;
@property (assign, nonatomic) BOOL showResult;
@end

@implementation JXLSearchDiscoverViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpLeftbarButtonWithImage:UIIMGName(@"back_normal")];

    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width - 60, 44)];
    self.customSearchBar.centerY = titleView.centerY;
    self.customSearchBar.width = titleView.width;
    [titleView addSubview:self.customSearchBar];
    
    self.navigationItem.titleView = titleView;
    
    [self setSearchBarActionHandler];
    
    self.hotArray = [[NSMutableArray alloc] initWithCapacity:2];
    self.historyArray = [[NSMutableArray alloc] initWithCapacity:2];
    self.resultArray = [[NSMutableArray alloc] initWithCapacity:2];

    [self loadHotData];
    
    [self setupCollectionView];
    [self setupTableView];
}

- (void)setupCollectionView{
    [self.collectionView registerClass:[SearchHistoryCell class] forCellWithReuseIdentifier:kSearchCellIdentifier];
    [self.collectionView registerClass:[HotCollectionViewCell class] forCellWithReuseIdentifier:kHotCellIdentifier];
    
    [self.collectionView registerClass:[HotHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:kHotHeaderViewIdentifier];
    [self.collectionView registerClass:[SearchHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:kSearchHeaderViewIdentifier];
    [self.collectionView registerClass:[SearchFooterView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:kSearchFooterViewIdentifier];
    
}

- (void)setupTableView{
    [self.tableView registerNib:[UINib nibWithNibName:@"DiscoverInfoTableViewCell" bundle:nil] forCellReuseIdentifier:kDiscoverInfoCellIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:@"DiscoverProjectTableViewCell" bundle:nil] forCellReuseIdentifier:kDiscoverProjectCellIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:@"DiscoverBrandTableViewCell" bundle:nil] forCellReuseIdentifier:kDiscoverBrandCellIdentifier];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - action

- (void)leftBarItemTouchUpInside:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setSearchBarActionHandler{
    @weakify(self);

    [self.customSearchBar setCategoryButtonHandler:^(UIButton *button) {
        @strongify(self);
        NSArray *categoryArray = @[@"资讯",@"项目",@"品牌"];
        
        [FTPopOverMenu setIselected:NO];
        [FTPopOverMenu setIsCustom:YES];
        [FTPopOverMenu setTintColor:[UIColor colorWithHexString:@"353637"]];
        [FTPopOverMenu showFromSenderFrame:CGRectMake( 70, 64, 3, 3) withMenu:categoryArray doneBlock:^(NSInteger selectedIndex) {
            @strongify(self);
            [self.customSearchBar setFtype:categoryArray[selectedIndex] completionBlock:^(NSString *ftypeString) {
                @strongify(self);
                self.ftype = ftypeString;
                if (self.customSearchBar.textString && self.customSearchBar.textString.length > 0) {
                    [self searchKeyword:self.customSearchBar.textString];
                }
            }];

        } dismissBlock:nil];
    }];
    
    [self.customSearchBar setCancelButtonHandler:^{
        @strongify(self);
        [self.resultArray removeAllObjects];
        self.tableView.hidden = YES;
        self.collectionView.hidden = NO;
    }];
    
    [self.customSearchBar setSearchButtonHandler:^(NSString *keyword) {
        @strongify(self);
        [self searchKeyword:keyword];
    }];
}

- (void)hotButtonClick:(HotIndexButton *)button{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:button.row inSection:button.section];
    [self collectionView:self.collectionView didSelectItemAtIndexPath:indexPath];
}
- (void)deleteHistoryButtonClick:(HotIndexButton *)button{
    
}

- (void)deleteAllHistory{
    NSString *g_uid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
    NSString *g_randcode = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"randcode"]];
    NSDictionary *data = @{@"type" : @"2"};
    NSDictionary *params = @{@"action" : @"search_history_clear",
                             @"uid":g_uid,
                             @"randcode":g_randcode,
                             @"data":[data yy_modelToJSONString]
                             };
    
    @weakify(self);
    [AFNTool post:Globe.baseUrlStr params:params success:^(id responsObject) {
        @strongify(self);
//        NSLog(@"热词：search_hot_load:%@",responsObject);
        if (responsObject && [responsObject isKindOfClass:[NSDictionary class]]) {
            NSInteger status = [responsObject[@"status"] integerValue];
            if (status == 1) {
                [self.historyArray removeAllObjects];
                [self.collectionView reloadData];
            }
        }
    } failure:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD showError:error.description toView:self.view];
        
        NSLog(@"%@",error.description);
    }];

}
#pragma mark - loadData
- (void)loadHotData{
    NSString *g_uid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
    NSString *g_randcode = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"randcode"]];
    
    NSDictionary *data = @{@"type" : @(2)};
    NSDictionary *params = @{@"action" : @"search_hot_load",
                             @"uid":g_uid,
                             @"randcode":g_randcode,
                             @"data":[data yy_modelToJSONString]
                             };
    
    @weakify(self);
    [AFNTool post:Globe.baseUrlStr params:params success:^(id responsObject) {
        @strongify(self);
//        NSLog(@"热词：search_hot_load:%@",responsObject);
        if (responsObject && [responsObject isKindOfClass:[NSDictionary class]]) {
            NSInteger status = [responsObject[@"status"] integerValue];
            if (status == 1) {
                NSArray *list = [responsObject[@"info"] objectForKey:@"list"];
                NSArray *keywordArray = [NSArray yy_modelArrayWithClass:[JXLSearchHotModel class] json:list];
                [self.hotArray addObjectsFromArray:keywordArray];
                [self.collectionView reloadData];
            }
        }
    } failure:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD showError:error.description toView:self.view];
        
        NSLog(@"%@",error.description);
    }];
    
    NSDictionary *historyParams = @{@"action" : @"search_history_load",
                                    @"uid":g_uid,
                                    @"randcode":g_randcode,
                                    @"data":[data yy_modelToJSONString]
                                    };
    
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [AFNTool post:Globe.baseUrlStr params:historyParams success:^(id responsObject) {
        @strongify(self);
        NSLog(@"历史记录：search_history_load:%@",responsObject);
        if (responsObject && [responsObject isKindOfClass:[NSDictionary class]]) {
            NSInteger status = [responsObject[@"status"] integerValue];
            if (status == 1) {
                NSArray *list = [responsObject[@"info"] objectForKey:@"list"];
                for (NSDictionary *dict in list) {
                 
                    NSString *historyString = dict[@"keyword"];
                    [self.historyArray addObject:historyString];
                }
                [self.collectionView reloadData];
            }
        }
    } failure:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD showError:error.description toView:self.view];
        
        NSLog(@"%@",error.description);
    }];
}

- (void)searchKeyword:(NSString *)keyword{
    NSString *g_uid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
    NSString *g_randcode = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"randcode"]];
    
    NSString *ftype = @"";
    if ([self.customSearchBar.ftype isEqualToString:@"资讯"] || [self.customSearchBar.ftype isEqualToString:@"info"]) {
        ftype = @"info";
        
    }else if ([ftype isEqualToString:@"项目"] || [self.customSearchBar.ftype isEqualToString:@"project"]){
        ftype = @"project";
        
    }else {
        ftype = @"brand";
    }
    self.ftype = ftype;
    
    NSDictionary *data = @{@"ftype" : ftype,
                           @"keyword" : keyword};
    NSDictionary *params = @{@"action" : @"find_search",
                             @"uid":g_uid,
                             @"randcode":g_randcode,
                             @"data":[data yy_modelToJSONString]
                             };
    @weakify(self);
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [AFNTool post:Globe.baseUrlStr params:params success:^(id responsObject) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        NSLog(@"搜索结果：search_find:%@",responsObject);
        if (responsObject && [responsObject isKindOfClass:[NSDictionary class]]) {
            NSInteger status = [responsObject[@"status"] integerValue];
            if (status == 1) {
                [self.resultArray removeAllObjects];
                if ([[responsObject[@"info"] objectForKey:@"ftype"] isEqualToString:@"info"]) {
                    NSArray *list = [responsObject[@"info"] objectForKey:@"list"];
                    NSArray *array = [NSArray yy_modelArrayWithClass:[JXLDiscoverInfoModel class] json:list];
                    [self.resultArray addObjectsFromArray:array];

                }else if ([[responsObject[@"info"] objectForKey:@"ftype"] isEqualToString:@"project"]){
                    NSArray *list = [responsObject[@"info"] objectForKey:@"list"];
                    NSArray *array = [NSArray yy_modelArrayWithClass:[JXLDiscoverProjectModel class] json:list];
                    [self.resultArray addObjectsFromArray:array];

                }else {
                    NSArray *list = [responsObject[@"info"] objectForKey:@"list"];
                    NSArray *array = [NSArray yy_modelArrayWithClass:[JXLDiscoverBrandModel class] json:list];
                    [self.resultArray addObjectsFromArray:array];

                }
                self.collectionView.hidden = YES;
                self.tableView.hidden = NO;
                [self.tableView reloadData];
            }
        }
    } failure:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:error.description toView:self.view];
        
        NSLog(@"%@",error.description);
    }];

    
}

#pragma mark - UITableViewDataSource, UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.resultArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 154;

}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([self.ftype isEqualToString:@"info"]) {
        DiscoverInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kDiscoverInfoCellIdentifier];
        JXLDiscoverInfoModel *cellModel = self.resultArray[indexPath.row];
        [cell configureCell:cellModel];
        return cell;
    }else if ([self.ftype isEqualToString:@"project"]){
        DiscoverProjectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kDiscoverProjectCellIdentifier];
        JXLDiscoverProjectModel *cellModel = self.resultArray[indexPath.row];
        [cell configureCell:cellModel];
        return cell;
    }else {
        DiscoverBrandTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kDiscoverBrandCellIdentifier];
        JXLDiscoverBrandModel *cellModel = self.resultArray[indexPath.row];
        [cell configureCell:cellModel];
        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    JXLDisType type = 0;
    NSString *identifier = nil;
    if ([self.ftype isEqualToString:@"info"]) {
        type = kDiscoverInfo;
        JXLDiscoverInfoModel *infoModel = self.resultArray[indexPath.row];
        identifier = infoModel.discoverInfo_id;
    }else if ([self.ftype isEqualToString:@"project"]){
        type = kDiscoverProject;
        JXLDiscoverProjectModel *model = self.resultArray[indexPath.row];
        identifier = model.project_id;
    }else {
        type = kDiscoverBrand;
        JXLDiscoverBrandModel *model = self.resultArray[indexPath.row];
        identifier = model.brand_id;
    }
    DiscoverDetailViewController *detailVC = [[DiscoverDetailViewController alloc] initWithDiscoverType:type];
    detailVC.identifier = identifier;
    detailVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:detailVC animated:YES];
}
#pragma mark - collectionView delegate dataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 2;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    switch (section) {
        case 0:{
            return self.hotArray.count;
        }
            break;
        case 1:{
            return self.historyArray.count;
        }
            break;
        default:
            return 0;
            break;
    }
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:{
            JXLSearchHotModel *hot = self.hotArray[indexPath.row];
            CGFloat cellWidth = [self collectionCellWidthText:hot.keyword];
            return CGSizeMake(cellWidth, kCollectionViewCellHeight);
        }
            break;
        case 1:{
            return CGSizeMake([UIScreen mainScreen].bounds.size.width,SearchHistoryCell.cellHeight);
        }
            break;
        default:
            return CGSizeZero;
            break;
    }
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return section == 0 ? kCollectionViewToTopMargin : 0;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    switch (section) {
        case 0:{
            return kCollectionViewCellsHorizonMargin;
        }
            break;
        case 1:{
            return 0;
        }
            break;
        default:
            return 0;
            break;
    }
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    switch (section) {
        case 0:{
            return UIEdgeInsetsMake(kCollectionViewToTopMargin, kCollectionViewToLeftMargin, kCollectionViewToBottomtMargin, kCollectionViewToRightMargin);
        }
            break;
        case 1:{
            return UIEdgeInsetsZero;
        }
            break;
        default:
            return UIEdgeInsetsZero;
            break;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return CGSizeMake([UIScreen mainScreen].bounds.size.width, section == 0 ? [HotHeaderView viewHeight] : [SearchHeaderView viewHeight]);
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    return CGSizeMake([UIScreen mainScreen].bounds.size.width, section == 0 ? 0 : [SearchFooterView viewHeight]);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:{
            HotCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kHotCellIdentifier forIndexPath:indexPath];
            cell.button.frame = CGRectMake( 0, 0, CGRectGetWidth(cell.frame), CGRectGetHeight(cell.frame));
            if (self.hotArray.count > 0) {
                JXLSearchHotModel *hot = self.hotArray[indexPath.row];
                [cell.button setTitle:hot.keyword forState:UIControlStateNormal];
                [cell.button addTarget:self action:@selector(hotButtonClick:) forControlEvents:UIControlEventTouchUpInside];
                cell.button.section = indexPath.section;
                cell.button.row = indexPath.row;
                
                cell.layer.borderColor = [UIColor colorWithHexString:@"e5e5e5"].CGColor;
                cell.layer.masksToBounds = YES;
                cell.layer.borderWidth = 1;
                cell.layer.cornerRadius = cell.contentView.height/2;
            }
            return cell;
        }
            break;
        case 1:{
            SearchHistoryCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kSearchCellIdentifier forIndexPath:indexPath];
            [cell.deleteBtn addTarget:self action:@selector(deleteHistoryButtonClick:) forControlEvents:UIControlEventTouchUpInside];
            if (self.historyArray.count > 0) {
                cell.historyLabel.text = self.historyArray[indexPath.row];
            }
            
            return cell;
        }
            break;
        default:
            return nil;
            break;
    }

}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if (kind == UICollectionElementKindSectionHeader) {
        switch (indexPath.section) {
            case 0:{
                HotHeaderView *hotHeaderView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:kHotHeaderViewIdentifier forIndexPath:indexPath];
                return hotHeaderView;
            }
                break;
            case 1:{
                SearchHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:kSearchHeaderViewIdentifier forIndexPath:indexPath];
                [headerView seHeaderViewtHidden:self.historyArray.count > 0 ? NO : YES];
                return headerView;
            }
                break;
            default:
                break;
        }
    }else {
        SearchFooterView *footView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:kSearchFooterViewIdentifier forIndexPath:indexPath];
        switch (indexPath.section) {
            case 0:{
                footView.hidden = NO;
            }
                break;
            case 1:{
                footView.hidden = self.historyArray.count > 0 ? NO : YES;
                [footView.cleanBtn addTarget:self action:@selector(deleteAllHistory) forControlEvents:UIControlEventTouchUpInside];
            }
                break;
            default:
                break;
        }
        return footView;
    }
    return nil;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:{
            JXLSearchHotModel *hot = self.hotArray[indexPath.row];
            [self.customSearchBar setTextString:hot.keyword];
            [self searchKeyword:hot.keyword];
        }
            break;
        case 1:{
            NSString *historyString = self.historyArray[indexPath.row];
            [self.customSearchBar setTextString:historyString];
            [self searchKeyword:historyString];
        }
            break;
        default:
            break;
    }
}


#pragma mark -
- (CGFloat)collectionCellWidthText:(NSString *)text{
    
    CGSize size = [text sizeWithAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:15]}];
    CGFloat cellWidth = size.width + kCellBtnCenterToBorderMargin;
    CGFloat limitWidth = CGRectGetWidth(self.collectionView.frame) - kCollectionViewToLeftMargin - kCollectionViewToRightMargin;
    if (cellWidth >= limitWidth) {
        cellWidth = limitWidth;
    }
    return cellWidth;
    
}
@end

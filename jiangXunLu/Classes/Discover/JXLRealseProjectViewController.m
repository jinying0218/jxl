//
//  JXLRealseProjectViewController.m
//  jiangXunLu
//
//  Created by 金莹 on 16/9/4.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXLRealseProjectViewController.h"
#import "jiangXunLu-swift.h"
#import "GlobalDefine.h"
#import "AFNTool.h"
#import "YYModel.h"
#import "UIImageView+WebCache.h"
#import "Masonry.h"
#import "EXTScope.h"
#import "MBProgressHUD+Add.h"
#import "KVOController.h"
#import "BlocksKit+UIKit.h"
#import "Hexcolor.h"
#import "YYText.h"
#import <MobileCoreServices/MobileCoreServices.h>

#import "UITableView+FDTemplateLayoutCell.h"
#import "JXLRealseCell.h"
#import "JXLReaseModel.h"
#import "YYThumbImagesView.h"
#import "JXLRealseProjectModel.h"
#import "JXLRealseProjectSecondViewController.h"
#import "JXLPickerViewController.h"

#import "JXListActionSheet.h"
#import "JXPickerActionSheet.h"
#import "CommonTools.h"
#import "UIImage+Resizing.h"
#import "JXLEditImageViewController.h"


static NSString *const kRealseCellIdentifier = @"kRealseCellIdentifier";
static NSString *const kRealseInfoSectionHeaderIdentifier = @"kRealseInfoSectionHeaderIdentifier";

@interface JXLRealseProjectViewController ()<UITableViewDelegate,UITableViewDataSource,YYThumbImagesViewDelegate,JXCustomActionSheetDelegate>
@property (strong, nonatomic) UIImagePickerController *imagePickerController;
@property (strong, nonatomic) JXLEditImageViewController *editImageViewController;

@property (strong, nonatomic) UIAlertController *actionSheet;
@property (assign, nonatomic) JXLRealseType realseType;
@property (strong, nonatomic) NSString *identifier;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *addCoverImageButton;
@property (weak, nonatomic) IBOutlet UIImageView *coverImageView;
@property (weak, nonatomic) IBOutlet UIView *chooseCoverBtnBackView;
@property (weak, nonatomic) IBOutlet UIButton *chooseCoverButton;
@property (strong, nonatomic) IBOutlet UIView *headerView;

@property (strong, nonatomic) JXLRealseProjectModel *projectModel;
@property (strong, nonatomic) YYThumbImagesView *thumbImageView;
@property (strong, nonatomic) JXLPickerViewController *dataPickerViewController;
@property (strong, nonatomic) NSArray *tableDatasArray;
@property (assign, nonatomic) BOOL isGetCover;       //是获取封面照片还是底部照片
@property (strong, nonatomic) NSArray *pickerDatas;
@property (strong, nonatomic) NSDictionary *realsePlist;
@property (strong, nonatomic) UIView *activeCell;

@end

@implementation JXLRealseProjectViewController
- (instancetype)initWithEditStatus:(JXLRealseType)type identifier:(NSString *)identifier
{
    self = [super init];
    if (self) {
        _realseType = type;
        _identifier = identifier;
        if (type == kDiscoverRealse) {
            _projectModel = [[JXLRealseProjectModel alloc] init];
            _projectModel.pics = [[NSMutableArray alloc] initWithCapacity:2];
        }
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpLeftbarButtonWithImage:UIIMGName(@"back_normal")];
    self.title = @"项目";

    if (self.realseType == kDiscoverEdit) {
        //编辑
        [self loadEditDataFromNet];
        
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [rightButton setTitle:@"发布" forState:UIControlStateNormal];
        [rightButton setTitleColor:[UIColor colorWithHexString:@"FECD08"] forState:UIControlStateNormal];
        [rightButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        [self setUpRightbarButtonWithCustomButton:rightButton];
    }
    
    UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake( 0, 0, self.view.width, 80)];
    bottomView.backgroundColor = [UIColor clearColor];
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    nextButton.frame = CGRectMake( ([UIScreen mainScreen].bounds.size.width - 125)/2, 25, 125, 40);
    [nextButton setBackgroundImage:[UIImage imageNamed:@"find_btn_next_step"] forState:UIControlStateNormal];
    [nextButton setTitle:@"下一步" forState:UIControlStateNormal];
    [nextButton addTarget:self action:@selector(nextButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:nextButton];

    
    [self prepareData];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"JXLRealseCell" bundle:nil] forCellReuseIdentifier:kRealseCellIdentifier];
    [self.tableView registerClass:[UITableViewHeaderFooterView class] forHeaderFooterViewReuseIdentifier:kRealseInfoSectionHeaderIdentifier];
    self.tableView.backgroundColor = [UIColor colorWithHexString:@"eeeeee"];
    self.tableView.tableFooterView = self.realseType == kDiscoverRealse ? bottomView : self.thumbImageView ;
    self.tableView.tableHeaderView = self.headerView;

    [self blindActionHandler];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self addKeyboardNotification];
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self removeKeyboardNotification];
}
- (void)refreshUI{
    
    self.chooseCoverButton.hidden = self.projectModel.cover.length > 0 ? NO : YES;
    self.chooseCoverBtnBackView.hidden = self.chooseCoverButton.hidden;
    
    self.addCoverImageButton.hidden = !self.chooseCoverButton.hidden;
    [self.tableView reloadData];
}

#pragma mark - loadData
- (void)prepareData{
    
    if (self.projectModel.cover) {
        [self.coverImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",Globe.baseUrlStr,self.projectModel.cover]]];
        self.chooseCoverButton.hidden = self.projectModel.cover.length > 0 ? NO : YES;
        self.chooseCoverBtnBackView.hidden = self.chooseCoverButton.hidden;
        self.addCoverImageButton.hidden = !self.chooseCoverButton.hidden;
    }
    if (self.projectModel.pics) {
        @weakify(self);
        for (NSString *imageString in self.projectModel.pics) {
            NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",Globe.baseUrlStr,imageString]];
            [[SDWebImageManager sharedManager] downloadImageWithURL:imageURL options:SDWebImageRetryFailed | SDWebImageAvoidAutoSetImage progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                @strongify(self);
                [self.thumbImageView insertOneImage:image refresh:^(YYThumbImagesView *thumbView) {
                    @strongify(self);
                    self.tableView.tableFooterView = thumbView;
                }];
            }];
        }
    }
    
    JXLReaseModel *cellModel1 = [[JXLReaseModel alloc] initWithTitleString:@"项目名称" placeholder:@"如：西安火车站E米国际" content:(self.projectModel.title ? self.projectModel.title : nil) keyString:@"title"];
    JXLReaseModel *cellModel2 = [[JXLReaseModel alloc] initWithTitleString:@"项目类型" placeholder:@"如：综合体" content:(self.projectModel.type ? self.projectModel.type : nil) keyString:@"type"];
    
    JXLReaseModel *cellModel3 = [[JXLReaseModel alloc] initWithTitleString:@"招商需求" placeholder:@"如：品牌集合" content:(self.projectModel.brand_industry ? self.projectModel.brand_industry : nil) keyString:@"brand_industry"];

    JXLReaseModel *cellModel4 = [[JXLReaseModel alloc] initWithTitleString:@"商业面积" placeholder:nil content:(self.projectModel.area ? self.projectModel.area : nil) keyString:@"area"];
    NSString *address = nil;
    if (self.projectModel.addr_city && self.projectModel.addr_prov) {
        address = [NSString stringWithFormat:@"%@-%@",self.projectModel.addr_prov,self.projectModel.addr_city];
    }
    JXLReaseModel *cellModel5 = [[JXLReaseModel alloc] initWithTitleString:@"项目地址" placeholder:@"省份 —- 城市" content:(address ? address : nil) keyString:@"address"];

    JXLReaseModel *cellModel6 = [[JXLReaseModel alloc] initWithTitleString:nil placeholder:@"请填写到街道门牌号" content:(self.projectModel.addr_detail ? self.projectModel.addr_detail : nil) keyString:@"addr_detail"];
    
    JXLReaseModel *cellModel7 = [[JXLReaseModel alloc] initWithTitleString:@"开发商" placeholder:@"如：乐天购物广场" content:(self.projectModel.company ? self.projectModel.company : nil) keyString:@"company"];
    JXLReaseModel *cellModel8 = [[JXLReaseModel alloc] initWithTitleString:@"开业时间" placeholder:nil content:(self.projectModel.opening_time ? self.projectModel.opening_time : nil) keyString:@"opening_time"];
    JXLReaseModel *cellModel9 = [[JXLReaseModel alloc] initWithTitleString:@"项目定位" placeholder:@"如：大众" content:(self.projectModel.position ? self.projectModel.position : nil) keyString:@"position"];
    JXLReaseModel *cellModel10 = [[JXLReaseModel alloc] initWithTitleString:nil placeholder:nil content:(self.projectModel.content ? self.projectModel.content : nil) keyString:@"content"];
    
    if (self.realseType == kDiscoverRealse) {
        self.tableDatasArray = @[cellModel1,cellModel2,cellModel3,cellModel4,cellModel5,cellModel6];
    }else {
        self.tableDatasArray = @[@[cellModel1,cellModel2,cellModel3,cellModel4,cellModel5,cellModel6,cellModel7,cellModel8,cellModel9],@[cellModel10]];
    }
}


- (void)loadEditDataFromNet{
    
    NSString *g_uid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
    NSString *g_randcode = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"randcode"]];
    
    NSDictionary *data = @{@"id" : self.identifier};
    
    NSDictionary *params = @{@"action" : @"find_project_edit_load",
                             @"uid" : g_uid,
                             @"randcode" : g_randcode,
                             @"data":[data yy_modelToJSONString]};
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    @weakify(self);
    [AFNTool post:Globe.baseUrlStr params:params success:^(id responsObject) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        //        NSLog(@"find_project_detail_load:%@",responsObject);
        if (responsObject && [responsObject isKindOfClass:[NSDictionary class]]) {
            NSInteger status = [responsObject[@"status"] integerValue];
            if (status == 1) {
                
                NSDictionary *infoDict = [responsObject[@"info"] objectForKey:@"info"];
                self.projectModel = [JXLRealseProjectModel yy_modelWithDictionary:infoDict];
                if (!self.projectModel.pics) {
                    self.projectModel.pics = [[NSMutableArray alloc] initWithCapacity:2];
                }
                [self prepareData];
                [self.tableView reloadData];
            }
        }
    } failure:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:error.description toView:self.view];
        
        NSLog(@"%@",error.description);
    }];
    
    
}
//上传图片
- (void)uploadImage:(UIImage *)uploadImage{
    NSString *g_uid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
    NSString *g_randcode = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"randcode"]];
    
    NSData *imageData = UIImageJPEGRepresentation(uploadImage, 0.8f);

    NSDictionary *data = @{@"type" : @"stream",
                           @"data" : imageData,
                           @"place" : @""};
    NSDictionary *params = @{@"action" : @"upload_img",
                             @"uid" : g_uid,
                             @"randcode" : g_randcode,
                             @"data":[data yy_modelToJSONString]};
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    @weakify(self);
    [AFNTool uploadRequestURL:Globe.baseUrlStr params:params fileData:imageData success:^(id responsObject) {
        NSLog(@"上传图片：%@",responsObject);
        @strongify(self);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if ([responsObject[@"status"] intValue] == 1) {
            NSString *urlString = [responsObject[@"info"] objectForKey:@"url"];

            if (self.realseType == kDiscoverRealse) {
                self.projectModel.cover = urlString;
                self.coverImageView.image = uploadImage;
                [self refreshUI];
            }else {
                if (self.isGetCover) {
                    self.projectModel.cover = urlString;
                    self.coverImageView.image = uploadImage;
                    [self refreshUI];
                }else {
                    [self.projectModel.pics addObject:urlString];
                    [self.thumbImageView insertOneImage:uploadImage refresh:^(YYThumbImagesView *thumbView) {
                        @strongify(self);
                        self.tableView.tableFooterView = thumbView;
                    }];
                }
            }
        }else {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [MBProgressHUD showError:responsObject[@"msg"] toView:self.view];
        }
        
    } failure:^(NSError *error) {
//        NSLog(@"上传图片：%@",error.description);
        @strongify(self);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:error.description toView:self.view];
        
    }];
    
}

#pragma mark - action

- (void)blindActionHandler{
    @weakify(self);
    [self.addCoverImageButton bk_addEventHandler:^(id sender) {
        @strongify(self);
        self.isGetCover = YES;
        [self presentViewController:self.actionSheet animated:YES completion:nil];
    } forControlEvents:UIControlEventTouchUpInside];
    
    [self.chooseCoverButton bk_addEventHandler:^(id sender) {
        @strongify(self);
        self.isGetCover = YES;
        [self presentViewController:self.actionSheet animated:YES completion:nil];
    } forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)leftBarItemTouchUpInside:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

//发布  - 修改
- (void)rightBarItemTouchUpInside:(id)sender{
    [self.view endEditing:YES];
    
    NSUInteger section = 0;
    for (NSArray *subArray in self.tableDatasArray) {
        for (JXLReaseModel *realseModel in subArray) {
            if (!realseModel.content || realseModel.content.length == 0) {
                [MBProgressHUD showError:@"请将信息填写完整" toView:self.view];
                NSUInteger location = [subArray indexOfObject:realseModel];
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:location inSection:section];
                
                JXLRealseCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
                cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"find_remind"]];
                return;
            }
        }
        section ++;
    }
    
    NSString *g_uid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
    NSString *g_randcode = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"randcode"]];
    
    NSDictionary *data = @{@"id" : self.projectModel.identifier,
                           @"title" : self.projectModel.title,
                           @"content" : self.projectModel.content,
                           @"cover" : self.projectModel.cover,
                           @"pics" : [self.projectModel.pics yy_modelToJSONString],
                           @"type" : self.projectModel.type,
                           @"brand_industry" : self.projectModel.brand_industry,
                           @"area" : self.projectModel.area,
                           @"addr_prov" : self.projectModel.addr_prov,
                           @"addr_city" : self.projectModel.addr_city,
                           @"addr_detail" : self.projectModel.addr_detail,
                           @"company" : self.projectModel.company,
                           @"position" : self.projectModel.position,
                           @"opening_time" : self.projectModel.opening_time};
    
    NSDictionary *params = @{@"action" : @"find_project_modify",
                             @"uid" : g_uid,
                             @"randcode" : g_randcode,
                             @"data":[data yy_modelToJSONString]};
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    @weakify(self);
    [AFNTool post:Globe.baseUrlStr params:params success:^(id responsObject) {
        @strongify(self);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        //        NSLog(@"find_project_detail_load:%@",responsObject);
        if (responsObject && [responsObject isKindOfClass:[NSDictionary class]]) {
            NSInteger status = [responsObject[@"status"] integerValue];
            if (status == 1) {
                [MBProgressHUD showSuccess:@"修改成功" toView:[UIApplication sharedApplication].keyWindow];
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
    } failure:^(NSError *error) {
        @strongify(self);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:error.description toView:self.view];
        
        NSLog(@"%@",error.description);
    }];
}

//下一步
- (void)nextButtonClick:(UIButton *)button{
    
    [self.view endEditing:YES];
    
    if (!self.projectModel.cover || self.projectModel.cover.length == 0) {
        [MBProgressHUD showError:@"请选择封面图片" toView:self.view];
        return;
    }
    
    //发布新项目
    if (self.realseType == kDiscoverRealse) {
        for (JXLReaseModel *realseModel in self.tableDatasArray) {
            
            if (!realseModel.content || realseModel.content.length == 0) {
                [MBProgressHUD showError:@"请将信息填写完整" toView:self.view];
                NSUInteger location = [self.tableDatasArray indexOfObject:realseModel];
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:location inSection:0];
                JXLRealseCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
                cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"find_remind"]];
                return;
            }
        }
    }
    
    JXLRealseProjectSecondViewController *secondeVC = [[JXLRealseProjectSecondViewController alloc] initWithProjectModel:self.projectModel];
    [self.navigationController pushViewController:secondeVC animated:YES];
}
#pragma mark - YYThumbImagesViewDelegate

- (void)addImageButtonClick:(UIButton *)button{
    self.isGetCover = NO;
    [self presentViewController:self.actionSheet animated:YES completion:nil];
}
- (void)deleteImageButton:(NSUInteger)deleteIndex{
    [self.projectModel.pics removeObjectAtIndex:deleteIndex];
}
#pragma mark - tableView delegate && tableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (self.realseType == kDiscoverRealse) {
        return 1;
    }else {
        return 2;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.realseType == kDiscoverRealse) {
        return self.tableDatasArray.count;
    }else {
        return [self.tableDatasArray[section] count];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (self.realseType == kDiscoverRealse) {
        return 0;
    }else {
        if (section == 0) {
            return 0;
        }
        return 52;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (self.realseType == kDiscoverRealse) {
        return 0;
    }else {
        if (section == 0) {
            return 0;
        }
        return 18;
    }
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (self.realseType == kDiscoverRealse) {
        return [[UIView alloc] init];
    }else {
        if (section == 0) {
            return [[UIView alloc] init];
        }
        UITableViewHeaderFooterView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:kRealseInfoSectionHeaderIdentifier];
        headerView.contentView.backgroundColor = [UIColor colorWithHexString:@"eeeeee"];
        headerView.textLabel.font = [UIFont systemFontOfSize:14];
        headerView.textLabel.textColor = [UIColor colorWithHexString:@"333333"];
        headerView.textLabel.text = @"正文";
        return headerView;
    }
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (self.realseType == kDiscoverRealse) {
        return [[UIView alloc] init];
    }else {
        if (section == 0) {
            return [[UIView alloc] init];
        }
        UITableViewHeaderFooterView *footerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:kRealseInfoSectionHeaderIdentifier];
        footerView.contentView.backgroundColor = [UIColor colorWithHexString:@"eeeeee"];
        return footerView;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    JXLReaseModel *cellModel = nil;
    if (self.realseType == kDiscoverRealse) {
        cellModel = self.tableDatasArray[indexPath.row];
    }else {
        cellModel = self.tableDatasArray[indexPath.section][indexPath.row];
    }
    if (!cellModel.content || cellModel.content.length == 0) {
        if (indexPath.section == 1) {
            return 100;
        }
        return 50;
    }else {
        if (indexPath.section == 1) {
            return (cellModel.cellHeight + 30) < 100 ? 100 : (cellModel.cellHeight + 30);
        }
        return (cellModel.cellHeight + 30) < 50 ? 50 : (cellModel.cellHeight + 30);
    }    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    JXLRealseCell *cell = [tableView dequeueReusableCellWithIdentifier:kRealseCellIdentifier];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(JXLRealseCell *)cell atIndexPath:(NSIndexPath *)indexPath{
    JXLReaseModel *cellModel = nil;
    if (self.realseType == kDiscoverRealse) {
        cellModel = self.tableDatasArray[indexPath.row];
    }else {
        cellModel = self.tableDatasArray[indexPath.section][indexPath.row];
    }
    
    @weakify(self);
    [cell configureTableView:self.tableView CellData:cellModel indexPath:indexPath handler:^(NSString *inputString) {
        @strongify(self);
        if ([inputString isEqualToString:@""] || !inputString) {
            return ;
        }
        if ([cellModel.keyString isEqualToString:@"address"]) {
            NSRange range = [inputString rangeOfString:@"-"];
            NSString *addr_prov = [inputString substringToIndex:range.location];
            NSString *addr_city = [inputString substringFromIndex:range.location + 1];
            [self.projectModel setValue:addr_prov forKey:@"addr_prov"];
            [self.projectModel setValue:addr_city forKey:@"addr_city"];
        }else {
            [self.projectModel setValue:inputString forKey:cellModel.keyString];
        }
    } activeBlock:^(UIView *activeView) {
        @strongify(self);
        self.activeCell = activeView;
    }];

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.view endEditing:YES];
    
    [self.dataPickerViewController setIsAllowMultableSelection:NO];
    JXLRealseCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    JXLReaseModel *cellModel = cell.cellModel;
    @weakify(self);
    //选择时间的
    if ([cellModel.keyString isEqualToString:@"opening_time"]) {
        [self.dataPickerViewController setDatePickerViewChooseCompletion:^(NSString *selectedString) {
            @strongify(self);
            cellModel.content = selectedString;
            [self.projectModel setValue:selectedString forKey:cellModel.keyString];
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        }];
        [self addChildViewController:self.dataPickerViewController];
        [self.view addSubview:self.dataPickerViewController.view];
        return;
    }
    //选择地址的
    if ([cellModel.keyString isEqualToString:@"address"]) {

        JXPickerActionSheet *actionSheet = [[JXPickerActionSheet alloc] initWithData:[JXPickerActionSheet getLocationStyleDic]
                                                                            delegate:self];
        actionSheet.tag = indexPath.row;
        [actionSheet show];
        
        return;
    }
    //设置是否多选
//    if ([cellModel.keyString isEqualToString:@"brand_industry"] || [cellModel.keyString isEqualToString:@"position"] || [cellModel.keyString isEqualToString:@"project_type"]) {
//        [self.dataPickerViewController setIsAllowMultableSelection:YES];
//    }
    [self.dataPickerViewController setIsAllowMultableSelection:YES];

    NSArray *array = self.realsePlist[cellModel.keyString];
    [self.dataPickerViewController setTableDatas:array chooseCompletion:^(NSString *selectedString) {
        @strongify(self);
        cellModel.content = selectedString;
        [self.projectModel setValue:selectedString forKey:cellModel.keyString];
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
    self.dataPickerViewController.titleLabel.text = cellModel.title;
    [self addChildViewController:self.dataPickerViewController];
    [self.view addSubview:self.dataPickerViewController.view];
}

#pragma mark - 
- (void)actionSheet:(JXCustomActionSheet *)actionSheet buttonClickedAtTag:(NSInteger)tag {
    JXPickerActionSheet *pickerSheet = (JXPickerActionSheet *)actionSheet;
    NSString * keyValue = [JXPickerActionSheet getLocationValueWithData:pickerSheet.pickerData
                                                                  index:pickerSheet.pickerIndex];
    
    if (keyValue && keyValue.length > 0) {
        NSRange range = [keyValue rangeOfString:@" "];
        NSString *addr_prov = [keyValue substringToIndex:range.location];
        NSString *addr_city = [keyValue substringFromIndex:range.location + 1];
        [self.projectModel setValue:addr_prov forKey:@"addr_prov"];
        [self.projectModel setValue:addr_city forKey:@"addr_city"];

        NSString *address = [NSString stringWithFormat:@"%@-%@",addr_prov,addr_city];
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:actionSheet.tag inSection:0];

        if (self.realseType == kDiscoverEdit) {
            JXLReaseModel *cellModel = self.tableDatasArray[0][indexPath.row];
            cellModel.content = address;
        }else {
            JXLReaseModel *cellModel = self.tableDatasArray[indexPath.row];
            cellModel.content = address;
        }
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];

//        NSLog(@"~~~~~!!!!~~~~~keyValue:%@",address);
    }
    
}

#pragma mark - getter
- (UIImagePickerController *)imagePickerController{
    if (!_imagePickerController) {
        _imagePickerController = [[UIImagePickerController alloc] init];
        _imagePickerController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
//        _imagePickerController.allowsEditing = YES;
        @weakify(self);
        [_imagePickerController setBk_didCancelBlock:^(UIImagePickerController *imagePickerController) {
            [imagePickerController dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [_imagePickerController setBk_didFinishPickingMediaBlock:^(UIImagePickerController *imagePickerController, NSDictionary <NSString *,id>*info) {
            @strongify(self);
            UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
            if (!image) {
                image = [info objectForKey:UIImagePickerControllerOriginalImage];
            }
            
            
            [imagePickerController dismissViewControllerAnimated:YES completion:^{
                
                if (self.isGetCover) {
                    self.editImageViewController = [[JXLEditImageViewController alloc] initWithImage:image completion:^(UIImage *editedImage) {
                        @strongify(self);
                        [self uploadImage:editedImage];
                    }];
                    
                    [self presentViewController:self.editImageViewController animated:NO completion:nil];
                    
                    //                                        image = [image cropToSize:CGSizeMake(image.size.width, image.size.width * 3/5) usingMode:NYXCropModeCenter];
                }else {
                    [self uploadImage:image];
                }
            }];

        }];
    }
    return _imagePickerController;
}
- (UIAlertController *)actionSheet{
    if (!_actionSheet){
        _actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        @weakify(self);
        [_actionSheet addAction:[UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            @strongify(self);
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
                self.imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                [self presentViewController:self.imagePickerController animated:YES completion:nil];
            }
            [_actionSheet dismissViewControllerAnimated:YES completion:nil];
        }]];
        [_actionSheet addAction:[UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            @strongify(self);
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                self.imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:self.imagePickerController animated:YES completion:nil];
            }else {
                [MBProgressHUD showError:@"没有安装摄像头" toView:self.view];
            }
            
            [_actionSheet dismissViewControllerAnimated:YES completion:nil];
        }]];
        [_actionSheet addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [_actionSheet dismissViewControllerAnimated:YES completion:nil];
        }]];
        
    }
    return _actionSheet;
}

- (YYThumbImagesView *)thumbImageView{
    if (!_thumbImageView) {
        _thumbImageView = [[YYThumbImagesView alloc] init];
        _thumbImageView.delegate = self;
        _thumbImageView.height = 101;
    }
    return _thumbImageView;
}

- (JXLPickerViewController *)dataPickerViewController{
    if (!_dataPickerViewController) {
        _dataPickerViewController = [[JXLPickerViewController alloc] init];
        _dataPickerViewController.view.frame = CGRectMake( 0, 0, self.view.width, self.view.height);
    }
    return _dataPickerViewController;
}

- (NSDictionary *)realsePlist{
    if (!_realsePlist) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"JXLRealseProperty" ofType:@"plist"];
        _realsePlist = [[NSDictionary alloc] initWithContentsOfFile:path];

    }
    return _realsePlist;
}

#pragma mark - keyboardNotification

//注册键盘通知
- (void)addKeyboardNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
}

//移除键盘通知
- (void)removeKeyboardNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

//键盘显示
- (void)keyboardWillShow:(NSNotification*)notification
{
    CGRect keyboardBounds = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    //    float height =  keyboardBounds.size.height;
    float duringTime = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    CGSize kbSize = keyboardBounds.size;
    
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
//    NSLog(@"keyboardWasShown");
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height = aRect.size.height - kbSize.height - 44;
    
    if (!CGRectContainsPoint(aRect, CGPointMake(self.activeCell.origin.x, self.activeCell.origin.y + self.activeCell.height))) {
        
        CGPoint scrollPoint = CGPointMake(0.0, (self.activeCell.frame.origin.y + self.activeCell.height - aRect.size.height) + 44);
        
        [UIView animateWithDuration:duringTime
                              delay:0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             [self.view layoutIfNeeded];
                             [self.tableView setContentOffset:scrollPoint animated:YES];
                         } completion:^(BOOL finished) {
                         }];
    }
    
    
    
}


//键盘隐藏
- (void)keyboardWillHide:(NSNotification*)notification
{
    float duringTime = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    
    [UIView animateWithDuration:duringTime
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self.view layoutIfNeeded];
                     } completion:^(BOOL finished) {
                         
                     }];
}
@end

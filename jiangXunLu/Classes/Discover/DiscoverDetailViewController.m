//
//  DiscoverDetailViewController.m
//  jiangXunLu
//
//  Created by 金莹 on 16/9/1.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "DiscoverDetailViewController.h"

#import "jiangXunLu-swift.h"
#import "YYDateFormatterTool.h"
#import "AFNTool.h"
#import "YYModel.h"
#import "GlobalDefine.h"
#import "UIImageView+WebCache.h"
#import "Masonry.h"
#import "EXTScope.h"
#import "MBProgressHUD+Add.h"
#import "KVOController.h"
#import "MJRefresh.h"
#import "HexColor.h"
#import "BlocksKit+UIKit.h"
#import <YYText/YYText.h>
#import "UITableView+FDTemplateLayoutCell.h"

#import "JXLDiscoverDetailModel.h"
#import "JXLContactCardCell.h"
#import "JXLFindReplyModel.h"
#import "JXLReplyListCell.h"
#import "JXLPersonCardModel.h"

#import "JXLRealseInfoViewController.h"
#import "JXLRealseProjectViewController.h"
#import "JXLRealseBrandViewController.h"

#import "UMSocial.h"
#import "FTPopOverMenu.h"
#import "SDWebImageManager.h"
#import "JXViewControlerRouter.h"


static NSString *kContactCardCellIdentifier = @"kContactCardCellIdentifier";
static NSString *kReplyListCellIdentifier = @"kReplyListCellIdentifier";
static NSString *kSectionHeaderViewIdentifier = @"kSectionHeaderViewIdentifier";



@interface DiscoverDetailViewController ()<UITableViewDelegate,UITableViewDataSource,UIWebViewDelegate,YYTextViewDelegate,UMSocialUIDelegate>
@property (strong, nonatomic) JXLDiscoverDetailModel *detailModel;
@property (assign, nonatomic) JXLDisType currentDisType;
@property (strong, nonatomic) NSString *ftypeString;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *praiseConstraint;
@property (assign, nonatomic) CGFloat headerViewHeight;
@property (weak, nonatomic) IBOutlet UIButton *praiseButton;
@property (weak, nonatomic) IBOutlet UILabel *praiseCountLabel;
@property (weak, nonatomic) IBOutlet UIButton *replyButton;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewBottomConstaint;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *authorHeaderImageView;
@property (weak, nonatomic) IBOutlet UILabel *authorNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *createDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *broswerLabel;
@property (weak, nonatomic) IBOutlet UILabel *collectNumberLabel;

@property (strong, nonatomic) YYTextView *replyInputView;

@property (assign, nonatomic) CGFloat webViewHeight;
@property (assign, nonatomic) NSUInteger count;

@property (strong, nonatomic) NSMutableArray *replyDataList;    //评论列表数据
@property (assign, nonatomic) NSUInteger startNumber;
@property (assign, nonatomic) NSUInteger dataCount;
@property (strong, nonatomic) JXLFindReplyModel *replyToPerson;
@property (strong, nonatomic) UIImage *shareImage;
@end

@implementation DiscoverDetailViewController

- (instancetype)initWithDiscoverType:(JXLDisType)disType
{
    self = [super init];
    if (self) {
        _currentDisType = disType;
        _replyDataList = [[NSMutableArray alloc] initWithCapacity:2];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSLog(@"项目路径:%@",[paths lastObject]);
    
    [self addKeyboardNotification];
    
    [self setUpRightbarButtonWithImage:[UIImage imageNamed:@"find_more"]];
    
    [self configureUI];
    
    @weakify(self);
    [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:self.detailModel.share_url] options:SDWebImageDownloaderUseNSURLCache progress:nil completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
        @strongify(self);
        if (finished) {
            self.shareImage = image;
        }
    }];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setUpLeftbarButtonWithImage:UIIMGName(@"back_normal")];
    [self.tableView.mj_header beginRefreshing];
}

- (void)viewWillDisappear:(BOOL)animated{
    [self removeKeyboardNotification];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
- (void)reloadView{
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:self.detailModel.content_url] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:15];
//    NSLog(@"webView:%@",self.detailModel.content_url);
    [self.webView loadRequest:request];
    [self.tableView reloadData];
}

- (void)configureUI{
    
    self.webView.scrollView.showsHorizontalScrollIndicator = NO;
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"JXLContactCardCell" bundle:nil] forCellReuseIdentifier:kContactCardCellIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:@"JXLReplyListCell" bundle:nil] forCellReuseIdentifier:kReplyListCellIdentifier];
    [self.tableView registerClass:[UITableViewHeaderFooterView class] forHeaderFooterViewReuseIdentifier:kSectionHeaderViewIdentifier];
    
    @weakify(self);
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        self.startNumber = 0;
        self.dataCount = 20;

        [self.replyDataList removeAllObjects];
        [self loadData];
    }];
    
    self.tableView.mj_footer = [MJRefreshBackStateFooter footerWithRefreshingBlock:^{
        @strongify(self);
        self.startNumber += 20;
        [self reloadReplyData];
    }];
    
    
    [self.tableView.mj_header beginRefreshing];

    
    self.authorHeaderImageView.layer.cornerRadius = self.authorHeaderImageView.width/2;
    self.authorHeaderImageView.layer.masksToBounds = YES;

    
    [self.bottomView addSubview:self.replyInputView];
    
    
    [self.replyInputView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.bottomView).with.offset(15);
        make.right.equalTo(self.replyButton.mas_left).with.offset(-10);
        make.top.equalTo(self.bottomView).with.offset(15);
        make.bottom.equalTo(self.bottomView).with.offset(-15);
    }];
    
    [self.bottomView setNeedsLayout];
    
    [self.KVOController observe:self.webView.scrollView
                        keyPath:@"contentSize"
                        options:NSKeyValueObservingOptionNew
                          block:^(DiscoverViewController *_Nullable observer, id  _Nonnull object, NSDictionary<NSString *,id> * _Nonnull change) {
                              @strongify(self);
                              [self updateUI];
                          }];
    
    switch (self.currentDisType) {
        case 1:{
            self.title = @"项目";
        }
            break;
        case 2:{
            self.title = @"品牌";
        }
            break;
        default:{
            self.title = @"资讯";
        }
            break;
    }
}

- (void)updateUI{
    
    self.titleLabel.text = self.detailModel.title;
    [self.authorHeaderImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",Globe.baseUrlStr,self.detailModel.created_by.head]]];
    self.authorNameLabel.text = self.detailModel.created_by.name;

//    [[YYDateFormatterTool shareInstance] setDateFormat:@"yyy-MM-dd HH:mm:ss"];
//    NSDate *date = [[YYDateFormatterTool shareInstance] dateFromString:self.detailModel.show_time];
//    [[YYDateFormatterTool shareInstance] setDateFormat:@"MM-dd"];

//    NSString *showDateString = [[YYDateFormatterTool shareInstance] stringFromDate:date];

    self.createDateLabel.text = self.detailModel.show_time;
    self.broswerLabel.text = self.detailModel.read_count;
    self.collectNumberLabel.text = self.detailModel.collect_count;

    
    if ([self.detailModel.thumbsup_status intValue] == 1) {
        self.praiseButton.selected = YES;
    }
    self.praiseCountLabel.text = [NSString stringWithFormat:@"%d赞",(int)self.detailModel.thumbsup];
    self.webView.scrollView.scrollEnabled = NO;
    self.headerView.height = self.webView.scrollView.contentSize.height + self.praiseConstraint.constant + 80;
    self.tableView.tableHeaderView = self.headerView;
//    NSLog(@"~~~~~~~:%f",self.webView.scrollView.contentSize.height);
}
#pragma mark -
//内容
- (void)loadData{
    NSDictionary *params = nil;
    
    NSString *g_uid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
    NSString *g_randcode = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"randcode"]];

    if (!self.identifier) {
        return;
    }
    NSDictionary *data = @{@"id" : self.identifier};
    
    switch (self.currentDisType) {
        case 3:{
            params = @{@"action" : @"find_info_detail_load",
                       @"uid" : g_uid,
                       @"randcode" : g_randcode,
                       @"data":[data yy_modelToJSONString]};
        }
            break;
        case 1:{
            params = @{@"action" : @"find_project_detail_load",
                       @"uid" : g_uid,
                       @"randcode" : g_randcode,
                       @"data":[data yy_modelToJSONString]};
        }
            break;
        case 2:{
            params = @{@"action" : @"find_brand_detail_load",
                       @"uid" : g_uid,
                       @"randcode" : g_randcode,
                       @"data":[data yy_modelToJSONString]};
        }
            break;
            
        default:
            break;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    @weakify(self);
    [AFNTool post:Globe.baseUrlStr params:params success:^(id responsObject) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self.tableView.mj_header endRefreshing];
//        NSLog(@"详情find_project_detail_load:%@",responsObject);
        if (responsObject && [responsObject isKindOfClass:[NSDictionary class]]) {
            NSInteger status = [responsObject[@"status"] integerValue];
            if (status == 1) {
                NSDictionary *detail = [responsObject[@"info"] objectForKey:@"detail"];
                self.detailModel = [JXLDiscoverDetailModel yy_modelWithDictionary:detail];
                [self.replyDataList removeAllObjects];
                
                [self reloadReplyData];
            }else {
                [MBProgressHUD showError:responsObject[@"msg"] toView:self.view];
            }
        }
    } failure:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:error.description toView:self.view];

        NSLog(@"%@",error.description);
    }];

}

- (void)reloadReplyData{
    NSString *g_uid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
    NSString *g_randcode = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"randcode"]];
    
    switch (self.currentDisType) {
        case 1:{
            self.ftypeString = @"project";
        }
            break;
        case 2:{
            self.ftypeString = @"brand";
        }
            break;
        default:{
            self.ftypeString = @"info";
        }
            break;
    }
    NSDictionary *replyData = @{@"id" : self.identifier,
                                @"ftype" : self.ftypeString,
                                @"fid" : self.identifier,
                                @"start" : @(self.startNumber),
                                @"count" : @(self.dataCount)
                                };
    
    NSDictionary *replyParams = @{@"action" : @"find_reply_list_load",
                                  @"uid" : g_uid,
                                  @"randcode" : g_randcode,
                                  @"data":[replyData yy_modelToJSONString]};
    __block NSArray *replyList = nil;
    @weakify(self);
    [AFNTool post:Globe.baseUrlStr params:replyParams success:^(id responsObject) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSLog(@"find_reply_list_load:%@",responsObject);
        if (responsObject && [responsObject isKindOfClass:[NSDictionary class]]) {
            NSInteger status = [responsObject[@"status"] integerValue];
            if (status == 1) {
                NSDictionary *info = responsObject[@"info"];
                replyList = [NSArray yy_modelArrayWithClass:[JXLFindReplyModel class] json:info[@"list"]];
                [self.replyDataList addObjectsFromArray:replyList];
            }else {
                [MBProgressHUD showError:responsObject[@"msg"] toView:self.view];
            }
        }
        if (replyList.count < self.dataCount) {
            [self.tableView.mj_footer endRefreshingWithNoMoreData];
        }else {
            [self.tableView.mj_footer endRefreshing];
        }
        [self.tableView.mj_header endRefreshing];
        [self reloadView];
    } failure:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:error.description toView:self.view];
        NSLog(@"%@",error.description);
    }];

}


#pragma mark - action
/*
 action:find_delete
	uid:用户id
	randcode:登陆状态验证码
	data:{
 ftype : (string)发现类型(info\brand\project)
 fid : (int)发现内容id
	}

 */

- (void)leftBarItemTouchUpInside:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark 菜单
- (void)rightBarItemTouchUpInside:(id)sender{
    
    NSString *g_uid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];

    NSArray *menus = nil;
    NSArray *icons = nil;

    NSString *collectImageName = nil;
    NSString *collectTitle = nil;

    if ([self.detailModel.collect_status intValue] == 1) {
        collectImageName = @"find_btn_collection_light";
        collectTitle = @"收藏";
        [FTPopOverMenu setIselected:YES];
    }else {
        collectImageName = @"find_btn_collection_normal";
        collectTitle = @"收藏";
        [FTPopOverMenu setIselected:NO];
    }
    
    if ([[NSString stringWithFormat:@"%@",self.detailModel.created_by.identifier] isEqualToString:g_uid]) {
        menus = @[@"分享",collectTitle,@"编辑",@"删除"];
            icons = @[@"find_btn_share_normal",collectImageName,@"find_btn_edit_norml",@"find_btn_delete_normal"];
    }else {
        menus = @[@"分享",collectTitle];
        icons = @[@"find_btn_share_normal",collectImageName];
    }
        
    [FTPopOverMenu showForSender:sender
                        withMenu:menus
                  imageNameArray:icons
                       doneBlock:^(NSInteger selectedIndex) {
                           switch (selectedIndex) {
                               case 1:{
                                   //收藏
                                   [self menuCollect];
                               }
                                   break;
                               case 2:{
                                   //编辑
                                   [self menuEdit];
                               }
                                   break;
                               case 3:{
                                   [self menuDelete];
                               }
                                   break;
                               default:{
                                   //分享
                                   UMSocialData.defaultData.extConfig.title = self.detailModel.share_title;
                                   UMSocialData.defaultData.extConfig.wechatSessionData.url = self.detailModel.share_url;
                                   UMSocialData.defaultData.extConfig.wechatTimelineData.url = self.detailModel.share_url;
                                   UMSocialData.defaultData.extConfig.qqData.url = self.detailModel.share_url;
                                   UMSocialData.defaultData.extConfig.qzoneData.url = self.detailModel.share_url;
                                   UIImage *image = self.shareImage ? self.shareImage : [UIImage imageNamed:@"logo_default"];
                                   [UMSocialSnsService presentSnsIconSheetView:self appKey:Globe.umengAppKey shareText:self.detailModel.share_desc shareImage:image shareToSnsNames:@[UMShareToWechatSession,UMShareToWechatTimeline,UMShareToSina,UMShareToQQ,UMShareToQzone] delegate:self];
                               }
                                   break;
                           }
                       } dismissBlock:^{
                           
                       }];
}

#pragma mark  --  收藏
- (void)menuCollect{
    NSString *g_uid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
    NSString *g_randcode = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"randcode"]];
    
    NSDictionary *data = nil;
    if ([self.detailModel.collect_status intValue] == 1) {
        data = @{@"ftype" : self.ftypeString,
                 @"fid" : self.identifier,
                 @"status" : @(0),
                 };
    }else {
        data = @{@"ftype" : self.ftypeString,
                 @"fid" : self.identifier,
                 @"status" : @(1),
                 };
    }
    
    NSDictionary *replyParams = @{@"action" : @"find_collect",
                                  @"uid" : g_uid,
                                  @"randcode" : g_randcode,
                                  @"data":[data yy_modelToJSONString]};
    
    @weakify(self);
    [AFNTool post:Globe.baseUrlStr params:replyParams success:^(id responsObject) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSLog(@"收藏:%@",responsObject);
        if (responsObject && [responsObject isKindOfClass:[NSDictionary class]]) {
            NSInteger status = [responsObject[@"status"] integerValue];
            if (status == 1) {
                self.detailModel.collect_status = [responsObject[@"info"] objectForKey:@"status"];
                [MBProgressHUD showSuccess:@"操作成功" toView:self.view];
                
                [FTPopOverMenu setIselected:[self.detailModel.collect_status boolValue]];
            }else {
                [MBProgressHUD showError:responsObject[@"msg"] toView:self.view];
            }
        }
    } failure:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:error.description toView:self.view];
        NSLog(@"%@",error.description);
    }];
}

#pragma mark -- 编辑
- (void)menuEdit{
    UIViewController *realseVC = nil;
    switch (self.currentDisType) {
        case kDiscoverProject:{
            realseVC = [[JXLRealseProjectViewController alloc] initWithEditStatus:kDiscoverEdit identifier:self.identifier];
        }
            break;
        case kDiscoverBrand:{
            realseVC = [[JXLRealseBrandViewController alloc] initWithEditStatus:kDiscoverEdit identifier:self.identifier];
        }
            break;
        default:{
            realseVC = [[JXLRealseInfoViewController alloc] initWithEditStatus:kDiscoverEdit identifier:self.identifier];
        }
            break;
    }
    realseVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:realseVC animated:YES];

}
#pragma mark -- 删除
- (void)menuDelete{
    
    [UIAlertView bk_showAlertViewWithTitle:nil message:@"删除后将无法恢复，确定删除？" cancelButtonTitle:@"取消" otherButtonTitles:@[@"删除"] handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
        switch (buttonIndex) {
            case 1:{
                //删除
                NSString *g_uid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
                NSString *g_randcode = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"randcode"]];
                
                NSDictionary *data = @{@"ftype" : self.ftypeString,
                                       @"fid" : self.identifier
                                       };
                NSDictionary *replyParams = @{@"action" : @"find_delete",
                                              @"uid" : g_uid,
                                              @"randcode" : g_randcode,
                                              @"data":[data yy_modelToJSONString]};
                
                @weakify(self);
                [AFNTool post:Globe.baseUrlStr params:replyParams success:^(id responsObject) {
                    @strongify(self);
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    NSLog(@"删除:%@",responsObject);
                    if (responsObject && [responsObject isKindOfClass:[NSDictionary class]]) {
                        NSInteger status = [responsObject[@"status"] integerValue];
                        if (status == 1) {
                            if (self.delegate && [self.delegate respondsToSelector:@selector(finishEidtDiscoverDetail)]) {
                                [self.delegate finishEidtDiscoverDetail];
                            }
                            [self.navigationController popViewControllerAnimated:YES];
                        }else {
                            [MBProgressHUD showError:responsObject[@"msg"] toView:self.view];
                        }
                    }
                } failure:^(NSError *error) {
                    @strongify(self);
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    [MBProgressHUD showError:error.description toView:self.view];
                    NSLog(@"%@",error.description);
                }];
            }
                break;
            default:
                break;
        }
    }];
    

}
#pragma mark 点赞
- (IBAction)praiseButtonClick:(UIButton *)sender {
    
    NSString *g_uid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
    NSString *g_randcode = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"randcode"]];
    
    NSDictionary *data = nil;
    if (sender.selected) {
        NSLog(@"取消点赞");
        sender.selected = !sender.selected;
        
        data = @{@"ftype" : self.ftypeString,
                 @"fid" : self.identifier,
                 @"content" : self.replyInputView.text,
                 @"status" : @"0"
                 };
    }else {
        NSLog(@"赞");

        sender.selected = !sender.selected;
        data = @{@"ftype" : self.ftypeString,
                 @"fid" : self.identifier,
                 @"content" : self.replyInputView.text,
                 @"status" : @"1"
                 };
    }
    
    NSDictionary *params = @{@"action" : @"find_thumbsup",
                             @"uid" : g_uid,
                             @"randcode" : g_randcode,
                             @"data":[data yy_modelToJSONString]};
    
    @weakify(self);
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [AFNTool post:Globe.baseUrlStr params:params success:^(id responsObject) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        if ([responsObject[@"status"] intValue] == 1) {
//            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            NSString *thumbsUpCount = [responsObject[@"info"] objectForKey:@"thumbsup"];
            self.praiseCountLabel.text = [NSString stringWithFormat:@"%@赞",thumbsUpCount];

        }else {
            [MBProgressHUD showError:responsObject[@"msg"] toView:self.view];
        }
    } failure:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:error.description toView:self.view];
    }];

}

#pragma mark 回复
- (IBAction)replyButtonClick:(UIButton *)sender {
    
    [self.view endEditing:YES];
    
    if ([self.replyInputView.text isEqualToString:@""]) {
        [MBProgressHUD showError:@"请输入内容！" toView:self.view];
        return;
    }
    
    NSString *g_uid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
    NSString *g_randcode = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"randcode"]];
    
    NSDictionary *replyData = @{@"ftype" : self.ftypeString,
                                @"fid" : self.identifier,
                                @"content" : self.replyInputView.text,
                                @"parent_id" : self.detailModel.identifier
                                };
    
    NSDictionary *params = @{@"action" : @"find_reply",
                              @"uid" : g_uid,
                              @"randcode" : g_randcode,
                              @"data":[replyData yy_modelToJSONString]};
    @weakify(self);
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [AFNTool post:Globe.baseUrlStr params:params success:^(id responsObject) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:responsObject[@"msg"] toView:self.view];

        if ([responsObject[@"status"] intValue] == 1) {
            self.replyInputView.text = @"";
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [self.replyDataList removeAllObjects];
            [self reloadReplyData];
        }
    } failure:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:error.description toView:self.view];
    }];
}


#pragma mark - tableView delegate & dataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 37;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UITableViewHeaderFooterView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:kSectionHeaderViewIdentifier];
    UIView *backgroundView = [[UIView alloc] init];
    backgroundView.backgroundColor = [UIColor colorWithHexString:@"EEEEEE"];
    headerView.backgroundView = backgroundView;
    headerView.textLabel.textColor = [UIColor colorWithHexString:@"666666"];
    headerView.textLabel.font = [UIFont systemFontOfSize:14];
    switch (section) {
        case 0:{
            headerView.textLabel.text = @"联系人名片";
        }
            break;
        case 1:{
            headerView.textLabel.text = [NSString stringWithFormat:@"评论(%d)",(int)self.replyDataList.count];
        }
            break;
        default:
            break;
    }
    return headerView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (section) {
        case 0:{
            return self.detailModel.connected.count;
        }
            break;
        case 1:{
            return self.replyDataList.count;
        }
            break;
        default:
            return 10;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:{
            return 154;
        }
            break;
        case 1:{
            CGFloat height = [tableView fd_heightForCellWithIdentifier:kReplyListCellIdentifier configuration:^(JXLReplyListCell *cell) {
                [self configureCell:cell atIndexPath:indexPath];
            }];

            return height;
            return 73;
        }
            break;
        default:
            return 64;
            break;
    }
}
- (void)configureCell:(JXLReplyListCell *)cell atIndexPath:(NSIndexPath *)indexPath{
    cell.fd_isTemplateLayoutCell = YES;
    @weakify(self);
    [cell setCellModel:self.replyDataList[indexPath.row] headerImageClick:^(JXLFindReplyModel *replyModel) {
       @strongify(self);
        [JXViewControlerRouter pushToProfileOtherViewController:self animated:YES data:@{@"targetId":replyModel.created_by}];
    }];
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    @weakify(self);
    switch (indexPath.section) {
        case 0:{
            JXLContactCardCell *cell = [tableView dequeueReusableCellWithIdentifier:kContactCardCellIdentifier];
            [cell setCellData:self.detailModel.connected[indexPath.row] headerClickHandler:^(JXLPersonCardModel *personCardModel) {
                @strongify(self);
                [JXViewControlerRouter pushToProfileOtherViewController:self animated:YES data:@{@"targetId":personCardModel.identifier}];
            }];
            return cell;
        }
            break;
        case 1:{
            JXLReplyListCell *cell = [tableView dequeueReusableCellWithIdentifier:kReplyListCellIdentifier];
            [self configureCell:cell atIndexPath:indexPath];
            
            return cell;
        }
            break;
        default:{
            JXLContactCardCell *cell = [tableView dequeueReusableCellWithIdentifier:kContactCardCellIdentifier];
            [cell setCellData:self.detailModel.connected[indexPath.row] headerClickHandler:^(JXLPersonCardModel *personCardModel) {
                @strongify(self);
                [JXViewControlerRouter pushToProfileOtherViewController:self animated:YES data:@{@"targetId":personCardModel.identifier}];
            }];

            return cell;
        }
            break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        
    }else {
        JXLFindReplyModel *replyTo = self.replyDataList[indexPath.row];
        self.replyToPerson = replyTo;
        self.replyInputView.text = [NSString stringWithFormat:@"回复@%@:",self.replyToPerson.author];
        [self.replyInputView becomeFirstResponder];
    }
}

#pragma mark - webView delegate

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [self updateUI];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(nullable NSError *)error{
//    [MBProgressHUD showError:error.description toView:self.view];
}

#pragma mark - YYTextView delegate

- (void)textViewDidEndEditing:(YYTextView *)textView{
    
}
#pragma mark - getter
- (YYTextView *)replyInputView{
    if (!_replyInputView) {
        _replyInputView = [YYTextView new];
        _replyInputView.layer.borderWidth = 1;
        _replyInputView.layer.cornerRadius = 5;
        _replyInputView.layer.borderColor = [UIColor colorWithHexString:@"cccccc"].CGColor;

//        _replyInputView.textContainerInset = UIEdgeInsetsMake( 5, 5, 5, 0);
        _replyInputView.delegate = self;
        _replyInputView.font = [UIFont systemFontOfSize:16];
        _replyInputView.textColor = [UIColor colorWithHexString:@"333333"];
        _replyInputView.placeholderText = @"回复:";
        _replyInputView.placeholderTextColor = [UIColor colorWithHexString:@"999999"];
    }
    return _replyInputView;
}

#pragma mark - post Reply
- (void)postReplyContent:(NSString *)content{
    
}

#pragma mark - keyboardNotification

//注册键盘通知
- (void)addKeyboardNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];

}

//移除键盘通知
- (void)removeKeyboardNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

//键盘显示
- (void)keyboardWillShow:(NSNotification*)notification
{
    CGRect keyboardBounds = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    float height =  keyboardBounds.size.height;
    float duringTime = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];

    self.bottomViewBottomConstaint.constant = height;

    CGPoint point = self.tableView.contentOffset;
    point.y += height;
    
    [UIView animateWithDuration:duringTime
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self.view layoutIfNeeded];
                         [self.tableView setContentOffset:point];
                     } completion:^(BOOL finished) {
                     }];
}


//键盘隐藏
- (void)keyboardWillHide:(NSNotification*)notification
{
    float duringTime = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];

    self.bottomViewBottomConstaint.constant = 0;
    self.replyToPerson = nil;
    self.replyInputView.placeholderText = @"回复:";
    [UIView animateWithDuration:duringTime
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self.view layoutIfNeeded];
                     } completion:^(BOOL finished) {
                         
                     }];
}

@end

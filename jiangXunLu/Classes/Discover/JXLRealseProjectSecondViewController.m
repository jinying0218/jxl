//
//  JXLRealseProjectSecondViewController.m
//  jiangXunLu
//
//  Created by 金莹 on 16/9/8.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXLRealseProjectSecondViewController.h"
#import "jiangXunLu-swift.h"
#import "GlobalDefine.h"
#import "AFNTool.h"
#import "YYModel.h"
#import "UIImageView+WebCache.h"
#import "Masonry.h"
#import "EXTScope.h"
#import "MBProgressHUD+Add.h"
#import "KVOController.h"
#import "BlocksKit+UIKit.h"
#import "Hexcolor.h"
#import "YYText.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "JXLRealseCell.h"
#import "JXLReaseModel.h"
#import "YYThumbImagesView.h"
#import "JXLRealseProjectModel.h"
#import "JXLPickerViewController.h"

static NSString *const kRealseCellIdentifier = @"kRealseCellIdentifier";
static NSString *const kRealseInfoSectionHeaderIdentifier = @"kRealseInfoSectionHeaderIdentifier";

@interface JXLRealseProjectSecondViewController ()<UITableViewDelegate,UITableViewDataSource,YYThumbImagesViewDelegate>
@property (strong, nonatomic) JXLRealseProjectModel *projectModel;

@property (strong, nonatomic) UIImagePickerController *imagePickerController;
@property (strong, nonatomic) UIAlertController *actionSheet;
@property (strong, nonatomic) YYThumbImagesView *thumbImageView;
@property (strong, nonatomic) NSArray *tableDatasArray;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) JXLPickerViewController *dataPickerViewController;

@property (strong, nonatomic) NSArray *pickerDatas;
@property (strong, nonatomic) NSDictionary *realsePlist;
@property (strong, nonatomic) UIView *activeCell;

@end

@implementation JXLRealseProjectSecondViewController

- (instancetype)initWithProjectModel:(JXLRealseProjectModel *)projectModel
{
    self = [super init];
    if (self) {
        _projectModel = projectModel;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"项目";

    [self setUpLeftbarButtonWithImage:UIIMGName(@"back_normal")];
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setTitle:@"发布" forState:UIControlStateNormal];
    [rightButton setTitleColor:[UIColor colorWithHexString:@"FECD08"] forState:UIControlStateNormal];
    [rightButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [self setUpRightbarButtonWithCustomButton:rightButton];

    [self prepareData];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"JXLRealseCell" bundle:nil] forCellReuseIdentifier:kRealseCellIdentifier];
    [self.tableView registerClass:[UITableViewHeaderFooterView class] forHeaderFooterViewReuseIdentifier:kRealseInfoSectionHeaderIdentifier];
    self.tableView.backgroundColor = [UIColor colorWithHexString:@"eeeeee"];
    self.tableView.tableFooterView = self.thumbImageView;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self addKeyboardNotification];
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self removeKeyboardNotification];
}
- (void)leftBarItemTouchUpInside:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
//发布
- (void)rightBarItemTouchUpInside:(id)sender{
    
    [self.view endEditing:YES];
    NSUInteger section = 0;
    for (NSArray *subArray in self.tableDatasArray) {
        for (JXLReaseModel *realseModel in subArray) {
            if (!realseModel.content || realseModel.content.length == 0) {
                [MBProgressHUD showError:@"请将信息填写完整" toView:self.view];
                NSUInteger location = [subArray indexOfObject:realseModel];
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:location inSection:section];
                JXLRealseCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
                cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"find_remind"]];
                return;
            }
        }
        section ++;
    }

    NSString *g_uid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
    NSString *g_randcode = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"randcode"]];
    
    NSDictionary *data = @{@"title" : self.projectModel.title,
                           @"content" : self.projectModel.content,
                           @"cover" : self.projectModel.cover,
                           @"pics" : [self.projectModel.pics yy_modelToJSONString],
                           @"type" : self.projectModel.type,
                           @"brand_industry" : self.projectModel.brand_industry,
                           @"area" : self.projectModel.area,
                           @"addr_prov" : self.projectModel.addr_prov,
                           @"addr_city" : self.projectModel.addr_city,
                           @"addr_detail" : self.projectModel.addr_detail,
                           @"company" : self.projectModel.company,
                           @"position" : self.projectModel.position,
                           @"opening_time" : self.projectModel.opening_time};
    
    NSDictionary *params = @{@"action" : @"find_project_release",
                             @"uid" : g_uid,
                             @"randcode" : g_randcode,
                             @"data":[data yy_modelToJSONString]};
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    @weakify(self);
    [AFNTool post:Globe.baseUrlStr params:params success:^(id responsObject) {
        @strongify(self);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        //        NSLog(@"find_project_detail_load:%@",responsObject);
        if (responsObject && [responsObject isKindOfClass:[NSDictionary class]]) {
            NSInteger status = [responsObject[@"status"] integerValue];
            if (status == 1) {
                for (UIViewController *childVC in self.navigationController.viewControllers) {
                    if ([childVC isKindOfClass:[DiscoverViewController class]]) {
                        [self.navigationController popToViewController:childVC animated:YES];
                        break;
                    }
                }
            }
        }
    } failure:^(NSError *error) {
        @strongify(self);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:error.description toView:self.view];
        
        NSLog(@"%@",error.description);
    }];
}

#pragma mark - loadData
- (void)prepareData{
    
    if (self.projectModel.pics) {
        @weakify(self);
        for (NSString *imageString in self.projectModel.pics) {
            NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",Globe.baseUrlStr,imageString]];
            [[SDWebImageManager sharedManager] downloadImageWithURL:imageURL options:SDWebImageRetryFailed | SDWebImageAvoidAutoSetImage progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                @strongify(self);
                [self.thumbImageView insertOneImage:image refresh:^(YYThumbImagesView *thumbView) {
                    @strongify(self);
                    self.tableView.tableFooterView = thumbView;
                }];
            }];
        }
    }
    
    
    JXLReaseModel *cellModel1 = [[JXLReaseModel alloc] initWithTitleString:@"开发商" placeholder:@"如：乐天购物广场" content:(self.projectModel.company ? self.projectModel.company : nil) keyString:@"company"];
    JXLReaseModel *cellModel2 = [[JXLReaseModel alloc] initWithTitleString:@"开业时间" placeholder:nil content:(self.projectModel.opening_time ? self.projectModel.opening_time : nil) keyString:@"opening_time"];
    JXLReaseModel *cellModel3 = [[JXLReaseModel alloc] initWithTitleString:@"项目定位" placeholder:@"如：大众" content:(self.projectModel.position ? self.projectModel.position : nil) keyString:@"position"];
    JXLReaseModel *cellModel4 = [[JXLReaseModel alloc] initWithTitleString:nil placeholder:nil content:(self.projectModel.content ? self.projectModel.content : nil) keyString:@"content"];

    self.tableDatasArray = @[@[cellModel1,cellModel2,cellModel3],@[cellModel4]];
}
//上传二进制 图片
- (void)uploadImage:(UIImage *)uploadImage{
    NSString *g_uid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
    NSString *g_randcode = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"randcode"]];
    
    NSData *imageData = UIImageJPEGRepresentation(uploadImage, 0.8f);
    
    NSDictionary *data = @{@"type" : @"stream",
                           @"data" : imageData,
                           @"place" : @""};
    NSDictionary *params = @{@"action" : @"upload_img",
                             @"uid" : g_uid,
                             @"randcode" : g_randcode,
                             @"data":[data yy_modelToJSONString]};
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    @weakify(self);
    [AFNTool uploadRequestURL:Globe.baseUrlStr params:params fileData:imageData success:^(id responsObject) {
        //        NSLog(@"%@",responsObject);
        @strongify(self);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if ([responsObject[@"status"] intValue] == 1) {
            NSString *urlString = [responsObject[@"info"] objectForKey:@"url"];
            
            [self.projectModel.pics addObject:urlString];
            [self.thumbImageView insertOneImage:uploadImage refresh:^(YYThumbImagesView *thumbView) {
                @strongify(self);
                self.tableView.tableFooterView = thumbView;
            }];
        }else {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [MBProgressHUD showError:responsObject[@"msg"] toView:self.view];
        }
    } failure:^(NSError *error) {
        @strongify(self);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:error.description toView:self.view];
    }];
}



#pragma mark - YYThumbImagesViewDelegate
- (void)addImageButtonClick:(UIButton *)button{
    [self.view endEditing:YES];
    [self presentViewController:self.actionSheet animated:YES completion:nil];
}
- (void)deleteImageButton:(NSUInteger)deleteIndex{
    [self.projectModel.pics removeObjectAtIndex:deleteIndex];
}
#pragma mark - tableView delegate && tableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.tableDatasArray.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.tableDatasArray[section] count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 0;
    }
    return 52;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 0) {
        return 0;
    }
    return 18;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return [[UIView alloc] init];
    }
    UITableViewHeaderFooterView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:kRealseInfoSectionHeaderIdentifier];
    headerView.contentView.backgroundColor = [UIColor colorWithHexString:@"eeeeee"];
    headerView.textLabel.font = [UIFont systemFontOfSize:14];
    headerView.textLabel.textColor = [UIColor colorWithHexString:@"333333"];
    headerView.textLabel.text = @"项目介绍";
    return headerView;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section == 0) {
        return [[UIView alloc] init];
    }
    UITableViewHeaderFooterView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:kRealseInfoSectionHeaderIdentifier];
    headerView.contentView.backgroundColor = [UIColor colorWithHexString:@"eeeeee"];
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    JXLReaseModel *cellModel = self.tableDatasArray[indexPath.section][indexPath.row];
    if (!cellModel.content || cellModel.content.length == 0) {
        if (indexPath.section == 1) {
            return 100;
        }
        return 50;
    }else {
        if (indexPath.section == 1) {
            return (cellModel.cellHeight + 30) < 100 ? 100 : (cellModel.cellHeight + 30);
        }
        return (cellModel.cellHeight + 30) < 50 ? 50 : (cellModel.cellHeight + 30);
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    JXLRealseCell *cell = [tableView dequeueReusableCellWithIdentifier:kRealseCellIdentifier];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}
- (void)configureCell:(JXLRealseCell *)cell atIndexPath:(NSIndexPath *)indexPath{
    
    JXLReaseModel *cellModel = self.tableDatasArray[indexPath.section][indexPath.row];
    @weakify(self);
    [cell configureTableView:self.tableView CellData:cellModel indexPath:indexPath handler:^(NSString *inputString) {
        @strongify(self);
        if ([inputString isEqualToString:@""] || !inputString) {
            return ;
        }
        [self.projectModel setValue:inputString forKey:cellModel.keyString];
    } activeBlock:^(UIView *activeView) {
        @strongify(self);
        self.activeCell = activeView;
    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.view endEditing:YES];
    JXLRealseCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    JXLReaseModel *cellModel = cell.cellModel;
    @weakify(self);
    if ([cellModel.keyString isEqualToString:@"opening_time"]) {
        [self.dataPickerViewController setDatePickerViewChooseCompletion:^(NSString *selectedString) {
            @strongify(self);
            cellModel.content = selectedString;
            [self.projectModel setValue:selectedString forKey:cellModel.keyString];
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        }];
        [self addChildViewController:self.dataPickerViewController];
        [self.view addSubview:self.dataPickerViewController.view];
        return;
    }

    //设置是否多选
//    if ([cellModel.keyString isEqualToString:@"position"]) {
//        [self.dataPickerViewController setIsAllowMultableSelection:YES];
//    }
    [self.dataPickerViewController setIsAllowMultableSelection:YES];

    NSArray *array = self.realsePlist[cellModel.keyString];
    [self.dataPickerViewController setTableDatas:array chooseCompletion:^(NSString *selectedString) {
        @strongify(self);
        cellModel.content = selectedString;
        [self.projectModel setValue:selectedString forKey:cellModel.keyString];
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
    self.dataPickerViewController.titleLabel.text = cellModel.title;
    [self addChildViewController:self.dataPickerViewController];
    [self.view addSubview:self.dataPickerViewController.view];
}


#pragma mark - getter
- (UIImagePickerController *)imagePickerController{
    if (!_imagePickerController) {
        _imagePickerController = [[UIImagePickerController alloc] init];
        _imagePickerController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        _imagePickerController.allowsEditing = NO;
        @weakify(self);
        [_imagePickerController setBk_didCancelBlock:^(UIImagePickerController *imagePickerController) {
            [imagePickerController dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [_imagePickerController setBk_didFinishPickingMediaBlock:^(UIImagePickerController *imagePickerController, NSDictionary <NSString *,id>*info) {
            @strongify(self);
            [imagePickerController dismissViewControllerAnimated:YES completion:^{
                UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
                if (!image) {
                    image = [info objectForKey:UIImagePickerControllerOriginalImage];
                }
                [self uploadImage:image];
            }];
        }];
    }
    return _imagePickerController;
}
- (UIAlertController *)actionSheet{
    if (!_actionSheet){
        _actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        @weakify(self);
        [_actionSheet addAction:[UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            @strongify(self);
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
                self.imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                [self presentViewController:self.imagePickerController animated:YES completion:nil];
            }
            [_actionSheet dismissViewControllerAnimated:YES completion:nil];
        }]];
        [_actionSheet addAction:[UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            @strongify(self);
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                self.imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:self.imagePickerController animated:YES completion:nil];
            }else {
                [MBProgressHUD showError:@"没有安装摄像头" toView:self.view];
            }
            
            [_actionSheet dismissViewControllerAnimated:YES completion:nil];
        }]];
        [_actionSheet addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [_actionSheet dismissViewControllerAnimated:YES completion:nil];
        }]];
        
    }
    return _actionSheet;
}

- (YYThumbImagesView *)thumbImageView{
    if (!_thumbImageView) {
        _thumbImageView = [[YYThumbImagesView alloc] init];
        _thumbImageView.delegate = self;
        _thumbImageView.height = 101;
    }
    return _thumbImageView;
}
- (JXLPickerViewController *)dataPickerViewController{
    if (!_dataPickerViewController) {
        _dataPickerViewController = [[JXLPickerViewController alloc] init];
        _dataPickerViewController.view.frame = CGRectMake( 0, 0, self.view.width, self.view.height);
    }
    return _dataPickerViewController;
}

- (NSDictionary *)realsePlist{
    if (!_realsePlist) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"JXLRealseProperty" ofType:@"plist"];
        _realsePlist = [[NSDictionary alloc] initWithContentsOfFile:path];
        
    }
    return _realsePlist;
}

#pragma mark - keyboardNotification

//注册键盘通知
- (void)addKeyboardNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
}

//移除键盘通知
- (void)removeKeyboardNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

//键盘显示
- (void)keyboardWillShow:(NSNotification*)notification
{
    CGRect keyboardBounds = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    //    float height =  keyboardBounds.size.height;
    float duringTime = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    CGSize kbSize = keyboardBounds.size;
    
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    NSLog(@"keyboardWasShown");
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= (kbSize.height + 64);
    
    if (!CGRectContainsPoint(aRect, CGPointMake(self.activeCell.origin.x, self.activeCell.origin.y + 64))) {
        
        CGPoint scrollPoint = CGPointMake(0.0, (self.activeCell.frame.origin.y + self.activeCell.height - aRect.size.height) + 44);
        
        [UIView animateWithDuration:duringTime
                              delay:0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             [self.view layoutIfNeeded];
                             [self.tableView setContentOffset:scrollPoint animated:YES];
                         } completion:^(BOOL finished) {
                         }];
    }
    
    
    
}


//键盘隐藏
- (void)keyboardWillHide:(NSNotification*)notification
{
    float duringTime = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    
    [UIView animateWithDuration:duringTime
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self.view layoutIfNeeded];
                     } completion:^(BOOL finished) {
                         
                     }];
}

@end

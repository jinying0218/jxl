//
//  JXProfileDetailTableManager.m
//  jiangXunLu
//
//  Created by lixu06 on 16/6/19.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXProfileDetailTableManager.h"
#import "JXProfileEditViewController.h"
#import "EXTScope.h"
#import "GlobalDefine.h"
#import "Color.h"
#import "String.h"
#import "Dimen.h"
#import "JXTipHud.h"
#import "JXViewControlerRouter.h"
#import "JXListActionSheet.h"
#import "JXPickerActionSheet.h"
#import "CommonTools.h"

#define PROFILE_EDIT_ACTION_SHEET_TAG_GENDER 1
#define PROFILE_EDIT_ACTION_SHEET_TAG_LOCATION 2
#define PROFILE_EDIT_ACTION_SHEET_TAG_SHOWFLAG 3
#define PROFILE_EDIT_ACTION_SHEET_TAG_PARTY 4

@interface JXProfileDetailTableManager ()<JXCustomActionSheetDelegate>

@property (nonatomic,strong) NSDictionary *settingDic; // 传入控制



@end

@implementation JXProfileDetailTableManager

- (instancetype)initWithDictionary:(NSDictionary*)dic
                    viewController:(UIViewController *)controller
                         tableView:(UITableView *)tableView
                     finishHanlder:(CommomEditBlock)finishBlock {
    if (self = [super initWithDictionary:dic
                          viewController:controller
                               tableView:tableView
                           finishHanlder:finishBlock]) {
        self.settingDic = dic[@"settingDic"]?dic[@"settingDic"]:@{};
        self.postDic = [NSMutableDictionary dictionaryWithCapacity:0];
        return self;
    }
    return nil;
}

- (NSMutableArray *)getHeaderInfoList{
    self.headerInfoList = [super getHeaderInfoList];
    [self.headerInfoList addObject:[@{@"ViewIdentifier":@"null",
                                      } mutableCopy]];
    return self.headerInfoList;
}

- (NSMutableArray *)getCellInfoList {
    self.cellInfoList = [super getCellInfoList];
    
    BOOL editable = self.settingDic[@"editable"]?[self.settingDic[@"editable"] boolValue]:NO;

    NSString *name;
    NSString *job;
    NSString *phone;
    NSString *region;
    NSString *gender;
    NSString *brand;
    NSString *company;
//    NSString *programName;
    NSString *programIntro;
    NSString *programInfo;
    NSInteger showFlag;
    NSInteger party;

    
    NSDictionary *userDic = self.resultDic[@"user_info"];
    NSDictionary *programDic = self.resultDic[@"project_info"];

    if (editable) {
        name = [CommonTools getValidText:userDic[@"name"] withPlaceHolder:STRING_PROFILE_EDIT_NAME_DEFAULT];
        gender = [CommonTools getValidText:userDic[@"sex"] withPlaceHolder:STRING_PROFILE_EDIT_GENDER_DEFAULT];
        phone = [CommonTools getValidText:userDic[@"phone"] withPlaceHolder:STRING_PROFILE_EDIT_PHONE_DEFAULT];
        job = [CommonTools getValidText:userDic[@"job"] withPlaceHolder:STRING_PROFILE_EDIT_POSITION_DEFAULT];
        company = [CommonTools getValidText:userDic[@"company"] withPlaceHolder:STRING_PROFILE_COMPANY_LABLE_DEFAULT];
        brand = [CommonTools getValidText:userDic[@"brand"] withPlaceHolder:STRING_PROFILE_BRAND_LABLE_DEFAULT];
        region = [CommonTools getValidText:userDic[@"region"] withPlaceHolder:STRING_PROFILE_LOCATION_DEFAULT];
        showFlag = [[CommonTools getValidText:userDic[@"show_flag"] withPlaceHolder:STRING_PROFILE_EDIT_SHOW_FLAG_DEFAULT] integerValue];
//        programName = [CommonTools getValidText:programDic[@"name"] withPlaceHolder:STRING_PROFILE_EDIT_PROGRAM_HINT];
        programIntro = [CommonTools getValidText:programDic[@"brief"] withPlaceHolder:STRING_PROFILE_EDIT_PROGRAM_BRIEF_DEFAULT];
        programInfo = [CommonTools getValidText:programDic[@"detail"] withPlaceHolder:STRING_PROFILE_EDIT_PROGRAM_INFO_DEFAULT];
        party = [[CommonTools getValidText:userDic[@"party"] withPlaceHolder:STRING_PROFILE_EDIT_PARTY_DEFAULT] integerValue];

    } else {
        name = [CommonTools getValidText:userDic[@"name"] withPlaceHolder:STRING_PROFILE_NAME_DEFAULT];
        gender = [CommonTools getValidText:userDic[@"sex"] withPlaceHolder:STRING_PROFILE_GENDER_DEFAULT];
        phone = [CommonTools getValidText:userDic[@"phone"] withPlaceHolder:STRING_PROFILE_PHONE_DEFAULT];
        job = [CommonTools getValidText:userDic[@"job"] withPlaceHolder:STRING_PROFILE_POSITION_DEFAULT];
        company = [CommonTools getValidText:userDic[@"company"] withPlaceHolder:STRING_PROFILE_COMPANY_LABLE_DEFAULT];
        brand = [CommonTools getValidText:userDic[@"brand"] withPlaceHolder:STRING_PROFILE_BRAND_LABLE_DEFAULT];
        region = [CommonTools getValidText:userDic[@"region"] withPlaceHolder:STRING_PROFILE_LOCATION_DEFAULT];
        showFlag = [[CommonTools getValidText:userDic[@"show_flag"] withPlaceHolder:STRING_PROFILE_EDIT_SHOW_FLAG_DEFAULT] integerValue];
//        programName = [CommonTools getValidText:programDic[@"name"] withPlaceHolder:STRING_PROFILE_PROGRAM_LABLE_DEFAULT];
        programIntro = [CommonTools getValidText:programDic[@"brief"] withPlaceHolder:STRING_PROFILE_PROGRAM_INTRO_DEFAULT];
        programInfo = [CommonTools getValidText:programDic[@"detail"] withPlaceHolder:STRING_PROFILE_PROGRAM_INFO_DEFAULT];
        party = [[CommonTools getValidText:userDic[@"party"] withPlaceHolder:STRING_PROFILE_EDIT_PARTY_DEFAULT] integerValue];
    }
    
    NSMutableArray *firstSectionList = [NSMutableArray arrayWithCapacity:0];
    [firstSectionList addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                 @"JXCustomSectionVariableTableViewCell", @"CellIdentifier",
                                 @(10),@"height",
                                 @(NO),@"isEnd",
                                 UIColorFromRGB(DEFINE_COLOR_BACK),@"backgroundColor",
                                 nil]];
    [firstSectionList addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                 @"JXCustomInputTableViewCell", @"CellIdentifier",
                                 @(UITableViewCellSelectionStyleGray),@"selectionStyle",
                                 @(NO), @"isEnd",
                                 @(YES), @"isFold",
                                 @(YES), @"isFixed",
                                 @(editable), @"isActive",
                                 @(YES), @"isFullLine",
                                 @(1),@"numberOfLines",
                                 @(0), @"maxChar",
                                 STRING_PROFILE_EDIT_NAME, @"inputLable",
                                 UIColorFromRGB(0xAAAAAA),@"textColor",
                                 @(NSTextAlignmentLeft), @"textAlignment",
                                 name, @"textValue",
                                 @"name", @"keyName",
                                 !editable?@"voidFunction:":@"pushToProfileEditViewControllerName:",@"clickBlock",
                                 @"textChangedForKey:",@"changedBlock",
                                 nil]];

    NSString *genderString = @"";
    if ([gender integerValue] == 1) {
        genderString = STRING_PROFILE_EDIT_GENDER_MALE;
    } else if ([gender integerValue] == 2) {
        genderString = STRING_PROFILE_EDIT_GENDER_FEMALE;
    }
    [firstSectionList addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                 @"JXCustomLabelTableViewCell", @"CellIdentifier",
                                 @(UITableViewCellSelectionStyleGray),@"selectionStyle",
                                 @(NO), @"isEnd",
                                 @(!editable), @"isFold",
                                 @(YES), @"isFixed",
                                 @(NO), @"isActive",
                                 @(YES), @"isFullLine",
                                 @(0), @"maxChar",
                                 @(1),@"numberOfLines",
                                 STRING_PROFILE_EDIT_GENDER, @"inputLable",
                                 UIColorFromRGB(0xAAAAAA),@"textColor",
                                 @(NSTextAlignmentLeft), @"textAlignment",
                                 genderString, @"textValue",
                                 @"sex", @"keyName",
                                 !editable?@"voidFunction:":@"selectGender:",@"clickBlock",
                                 nil]];

    if (editable) {
        [firstSectionList addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                     @"JXCustomInputCheckTableViewCell", @"CellIdentifier",
                                     @(UITableViewCellSelectionStyleGray),@"selectionStyle",
                                     @(NO), @"isEnd",
                                     @(NO), @"isFold",
                                     @(YES), @"isFixed",
                                     @(editable), @"isActive",
                                     @(YES), @"isFullLine",
                                     @(11), @"maxChar",
                                     @(1),@"numberOfLines",
                                     STRING_PROFILE_EDIT_PHONE, @"inputLable",
                                     UIColorFromRGB(0xAAAAAA),@"textColor",
                                     @(NSTextAlignmentLeft), @"textAlignment",
                                     phone, @"textValue",
                                     @"phone", @"keyName",
                                     @(showFlag == 1),@"isChecked",
                                     !editable?@"voidFunction:":@"pushToProfileEditViewControllerPhone:",@"clickBlock",
                                     @"checkAction:",@"checkBlock",
                                     nil]];
    } else{
        if (showFlag == 1) {
            [firstSectionList addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                         @"JXCustomLabelTableViewCell", @"CellIdentifier",
                                         @(UITableViewCellSelectionStyleGray),@"selectionStyle",
                                         @(NO), @"isEnd",
                                         @(!editable), @"isFold",
                                         @(YES), @"isFixed",
                                         @(NO), @"isActive",
                                         @(YES), @"isFullLine",
                                         @(0), @"maxChar",
                                         @(1),@"numberOfLines",
                                         STRING_PROFILE_EDIT_PHONE, @"inputLable",
                                         UIColorFromRGB(0xAAAAAA),@"textColor",
                                         @(NSTextAlignmentLeft), @"textAlignment",
                                         phone, @"textValue",
                                         @"phone", @"keyName",
                                         !editable?@"voidFunction:":@"pushToProfileEditViewControllerPhone:",@"clickBlock",
                                         nil]];
        }
    }
    
    [firstSectionList addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                 @"JXCustomInputTableViewCell", @"CellIdentifier",
                                 @(UITableViewCellSelectionStyleGray),@"selectionStyle",
                                 @(NO), @"isEnd",
                                 @(YES), @"isFold",
                                 @(YES), @"isFixed",
                                 @(editable), @"isActive",
                                 @(YES), @"isFullLine",
                                 @(0), @"maxChar",
                                 @(1),@"numberOfLines",
                                 STRING_PROFILE_EDIT_COMPANY, @"inputLable",
                                 UIColorFromRGB(0xAAAAAA),@"textColor",
                                 @(NSTextAlignmentLeft), @"textAlignment",
                                 company, @"textValue",
                                 @"company", @"keyName",
                                 !editable?@"voidFunction:":@"pushToProfileEditViewControllerCompany:",@"clickBlock",
                                 @"textChangedForKey:",@"changedBlock",
                                 nil]];
    
    [firstSectionList addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                 @"JXCustomInputTableViewCell", @"CellIdentifier",
                                 @(UITableViewCellSelectionStyleGray),@"selectionStyle",
                                 @(NO), @"isEnd",
                                 @(YES), @"isFold",
                                 @(YES), @"isFixed",
                                 @(editable), @"isActive",
                                 @(YES), @"isFullLine",
                                 @(0), @"maxChar",
                                 @(1),@"numberOfLines",
                                 STRING_PROFILE_EDIT_POSITION, @"inputLable",
                                 UIColorFromRGB(0xAAAAAA),@"textColor",
                                 @(NSTextAlignmentLeft), @"textAlignment",
                                 job, @"textValue",
                                 @"job", @"keyName",
                                 !editable?@"voidFunction:":@"pushToProfileEditViewControllerPosition:",@"clickBlock",
                                 @"textChangedForKey:",@"changedBlock",
                                 nil]];
    
    [firstSectionList addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                 @"JXCustomLabelTableViewCell", @"CellIdentifier",
                                 @(UITableViewCellSelectionStyleGray),@"selectionStyle",
                                 @(NO), @"isEnd",
                                 @(!editable), @"isFold",
                                 @(YES), @"isFixed",
                                 @(NO), @"isActive",
                                 @(YES), @"isFullLine",
                                 @(0), @"maxChar",
                                 @(1),@"numberOfLines",
                                 STRING_PROFILE_EDIT_LOCATION, @"inputLable",
                                 UIColorFromRGB(0xAAAAAA),@"textColor",
                                 @(NSTextAlignmentLeft), @"textAlignment",
                                 region, @"textValue",
                                 @"region", @"keyName",
                                 !editable?@"voidFunction:":@"selectLocation:",@"clickBlock",
                                 nil]];
    [firstSectionList addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                 @"JXCustomSectionVariableTableViewCell", @"CellIdentifier",
                                 @(10),@"height",
                                 @(NO),@"isEnd",
                                 UIColorFromRGB(DEFINE_COLOR_BACK),@"backgroundColor",
                                 nil]];
    
//    if (editable) {
//        NSString *showFlagString = @"";
//        if (showFlag == 1) {
//            showFlagString = STRING_PROFILE_EDIT_SHOW_FLAG_YES;
//        } else if (showFlag == 0) {
//            showFlagString = STRING_PROFILE_EDIT_SHOW_FLAG_NO;
//        }
//        
//        [firstSectionList addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                     @"JXCustomLabelTableViewCell", @"CellIdentifier",
//                                     @(UITableViewCellSelectionStyleGray),@"selectionStyle",
//                                     @(NO), @"isEnd",
//                                     @(!editable), @"isFold",
//                                     @(YES), @"isFixed",
//                                     @(NO), @"isActive",
//                                     @(YES), @"isFullLine",
//                                     @(0), @"maxChar",
//                                     @(1),@"numberOfLines",
//                                     STRING_PROFILE_EDIT_SHOW_FLAG, @"inputLable",
//                                     UIColorFromRGB(0xAAAAAA),@"textColor",
//                                     @(NSTextAlignmentLeft), @"textAlignment",
//                                     showFlagString, @"textValue",
//                                     @"showFlag", @"keyName",
//                                     !editable?@"voidFunction:":@"selectShowFlag:",@"clickBlock",
//                                     nil]];
//    }

    NSString *partyString = @"";
    if (party == 1) {
        partyString = STRING_PROFILE_EDIT_PARTY_A;
    } else if (party == 2) {
        partyString = STRING_PROFILE_EDIT_PARTY_B;
    } else if (party == 3) {
        partyString = STRING_PROFILE_EDIT_PARTY_OTHER;
    } else {
        partyString = STRING_PROFILE_EDIT_PARTY_UNSET;
    }
    
    [firstSectionList addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                 @"JXCustomLabelTableViewCell", @"CellIdentifier",
                                 @(UITableViewCellSelectionStyleGray),@"selectionStyle",
                                 @(NO), @"isEnd",
                                 @(!editable), @"isFold",
                                 @(YES), @"isFixed",
                                 @(NO), @"isActive",
                                 @(YES), @"isFullLine",
                                 @(0), @"maxChar",
                                 @(1),@"numberOfLines",
                                 STRING_PROFILE_EDIT_PARTY, @"inputLable",
                                 UIColorFromRGB(0xAAAAAA),@"textColor",
                                 @(NSTextAlignmentLeft), @"textAlignment",
                                 partyString, @"textValue",
                                 @"party", @"keyName",
                                 !editable?@"voidFunction:":@"selectParty:",@"clickBlock",
                                 nil]];
    
    [firstSectionList addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                 @"JXCustomSectionVariableTableViewCell", @"CellIdentifier",
                                 @(10),@"height",
                                 @(NO),@"isEnd",
                                 UIColorFromRGB(DEFINE_COLOR_BACK),@"backgroundColor",
                                 nil]];
    
    [firstSectionList addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                 @"JXCustomInputTableViewCell", @"CellIdentifier",
                                 @(UITableViewCellSelectionStyleGray),@"selectionStyle",
                                 @(NO), @"isEnd",
                                 @(YES), @"isFold",
                                 @(YES), @"isFixed",
                                 @(editable), @"isActive",
                                 @(YES), @"isFullLine",
                                 @(0), @"maxChar",
                                 @(1),@"numberOfLines",
                                 STRING_PROFILE_EDIT_BRAND, @"inputLable",
                                 UIColorFromRGB(0xAAAAAA),@"textColor",
                                 @(NSTextAlignmentLeft), @"textAlignment",
                                 brand, @"textValue",
                                 @"brand", @"keyName",
                                 !editable?@"voidFunction:":@"pushToProfileEditViewControllerBrand:",@"clickBlock",
                                 @"textChangedForKey:",@"changedBlock",
                                 nil]];
//    [firstSectionList addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                 @"JXCustomLabelTableViewCell", @"CellIdentifier",
//                                 @(UITableViewCellSelectionStyleGray),@"selectionStyle",
//                                 @(NO), @"isEnd",
//                                 @(!editable), @"isFold",
//                                 @(YES), @"isFixed",
//                                 @(NO), @"isActive",
//                                 @(YES), @"isFullLine",
//                                 @(0), @"maxChar",
//                                 @(1),@"numberOfLines",
//                                 STRING_PROFILE_EDIT_PROGRAM, @"inputLable",
//                                 UIColorFromRGB(0xAAAAAA),@"textColor",
//                                 @(NSTextAlignmentLeft), @"textAlignment",
//                                 programName, @"textValue",
//                                 @"brandName", @"keyName",
//                                 !editable?@"voidFunction:":@"pushToProfileEditViewControllerProgram:",@"clickBlock",
//                                 nil]];
    [firstSectionList addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                 @"JXCustomSectionTitleTableViewCell", @"CellIdentifier",
                                 STRING_PROFILE_EDIT_PROGRAM_BRIEF, @"lableValue",
                                 UIColorFromRGB(DEFINE_COLOR_BACK), @"backgroundColor",
                                 @(1.0f), @"borderWidth",
                                 @(7.0f), @"padding",
                                 FONTS(12.0f),@"font",
                                 @(8),@"paddingTop",
                                 @(20),@"paddingRight",
                                 @(8),@"paddingBottom",
                                 @(20),@"paddingLeft",
                                 nil]];
    
    [firstSectionList addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                 @"JXCustomLabelTableViewCell", @"CellIdentifier",
                                 @(UITableViewCellSelectionStyleGray),@"selectionStyle",
                                 @(NO), @"isEnd",
                                 @(!editable), @"isFold",
                                 @(YES), @"isFixed",
                                 @(NO), @"isActive",
                                 @(YES), @"isFullLine",
                                 @(0), @"maxChar",
                                 @(75),@"height",
                                 ([STRING_PROFILE_EDIT_PROGRAM_BRIEF_DEFAULT isEqualToString:programIntro])?@(1): @(3),@"numberOfLines",
                                 @"", @"inputLable",
                                 UIColorFromRGB(0xAAAAAA),@"textColor",
                                 ([STRING_PROFILE_EDIT_PROGRAM_BRIEF_DEFAULT isEqualToString:programIntro])?@(NSTextAlignmentCenter): @(NSTextAlignmentLeft), @"textAlignment",
                                 programIntro, @"textValue",
                                 @"programIntro", @"keyName",
                                 !editable?@"voidFunction:":@"pushToProfileEditViewControllerProgramBrief:",@"clickBlock",
                                 nil]];
    
    [firstSectionList addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                 @"JXCustomSectionTitleTableViewCell", @"CellIdentifier",
                                 STRING_PROFILE_EDIT_PROGRAM_INFO, @"lableValue",
                                 UIColorFromRGB(DEFINE_COLOR_BACK), @"backgroundColor",
                                 @(1.0f), @"borderWidth",
                                 @(7.0f), @"padding",
                                 FONTS(12.0f),@"font",
                                 @(8),@"paddingTop",
                                 @(20),@"paddingRight",
                                 @(8),@"paddingBottom",
                                 @(20),@"paddingLeft",
                                 nil]];
    
    [firstSectionList addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                 @"JXCustomLabelTableViewCell", @"CellIdentifier",
                                 @(UITableViewCellSelectionStyleGray),@"selectionStyle",
                                 @(NO), @"isEnd",
                                 @(!editable), @"isFold",
                                 @(YES), @"isFixed",
                                 @(NO), @"isActive",
                                 @(YES), @"isFullLine",
                                 @(0), @"maxChar",
                                 @(75),@"height",
                                 ([STRING_PROFILE_EDIT_PROGRAM_INFO_DEFAULT isEqualToString:programInfo])?@(1): @(3),@"numberOfLines",
                                 @"", @"inputLable",
                                 UIColorFromRGB(0xAAAAAA),@"textColor",
                                 ([STRING_PROFILE_EDIT_PROGRAM_INFO_DEFAULT isEqualToString:programInfo])?@(NSTextAlignmentCenter): @(NSTextAlignmentLeft), @"textAlignment",
                                 programInfo, @"textValue",
                                 @"location", @"keyName",
                                 !editable?@"voidFunction:":@"pushToProfileEditViewControllerProgramInfo:",@"clickBlock",
                                 nil]];
    if (editable) {
//        [firstSectionList addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                     @"JXCustomSubmitTableViewCell", @"CellIdentifier",
//                                     UIColorFromRGB(DEFINE_COLOR_BACK), @"backgroundColor",
//                                     @(150), @"height",
//                                     @(52), @"marginTop",
//                                     @(80), @"buttonWidth",
//                                     @(26), @"buttonHeight",
//                                     STRING_PROFILE_EDIT_SUBMIT_TEXT, @"buttonText",
//                                     UIColorFromRGB(DEFINE_COLOR_BROWN), @"bottonBackgroundColor",
//                                     UIColorFromRGB(DEFINE_COLOR_WHITE), @"buttonTextColor",
//                                     @(3), @"buttonCornerRadius",
//                                     @"requestDataUpdate",@"submitBlock",
//                                     FONTS(FONTSIZE_MIDDLE), @"buttonTextFont",
//                                     nil]];

    } else {
        [firstSectionList addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                     @"JXCustomSectionVariableTableViewCell", @"CellIdentifier",
                                     @(15),@"height",
                                     @(NO),@"isEnd",
                                     UIColorFromRGB(DEFINE_COLOR_BACK),@"backgroundColor",
                                     nil]];
    }
    
    [self.cellInfoList addObject:firstSectionList];
    
    return self.cellInfoList;
}

- (void)registerTableCell {
    [super registerTableCell];
}

#pragma mark -
#pragma mark Private Methods
-(void)checkAction:(NSDictionary *)cellDic{
    if (cellDic[@"isChecked"] && [cellDic[@"isChecked"] boolValue]) {
        self.postDic[@"show_flag"] = @1;
    } else {
        self.postDic[@"show_flag"] = @0;
    }
}
-(void)selectParty:(NSDictionary *)cellDic{
    [self.controller.view endEditing:YES];
    JXListActionSheet *actionSheet = [[JXListActionSheet alloc] initWithData:[JXListActionSheet getPartyStyleDic]
                                                                    delegate:self];
    actionSheet.tag = PROFILE_EDIT_ACTION_SHEET_TAG_PARTY;
    [actionSheet show];
}
-(void)selectShowFlag:(NSDictionary *)cellDic{
    [self.controller.view endEditing:YES];
    JXListActionSheet *actionSheet = [[JXListActionSheet alloc] initWithData:[JXListActionSheet getShowFlagStyleDic]
                                                                    delegate:self];
    actionSheet.tag = PROFILE_EDIT_ACTION_SHEET_TAG_SHOWFLAG;
    [actionSheet show];
}
-(void)selectGender:(NSDictionary *)cellDic{
    [self.controller.view endEditing:YES];
    JXListActionSheet *actionSheet = [[JXListActionSheet alloc] initWithData:[JXListActionSheet getGenderStyleDic]
                                                                    delegate:self];
    actionSheet.tag = PROFILE_EDIT_ACTION_SHEET_TAG_GENDER;
    [actionSheet show];
}
-(void)selectLocation:(NSDictionary *)cellDic{
    [self.controller.view endEditing:YES];
    JXPickerActionSheet *actionSheet = [[JXPickerActionSheet alloc] initWithData:[JXPickerActionSheet getLocationStyleDic]
                                                                    delegate:self];
    actionSheet.tag = PROFILE_EDIT_ACTION_SHEET_TAG_LOCATION;
    [actionSheet show];
}
-(void)pushToProfileEditViewControllerName:(NSDictionary *)cellDic{
    NSString *vcTitle = [NSString stringWithFormat:@"%@%@",STRING_PROFILE_EDIT_DEFAULT,cellDic[@"inputLable"]];
    NSDictionary *settingDic= @{@"ViewControllerTitle":vcTitle,
                                @"JXProfileEditViewControllerType":@(JXProfileEditViewControllerTypeView),
                                @"JXProfileEditViewControllerMask":@(JXProfileEditViewControllerMaskName),
                                @"keyName":cellDic[@"keyName"],
                                @"textValue":cellDic[@"textValue"],
                                @"hintValue":STRING_PROFILE_EDIT_NAME_HINT,
                                @"height":@(140),
                                @"heightFlexible":@(NO),
                                @"maxChar":@(20)};
    NSString *keyName = cellDic[@"keyName"];
    NSIndexPath * curIndex = cellDic[@"indexPath"];
    @weakify(self)
    [JXViewControlerRouter pushToProfileEditViewController:self.controller
                                                  animated:YES
                                                      data:settingDic
                                               finishBlock:^(NSDictionary *kvDic) {
                                                   @strongify(self)
                                                   if ([kvDic objectForKey:keyName]) {
                                                       NSString *keyValue = [NSString stringWithFormat:@"%@",kvDic[keyName]];
                                                       self.postDic[@"name"] = keyValue;
                                                       self.cellInfoList[curIndex.section][curIndex.row][@"textValue"] = keyValue;
                                                       [self.tableView reloadRowsAtIndexPaths:@[curIndex] withRowAnimation:NO];
                                                   }
                                                   
     }];
}

-(void)pushToProfileEditViewControllerPhone:(NSDictionary *)cellDic{
    NSString *vcTitle = [NSString stringWithFormat:@"%@%@",STRING_PROFILE_EDIT_DEFAULT,cellDic[@"inputLable"]];
    NSDictionary *settingDic= @{@"ViewControllerTitle":vcTitle,
                                @"JXProfileEditViewControllerType":@(JXProfileEditViewControllerTypeView),
                                @"JXProfileEditViewControllerMask":@(JXProfileEditViewControllerMaskPhone),
                                @"keyName":cellDic[@"keyName"],
                                @"textValue":cellDic[@"textValue"],
                                @"hintValue":STRING_PROFILE_EDIT_PHONE_HINT,
                                @"height":@(140),
                                @"heightFlexible":@(NO),
                                @"maxChar":@(11)};
    NSString *keyName = cellDic[@"keyName"];
    NSIndexPath * curIndex = cellDic[@"indexPath"];
    @weakify(self)
    [JXViewControlerRouter pushToProfileEditViewController:self.controller
                                                  animated:YES
                                                      data:settingDic
                                               finishBlock:^(NSDictionary *kvDic) {
                                                   @strongify(self)
                                                   if ([kvDic objectForKey:keyName]) {
                                                       NSString *keyValue = [NSString stringWithFormat:@"%@",kvDic[keyName]];
                                                       self.postDic[@"phone"] = keyValue;
                                                       self.cellInfoList[curIndex.section][curIndex.row][@"textValue"] = keyValue;
                                                       [self.tableView reloadRowsAtIndexPaths:@[curIndex] withRowAnimation:NO];
                                                   }
                                                  
                                               }];
}

-(void)pushToProfileEditViewControllerCompany:(NSDictionary *)cellDic{
    NSString *vcTitle = [NSString stringWithFormat:@"%@%@",STRING_PROFILE_EDIT_DEFAULT,cellDic[@"inputLable"]];
    NSDictionary *settingDic= @{@"ViewControllerTitle":vcTitle,
                                @"JXProfileEditViewControllerType":@(JXProfileEditViewControllerTypeView),
                                @"JXProfileEditViewControllerMask":@(JXProfileEditViewControllerMaskPhone),
                                @"keyName":cellDic[@"keyName"],
                                @"textValue":cellDic[@"textValue"],
                                @"hintValue":STRING_PROFILE_EDIT_COMPANY_HINT,
                                @"height":@(140),
                                @"heightFlexible":@(NO),
                                @"maxChar":@(20)};
    NSString *keyName = cellDic[@"keyName"];
    NSIndexPath * curIndex = cellDic[@"indexPath"];
    @weakify(self)
    [JXViewControlerRouter pushToProfileEditViewController:self.controller
                                                  animated:YES
                                                      data:settingDic
                                               finishBlock:^(NSDictionary *kvDic) {
                                                   @strongify(self)
                                                   if ([kvDic objectForKey:keyName]) {
                                                       NSString *keyValue = [NSString stringWithFormat:@"%@",kvDic[keyName]];
                                                       self.postDic[@"company"] = keyValue;
                                                       self.cellInfoList[curIndex.section][curIndex.row][@"textValue"] = keyValue;
                                                       [self.tableView reloadRowsAtIndexPaths:@[curIndex] withRowAnimation:NO];
                                                   }
                                               }];
}

-(void)pushToProfileEditViewControllerPosition:(NSDictionary *)cellDic{
    NSString *vcTitle = [NSString stringWithFormat:@"%@%@",STRING_PROFILE_EDIT_DEFAULT,cellDic[@"inputLable"]];
    NSDictionary *settingDic= @{@"ViewControllerTitle":vcTitle,
                                @"JXProfileEditViewControllerType":@(JXProfileEditViewControllerTypeView),
                                @"JXProfileEditViewControllerMask":@(JXProfileEditViewControllerMaskPosition),
                                @"keyName":cellDic[@"keyName"],
                                @"textValue":cellDic[@"textValue"],
                                @"hintValue":STRING_PROFILE_EDIT_POSITION_HINT,
                                @"height":@(140),
                                @"heightFlexible":@(NO),
                                @"maxChar":@(20)};
    NSString *keyName = cellDic[@"keyName"];
    NSIndexPath * curIndex = cellDic[@"indexPath"];
    @weakify(self)
    [JXViewControlerRouter pushToProfileEditViewController:self.controller
                                                  animated:YES
                                                      data:settingDic
                                               finishBlock:^(NSDictionary *kvDic) {
                                                   @strongify(self)
                                                   if ([kvDic objectForKey:keyName]) {
                                                       NSString *keyValue = [NSString stringWithFormat:@"%@",kvDic[keyName]];
                                                       self.postDic[@"job"] = keyValue;
                                                       self.cellInfoList[curIndex.section][curIndex.row][@"textValue"] = keyValue;
                                                       [self.tableView reloadRowsAtIndexPaths:@[curIndex] withRowAnimation:NO];
                                                   }
                                               }];
}

-(void)pushToProfileEditViewControllerBrand:(NSDictionary *)cellDic{
    NSString *vcTitle = [NSString stringWithFormat:@"%@%@",STRING_PROFILE_EDIT_DEFAULT,cellDic[@"inputLable"]];
    NSDictionary *settingDic= @{@"ViewControllerTitle":vcTitle,
                                @"JXProfileEditViewControllerType":@(JXProfileEditViewControllerTypeView),
                                @"JXProfileEditViewControllerMask":@(JXProfileEditViewControllerMaskBrand),
                                @"keyName":cellDic[@"keyName"],
                                @"textValue":cellDic[@"textValue"],
                                @"hintValue":STRING_PROFILE_EDIT_BRAND_HINT,
                                @"height":@(140),
                                @"heightFlexible":@(NO),
                                @"maxChar":@(20)};
    NSString *keyName = cellDic[@"keyName"];
    NSIndexPath * curIndex = cellDic[@"indexPath"];
    @weakify(self)
    [JXViewControlerRouter pushToProfileEditViewController:self.controller
                                                  animated:YES
                                                      data:settingDic
                                               finishBlock:^(NSDictionary *kvDic) {
                                                   @strongify(self)
                                                   if ([kvDic objectForKey:keyName]) {
                                                       NSString *keyValue = [NSString stringWithFormat:@"%@",kvDic[keyName]];
                                                       self.postDic[@"brand"] = keyValue;
                                                       self.cellInfoList[curIndex.section][curIndex.row][@"textValue"] = keyValue;
                                                       [self.tableView reloadRowsAtIndexPaths:@[curIndex] withRowAnimation:NO];
                                                   }
                                               }];
}

-(void)pushToProfileEditViewControllerProgram:(NSDictionary *)cellDic{
    NSString *vcTitle = [NSString stringWithFormat:@"%@%@",STRING_PROFILE_EDIT_DEFAULT,cellDic[@"inputLable"]];
    NSDictionary *settingDic= @{@"ViewControllerTitle":vcTitle,
                                @"JXProfileEditViewControllerType":@(JXProfileEditViewControllerTypeView),
                                @"JXProfileEditViewControllerMask":@(JXProfileEditViewControllerMaskProgram),
                                @"keyName":cellDic[@"keyName"],
                                @"textValue":cellDic[@"textValue"],
                                @"hintValue":STRING_PROFILE_EDIT_BRAND_HINT,
                                @"height":@(140),
                                @"heightFlexible":@(NO),
                                @"maxChar":@(20)};
    NSString *keyName = cellDic[@"keyName"];
    NSIndexPath * curIndex = cellDic[@"indexPath"];
    @weakify(self)
    [JXViewControlerRouter pushToProfileEditViewController:self.controller
                                                  animated:YES
                                                      data:settingDic
                                               finishBlock:^(NSDictionary *kvDic) {
                                                   @strongify(self)
                                                   if ([kvDic objectForKey:keyName]) {
                                                       NSString *keyValue = [NSString stringWithFormat:@"%@",kvDic[keyName]];
                                                       self.cellInfoList[curIndex.section][curIndex.row][@"textValue"] = keyValue;
                                                       [self.tableView reloadRowsAtIndexPaths:@[curIndex] withRowAnimation:NO];                                                   }
                                               }];
}

-(void)pushToProfileEditViewControllerProgramBrief:(NSDictionary *)cellDic{
    NSString *vcTitle = [NSString stringWithFormat:@"%@%@",STRING_PROFILE_EDIT_DEFAULT,STRING_PROFILE_EDIT_PROGRAM_BRIEF];
    NSString *keyName = cellDic[@"keyName"];
    NSIndexPath * curIndex = cellDic[@"indexPath"];
    NSString *textValue = ([STRING_PROFILE_EDIT_PROGRAM_BRIEF_DEFAULT isEqualToString:cellDic[@"textValue"]])?@"":cellDic[@"textValue"];
    NSDictionary *settingDic= @{@"ViewControllerTitle":vcTitle,
                                @"JXProfileEditViewControllerType":@(JXProfileEditViewControllerTypeView),
                                @"JXProfileEditViewControllerMask":@(JXProfileEditViewControllerMaskProgramBrief),
                                @"keyName":cellDic[@"keyName"],
                                @"textValue":textValue,
                                @"hintValue":STRING_PROFILE_EDIT_PROGRAM_BRIEF_HINT,
                                @"height":@(DEVICE_HEIGHT*0.4),
                                @"heightFlexible":@(YES),
                                @"maxChar":@(1000)};
    
    @weakify(self)
    [JXViewControlerRouter pushToProfileEditViewController:self.controller
                                                  animated:YES
                                                      data:settingDic
                                               finishBlock:^(NSDictionary *kvDic) {
                                                   @strongify(self)
                                                   if ([kvDic objectForKey:keyName]) {
                                                       NSMutableDictionary *cellDic = self.cellInfoList[curIndex.section][curIndex.row];
                                                       NSString *keyValue = [NSString stringWithFormat:@"%@",kvDic[keyName]];
                                                       NSString *textValue = keyValue;
                                                       cellDic[@"textValue"] = textValue;
                                                       self.postDic[@"brief"] = textValue;
                                                       cellDic[@"textAlignment"] = @(NSTextAlignmentLeft);
                                                       cellDic[@"numberOfLines"] = @(3);
                                                       if ([@"" isEqualToString:keyValue]) {
                                                           cellDic[@"textValue"] = STRING_PROFILE_EDIT_PROGRAM_BRIEF_DEFAULT;
                                                           cellDic[@"textAlignment"] = @(NSTextAlignmentCenter);
                                                           cellDic[@"numberOfLines"] = @(1);
                                                       }
                                                       [self.tableView reloadRowsAtIndexPaths:@[curIndex] withRowAnimation:NO];
                                                   }
                                               }];
}

-(void)pushToProfileEditViewControllerProgramInfo:(NSDictionary *)cellDic{
    NSString *keyName = cellDic[@"keyName"];
    NSIndexPath * curIndex = cellDic[@"indexPath"];
    NSString *vcTitle = [NSString stringWithFormat:@"%@%@",STRING_PROFILE_EDIT_DEFAULT,STRING_PROFILE_EDIT_PROGRAM_INFO];
    NSString *textValue = ([STRING_PROFILE_EDIT_PROGRAM_INFO_DEFAULT isEqualToString:cellDic[@"textValue"]])?@"":cellDic[@"textValue"];
    NSDictionary *settingDic= @{@"ViewControllerTitle":vcTitle,
                                @"JXProfileEditViewControllerType":@(JXProfileEditViewControllerTypeView),
                                @"JXProfileEditViewControllerMask":@(JXProfileEditViewControllerMaskProgramEdit),
                                @"keyName":cellDic[@"keyName"],
                                @"textValue":textValue,
                                @"hintValue":STRING_PROFILE_EDIT_PROGRAM_INFO_HINT,
                                @"height":@(DEVICE_HEIGHT),
                                @"heightFlexible":@(YES),
                                @"maxChar":@(1000)};
    
    @weakify(self)
    [JXViewControlerRouter pushToProfileEditViewController:self.controller
                                                  animated:YES
                                                      data:settingDic
                                               finishBlock:^(NSDictionary *kvDic) {
                                                   @strongify(self)
                                                   if ([kvDic objectForKey:keyName]) {
                                                       NSMutableDictionary *cellDic = self.cellInfoList[curIndex.section][curIndex.row];
                                                       NSString *keyValue = [NSString stringWithFormat:@"%@",kvDic[keyName]];
                                                       NSString *textValue = keyValue;
                                                       cellDic[@"textValue"] = textValue;
                                                       self.postDic[@"detail"] = textValue;
                                                       cellDic[@"textAlignment"] = @(NSTextAlignmentLeft);
                                                       cellDic[@"numberOfLines"] = @(3);
                                                       if ([@"" isEqualToString:keyValue]) {
                                                           cellDic[@"textValue"] = STRING_PROFILE_EDIT_PROGRAM_INFO_DEFAULT;
                                                           cellDic[@"textAlignment"] = @(NSTextAlignmentCenter);
                                                           cellDic[@"numberOfLines"] = @(1);
                                                       }
                                                       [self.tableView reloadRowsAtIndexPaths:@[curIndex] withRowAnimation:NO];
                                                   }
                                               }];
}


- (void)requestDataUpdate {
    /**
     18 更新用户信息
     - parameter path:   请求地址字符串
     - parameter params: ["action":"card_info_update","uid":String,"randcode":String,"data":["uid":用户id,"name":姓名(可选),"sex":1男 2女(可选),"phone":电话(可选),"company":公司(可选),"job":职位(可选),"region":地区(可选,格式“xx 省 xx 市”),"show_flag":是否显示手机号码(可选),"brand":品牌(可选),"brief":项目简介(可选),"detail":项目详情(可选)]]
     - parameter block:  回调
     */
    [self.controller.view endEditing:YES];
    [JXTipHud showLoadingWithFinishHandler:nil];
    
    NSString *userId = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
    NSString *randcode = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"randcode"]];
    self.postDic[@"uid"] = userId;
    NSArray *allkeys = [self.postDic allKeys];
    if (allkeys.count ==1) {
        [JXTipHud showTip:@"没有修改" dismissAfter:0.8 withFinishHandler:nil];
        return;
    }
    JxlNetAPIManager *netmanager = [JxlNetAPIManager sharedManager];
    NSDictionary *params = @{@"action":@"card_info_update",
                             @"uid":userId,
                             @"randcode":randcode,
                             @"data":self.postDic
                             };
    @weakify(self);
    [netmanager request_UpdateCardInfoWithBlock:Globe.baseUrlStr
                                         Params:params
                                       andBlock:^(id _Nullable data, NSError * _Nullable error){
                                           @strongify(self);
                                           JXLog(@"%@",data);
                                           [JXTipHud dismiss];
                                           if (error) {
                                               //NSString *errorMsg = [error domain];
                                               //[JXTipHud showTip:errorMsg dismissAfter:0.8 withFinishHandler:nil];
                                               return;
                                           }
                                           if (!data) {
                                               [JXTipHud showTip:@"访问失败" dismissAfter:0.8 withFinishHandler:nil];
                                               return;
                                           }
                                           NSInteger status = [data[@"status"] integerValue];
                                           if (status != 1) {
                                               NSString *errorMsg = data[@"msg"];
                                               [JXTipHud showTip:errorMsg dismissAfter:0.8 withFinishHandler:nil];
                                               return;
                                           }
                                           id result = data[@"info"];
                                           if (!result) {
                                               [JXTipHud showTip:@"访问失败" dismissAfter:0.8 withFinishHandler:nil];
                                               return;
                                           }
                                           [self editSuccess:self.postDic];
                                       }];
}
- (void)editSuccess:(NSDictionary *)postDic{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *userInfoMutableDic = [[userDefaults valueForKey:@"user_dict"] mutableCopy];
    NSMutableDictionary *userDic = [userInfoMutableDic[@"user_info"] mutableCopy];
    NSMutableDictionary *programDic = [userInfoMutableDic[@"project_info"] mutableCopy];
    
    NSArray *userInfoKeys = @[@"name",
                              @"sex",
                              @"phone",
                              @"show_flag",
                              @"company",
                              @"job",
                              @"region",
                              @"party",
                              @"brand"];
    
    NSArray *programInfoKeys = @[@"name",
                                 @"brief",
                                 @"detail"];
    
    for (NSString *item in userInfoKeys) {
        if (postDic[item]) {
            userDic[item] = [NSString stringWithFormat:@"%@",postDic[item]];
        }
    }
    
    for (NSString *item in programInfoKeys) {
        if (postDic[item]) {
            programDic[item] = [NSString stringWithFormat:@"%@",postDic[item]];
        }
    }

    userInfoMutableDic[@"user_info"] = userDic;
    userInfoMutableDic[@"project_info"] = programDic;

    NSDictionary *userInfoDic = [NSDictionary dictionaryWithDictionary:userInfoMutableDic];

    [userDefaults setObject:userInfoDic forKey:@"user_dict"];

    @weakify(self)
    [JXTipHud showTip:@"保存成功" dismissAfter:0.8 withFinishHandler:^(){
        @strongify(self)
        if (self.finishBlock) {
            self.finishBlock(nil);
        }
        [self.controller.navigationController popViewControllerAnimated:YES];
        
    }];
}
- (void)actionSheet:(JXCustomActionSheet *)actionSheet buttonClickedAtTag:(NSInteger)tag {
    switch (actionSheet.tag) {
        case PROFILE_EDIT_ACTION_SHEET_TAG_GENDER:{
            switch (tag) {
                case 1:{ // 男
                    NSString *genderString = @"";
                    genderString = STRING_PROFILE_EDIT_GENDER_MALE;
                    self.postDic[@"sex"] = @(1);
                    NSIndexPath * curIndex = self.curIndexPath;
                    self.cellInfoList[curIndex.section][curIndex.row][@"textValue"] = genderString;
                    [self.tableView reloadRowsAtIndexPaths:@[curIndex] withRowAnimation:NO];
                    break;
                }
                case 2:{ // 女
                    NSString *genderString = @"";
                    genderString = STRING_PROFILE_EDIT_GENDER_FEMALE;
                    self.postDic[@"sex"] = @(2);
                    NSIndexPath * curIndex = self.curIndexPath;
                    self.cellInfoList[curIndex.section][curIndex.row][@"textValue"] = genderString;
                    [self.tableView reloadRowsAtIndexPaths:@[curIndex] withRowAnimation:NO];
                    break;
                }
                default:
                    break;
            }
            break;
        }
        case PROFILE_EDIT_ACTION_SHEET_TAG_SHOWFLAG:{
            switch (tag) {
                case 1:{ // 是
                    NSString *showFlagString = STRING_PROFILE_EDIT_SHOW_FLAG_YES;
                    self.postDic[@"show_flag"] = @(1);
                    NSIndexPath * curIndex = self.curIndexPath;
                    self.cellInfoList[curIndex.section][curIndex.row][@"textValue"] = showFlagString;
                    [self.tableView reloadRowsAtIndexPaths:@[curIndex] withRowAnimation:NO];
                    break;
                }
                case 2:{ // 否
                    NSString *showFlagString = STRING_PROFILE_EDIT_SHOW_FLAG_NO;
                    self.postDic[@"show_flag"] = @(0);
                    NSIndexPath * curIndex = self.curIndexPath;
                    self.cellInfoList[curIndex.section][curIndex.row][@"textValue"] = showFlagString;
                    [self.tableView reloadRowsAtIndexPaths:@[curIndex] withRowAnimation:NO];
                    break;
                }
                default:
                    break;
            }
            break;
        }
        case PROFILE_EDIT_ACTION_SHEET_TAG_LOCATION: {
            switch (tag) {
                case 0:{
                    break;
                }
                case 1:{
                    JXPickerActionSheet *pickerSheet = (JXPickerActionSheet *)actionSheet;
                    NSString * keyValue = [JXPickerActionSheet getLocationValueWithData:pickerSheet.pickerData
                                                                                  index:pickerSheet.pickerIndex];
                    self.postDic[@"region"] = keyValue;
                    NSIndexPath * curIndex = self.curIndexPath;
                    self.cellInfoList[curIndex.section][curIndex.row][@"textValue"] = keyValue;
                    [self.tableView reloadRowsAtIndexPaths:@[curIndex] withRowAnimation:NO];
                    break;
                }
                default:
                    break;
            }
            break;
        }
        case PROFILE_EDIT_ACTION_SHEET_TAG_PARTY: {
            switch (tag) {
                case 0:{
                    break;
                }
                case 1:{
                    NSString *partyString = STRING_PROFILE_EDIT_PARTY_A;
                    self.postDic[@"party"] = @(1);
                    NSIndexPath * curIndex = self.curIndexPath;
                    self.cellInfoList[curIndex.section][curIndex.row][@"textValue"] = partyString;
                    [self.tableView reloadRowsAtIndexPaths:@[curIndex] withRowAnimation:NO];
                    break;
                }
                case 2:{
                    NSString *partyString = STRING_PROFILE_EDIT_PARTY_B;
                    self.postDic[@"party"] = @(2);
                    NSIndexPath * curIndex = self.curIndexPath;
                    self.cellInfoList[curIndex.section][curIndex.row][@"textValue"] = partyString;
                    [self.tableView reloadRowsAtIndexPaths:@[curIndex] withRowAnimation:NO];
                    break;
                }
                case 3:{
                    NSString *partyString = STRING_PROFILE_EDIT_PARTY_OTHER;
                    self.postDic[@"party"] = @(3);
                    NSIndexPath * curIndex = self.curIndexPath;
                    self.cellInfoList[curIndex.section][curIndex.row][@"textValue"] = partyString;
                    [self.tableView reloadRowsAtIndexPaths:@[curIndex] withRowAnimation:NO];
                    break;
                }
                default:
                    break;
            }
            break;
        }
        default: {
            break;
        }
    }
}

@end

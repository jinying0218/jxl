//
//  JXLMyCenterViewController.m
//  jiangXunLu
//
//  Created by 金莹 on 16/9/3.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXLMyCenterViewController.h"
#import "Color.h"
#import "GlobalDefine.h"
#import "AFNTool.h"
#import "MBProgressHUD+Add.h"
#import "YYModel.h"
#import "jiangXunLu-swift.h"
#import "UIImageView+WebCache.h"
#import "Masonry.h"
#import "EXTScope.h"
#import "MJRefresh.h"
#import "HexColor.h"

#import "JXLMyCenterCell.h"
#import "JXLPersonalCenterModel.h"
#import "JXProfileViewController.h"
#import "JXLSettingViewController.h"
#import "JXViewControlerRouter.h"


static NSString *const kMyCenterCellIdentifier = @"kMyCenterCellIdentifier";

@interface JXLMyCenterViewController ()
{
    UIImageView *navBarHairlineImageView;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *positionLabel;
@property (weak, nonatomic) IBOutlet UILabel *myCollectionLabel;
@property (weak, nonatomic) IBOutlet UILabel *myContactsLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UIImageView *sexImageView;

@property (strong, nonatomic) NSArray *tableDatas;
@property (strong, nonatomic) JXLPersonalCenterModel *personalCenterModel;
@end

@implementation JXLMyCenterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSDictionary *user_dict = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_dict"];
    NSDictionary *userInfo = user_dict[@"user_info"];
    
    self.userNameLabel.text = userInfo[@"name"];
    self.positionLabel.text = userInfo[@"job"];

    
    NSDictionary *dict0 = @{@"icon" : @"icon_mingpian",
                           @"title" : @"我的名片",
                            @"ViewControll" : @"JXProfileViewController"};
    NSDictionary *dict1 = @{@"icon" : @"icon_faxian",
                            @"title" : @"我的发现",
                            @"ViewControll" : @"JXLMyDiscoverViewController"};
    NSDictionary *dict2 = @{@"icon" : @"icon_guanyuwom",
                            @"title" : @"关于我们",
                            @"ViewControll" : @"JXLAboutViewController"};
    NSDictionary *dict3 = @{@"icon" : @"icon_shezhi",
                            @"title" : @"设置",
                            @"ViewControll" : @"JXLSettingViewController"};
    self.tableDatas = @[@[dict0,dict1],@[dict2,dict3]];
    

    navBarHairlineImageView = [self findHairlineImageViewUnder:self.navigationController.navigationBar];
    [self.tableView registerNib:[UINib nibWithNibName:@"JXLMyCenterCell" bundle:nil] forCellReuseIdentifier:kMyCenterCellIdentifier];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.tableView.backgroundColor = UIColorFromRGB(DEFINE_COLOR_BACK);
    self.tableView.tableHeaderView = self.headerView;
    
    self.headImageView.layer.cornerRadius = self.headImageView.width/2;
    self.headImageView.layer.masksToBounds = YES;
    
    self.title = @"个人中心";
}
- (UIImageView *)findHairlineImageViewUnder:(UIView *)view {
    if ([view isKindOfClass:UIImageView.class] && view.bounds.size.height <= 1.0) {
        return (UIImageView *)view;
    }
    for (UIView *subview in view.subviews) {
        UIImageView *imageView = [self findHairlineImageViewUnder:subview];
        if (imageView) {
            return imageView;
        }
    }
    return nil;
}
-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    navBarHairlineImageView.hidden = NO;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    navBarHairlineImageView.hidden = YES;
    
    [self loadCenterData];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)refreshHeaderView{
    
    self.userNameLabel.text = self.personalCenterModel.name;
    self.positionLabel.text = self.personalCenterModel.job;
    self.myCollectionLabel.text = self.personalCenterModel.attent_num;
    self.myContactsLabel.text = self.personalCenterModel.collected_num;
    self.addressLabel.text = self.personalCenterModel.region;
    self.sexImageView.image = [self.personalCenterModel.sex isEqualToString:@"1"] ? [UIImage imageNamed:@"personal_sex_man"] : [UIImage imageNamed:@"personal_sex_woman"];
    if ([self.personalCenterModel.sex intValue] != 1 && [self.personalCenterModel.sex intValue] != 2) {
        self.sexImageView.hidden = YES;
    }else {
        self.sexImageView.hidden = NO;
    }
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",Globe.baseUrlStr,self.personalCenterModel.head]] placeholderImage:[UIImage imageNamed:@"Head_portrait"]];
    NSLog(@"%@",[NSString stringWithFormat:@"%@%@",Globe.baseUrlStr,self.personalCenterModel.head]);
}

#pragma mark - action

- (IBAction)collectionButtonClick:(UIButton *)sender {
    
    UIViewController *rootController = [[UIApplication sharedApplication] keyWindow].rootViewController;
    if ([rootController isKindOfClass:[MainTabBarController class]]) {
        MainTabBarController *tabController = (MainTabBarController *)rootController;
        tabController.selectedIndex = 1;
        tabController.selectedViewController = tabController.childViewControllers[1];
    }

    
}
- (IBAction)myContactsButtonClick:(UIButton *)sender {
    
    [JXViewControlerRouter pushToAcquaitanceViewController:self animated:YES data:@{@"targetId" : self.personalCenterModel.identifier}];
}

/*
 
 NSDictionary *userDic = self.resultDic[@"user_info"];
 NSString *targetId = [NSString stringWithFormat:@"%@", userDic[@"id"]];
 [JXViewControlerRouter pushToAcquaitanceViewController:self.controller
 animated:YES
 data:@{@"targetId":targetId}];
 
 */

#pragma mark - loaddata
- (void)loadCenterData{
    NSString *g_uid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
    NSString *g_randcode = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"randcode"]];
    
    NSDictionary *params = @{@"action" : @"personal_center_info_load",
                             @"uid" : g_uid,
                             @"randcode" : g_randcode,
                             @"data" : @""};
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    @weakify(self);
    [AFNTool post:Globe.baseUrlStr params:params success:^(id responsObject) {
        @strongify(self);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if ([responsObject[@"status"] intValue] == 1) {
            NSDictionary *user_info = [responsObject[@"info"] objectForKey:@"user_info"];
            self.personalCenterModel = [JXLPersonalCenterModel yy_modelWithDictionary:user_info];
            
            [self refreshHeaderView];
        }
    } failure:^(NSError *error) {
        @strongify(self);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:error.description toView:self.view];
    }];

}

#pragma mark - tableView delegate & dataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.tableDatas.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return [self.tableDatas[section] count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 0;
    }
    return 14;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    JXLMyCenterCell *cell = [tableView dequeueReusableCellWithIdentifier:kMyCenterCellIdentifier];
    
    [cell configureCell:self.tableDatas[indexPath.section][indexPath.row]];
    if (indexPath.section == 1 && indexPath.row == 1) {
        cell.seperatorLine.hidden = YES;
    }else {
        cell.seperatorLine.hidden = NO;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dict = self.tableDatas[indexPath.section][indexPath.row];
    NSString *className = dict[@"ViewControll"];
    UIViewController *controller = [[NSClassFromString(className) alloc] init];
    if ([controller isKindOfClass:[JXProfileViewController class]]) {
        JXProfileViewController *profileVC = (JXProfileViewController *)controller;
        profileVC.vcMask = JXViewControllerProfileMaskSelf;
    }
    if ([controller isKindOfClass:[JXLSettingViewController class]]) {
        
        JXLSettingViewController *setVC = (JXLSettingViewController *)controller;
        setVC.msgpush_flag = self.personalCenterModel.msgpush_flag;
    }
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:YES];
}



@end

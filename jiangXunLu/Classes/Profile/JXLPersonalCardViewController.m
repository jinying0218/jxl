//
//  JXLPersonalCardViewController.m
//  jiangXunLu
//
//  Created by 金莹 on 16/9/4.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXLPersonalCardViewController.h"
#import "jiangXunLu-swift.h"
#import "GlobalDefine.h"

#import "MBProgressHUD+Add.h"

@interface JXLPersonalCardViewController ()

@end

@implementation JXLPersonalCardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpLeftbarButtonWithImage:UIIMGName(@"back_normal")];
    
    self.title = @"我的名片";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)leftBarItemTouchUpInside:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

@end

//
//  JXLShareViewController.m
//  jiangXunLu
//
//  Created by 金莹 on 16/9/4.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXLShareViewController.h"
#import "jiangXunLu-swift.h"
#import "GlobalDefine.h"
#import "MBProgressHUD+Add.h"
#import "ZJSwitch.h"
#import "HexColor.h"

@interface JXLShareViewController ()<UMSocialUIDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) NSString *shareURLString;
@end

@implementation JXLShareViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"邀请好友下载";
    
    [self setUpLeftbarButtonWithImage:UIIMGName(@"back_normal")];

    NSString *urlString = [NSString stringWithFormat:@"%@forapp/share.php",Globe.baseUrlStr];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    [self.webView loadRequest:request];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - action

- (void)leftBarItemTouchUpInside:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)shareButtonClick:(UIButton *)sender {
    [UMSocialData defaultData].extConfig.title = @"将讯录";
        UMSocialUrlResource *urlResource = [[UMSocialUrlResource alloc] initWithSnsResourceType:UMSocialUrlResourceTypeWeb url:self.shareURLString];
    switch (sender.tag - 10000) {
        case 0:{
            [UMSocialData defaultData].extConfig.wechatSessionData.url = self.shareURLString;
            [[UMSocialDataService defaultDataService] postSNSWithTypes:@[UMShareToWechatSession] content:@"云集商业地产甲方乙方将才精英的通讯录，项目招商与品牌拓展人士扩展交际的必备通讯工具" image:[UIImage imageNamed:@"logo_default"]  location:nil urlResource:urlResource presentedController:self completion:^(UMSocialResponseEntity *shareResponse){
                if (shareResponse.responseCode == UMSResponseCodeSuccess) {
                    NSLog(@"分享成功！");
                }
            }];
        }
            break;
        case 1:{
            [UMSocialData defaultData].extConfig.wechatTimelineData.url = self.shareURLString;
            [[UMSocialDataService defaultDataService] postSNSWithTypes:@[UMShareToWechatTimeline] content:@"云集商业地产甲方乙方将才精英的通讯录，项目招商与品牌拓展人士扩展交际的必备通讯工具" image:[UIImage imageNamed:@"logo_default"]  location:nil urlResource:urlResource presentedController:self completion:^(UMSocialResponseEntity *shareResponse){
                if (shareResponse.responseCode == UMSResponseCodeSuccess) {
                    NSLog(@"分享成功！");
                }
            }];
        }
            break;
        default:{
            [UMSocialData defaultData].extConfig.qqData.url = self.shareURLString;
            [[UMSocialDataService defaultDataService] postSNSWithTypes:@[UMShareToQQ] content:@"云集商业地产甲方乙方将才精英的通讯录，项目招商与品牌拓展人士扩展交际的必备通讯工具" image:[UIImage imageNamed:@"logo_default"]  location:nil urlResource:urlResource presentedController:self completion:^(UMSocialResponseEntity *shareResponse){
                if (shareResponse.responseCode == UMSResponseCodeSuccess) {
                    NSLog(@"分享成功！");
                }
            }];
        }
            break;
    }


}

#pragma mark - UMSocialUIDelegate
-(void)didSelectSocialPlatform:(NSString *)platformName withSocialData:(UMSocialData *)socialData{
    NSLog(@"%@",platformName);
}
@end

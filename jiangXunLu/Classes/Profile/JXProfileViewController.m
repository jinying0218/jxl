//
//  JXProfileViewController.m
//  jiangXunLu
//
//  Created by lixu06 on 16/6/17.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXProfileViewController.h"
#import "GlobalDefine.h"
#import "CommonTools.h"
#import "Color.h"
#import "String.h"
#import "Dimen.h"
#import "EXTScope.h"
#import "JXCustomTableModel.h"
#import "JXTipHud.h"
#import "JXProfileTableManager.h"
#import "JXProfileBottomBar.h"
#import "JXProfileHeaderView.h"
#import "JXViewControlerRouter.h"
#import "JXShareActionSheet.h"
#import "NSObject+BMRuntime.h"
#import "jiangXunLu-swift.h"
#import "UMSocialData.h"
#import "UMSocialSnsService.h"
#import "MJExtension.h"
#import "JXViewControlerRouter.h"
#import "MJRefresh.h"
#import "SVProgressHUD.h"
#import "HexColor.h"

@interface JXProfileViewController () <UMSocialUIDelegate>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) JXCustomTableModel *tableManager;
@property (nonatomic, strong) NSMutableArray *cellInfoList;
@property (nonatomic, strong) NSMutableArray *photoArray;
@property (nonatomic, strong) NSMutableDictionary *resultDic;
@property (nonatomic, strong) JXProfileBottomBar *bottomBar;
@end

@implementation JXProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = UIColorFromRGB(DEFINE_COLOR_BACK);
    self.title = STRING_PROFILE_CONTROLLER_TITLE;
    
    switch (self.vcMask) {
        case JXViewControllerProfileMaskSelf:{
            self.caseDic = [[NSUserDefaults standardUserDefaults] valueForKey:@"user_dict"];
            break;
        }
        case JXViewControllerProfileMaskOther:{
            CGFloat bottomBarHeight = BOTTOM_BAR_HEIGHT_PROFILE;
            JXProfileBottomBar *bottomBar = [JXProfileBottomBar defaultView];
            bottomBar.frame = RECT(0, DEVICE_HEIGHT - bottomBarHeight, DEVICE_WIDTH, bottomBarHeight);
//            [bottomBar setUpCollect:YES];
            bottomBar.firstBlock = ^(id data){
                [self requestCollect];
            };
            bottomBar.secondBlock = ^(id data){
                [self setLeftBackButtonItemForChat];
                //闭包刷新
                [AppDelegate shareDelegate].refreshUnreadMessage = ^{
                    if (self.navigationItem.backBarButtonItem) {
                        [self setLeftBackButtonItemForChat];
                    }
                };
                
                User *currentUser = [[User alloc] initWithDict:self.resultDic];
                if (currentUser.user_info) {
                    TSChatViewController *chatVC = (TSChatViewController*)[TSChatViewController initFromNib];
                    chatVC.chatUser = currentUser;
                    [self.navigationController pushViewController:chatVC animated:true];
                } else {
                    [SVProgressHUD showInfoWithStatus:@"似乎已断开与互联网的连接。"];
                }
            };
            bottomBar.thirdBlock = ^(id data){
                [JXViewControlerRouter pushToProfileDetailDisabledViewController:self
                                                                        animated:YES
                                                                            data:self.resultDic];
            };
            self.bottomBar = bottomBar;
            [self.view addSubview:bottomBar];
            break;
        }
        default:{
            break;
        }
    }
    self.tableView = [[UITableView alloc] init];
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        NSString *targetId;
        if (self.vcMask == JXViewControllerProfileMaskOther) {
            targetId = self.queryDic[@"targetId"];
            [self requestData:targetId];
        } else if (self.vcMask == JXViewControllerProfileMaskSelf) {
            targetId = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
            self.caseDic = [[NSUserDefaults standardUserDefaults] valueForKey:@"user_dict"];
            [self requestData:targetId];
        }
    }];
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.contentView = self.tableView;
    [self setUpContentView];
    
    if (self.resultDic) {
    } else {
        if (self.caseDic) {
            self.resultDic = [NSMutableDictionary dictionaryWithDictionary:self.caseDic];
        }else {
            self.resultDic = [NSMutableDictionary dictionaryWithCapacity:0];
        }
    }
    NSDictionary * allDic =@{@"resultDic":self.resultDic,
                             @"settingDic":@{
                                        @"editable": @(self.vcMask == JXViewControllerProfileMaskSelf)
                                     }
                             };
    JXProfileTableManager *tableManager= [[JXProfileTableManager alloc] initWithDictionary:allDic
                                                                            viewController:self
                                                                                 tableView:self.tableView
                                                                             finishHanlder:nil];
    tableManager.vcMask = self.vcMask;
    [tableManager getHeaderView];
    self.cellInfoList = [tableManager getCellInfoList];
    self.tableView.delegate = tableManager;
    self.tableView.dataSource = tableManager;
    [self.tableView reloadData];
    
    UIImageView *backView = [[UIImageView alloc] init];
    backView.image = UIIMGName(@"discover_top");
    backView.contentMode = UIViewContentModeScaleAspectFill;
    tableManager.headerViewBack = backView;
    tableManager.headerViewBack.frame = RECT(0,0,DEVICE_WIDTH,[JXProfileHeaderView viewHeight:nil]);
    [self.view insertSubview:tableManager.headerViewBack belowSubview:self.tableView];
    
    self.tableManager = tableManager;
}

- (void)setLeftBackButtonItemForChat {
    NSInteger unreadmsg_num = [AppDelegate shareDelegate].num_unreadmsg;
    NSString *newTitle = unreadmsg_num > 0 ? [NSString stringWithFormat:@"消息%@%li%@",@"(",(long)unreadmsg_num,@")"] : @"消息" ;
    UIBarButtonItem *backBtnItem = [[UIBarButtonItem alloc] initWithTitle:newTitle style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = backBtnItem;
}

- (void)restoreLeftBackButtonItem{
    self.navigationItem.backBarButtonItem = nil;
}

- (void)setNavigationBarStyle{
    [self setNavigationBarStyleTransparent];
}
-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    if (self.vcMask == JXViewControllerProfileMaskOther) {
        [super setNavigationBarStyleCustom];
    }
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [super setNavigationBarStyleTransparent];
    if (self.vcMask == JXViewControllerProfileMaskOther) {
        [self restoreLeftBackButtonItem];
        [self setUpLeftbarButtonWithImage:UIIMGName(@"back_normal")];
    } else {
//        [self setUpLeftbarButtonWithImage:UIIMGName(@"back_normal") frame:RECT(6, 6, 50, 15)];
        [self setUpLeftbarButtonWithImage:UIIMGName(@"back_normal")];

//        [self setUpLeftbarButtonWithImage:UIIMGName(@"edit_normal") frame:RECT(6, 6, 50, 15)];
        [self setUpRightbarButtonWithImage:UIIMGName(@"edit_normal") frame:RECT(self.view.width - 50 - 15, 6, 50, 15)];
        
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [rightButton setTitle:@"编辑" forState:UIControlStateNormal];
        [rightButton setImage:[UIImage imageNamed:@"pers_btn_edit_n"] forState:UIControlStateNormal];
        [rightButton setTitleColor:[UIColor colorWithHexString:@"ffffff"] forState:UIControlStateNormal];
        [rightButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
        rightButton.bounds = CGRectMake(0, 0, 70, 44);
        [rightButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
        [rightButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 10)];
        [self setUpRightbarButtonWithCustomButton:rightButton];

//        [self setUpRightbarButtonWithImage:UIIMGName(@"nav_share_normal")];
    }
    
    NSString *targetId;
    if (self.vcMask == JXViewControllerProfileMaskOther) {
        targetId = self.queryDic[@"targetId"];
        [self requestData:targetId];
    } else if (self.vcMask == JXViewControllerProfileMaskSelf) {
        targetId = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
        self.caseDic = [[NSUserDefaults standardUserDefaults] valueForKey:@"user_dict"];
        [self requestData:targetId];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - 
- (void)leftBarItemTouchUpInside:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)pushToProfileDetailViewController{
        [JXViewControlerRouter pushToProfileDetailEditableViewController:self
                                                                animated:YES
                                                                    data:self.caseDic
                                                             finishBlock:self.finishBlock];
    
}

- (void)rightBarItemTouchUpInside:(id)sender{
//    UMSocialData *shareData = [UMSocialData defaultData];
//    shareData.extConfig.title = STRING_PROFILE_SHARE_CONTENT_TITLE;
//    shareData.extConfig.qqData.url = STRING_PROFILE_SHARE_CONTENT_QQDATA_URL;
//    NSString *shareText = @"云集商业地产甲方乙方将才精英的通讯录，项目招商与品牌拓展人士扩展交际的必备通讯工具";
//    NSArray *targetPlatforms = @[UMShareToWechatSession,UMShareToWechatTimeline,UMShareToSina,UMShareToQQ,UMShareToQzone];
//    [UMSocialSnsService presentSnsIconSheetView:self
//                                         appKey:Globe.umengAppKey
//                                      shareText:shareText
//                                     shareImage:UIIMGName(@"logo_default")
//                                shareToSnsNames:targetPlatforms
//                                       delegate:self];
    
    switch (self.vcMask) {
        case JXViewControllerProfileMaskSelf:{
            [self pushToProfileDetailViewController];
            break;
        }
        case JXViewControllerProfileMaskOther:{
            [JXViewControlerRouter popCurrentViewController:self animated:YES];
            break;
        }
        default:
            break;
    }

}
- (CGFloat)gapToBottom{
    switch (self.vcMask) {
        case JXViewControllerProfileMaskSelf:{
            return 0;
        }
        case JXViewControllerProfileMaskOther:{
            return BOTTOM_BAR_HEIGHT_PROFILE;
        }
    }
    return 0;
}
- (void)requestData:(NSString *)targetId{
    /**
     8 加载名片信息
     - parameter path:   请求地址字符串
     - parameter params: ["action":"card_info_load","uid":String,"randcode":String,"data":["uid":Sting]]
     - parameter block:  回调
     */
    [JXTipHud showLoadingWithFinishHandler:nil];

    NSString *userId = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
    NSString *randcode = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"randcode"]];
    if (!targetId) {
        [JXTipHud showTip:@"参数错误" dismissAfter:0.8 withFinishHandler:nil];
        return;
    }
    JxlNetAPIManager *netmanager = [JxlNetAPIManager sharedManager];
    NSDictionary *params = @{@"action":@"card_info_load",
                             @"uid":userId,
                             @"randcode":randcode,
                             @"data":@{
                                     @"uid":targetId
                                     }
                             };
    @weakify(self);
    [netmanager request_CardInfoWithBlock:Globe.baseUrlStr
                                   Params:params
                                 andBlock:^(id _Nullable data, NSError * _Nullable error){
                                     @strongify(self);
                                     JXLog(@"%@",data);
                                     [JXTipHud dismiss];
                                     if (error) {
                                         //NSString *errorMsg = [error domain];
                                         //[JXTipHud showTip:errorMsg dismissAfter:0.8 withFinishHandler:nil];
                                         [self.tableView.mj_header endRefreshing];
                                         return;
                                     }
                                     if (!data) {
                                         [JXTipHud showTip:@"访问失败" dismissAfter:0.8 withFinishHandler:nil];
                                         [self.tableView.mj_header endRefreshing];
                                         return;
                                     }
                                     NSInteger status = [data[@"status"] integerValue];
                                     if (status != 1) {
                                         NSString *errorMsg = data[@"msg"];
                                         [JXTipHud showTip:errorMsg dismissAfter:0.8 withFinishHandler:nil];
                                         [self.tableView.mj_header endRefreshing];
                                         return;
                                     }
                                     id result = data[@"info"];
                                     if (!result) {
                                         [JXTipHud showTip:@"访问失败" dismissAfter:0.8 withFinishHandler:nil];
                                         [self.tableView.mj_header endRefreshing];
                                         return;
                                     }
                                     self.resultDic = [result mutableCopy];
                                     self.tableManager.resultDic = self.resultDic;
                                     [(JXProfileTableManager *)self.tableManager updateHeaderView];
                                     self.cellInfoList = [self.tableManager getCellInfoList];
                                     [self setBottomBarCollectState];
                                     [self.tableView reloadData];
                                     [self.tableView.mj_header endRefreshing];
                                 }];
}
-(void)requestCollect {
    /**
     9 收藏／取消收藏
     - parameter path:   请求地址字符串
     - parameter params: ["action":"collection","uid":String,"randcode":String,"data":["uid":被收藏／取消收藏的用户id,"status":1收藏 0取消收藏]]
     - parameter block:  回调
     */
    [JXTipHud showLoadingWithFinishHandler:nil];

    NSString *userId = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
    NSString *randcode = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"randcode"]];
    if (!self.queryDic || !self.queryDic[@"targetId"]) {
        [JXTipHud showTip:@"参数错误" dismissAfter:0.8 withFinishHandler:nil];
        return;
    }
    NSInteger targetId = [self.queryDic[@"targetId"] intValue];
    NSInteger status = [[NSString stringWithFormat:@"%@",self.resultDic[@"collection_status"]] integerValue];//1关注0未关注
    if (status == 1) {
        status = 0;
    } else {
        status = 1;
    }
    JxlNetAPIManager *netmanager = [JxlNetAPIManager sharedManager];
    NSDictionary *params = @{@"action":@"collection",
                             @"uid":userId,
                             @"randcode":randcode,
                             @"data":@{
                                     @"uid":@(targetId),
                                     @"status":@(status)
                                     }
                             };
    @weakify(self);
    [netmanager request_CollectionWithBlock:Globe.baseUrlStr
                                     Params:params
                                   andBlock:^(id _Nullable data, NSError * _Nullable error){
                                       @strongify(self);
                                       [JXTipHud dismiss];
                                       if (error) {
                                           //NSString *errorMsg = [error domain];
                                           //[JXTipHud showTip:errorMsg dismissAfter:0.8 withFinishHandler:nil];
                                           return;
                                       }
                                       if (!data) {
                                           [JXTipHud showTip:@"访问失败" dismissAfter:0.8 withFinishHandler:nil];
                                           return;
                                       }
                                       NSInteger status = [data[@"status"] integerValue];
                                       if (status != 1) {
                                           NSString *errorMsg = data[@"msg"];
                                           [JXTipHud showTip:errorMsg dismissAfter:0.8 withFinishHandler:nil];
                                           return;
                                       }
                                       id result = data[@"info"];
                                       if (!result) {
                                           [JXTipHud showTip:@"访问失败" dismissAfter:0.8 withFinishHandler:nil];
                                           return;
                                       }
                                       NSString *collectStatusStr = [CommonTools getValidText:result[@"status"] withPlaceHolder:@"-1"];
                                       NSInteger collectStatus = [collectStatusStr integerValue];//: (int)1收藏;0取消收藏,错误时返回-1
                                       if (collectStatus == 0) {
                                           self.resultDic[@"collection_status"] = @(collectStatus);
                                           [JXTipHud showTip:@"取消收藏成功" dismissAfter:0.8 withFinishHandler:nil];
                                       } else if (collectStatus == 1) {
                                           self.resultDic[@"collection_status"] = @(collectStatus);
                                           [JXTipHud showTip:@"收藏成功" dismissAfter:0.8 withFinishHandler:nil];
                                       }
                                       NSDictionary *finishDict = @{@"targetId" : @(targetId),
                                                                    @"status" : @(collectStatus)};
                                       if (self.finishBlock) {
                                           self.finishBlock(finishDict);
                                       }
                                       [self setBottomBarCollectState];
                                   }];
}

-(void)setBottomBarCollectState{
    NSString *collectStatusStr = self.resultDic[@"collection_status"];
    NSInteger collectStatus = [collectStatusStr integerValue];
    if (collectStatus == 0) {
        [self.bottomBar setUpCollect:NO];
    } else if (collectStatus == 1) {
        [self.bottomBar setUpCollect:YES];
    }
}

#pragma mark - UMSocialUIDelegate
-(void)didSelectSocialPlatform:(NSString *)platformName withSocialData:(UMSocialData *)socialData{
    
}
@end

//
//  JXLSettingViewController.m
//  jiangXunLu
//
//  Created by 金莹 on 16/9/4.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXLSettingViewController.h"
#import "jiangXunLu-swift.h"
#import "GlobalDefine.h"
#import "ZJSwitch.h"
#import "HexColor.h"
#import "AFNTool.h"
#import "MBProgressHUD+Add.h"
#import "EXTScope.h"
#import "YYModel.h"

static NSString *const kSettingCellIdentifier = @"kSettingCellIdentifier";

@interface JXLSettingViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *logoutButton;
@property (strong, nonatomic) NSArray *tableDatas;
@end

@implementation JXLSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"设置";
    [self setUpLeftbarButtonWithImage:UIIMGName(@"back_normal")];
    
    NSDictionary *dict0 = @{@"title" : @"密码修改",
                            @"ViewControll" : @"JXLPasswordViewController"};
    NSDictionary *dict1 = @{@"title" : @"新消息通知",
                            @"ViewControll" : @"JXLMyDiscoverViewController"};
    NSDictionary *dict2 = @{@"title" : @"邀请好友下载",
                            @"ViewControll" : @"JXLShareViewController"};
//    NSDictionary *dict3 = @{@"icon" : @"icon_shezhi",
//                            @"title" : @"设置",
//                            @"ViewControll" : @"JXLSettingViewController"};
    self.tableDatas = @[dict0,dict1,dict2];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kSettingCellIdentifier];
    
    self.tableView.tableFooterView = [[UIView alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - action

- (void)leftBarItemTouchUpInside:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
/*
 action:personal_set_msg_push_status
	uid:用户id
	randcode:登陆状态验证码
	data:{
 status : (int)1通知0不通知
	}
 */
- (void)handleSwitchEvent:(ZJSwitch *)switchButton{

    NSUInteger status = switchButton.on;
    
    NSString *g_uid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
    NSString *g_randcode = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"randcode"]];
    
    NSDictionary *data = @{@"status" : @(status)};
    NSDictionary *params = @{@"action" : @"personal_set_msg_push_status",
                             @"uid" : g_uid,
                             @"randcode" : g_randcode,
                             @"data" : [data yy_modelToJSONString]};
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    @weakify(self);
    [AFNTool post:Globe.baseUrlStr params:params success:^(id responsObject) {
        @strongify(self);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if ([responsObject[@"status"] intValue] == 1) {
            [MBProgressHUD showError:@"修改成功" toView:self.view];
        }else {
            [MBProgressHUD showError:responsObject[@"msg"] toView:self.view];
        }
    } failure:^(NSError *error) {
        @strongify(self);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:error.description toView:self.view];
    }];

}
- (IBAction)logoutButtonClick:(UIButton *)sender {
    [[AppDelegate shareDelegate] setupLogionViewController];
}

#pragma mark - tableView delegate & dataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.tableDatas.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kSettingCellIdentifier];
    NSDictionary *dict = self.tableDatas[indexPath.row];
    cell.textLabel.font = [UIFont systemFontOfSize:15];
    cell.textLabel.textColor = [UIColor colorWithHexString:@"333333"];
    cell.textLabel.text = dict[@"title"];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    for (UIView *subView in cell.contentView.subviews) {
        [subView removeFromSuperview];
    }
    if (indexPath.row == 1) {
        cell.accessoryType = UITableViewCellAccessoryNone;
        ZJSwitch *switchButton = [[ZJSwitch alloc] initWithFrame:CGRectMake( [UIScreen mainScreen].bounds.size.width - 15 - 60, 10, 60, 30)];
        switchButton.style = ZJSwitchStyleNoBorder;
        switchButton.onTintColor = [UIColor colorWithHexString:@"373635"];
        switchButton.tintColor = [UIColor colorWithHexString:@"373635"];
        switchButton.thumbTintColor = [UIColor colorWithHexString:@"FECD08"];
        switchButton.onText = @"ON";
        switchButton.offText = @"OFF";
        switchButton.onTextColor = [UIColor colorWithHexString:@"ffffff"];
        if ([self.msgpush_flag isEqualToString:@"1"]) {
            switchButton.on = YES;
        }else {
            switchButton.on = NO;
        }
        [switchButton addTarget:self action:@selector(handleSwitchEvent:) forControlEvents:UIControlEventValueChanged];
        [cell.contentView addSubview:switchButton];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 1) {
        return;
    }
    NSDictionary *dict = self.tableDatas[indexPath.row];
    NSString *className = dict[@"ViewControll"];
    UIViewController *controller = [[NSClassFromString(className) alloc] init];
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:YES];
}



@end

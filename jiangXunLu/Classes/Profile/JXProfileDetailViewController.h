//
//  JXProfileDetailViewController.h
//  jiangXunLu
//
//  Created by lixu06 on 16/6/19.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JXViewController.h"

@interface JXProfileDetailViewController : JXViewController


typedef NS_ENUM(NSInteger, JXViewControllerEditMask) {
    /** 详情可编辑 */
    JXViewControllerEditMaskEnable             = 1,
    /** 详情不可编辑 */
    JXViewControllerEditMaskDisable            = 2
};

@property (nonatomic) JXViewControllerEditMask vcMask;

@property (nonatomic,strong) NSDictionary *caseDic; // 传入控制参数

@property (nonatomic, copy) JXViewControllerBlock finishBlock;

@end

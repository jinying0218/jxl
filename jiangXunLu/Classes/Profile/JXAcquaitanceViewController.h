//
//  JXAcquaitanceViewController.h
//  jiangXunLu
//
//  Created by lixu on 16/7/19.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JXViewController.h"
#import "NSObject+JXRefresh.h"

/*
 * JXAcquaitanceViewController
 * @description: 我的人脉页
 */

@interface JXAcquaitanceViewController : JXViewController

/*
 * @description: 传入控制参数
 * 例：@{@"targetId":targetId}
 */
@property (nonatomic,strong) NSDictionary *queryDic;

@end

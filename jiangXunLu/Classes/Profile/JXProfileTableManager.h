//
//  JXProfileTableManager.h
//  jiangXunLu
//
//  Created by lixu06 on 16/6/17.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXCustomTableModel.h"
#import "JXProfileViewController.h"
@interface JXProfileTableManager : JXCustomTableModel

@property (nonatomic, strong) UIView *headerViewBack;
@property (nonatomic) JXViewControllerProfileMask vcMask;

- (void)choosePhoto:(NSDictionary *)kvDic;
- (void)updateHeaderView;

@end

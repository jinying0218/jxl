//
//  JXLAboutViewController.m
//  jiangXunLu
//
//  Created by 金莹 on 16/9/3.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXLAboutViewController.h"
#import "jiangXunLu-swift.h"
#import "GlobalDefine.h"

#import "MBProgressHUD+Add.h"

@interface JXLAboutViewController ()<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation JXLAboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setUpLeftbarButtonWithImage:UIIMGName(@"back_normal")];
    self.title = @"关于我们";
    
    NSString *urlString = [NSString stringWithFormat:@"%@forapp/aboutus.php",Globe.baseUrlStr];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    [self.webView loadRequest:request];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)leftBarItemTouchUpInside:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

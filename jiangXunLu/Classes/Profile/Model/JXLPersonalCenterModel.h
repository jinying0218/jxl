//
//  JXLPersonalCenterModel.h
//  jiangXunLu
//
//  Created by 金莹 on 16/9/3.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JXLPersonalCenterModel : NSObject
@property (strong, nonatomic) NSString *attent_num;
@property (strong, nonatomic) NSString *brand;
@property (strong, nonatomic) NSString *collected_num;
@property (strong, nonatomic) NSString *company;
@property (strong, nonatomic) NSString *head;
@property (strong, nonatomic) NSString *identifier;
@property (strong, nonatomic) NSString *job;
@property (strong, nonatomic) NSString *msgpush_flag;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *party;
@property (strong, nonatomic) NSString *phone;
@property (strong, nonatomic) NSString *region;
@property (strong, nonatomic) NSString *sex;
@property (strong, nonatomic) NSString *show_flag;

@end

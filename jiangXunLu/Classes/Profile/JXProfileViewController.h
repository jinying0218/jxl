//
//  JXProfileViewController.h
//  jiangXunLu
//
//  Created by lixu06 on 16/6/17.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JXViewController.h"


@interface JXProfileViewController : JXViewController


typedef NS_ENUM(NSInteger, JXViewControllerProfileMask) {
    /** 查看本人档案 */
    JXViewControllerProfileMaskSelf             = 1,
    /** 查看他人档案 */
    JXViewControllerProfileMaskOther            = 2
};

@property (nonatomic) JXViewControllerProfileMask vcMask;

@property (nonatomic,strong) NSDictionary *queryDic; // 传入post参数
@property (nonatomic,strong) NSDictionary *caseDic; // 传入控制参数
@property (nonatomic, copy) JXViewControllerBlock finishBlock;
@end

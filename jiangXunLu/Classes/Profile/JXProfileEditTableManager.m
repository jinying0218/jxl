//
//  JXProfileEditTableManager.m
//  jiangXunLu
//
//  Created by lixu06 on 16/6/26.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXProfileEditTableManager.h"
#import "EXTScope.h"
#import "GlobalDefine.h"
#import "Color.h"
#import "String.h"
#import "Dimen.h"
#import "JXViewControlerRouter.h"

@interface JXProfileEditTableManager ()

@end

@implementation JXProfileEditTableManager

- (instancetype)initWithDictionary:(NSDictionary*)dic
                    viewController:(UIViewController *)controller
                         tableView:(UITableView *)tableView
                     finishHanlder:(CommomEditBlock)finishBlock {
    if (self = [super initWithDictionary:dic
                          viewController:controller
                               tableView:tableView
                           finishHanlder:finishBlock]) {
        return self;
    }
    return nil;
}


- (NSMutableArray *)getHeaderInfoList{
    self.headerInfoList = [super getHeaderInfoList];
    [self.headerInfoList addObject:[@{@"ViewIdentifier":@"null",
                                      } mutableCopy]];
    return self.headerInfoList;
}

- (NSMutableArray *)getCellInfoList {
    self.cellInfoList = [super getCellInfoList];
    
    NSMutableArray *firstSectionList = [NSMutableArray arrayWithCapacity:0];
    NSString *keyName = self.resultDic[@"keyName"]?self.resultDic[@"keyName"]:@"";
    NSString *textValue = self.resultDic[@"textValue"]?self.resultDic[@"textValue"]:@"";
    NSString *hintValue = self.resultDic[@"hintValue"]?self.resultDic[@"hintValue"]:@"";
    CGFloat height = self.resultDic[@"height"]?[self.resultDic[@"height"] floatValue]:100;
    BOOL heightFlexible = self.resultDic[@"heightFlexible"]?[self.resultDic[@"heightFlexible"] boolValue]:NO;
    long maxChar = self.resultDic[@"maxChar"]?[self.resultDic[@"maxChar"] longValue]:0;
    
    [firstSectionList addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                 @"JXCustomTextTableViewCell", @"CellIdentifier",
                                 UIColorFromRGB(DEFINE_COLOR_BACK), @"backgroundColor",
                                 textValue, @"textValue",
                                 hintValue, @"hintValue",
                                 keyName, @"keyName",
                                 textValue, @"textValue",
                                 hintValue, @"hintValue",
                                 @(height), @"height",
                                 @(heightFlexible),@"heightFlexible",
                                 @(maxChar),@"maxChar",
                                 @"textChangedForKey:",@"changedBlock",
                                 nil]];
    
    [self.cellInfoList addObject:firstSectionList];
    
    return self.cellInfoList;
}

- (void)registerTableCell {
    [super registerTableCell];
}

@end

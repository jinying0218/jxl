//
//  JXProfileTableManager.m
//  jiangXunLu
//
//  Created by lixu06 on 16/6/17.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXProfileTableManager.h"
#import "EXTScope.h"
#import "GlobalDefine.h"
#import "Color.h"
#import "String.h"
#import "Dimen.h"
#import "JXTipHud.h"
#import "CommonTools.h"
#import "JXViewControlerRouter.h"
#import "JXProfileHeaderView.h"
#import "JXProfileHeaderCell.h"
#import "JXProfileCollectCell.h"
#import "JXPhotoActionSheet.h"
#import "JXShareActionSheetView.h"
#import "JXShareActionSheet.h"
#import "NSObject+BMRuntime.h"
#import "jiangXunLu-swift.h"
#import "UIImage+Rotate.h"
#import "UIImage+Resizing.h"


#define PROFILE_ACTION_SHEET_TAG_PHOTO 0
#define PROFILE_ACTION_SHEET_TAG_SHARE 1

@interface JXProfileTableManager () <JXCustomActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (nonatomic,strong) NSDictionary *settingDic; // 传入控制

@end

@implementation JXProfileTableManager

- (instancetype)initWithDictionary:(NSDictionary*)dic
                    viewController:(UIViewController *)controller
                         tableView:(UITableView *)tableView
                     finishHanlder:(CommomEditBlock)finishBlock {
    if (self = [super initWithDictionary:dic
                          viewController:controller
                               tableView:tableView
                           finishHanlder:finishBlock]) {
        self.settingDic = dic[@"settingDic"]?dic[@"settingDic"]:@{};
        return self;
    }
    return nil;
}
-(UIView *)getHeaderView{
    
    NSDictionary *userDic = self.resultDic[@"user_info"];
    
    NSString *name = [CommonTools getValidText:userDic[@"name"] withPlaceHolder:STRING_PROFILE_NAME_DEFAULT];
    NSString *position = [CommonTools getValidText:userDic[@"job"] withPlaceHolder:STRING_PROFILE_POSITION_DEFAULT];
    NSString *phone = [CommonTools getValidText:userDic[@"phone"] withPlaceHolder:STRING_PROFILE_PHONE_DEFAULT];
    NSString *location = [CommonTools getValidText:userDic[@"region"] withPlaceHolder:STRING_PROFILE_LOCATION_DEFAULT];
    NSString *hotCount = [CommonTools getValidText:userDic[@"collected_num"] withPlaceHolder:STRING_PROFILE_HOTCOUNT_DEFAULT];
    NSString *gender = [CommonTools getValidText:userDic[@"sex"] withPlaceHolder:STRING_PROFILE_GENDER_DEFAULT];
    NSInteger showFlag = [[CommonTools getValidText:userDic[@"show_flag"] withPlaceHolder:@(0)] integerValue];
    NSString *avitar = [CommonTools getValidText:userDic[@"head"] withPlaceHolder:STRING_PROFILE_AVITAR_DEFAULT];
    
    BOOL editable = self.settingDic[@"editable"]?[self.settingDic[@"editable"] boolValue]:NO;
    
    self.headerInfoDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                          @"JXProfileHeaderView", @"ViewIdentifier",
                          @"popCurrentViewController",@"backBlock",
                          @"shareContent",@"shareBlock",
                          @"pushToProfileDetailViewController",@"editBlock",
                          @"choosePhoto:",@"avitarBlock",
                          name,@"name",
                          position,@"position",
                          phone,@"phone",
                          location,@"location",
                          hotCount,@"hotCount",
                          avitar,@"avitar",
                          @([gender integerValue]),@"gender",
                          @(editable),@"editable",
                          @(showFlag),@"showFlag",
                          nil];
    self.tableViewHeader = [self filterHeaderView];
    return self.tableViewHeader;
}
- (void)updateHeaderView{
    NSDictionary *userDic = self.resultDic[@"user_info"];
    NSString *name = [CommonTools getValidText:userDic[@"name"] withPlaceHolder:STRING_PROFILE_NAME_DEFAULT];
    NSString *position = [CommonTools getValidText:userDic[@"job"] withPlaceHolder:STRING_PROFILE_POSITION_DEFAULT];
    NSString *phone = [CommonTools getValidText:userDic[@"phone"] withPlaceHolder:STRING_PROFILE_PHONE_DEFAULT];
    NSString *location = [CommonTools getValidText:userDic[@"region"] withPlaceHolder:STRING_PROFILE_LOCATION_DEFAULT];
    NSString *hotCount = [CommonTools getValidText:userDic[@"collected_num"] withPlaceHolder:STRING_PROFILE_HOTCOUNT_DEFAULT];
    NSString *gender = [CommonTools getValidText:userDic[@"sex"] withPlaceHolder:STRING_PROFILE_GENDER_DEFAULT];
    NSInteger showFlag = [[CommonTools getValidText:userDic[@"show_flag"] withPlaceHolder:@(0)] integerValue];
    NSString *avitar = [CommonTools getValidText:userDic[@"head"] withPlaceHolder:STRING_PROFILE_AVITAR_DEFAULT];

    BOOL editable = self.settingDic[@"editable"]?[self.settingDic[@"editable"] boolValue]:NO;
    
    self.headerInfoDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                          @"JXProfileHeaderView", @"ViewIdentifier",
                          @"popCurrentViewController",@"backBlock",
                          @"shareContent",@"shareBlock",
                          @"pushToProfileDetailViewController",@"editBlock",
                          @"choosePhoto:",@"avitarBlock",
                          name,@"name",
                          position,@"position",
                          phone,@"phone",
                          location,@"location",
                          hotCount,@"hotCount",
                          @([gender integerValue]),@"gender",
                          @(editable),@"editable",
                          @(showFlag),@"showFlag",
                          avitar,@"avitar",
                          nil];
    ((JXProfileHeaderView *)self.tableViewHeader).styleDic = self.headerInfoDic;
    [(JXProfileHeaderView *)self.tableViewHeader updateViewInfo:self.headerInfoDic];
    
}

- (NSMutableArray *)getHeaderInfoList{
    self.headerInfoList = [super getHeaderInfoList];
    [self.headerInfoList addObject:[@{@"ViewIdentifier":@"null",
                                      } mutableCopy]];
    return self.headerInfoList;
}

- (NSMutableArray *)getCellInfoList {
    self.cellInfoList = [super getCellInfoList];
    
    NSDictionary *userDic = self.resultDic[@"user_info"];
    
    NSString *brandName = [CommonTools getValidText:userDic[@"brand"] withPlaceHolder:STRING_PROFILE_BRAND_LABLE_DEFAULT];
    NSString *companyName = [CommonTools getValidText:userDic[@"company"] withPlaceHolder:STRING_PROFILE_COMPANY_LABLE_DEFAULT];
    NSInteger collectedNum = [[CommonTools getValidText:userDic[@"collected_num"] withPlaceHolder:0] integerValue];
    NSString *collectInfo = [NSString stringWithFormat:@"%ld位用户收藏",collectedNum];
    NSArray *connectionsList = self.resultDic[@"connections_list"]?self.resultDic[@"connections_list"]:@[];
    
    NSDictionary *programDic = self.resultDic[@"project_info"];
    
//    NSString *programName = [CommonTools getValidText:programDic[@"name"] withPlaceHolder:STRING_PROFILE_PROGRAM_LABLE_DEFAULT];
    NSString *programIntro = [CommonTools getValidText:programDic[@"brief"] withPlaceHolder:STRING_PROFILE_PROGRAM_INTRO_DEFAULT];
//    NSString *programInfo = [CommonTools getValidText:programDic[@"detail"] withPlaceHolder:STRING_PROFILE_PROGRAM_INFO_DEFAULT];
    
//    BOOL editable = self.settingDic[@"editable"]?[self.settingDic[@"editable"] boolValue]:NO;
    
    NSMutableArray *firstSectionList = [NSMutableArray arrayWithCapacity:0];
    
    //    [firstSectionList addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
    //                                 @"JXProfileHeaderCell", @"CellIdentifier",
    //                                 @"popCurrentViewController",@"backBlock",
    //                                 @"shareContent",@"shareBlock",
    //                                 @"pushToProfileDetailViewController",@"editBlock",
    //                                 @"测试名",@"name",
    //                                 @"测试职位",@"position",
    //                                 @"15099334433",@"phone",
    //                                 @"北京",@"location",
    //                                 @"1234",@"hotCount",
    //                                 @(0),@"gender",
    //                                 self.settingDic[@"editable"]?(self.settingDic[@"editable"]):@(NO),@"editable",
    //                                 nil]];
    
    [firstSectionList addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                 @"JXCustomLabelTableViewCell", @"CellIdentifier",
                                 @(NO), @"isEnd",
                                 @(YES), @"isFold",
                                 @(YES), @"isFixed",
                                 @(NO), @"isActive",
                                 @(YES), @"isFullLine",
                                 @(64),@"height",
                                 @(1),@"numberOfLines",
                                 @(0), @"maxChar",
                                 STRING_PROFILE_BRAND_LABLE, @"inputLable",
                                 UIColorFromRGB(0xAAAAAA),@"textColor",
                                 @(NSTextAlignmentLeft), @"textAlignment",
                                 brandName, @"textValue",
                                 @"brandName", @"keyName",
                                 nil]];
    [firstSectionList addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                 @"JXCustomLabelTableViewCell", @"CellIdentifier",
                                 @(NO), @"isEnd",
                                 @(YES), @"isFold",
                                 @(YES), @"isFixed",
                                 @(NO), @"isActive",
                                 @(YES), @"isFullLine",
                                 @(0), @"maxChar",
                                 @(1),@"numberOfLines",
                                 STRING_PROFILE_COMPANY_LABLE, @"inputLable",
                                 UIColorFromRGB(0xAAAAAA),@"textColor",
                                 @(NSTextAlignmentLeft), @"textAlignment",
                                 companyName, @"textValue",
                                 @"companyName", @"keyName",
                                 nil]];
    
    [firstSectionList addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                 @"JXProfileCollectCell", @"CellIdentifier",
                                 @(NO),@"isFold", //@(!editable)
                                 STRING_PROFILE_COLLECT_LABLE, @"lableValue",
                                 UIColorFromRGB(0x666666), @"lableColor",
                                 
                                 collectInfo, @"collectText",
                                 connectionsList,@"collectArray",
                                 UIColorFromRGB(0xAAAAAA), @"collectColor",
                                 @"pushToAcquaitanceViewController:", @"clickBlock",
                                 @"collectInfo", @"keyName",
                                 nil]];
//    [firstSectionList addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                 @"JXCustomLabelTableViewCell", @"CellIdentifier",
//                                 @(NO), @"isEnd",
//                                 @(YES), @"isFold",
//                                 @(YES), @"isFixed",
//                                 @(NO), @"isActive",
//                                 @(YES), @"isFullLine",
//                                 @(0), @"maxChar",
//                                 @(1),@"numberOfLines",
//                                 STRING_PROFILE_PROGRAM_LABLE, @"inputLable",
//                                 UIColorFromRGB(0xAAAAAA),@"textColor",
//                                 @(NSTextAlignmentLeft), @"textAlignment",
//                                 programName, @"textValue",
//                                 @"programName", @"keyName",
//                                 nil]];
    [firstSectionList addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                 @"JXCustomLabelTableViewCell", @"CellIdentifier",
                                 @(NO), @"isEnd",
                                 @(YES), @"isFold",
                                 @(YES), @"isFixed",
                                 @(NO), @"isActive",
                                 @(YES), @"isFullLine",
                                 @(0), @"maxChar",
                                 @(3),@"numberOfLines",
                                 STRING_PROFILE_PROGRAM_INTRO, @"inputLable",
                                 UIColorFromRGB(0xAAAAAA),@"textColor",
                                 @(NSTextAlignmentLeft), @"textAlignment",
                                 programIntro, @"textValue",
                                 @"brandInfo", @"keyName",
                                 nil]];
     
/*
//     他人界面
     if (self.vcMask == JXViewControllerProfileMaskSelf) {
        [firstSectionList addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                     @"JXCustomSubmitTableViewCell", @"CellIdentifier",
                                     UIColorFromRGB(DEFINE_COLOR_WHITE), @"backgroundColor",
                                     @(80), @"height",
                                     @(19), @"marginTop",
                                     @(80), @"buttonWidth",
                                     @(26), @"buttonHeight",
                                     @"退出登录", @"buttonText",
                                     UIColorFromRGB(DEFINE_COLOR_BROWN), @"bottonBackgroundColor",
                                     UIColorFromRGB(DEFINE_COLOR_WHITE), @"buttonTextColor",
                                     @(3), @"buttonCornerRadius",
                                     FONTS(FONTSIZE_MIDDLE), @"buttonTextFont",
                                     @"loginOut:",@"submitBlock",
                                     nil]];
    } else {
         他人界面
        [firstSectionList addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                     @"JXCustomSubmitTableViewCell", @"CellIdentifier",
                                     UIColorFromRGB(DEFINE_COLOR_WHITE), @"backgroundColor",
                                     @(150), @"height",
                                     @(52), @"marginTop",
                                     @(80), @"buttonWidth",
                                     @(26), @"buttonHeight",
                                     STRING_PROFILE_PROGRAM_INFO, @"buttonText",
                                     UIColorFromRGB(DEFINE_COLOR_BROWN), @"bottonBackgroundColor",
                                     UIColorFromRGB(DEFINE_COLOR_WHITE), @"buttonTextColor",
                                     @(3), @"buttonCornerRadius",
                                     FONTS(FONTSIZE_MIDDLE), @"buttonTextFont",
                                     programInfo,@"textValue",
                                     @"programDetail", @"keyName",
                                     @"pushToProfileEditViewControllerProgramDetail:",@"submitBlock",
                                     nil]];
 
    }
*/

    
    [self.cellInfoList addObject:firstSectionList];
    return self.cellInfoList;
}

-(UIView *)filterHeaderView{
    if(!self.headerInfoDic){
        return nil;
    }
    NSString *viewIdentifier = self.headerInfoDic[@"ViewIdentifier"];
    if ([NSStringFromClass([JXProfileHeaderView class]) isEqualToString:viewIdentifier]) {
        JXProfileHeaderView *headerView = [JXProfileHeaderView defaultView];
        headerView.frame = RECT(0, 0, DEVICE_WIDTH, 250);
        self.tableView.tableHeaderView = headerView;
        headerView.styleDic = self.headerInfoDic;
        if (self.headerInfoDic[@"editBlock"]) {
            @weakify(self);
            headerView.editBlock = ^(NSDictionary *kvDic) {
                @strongify(self);
                SEL selector = NSSelectorFromString(self.headerInfoDic[@"editBlock"]);
                [self performSelectorSEL:selector withObject:kvDic];
            };
        }
        if (self.headerInfoDic[@"avitarBlock"]) {
            @weakify(self);
            headerView.avitarBlock = ^(NSDictionary *kvDic) {
                @strongify(self);
                SEL selector = NSSelectorFromString(self.headerInfoDic[@"avitarBlock"]);
                [self performSelectorSEL:selector withObject:kvDic];
            };
        }
        
        [headerView updateViewInfo:self.headerInfoDic];
        return headerView;
    }
    return nil;
}


- (void)registerTableCell {
    [super registerTableCell];
    [self registerNibCell:NSStringFromClass([JXProfileHeaderCell class])];
    [self registerNibCell:NSStringFromClass([JXProfileCollectCell class])];
}

#pragma mark -
#pragma mark UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGPoint offset = scrollView.contentOffset;
    if (offset.y < 0) { // down
        self.headerViewBack.frame = RECT(0, 0, DEVICE_WIDTH, 250 - offset.y);
    } else if (offset.y == 0) {
        self.headerViewBack.frame = RECT(0, 0, DEVICE_WIDTH, 250);
    } else if (offset.y > 0) { // up
        self.headerViewBack.frame = RECT(0, -offset.y, DEVICE_WIDTH, 250);
        if(offset.y > 44){
            [self.controller.navigationController setNavigationBarHidden:YES animated:NO];
        } else{
            [self.controller.navigationController setNavigationBarHidden:NO animated:NO];
        }
    }
}

#pragma mark -
#pragma mark Private Methods

- (void)pushToProfileDetailViewController{
    BOOL editable = self.settingDic[@"editable"]?[self.settingDic[@"editable"] boolValue]:NO;
    
    if (editable) {
        [JXViewControlerRouter pushToProfileDetailEditableViewController:self.controller
                                                                animated:YES
                                                                    data:self.resultDic
         finishBlock:^(id data) {
//             self.resultDic = [[[NSUserDefaults standardUserDefaults] valueForKey:@"user_dict"] mutableCopy];
//             [self updateHeaderView];
//             [self getCellInfoList];
//             [self.tableView reloadData];
         }];
    } else {
        [JXViewControlerRouter pushToProfileDetailDisabledViewController:self.controller
                                                                animated:YES
                                                                    data:self.resultDic];
    }
    
}

-(void)pushToAcquaitanceViewController:(NSDictionary *)cellDic {
    NSDictionary *userDic = self.resultDic[@"user_info"];
    NSString *targetId = [NSString stringWithFormat:@"%@", userDic[@"id"]];
    [JXViewControlerRouter pushToAcquaitanceViewController:self.controller
                                                  animated:YES
                                                      data:@{@"targetId":targetId}];
}

-(void)pushToProfileEditViewControllerProgramDetail:(NSDictionary *)cellDic{
    NSString *keyName = cellDic[@"keyName"];
    NSString *vcTitle = STRING_PROGRAM_DETAIL_CONTROLLER_TITLE;
    NSString *textValue = (cellDic[@"textValue"])?cellDic[@"textValue"]:@"";
    NSDictionary *settingDic= @{@"ViewControllerTitle":vcTitle,
                                @"JXProfileEditViewControllerType":@(JXProfileEditViewControllerTypeView),
                                @"JXProfileEditViewControllerMask":@(JXProfileEditViewControllerMaskProgramShow),
                                @"keyName":keyName,
                                @"textValue":textValue,
                                @"hintValue":STRING_PROGRAM_DETAIL_INFO_DEFAULT,
                                @"height":@(DEVICE_HEIGHT),
                                @"heightFlexible":@(YES),
                                @"maxChar":@(1000),
                                @"userInteractionEnabled":@(YES),
                                @"editable":@(NO)};
    
    [JXViewControlerRouter pushToProfileEditViewController:self.controller
                                                  animated:YES
                                                      data:settingDic
                                               finishBlock:nil];
}


- (void)loginOut:(NSDictionary *)cellDic {
    [[AppDelegate shareDelegate] setupLogionViewController];
}

- (void)choosePhoto:(NSDictionary *)kvDic {
    [self.controller.view endEditing:YES];
    JXPhotoActionSheet *actionSheet = [[JXPhotoActionSheet alloc] initWithData:@{} delegate:self];
    actionSheet.tag = PROFILE_ACTION_SHEET_TAG_PHOTO;
    [actionSheet show];
}

- (void)shareAction:(NSDictionary *)kvDic {
    [self.controller.view endEditing:YES];
    JXShareActionSheet *actionSheet = [[JXShareActionSheet alloc] initWithData:[JXShareActionSheet getDefaultStyleDic]
                                                                      delegate:self];
    actionSheet.tag = PROFILE_ACTION_SHEET_TAG_SHARE;
    [actionSheet show];
}

#pragma mark -
#pragma mark JXCustomActionSheetDelegate
- (void)actionSheet:(JXCustomActionSheet *)actionSheet buttonClickedAtTag:(NSInteger)tag{
    switch (actionSheet.tag) {
        case PROFILE_ACTION_SHEET_TAG_PHOTO:{
            switch (tag) {
                case JXPhotoActionSheetTagViewCancel:{
                    break;
                }
                case JXPhotoActionSheetTagViewTake:{ // 来源:相机
                    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
                    imagePickerController.delegate = self;
                    imagePickerController.allowsEditing = YES;
                    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                    // 判断是否支持相机
                    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                        // 跳转到相机页面
                        [self.controller presentViewController:imagePickerController animated:YES completion:^{
                            
                        }];
                        
                    } else{
                        [JXTipHud showTip:@"手机不支持拍照" dismissAfter:0.8 withFinishHandler:^(){
                            [self.controller presentViewController:imagePickerController animated:YES completion:^{
                                
                            }];
                        }];
                    }
                    break;
                }
                case JXPhotoActionSheetTagViewChoose:{ // 来源:相册
                    NSUInteger sourceType = UIImagePickerControllerSourceTypePhotoLibrary;//UIImagePickerControllerSourceTypeSavedPhotosAlbum ;  UIImagePickerControllerSourceTypePhotoLibrary
                    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
                    imagePickerController.delegate = self;
                    imagePickerController.allowsEditing = YES;
                    imagePickerController.sourceType = sourceType;
                    
                    [self.controller presentViewController:imagePickerController animated:YES completion:^{
                        
                    }];
                    break;
                }
                default:{
                    break;
                }
            }
            break;
        }
        case PROFILE_ACTION_SHEET_TAG_SHARE:{
            switch (tag) {
                case JXShareActionSheetViewTagCancel:{
                    break;
                }
                case JXShareActionSheetViewTagWeixinFriend:{
                    break;
                }
                case JXShareActionSheetViewTagCircle:{
                    break;
                }
                case JXShareActionSheetViewTagCollect:{
                    break;
                }
                case JXShareActionSheetViewTagSafari:{
                    break;
                }
                case JXShareActionSheetViewTagQQ:{
                    break;
                }
                case JXShareActionSheetViewTagPublic:{
                    break;
                }
                case JXShareActionSheetViewTagLink:{
                    break;
                }
                case JXShareActionSheetViewTagFont:{
                    break;
                }
                case JXShareActionSheetViewTagMode:{
                    break;
                }
                case JXShareActionSheetViewTagRefresh:{
                    break;
                }
                default:{
                    break;
                }
            }
            break;
        }
        default:{
            break;
        }
    }
}
- (void)actionSheetMaskClicked:(JXCustomActionSheet *)actionSheet{
    switch (actionSheet.tag) {
        case PROFILE_ACTION_SHEET_TAG_PHOTO:{
            break;
        }
        case PROFILE_ACTION_SHEET_TAG_SHARE:{
            break;
        }
        default:{
            break;
        }
    }
}

#pragma mark -
#pragma mark UIImagePickerControllerDelegate
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    @weakify(self)
    [picker dismissViewControllerAnimated:YES completion:^{
        @strongify(self)
        UIImage *image = nil;
        image = [UIImage fixOrientation:[info objectForKey:UIImagePickerControllerEditedImage]];
        
        image = [image cropToSize:CGSizeMake(image.size.width, image.size.width) usingMode:NYXCropModeCenter];
        
        [self requestModifyImage:image];
    }];
}

- (void)requestModifyImage:(UIImage *)image {
    /**
     19 更新用户头像
     - parameter path:   请求地址字符串
     - parameter params: ["action":"user_head_modify","uid":String,"randcode":String,"data":["uid":用户id,"image":base64图像数据,"type":"base64"/"stream"/"app_stream"]
     - parameter block:  回调
     */
    [JXTipHud showLoadingWithFinishHandler:nil];
    
    NSString *userId = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
    NSString *randcode = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"randcode"]];
    NSString *type = @"base64";
    
    NSData *imageData = UIImageJPEGRepresentation(image, 0.8f);
    NSString *base64String = [imageData base64EncodedStringWithOptions:0];
    NSString *stringImage = [NSString stringWithFormat:@"data:image/jpeg;base64,%@",base64String];


    JxlNetAPIManager *netmanager = [JxlNetAPIManager sharedManager];
    NSDictionary *params = @{@"action":@"user_head_modify",
                             @"uid":userId,
                             @"randcode":randcode,
                             @"data":@{
                                     @"uid":userId,
                                     @"type":type,
                                     @"image":stringImage
                                     }
                             };
    @weakify(self);
    [netmanager request_UpdateUserIconWithBlock:Globe.baseUrlStr
                                         Params:params
                                       andBlock:^(id _Nullable data, NSError * _Nullable error){
                                           @strongify(self);
                                           if (error) {
                                               //NSString *errorMsg = [error domain];
                                               [JXTipHud dismiss];
                                               //[JXTipHud showTip:errorMsg dismissAfter:0.8 withFinishHandler:nil];
                                               return;
                                           }
                                           if (!data) {
                                               [JXTipHud dismiss];
                                               [JXTipHud showTip:@"访问失败" dismissAfter:0.8 withFinishHandler:nil];
                                               return;
                                           }
                                           NSInteger status = [data[@"status"] integerValue];
                                           if (status != 1) {
                                               NSString *errorMsg = data[@"msg"];
                                               [JXTipHud dismiss];
                                               [JXTipHud showTip:errorMsg dismissAfter:0.8 withFinishHandler:nil];
                                               return;
                                           }
                                           NSDictionary *infoData = data[@"info"];
                                           if (infoData) {
                                               NSString *head = [CommonTools getValidText:infoData[@"head"] withPlaceHolder:@""];
                                               if ([head length]>0) {
                                                   [self modifySucees:head];
                                               } else {
                                                   [JXTipHud dismiss];
                                               }
                                           }
                                       }];
    
}
- (void)modifySucees:(NSString *)head{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *userInfoMutableDic = [[userDefaults valueForKey:@"user_dict"] mutableCopy];
    NSMutableDictionary *userDic = [userInfoMutableDic[@"user_info"] mutableCopy];
    
    userDic[@"head"] = head;
    
    userInfoMutableDic[@"user_info"] = userDic;
    NSDictionary *userInfoDic = [NSDictionary dictionaryWithDictionary:userInfoMutableDic];
    [userDefaults setObject:userInfoDic forKey:@"user_dict"];
    
    self.headerInfoDic[@"avitar"] = head;
    [(JXView *)self.tableViewHeader updateViewInfo:self.headerInfoDic];
    [JXTipHud dismiss];
}
@end

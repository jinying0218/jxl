//
//  JXProfileDetailViewController.m
//  jiangXunLu
//
//  Created by lixu06 on 16/6/19.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXProfileDetailViewController.h"
#import "GlobalDefine.h"
#import "CommonTools.h"
#import "Color.h"
#import "String.h"
#import "Dimen.h"
#import "JXCustomTableModel.h"
#import "JXTipHud.h"
#import "JXProfileDetailTableManager.h"
#import "JXViewControlerRouter.h"
#import "HexColor.h"

@interface JXProfileDetailViewController ()

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) JXCustomTableModel *tableManager;
@property (nonatomic, strong) NSMutableArray *cellInfoList;
@property (nonatomic, strong) NSMutableArray *photoArray;
@property (nonatomic, strong) NSMutableDictionary *resultDic;

@end

@implementation JXProfileDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = UIColorFromRGB(DEFINE_COLOR_BACK);
    self.title = STRING_PROFILE_DETAIL_CONTROLLER_TITLE;
    
    self.tableView = [[UITableView alloc] init];
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.contentView = self.tableView;
    [self setUpContentView];
    
    if (self.resultDic) {
    } else {
        if (self.caseDic) {
            self.resultDic = [NSMutableDictionary dictionaryWithDictionary:self.caseDic];
        }else {
            self.resultDic = [NSMutableDictionary dictionaryWithCapacity:0];
        }
    }
    NSDictionary * allDic =@{@"resultDic":self.resultDic,
                             @"settingDic":@{
                                     @"editable":@(self.vcMask == JXViewControllerEditMaskEnable)
                                     }
                             };
    self.tableManager = [[JXProfileDetailTableManager alloc] initWithDictionary:allDic
                                                           viewController:self
                                                                tableView:self.tableView
                                                            finishHanlder:nil];
    self.cellInfoList = [self.tableManager getCellInfoList];
    self.tableView.delegate = self.tableManager;
    self.tableView.dataSource = self.tableManager;
    self.tableManager.finishBlock = self.finishBlock;
    [self.tableView reloadData];
    
    
    if (self.vcMask == JXViewControllerEditMaskEnable) {
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [rightButton setTitle:@"保存" forState:UIControlStateNormal];
        [rightButton setTitleColor:[UIColor colorWithHexString:@"FECD08"] forState:UIControlStateNormal];
        [rightButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        [self setUpRightbarButtonWithCustomButton:rightButton];
    }
}

- (void)leftBarItemTouchUpInside:(id)sender{
    [JXViewControlerRouter popCurrentViewController:self animated:YES];
}

- (void)rightBarItemTouchUpInside:(id)sender{
    if ([self.tableManager isKindOfClass:[JXProfileDetailTableManager class]]) {
        JXProfileDetailTableManager *manager = (JXProfileDetailTableManager *)self.tableManager;
        [manager requestDataUpdate];
    }
}

//-(void)setNavigationBarStyleCustom{
//    [super setNavigationBarStyleCustom];
//}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setUpLeftbarButtonWithImage:UIIMGName(@"back_normal")];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end

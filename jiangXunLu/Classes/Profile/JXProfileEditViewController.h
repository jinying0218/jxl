//
//  JXProfileEditViewController.h
//  jiangXunLu
//
//  Created by lixu06 on 16/6/26.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXViewController.h"

@interface JXProfileEditViewController : JXViewController

typedef NS_ENUM(NSInteger, JXProfileEditViewControllerType) {
    /** cell编辑 */
    JXProfileEditViewControllerTypeView             = 1,
    /** view编辑 */
    JXProfileEditViewControllerTypeTable            = 2,
};
@property (nonatomic) JXProfileEditViewControllerType vcType;

typedef NS_ENUM(NSInteger, JXProfileEditViewControllerMask) {
    /** 编辑姓名 */
    JXProfileEditViewControllerMaskName             = 1,
    /** 编辑手机 */
    JXProfileEditViewControllerMaskPhone            = 2,
    /** 编辑公司 */
    JXProfileEditViewControllerMaskCompany          = 3,
    /** 编辑职位 */
    JXProfileEditViewControllerMaskPosition         = 4,
    /** 编辑品牌 */
    JXProfileEditViewControllerMaskBrand            = 5,
    /** 编辑项目名 */
    JXProfileEditViewControllerMaskProgram          = 6,
    /** 编辑项目简介 */
    JXProfileEditViewControllerMaskProgramBrief     = 7,
    /** 编辑项目详情 */
    JXProfileEditViewControllerMaskProgramEdit      = 8,
    /** 显示项目详情 */
    JXProfileEditViewControllerMaskProgramShow      = 9,
};

@property (nonatomic) JXProfileEditViewControllerMask vcMask;

@property (nonatomic,strong) NSDictionary *caseDic; // 传入控制参数


@property (nonatomic, copy) JXViewControllerBlock finishBlock;

@end

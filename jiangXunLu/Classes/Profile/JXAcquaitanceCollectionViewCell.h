//
//  JXAcquaitanceCollectionViewCell.h
//  jiangXunLu
//
//  Created by lixu on 16/7/19.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <UIKit/UIKit.h>
/*
 * JXAcquaitanceCollectionViewCell
 * @description: 我的人脉页的UICollectionViewCell
 */
@interface JXAcquaitanceCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIImageView *avitarImg;
@property (weak, nonatomic) IBOutlet UIButton *collectBtn;
@property (weak, nonatomic) IBOutlet UILabel *countLbl;
@property (weak, nonatomic) IBOutlet UIImageView *hotImg;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *telLbl;
@property (weak, nonatomic) IBOutlet UILabel *companyLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *countWidthLayout;

/*
 * updateCellInfo:
 * @description: 绑定数据
 * @params: (id)cellData, cell的控制数据
 */
-(void)updateCellInfo:(id)cellData;

@end

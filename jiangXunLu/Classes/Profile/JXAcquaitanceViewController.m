//
//  JXAcquaitanceViewController.m
//  jiangXunLu
//
//  Created by lixu on 16/7/19.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXAcquaitanceViewController.h"
#import "GlobalDefine.h"
#import "CommonTools.h"
#import "Color.h"
#import "String.h"
#import "Dimen.h"
#import "JXTipHud.h"
#import "JXViewControlerRouter.h"
#import "JXAcquaitanceCollectionViewCell.h"
#import "JXCustomRefreshView.h"
#import "EXTScope.h"

#define CONNECTION_LIST_REFRESH_TYPE_HEAD 0
#define CONNECTION_LIST_REFRESH_TYPE_FOOT 1

@interface JXAcquaitanceViewController () <UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) UICollectionViewFlowLayout *collectionViewLayout;
@property (nonatomic, assign) NSInteger pageSize;
@property (nonatomic, assign) NSInteger pageIndex;
@property (nonatomic, strong) NSMutableArray *dataArray;

@end

@implementation JXAcquaitanceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(DEFINE_COLOR_BACK);
    
    NSString *userId = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
    
    NSString *targetId = self.queryDic[@"targetId"];

    if ([userId isEqualToString:targetId]) {
        self.title = STRING_ACQUINTANCE_CONTROLLER_TITLE_SELF;
    } else {
        self.title = STRING_ACQUINTANCE_CONTROLLER_TITLE_OTHER;
    }
    
    self.pageSize = 10;
    self.pageIndex = 0;
    self.dataArray = [NSMutableArray arrayWithCapacity:0];
    self.collectionViewLayout = [[UICollectionViewFlowLayout alloc] init];
    [self.collectionViewLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    self.collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds
                                          collectionViewLayout:self.collectionViewLayout];
    [self.collectionView setShowsVerticalScrollIndicator:NO];
    [self.collectionView setBackgroundColor:[UIColor clearColor]];
    self.collectionView.alwaysBounceVertical = YES;
    
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.contentView = self.collectionView;
    
    [self setUpContentView];
    [self setUpRefreshContent:self.collectionView];
    
    JXCustomRefreshView *headerRefreshView = [JXCustomRefreshView defaultView];
    headerRefreshView.styleDic = [@{@"textValue":@"下拉刷新"} mutableCopy] ;
    CGFloat headerHeight = [JXCustomRefreshView viewHeight:headerRefreshView.styleDic];
    headerRefreshView.frame = CGRectMake(0,
                                         self.contentView.frame.origin.y - headerHeight,
                                         DEVICE_WIDTH,
                                         headerHeight);
    [headerRefreshView updateViewInfo:headerRefreshView.styleDic];
    [self setUpRefreshHeader:headerRefreshView onSuperView:self.view];
    
    JXCustomRefreshView *footerRefreshView = [JXCustomRefreshView defaultView];
    footerRefreshView.styleDic = [@{@"textValue":@"上拉加载更多"} mutableCopy] ;
    CGFloat footerHeight = [JXCustomRefreshView viewHeight:footerRefreshView.styleDic];
    footerRefreshView.frame = CGRectMake(0,
                                         self.contentView.frame.origin.y + self.contentView.frame.size.height,
                                         DEVICE_WIDTH,
                                         footerHeight);
    [footerRefreshView updateViewInfo:footerRefreshView.styleDic];
    [self setUpRefreshFooter:footerRefreshView onSuperView:self.view];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"JXAcquaitanceCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"JXAcquaitanceCollectionViewCell"];
    
    [self requestData:CONNECTION_LIST_REFRESH_TYPE_HEAD];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(collectSuccessNotification:) name:@"CollectSuccessNotification" object:nil];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setUpLeftbarButtonWithImage:UIIMGName(@"back_normal")];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)collectSuccessNotification:(NSNotification *)info{
    NSLog(@"收藏结果");
}

- (void)leftBarItemTouchUpInside:(id)sender{
    [JXViewControlerRouter popCurrentViewController:self animated:YES];
}

//-(void)setNavigationBarStyleCustom{
//    [super setNavigationBarStyleCustom];
//}
#pragma mark - UICollectionViewDataSource

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString *cellIdentifier = @"JXAcquaitanceCollectionViewCell";
    JXAcquaitanceCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    [cell updateCellInfo:self.dataArray[indexPath.row]];
    return cell;
}
#pragma mark --UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake((DEVICE_WIDTH - 20 - 10)/2, 80);
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(10, 10, 10, 10);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 10;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 10;
}

#pragma mark --UICollectionViewDelegate
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
//    UICollectionViewCell * cell = (UICollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [self pushToProfileViewController:self.dataArray[indexPath.row]];
}
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

#pragma mark -
#pragma mark UIScrollViewDelegate

- (BOOL)headerRefreshable {
    return YES;
}
- (BOOL)footerRefreshable {
    return YES;
}
- (void)headerRefresh {
    [self requestData:CONNECTION_LIST_REFRESH_TYPE_HEAD];
}
- (void)footerRefresh {
    [self requestData:CONNECTION_LIST_REFRESH_TYPE_FOOT];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [self refresh_scrollViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [self refresh_scrollViewDidEndDragging:scrollView willDecelerate:decelerate];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    [self refresh_scrollViewDidEndDecelerating:scrollView];
}

- (void)requestData:(NSInteger)type{
    /**
     25 获取人脉信息列表
     - parameter path:   请求地址字符串
     - parameter params: ["action":"connections_list_load","uid":String,"randcode":String,"data":["uid":聊天的用户id,"start":Int,"count":Int]]
     - parameter block:  回调
     */
    
    [JXTipHud showLoadingWithFinishHandler:nil];
    
    NSString *userId = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
    NSString *randcode = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"randcode"]];
    NSInteger start = [self.dataArray count];
    NSInteger count = self.pageSize;
    if (type == CONNECTION_LIST_REFRESH_TYPE_HEAD) {
        start = 0;
    }
    if (!self.queryDic || !self.queryDic[@"targetId"]) {
        [JXTipHud showTip:@"参数错误" dismissAfter:0.8 withFinishHandler:nil];
        return;
    }
    NSString *targetId = self.queryDic[@"targetId"];
    JxlNetAPIManager *netmanager = [JxlNetAPIManager sharedManager];
    NSDictionary *params = @{@"action":@"connections_list_load",
                             @"uid":userId,
                             @"randcode":randcode,
                             @"data":@{
                                     @"uid":targetId,
                                     @"start":@(start),
                                     @"count":@(count),
                                     }
                             };
    @weakify(self);
    [netmanager request_ConnectionsListWithBlock:Globe.baseUrlStr
                                         Params:params
                                       andBlock:^(id _Nullable data, NSError * _Nullable error){
                                           @strongify(self);
                                           JXLog(@"%@",data);
                                           if (type == CONNECTION_LIST_REFRESH_TYPE_HEAD) {
                                               [self headerRefreshEnd];
                                           } else {
                                               [self footerRefreshEnd];
                                           }
                                           [JXTipHud dismiss];
                                           if (error) {
                                               //NSString *errorMsg = [error domain];
                                               //[JXTipHud showTip:errorMsg dismissAfter:0.8 withFinishHandler:nil];
                                               return;
                                           }
                                           if (!data) {
                                               [JXTipHud showTip:@"访问失败" dismissAfter:0.8 withFinishHandler:nil];
                                               return;
                                           }
                                           NSInteger status = [data[@"status"] integerValue];
                                           if (status != 1) {
                                               NSString *errorMsg = data[@"msg"];
                                               [JXTipHud showTip:errorMsg dismissAfter:0.8 withFinishHandler:nil];
                                               return;
                                           }
                                           id result = data[@"info"];
                                           if (!result) {
                                               [JXTipHud showTip:@"访问失败" dismissAfter:0.8 withFinishHandler:nil];
                                               return;
                                           }
                                           id list = result[@"list"];
                                           if ( list && [list isKindOfClass:[NSArray class]]) {
                                               [self updateListData:list withType:type];
                                           } else{
                                               [JXTipHud showTip:@"数据错误" dismissAfter:0.8 withFinishHandler:nil];
                                               return;
                                           }
                                       }];
}

-(void)updateListData:(NSArray *)list withType:(NSInteger)type{
    if (type == CONNECTION_LIST_REFRESH_TYPE_HEAD) {
        [self.dataArray removeAllObjects];
    }
    if ([list count] == 0) {
        [JXTipHud showTip:@"没有更多数据" dismissAfter:0.8 withFinishHandler:nil];
        return;
    }
    
    [self.dataArray addObjectsFromArray:list];
    
    [self.collectionView reloadData];
}

- (void)pushToProfileViewController:(id)data{
    NSString *targetId = data[@"id"];
    NSDictionary *transdata = @{@"targetId":targetId};
//    [JXViewControlerRouter pushToProfileOtherViewController:self
//                                                   animated:YES
//                                                       data:transdata];
    @weakify(self);
    [JXViewControlerRouter pushToProfileOtherViewController:self
                                                   animated:YES
                                                       data:transdata
                                                finishBlock:^(NSDictionary *data) {
                                                    @strongify(self);
                                                    NSInteger collectStatus = [data[@"status"] integerValue];
                                                    NSInteger targetId = [data[@"targetId"] integerValue];
                                                    NSMutableDictionary *newDict = nil;
                                                    NSUInteger index = 0;
                                                    for (NSDictionary *dict in self.dataArray) {
                                                        NSInteger identifier = [dict[@"id"] integerValue];
                                                        if (identifier == targetId) {
                                                            newDict = [NSMutableDictionary dictionaryWithDictionary:dict];
                                                            [newDict setValue:@(collectStatus) forKey:@"coll_status"];
                                                            break;
                                                        }
                                                        index ++;
                                                    }
                                                    if (newDict) {
                                                        [self.dataArray replaceObjectAtIndex:index withObject:newDict];
                                                    }
                                                    [self.collectionView reloadData];
        
    }];
    
}

@end

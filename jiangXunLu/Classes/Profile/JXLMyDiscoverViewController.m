//
//  JXLMyDiscoverViewController.m
//  jiangXunLu
//
//  Created by 金莹 on 16/9/4.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXLMyDiscoverViewController.h"
#import "jiangXunLu-swift.h"
#import "GlobalDefine.h"
#import "AFNTool.h"
#import "YYModel.h"
#import "UIImageView+WebCache.h"
#import "Masonry.h"
#import "MJRefresh.h"
#import "EXTScope.h"
#import "MBProgressHUD+Add.h"
#import "KVOController.h"

#import "JXLMyDiscoverHeaderView.h"
#import "DiscoverInfoTableViewCell.h"
#import "DiscoverProjectTableViewCell.h"
#import "DiscoverBrandTableViewCell.h"
#import "JXLDiscoverInfoModel.h"
#import "JXLDiscoverProjectModel.h"
#import "JXLDiscoverBrandModel.h"

#import "JXLReplayInfoTableViewCell.h"
#import "JXLReplayProjectTableViewCell.h"
#import "JXLReplayBrandTableViewCell.h"

#import "DiscoverDetailViewController.h"


static NSString *kMyDiscoverInfoCellIdentifier = @"DiscoverInfoTableViewCellIdentifier";
static NSString *kMyDiscoverProjectCellIdentifier = @"DiscoverProjectTableViewCellIdentifier";
static NSString *kMyDiscoverBrandCellIdentifier = @"DiscoverBrandCellIdentifier";

static NSString *MyDiscoverReplyInfoCellIdentifier = @"MyDiscoverReplyInfoCellIdentifier";
static NSString *MyDiscoverReplyProjectCellIdentifier = @"MyDiscoverReplyProjectCellIdentifier";
static NSString *MyDiscoverReplyBrandCellIdentifier = @"MyDiscoverReplyBrandCellIdentifier";

@interface JXLMyDiscoverViewController ()<JXLMyDiscoverHeaderViewDelegate,UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet JXLMyDiscoverHeaderView *headerView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *nullView;
@property (strong, nonatomic) NSMutableArray *postDatas;
@property (strong, nonatomic) NSMutableArray *replyDatas;
@property (strong, nonatomic) NSMutableArray *collectDatas;
@property (strong, nonatomic) NSMutableArray *tempArray;

@property (assign, nonatomic) NSUInteger index;

@property (assign, nonatomic) NSUInteger startNumber;
@property (assign, nonatomic) NSUInteger countNumber;

@end

@implementation JXLMyDiscoverViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"我的发现";
    self.index = 0;

    self.postDatas = [[NSMutableArray alloc] initWithCapacity:2];
    self.replyDatas = [[NSMutableArray alloc] initWithCapacity:2];
    self.collectDatas = [[NSMutableArray alloc] initWithCapacity:2];
    self.tempArray = [[NSMutableArray alloc] initWithCapacity:2];

    self.headerView.delegate = self;
    [self setupTableView];
    
    [self setUpLeftbarButtonWithImage:UIIMGName(@"back_normal")];
}

- (void)setupTableView{
    [self.tableView registerNib:[UINib nibWithNibName:@"DiscoverInfoTableViewCell" bundle:nil] forCellReuseIdentifier:kMyDiscoverInfoCellIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:@"DiscoverProjectTableViewCell" bundle:nil] forCellReuseIdentifier:kMyDiscoverProjectCellIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:@"DiscoverBrandTableViewCell" bundle:nil] forCellReuseIdentifier:kMyDiscoverBrandCellIdentifier];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"JXLReplayInfoTableViewCell" bundle:nil] forCellReuseIdentifier:MyDiscoverReplyInfoCellIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:@"JXLReplayProjectTableViewCell" bundle:nil] forCellReuseIdentifier:MyDiscoverReplyProjectCellIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:@"JXLReplayBrandTableViewCell" bundle:nil] forCellReuseIdentifier:MyDiscoverReplyBrandCellIdentifier];

    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    @weakify(self);
    
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        self.startNumber = 0;
        self.countNumber = 20;
        
        [self.postDatas removeAllObjects];
        [self.replyDatas removeAllObjects];
        [self.collectDatas removeAllObjects];
        
        [self reloadTableDatas];
    }];
    
    self.tableView.mj_footer = [MJRefreshBackStateFooter footerWithRefreshingBlock:^{
        @strongify(self);
        self.startNumber += 20;
        [self reloadTableDatas];
    }];
    
    
    [self.tableView.mj_header beginRefreshing];
        
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - loadData
/*
 action:personal_find_release_list_load
	uid:用户id
	randcode:登陆状态验证码
	data:{
 start : 起始行,
 count : 数据行数
	}


 */

- (void)reloadTableDatas{
    
    [self.tempArray removeAllObjects];

    NSString *userId = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
    NSString *randcode = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"randcode"]];
    NSDictionary *subDict = @{@"start" : @(self.startNumber),
                              @"count" : @(self.countNumber)};
    
    NSDictionary *params = nil;
    switch (self.index) {
        case 0:{
            params = @{@"action" : @"personal_find_release_list_load",
                       @"uid" : userId,
                       @"randcode" : randcode,
                       @"data" : [subDict yy_modelToJSONString]
                       };
        }
            break;
        case 1:{
            params = @{@"action" : @"personal_find_reply_list_load",
                       @"uid" : userId,
                       @"randcode" : randcode,
                       @"data" : [subDict yy_modelToJSONString]
                       };
        }
            break;
            
        default:{
            params = @{@"action" : @"personal_find_collection_list_load",
                       @"uid" : userId,
                       @"randcode" : randcode,
                       @"data" : [subDict yy_modelToJSONString]
                       };
        }
            break;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    @weakify(self);
    [AFNTool post:Globe.baseUrlStr params:params success:^(id responsObject) {
        @strongify(self);
        NSLog(@"%@",responsObject);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        if (responsObject && [responsObject isKindOfClass:[NSDictionary class]]) {
            NSInteger status = [responsObject[@"status"] integerValue];
            if (status == 1) {
                NSArray *list = [responsObject[@"info"] objectForKey:@"list"];
                [self.tempArray addObjectsFromArray:list];
                self.tableView.hidden = NO;
                self.nullView.hidden = YES;
                switch (self.index) {
                    case 0:{
                        [self transformArray:list targetArray:self.postDatas];
                        
                        if ([self showNullView:self.postDatas]) {
                            return ;
                        }
                    }
                        break;
                    case 1:{
                        [self transformArray:list targetArray:self.replyDatas];
                        if ([self showNullView:self.replyDatas]) {
                            return ;
                        }
                    }
                        break;
                    default:{
                        [self transformArray:list targetArray:self.collectDatas];
                        if ([self showNullView:self.collectDatas]) {
                            return ;
                        }
                    }
                        break;
                }
                if (self.tempArray.count < self.countNumber) {
                    [self.tableView.mj_footer endRefreshingWithNoMoreData];
                }else {
                    [self.tableView.mj_footer endRefreshing];
                }
                [self.tableView.mj_header endRefreshing];
                
                [self.tableView reloadData];
            }else {
                [MBProgressHUD showError:responsObject[@"msg"] toView:self.view];
            }
        }
    } failure:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [MBProgressHUD showError:error.description toView:self.view];

//        NSLog(@"%@",error.description);
    }];

}

- (void)transformArray:(NSArray *)list targetArray:(NSMutableArray *)targetArray{
    for (NSDictionary *dict in list) {
        if ([dict[@"ftype"] isEqualToString:@"info"]) {
            JXLDiscoverInfoModel *model = [JXLDiscoverInfoModel yy_modelWithDictionary:dict[@"info"]];
            [targetArray addObject:model];
        }else if ([dict[@"ftype"] isEqualToString:@"project"]){
            JXLDiscoverProjectModel *model = [JXLDiscoverProjectModel yy_modelWithDictionary:dict[@"info"]];
            [targetArray addObject:model];
        }else {
            JXLDiscoverBrandModel *model = [JXLDiscoverBrandModel yy_modelWithDictionary:dict[@"info"]];
            [targetArray addObject:model];
        }
    }
}


- (BOOL)showNullView:(NSMutableArray *)tableDatas{
    
    if (tableDatas.count == 0) {
        self.tableView.hidden = YES;
        self.nullView.hidden = NO;
        return YES;
    }
    return NO;
}

#pragma mark - action

- (void)leftBarItemTouchUpInside:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - JXLMyDiscoverHeaderViewDelegate
- (void)changeHeaderSelectedButton:(UIButton *)selectedButton{
    self.index = selectedButton.tag - 3000;
    self.tableView.hidden = NO;
    self.nullView.hidden = YES;
    switch (self.index) {
        case 0:{
            if (![self showNullView:self.postDatas]) {
                [self.tableView reloadData];
                return ;
            }
        }
            break;
        case 1:{
            if (![self showNullView:self.replyDatas]) {
                [self.tableView reloadData];

                return ;
            }
        }
            break;
        default:{
            if (![self showNullView:self.collectDatas]) {
                [self.tableView reloadData];
                return ;
            }
        }
            break;
    }
//    
//    [self.postDatas removeAllObjects];
//    [self.replyDatas removeAllObjects];
//    [self.collectDatas removeAllObjects];
    
    [self reloadTableDatas];
}

#pragma mark - tableView delegate & dataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (self.index) {
        case 0:{
            return self.postDatas.count;
        }
            break;
        case 1:{
            return self.replyDatas.count;

        }
            break;
        default:{
            return self.collectDatas.count;
        }
            break;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 0;
    }
    return 6;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.index == 1) {
        return 192;
    }
    return 152;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    id cellModel = nil;
    switch (self.index) {
        case 0:{
            cellModel = self.postDatas[indexPath.row];
            return [self getCellClassByCellModel:cellModel];
        }
            break;
        case 1:{
            cellModel = self.replyDatas[indexPath.row];
            return [self getReplyCellClassByCellModel:cellModel];
        }
            break;
        default:{
            cellModel = self.collectDatas[indexPath.row];
            return [self getCellClassByCellModel:cellModel];
        }
            break;
    }
}

- (id)getCellClassByCellModel:(id)cellModel{
    if ([cellModel isKindOfClass:[JXLDiscoverInfoModel class]]) {
        DiscoverInfoTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:kMyDiscoverInfoCellIdentifier];
        [cell configureCell:cellModel];
        return cell;
    }else if ([cellModel isKindOfClass:[JXLDiscoverProjectModel class]]) {
        DiscoverProjectTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:kMyDiscoverProjectCellIdentifier];
        [cell configureCell:cellModel];
        return cell;
    }else {
        DiscoverBrandTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:kMyDiscoverBrandCellIdentifier];
        [cell configureCell:cellModel];
        return cell;
    }
}
//回复的cell

- (id)getReplyCellClassByCellModel:(id)cellModel{
    if ([cellModel isKindOfClass:[JXLDiscoverInfoModel class]]) {
        JXLReplayInfoTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:MyDiscoverReplyInfoCellIdentifier];
        [cell configureCell:cellModel];
        return cell;
    }else if ([cellModel isKindOfClass:[JXLDiscoverProjectModel class]]) {
        JXLReplayProjectTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:MyDiscoverReplyProjectCellIdentifier];
        [cell configureCell:cellModel];
        return cell;
    }else {
        JXLReplayBrandTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:MyDiscoverReplyBrandCellIdentifier];
        [cell configureCell:cellModel];
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    id cellModel = nil;
    JXLDisType ftype = 0;
    NSString *identifier = nil;
    switch (self.index) {
        case 0:{
            cellModel = self.postDatas[indexPath.row];
        }
            break;
        case 1:{
            cellModel = self.replyDatas[indexPath.row];
        }
            break;
        default:{
            cellModel = self.collectDatas[indexPath.row];
        }
            break;
    }
    if ([cellModel isKindOfClass:[JXLDiscoverInfoModel class]]) {
        ftype = kDiscoverInfo;
        JXLDiscoverInfoModel *model = cellModel;
        identifier = model.discoverInfo_id;
    }else if ([cellModel isKindOfClass:[JXLDiscoverProjectModel class]]) {
        ftype = kDiscoverProject;
        JXLDiscoverProjectModel *model = cellModel;
        identifier = model.project_id;

    }else {
        ftype = kDiscoverBrand;
        JXLDiscoverBrandModel *model = cellModel;
        identifier = model.brand_id;
    }

    DiscoverDetailViewController *detailVC = [[DiscoverDetailViewController alloc] initWithDiscoverType:ftype];
    detailVC.identifier = identifier;
    [self.navigationController pushViewController:detailVC animated:YES];
}


@end

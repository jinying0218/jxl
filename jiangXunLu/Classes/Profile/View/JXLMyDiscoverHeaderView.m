//
//  JXLMyDiscoverHeaderView.m
//  jiangXunLu
//
//  Created by 金莹 on 16/9/4.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXLMyDiscoverHeaderView.h"
#import "jiangXunLu-swift.h"
#import "XXNibBridge.h"


@interface JXLMyDiscoverHeaderView()<XXNibBridge>
@property (weak, nonatomic) IBOutlet UIButton *postButton;
@property (weak, nonatomic) IBOutlet UIButton *replyButton;
@property (weak, nonatomic) IBOutlet UIButton *collectButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *indicatorLineLeadingConstaint;
@property (strong, nonatomic) UIButton *selectedButton;

@end

@implementation JXLMyDiscoverHeaderView

- (void)awakeFromNib{
    [super awakeFromNib];
    
    self.selectedButton = self.postButton;
}

- (IBAction)segmentButtonClick:(UIButton *)sender {
    if (self.selectedButton != sender) {
        self.selectedButton.selected = NO;
        self.selectedButton = sender;
        self.selectedButton.selected = YES;
        
        NSUInteger index = self.selectedButton.tag - 3000;
        self.indicatorLineLeadingConstaint.constant = self.selectedButton.width * index;
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(changeHeaderSelectedButton:)]) {
            [self.delegate changeHeaderSelectedButton:sender];
        }
        
        [UIView animateWithDuration:0.3 animations:^{
            [self layoutIfNeeded];
        }];
    }
    
}


@end

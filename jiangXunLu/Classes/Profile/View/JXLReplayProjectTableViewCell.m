//
//  JXLReplayProjectTableViewCell.m
//  jiangXunLu
//
//  Created by 金莹 on 16/9/21.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXLReplayProjectTableViewCell.h"
#import "JXLDiscoverProjectModel.h"
#import "UIImageView+WebCache.h"
#import "jiangXunLu-swift.h"
#import "YYDateFormatterTool.h"


@interface JXLReplayProjectTableViewCell ()
@property (strong, nonatomic) JXLDiscoverProjectModel *cellModel;

@property (weak, nonatomic) IBOutlet UILabel *replyContentLabel;
@property (weak, nonatomic) IBOutlet UILabel *replyTimeLabel;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *createdTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UILabel *projectTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *areaLabel;
@property (weak, nonatomic) IBOutlet UILabel *showDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *authorLabel;
@property (weak, nonatomic) IBOutlet UILabel *readCountLabel;
@property (weak, nonatomic) IBOutlet UIImageView *coverImageView;


@end
@implementation JXLReplayProjectTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (CGFloat)cellHeight{
    return 192;
}

- (void)configureCell:(id)cellModel{
    if (_cellModel != cellModel) {
        _cellModel = cellModel;
        
        self.replyContentLabel.text = _cellModel.reply_content;
        self.replyTimeLabel.text = _cellModel.show_time;
        
        
//        [[YYDateFormatterTool shareInstance] setDateFormat:@"yyy-MM-dd HH:mm:ss"];
        //        NSDate *created_time = [[YYDateFormatterTool shareInstance] dateFromString:_cellModel.created_time];
//        NSDate *show_time = [[YYDateFormatterTool shareInstance] dateFromString:_cellModel.show_time];
        
        //        [[YYDateFormatterTool shareInstance]  setDateFormat:@"yyyy-MM-dd"];
        //        NSString *createDate = [[YYDateFormatterTool shareInstance] stringFromDate:created_time];
//        [[YYDateFormatterTool shareInstance]  setDateFormat:@"yyyy-MM"];
//        NSString *show_timeString = [[YYDateFormatterTool shareInstance] stringFromDate:show_time];
        
        self.titleLabel.text = _cellModel.title;
        self.createdTimeLabel.text = _cellModel.created_time;
        self.locationLabel.text = _cellModel.position;
        self.projectTypeLabel.text = _cellModel.type;
        self.areaLabel.text = _cellModel.area;
        self.showDateLabel.text = _cellModel.opening_time;
        self.authorLabel.text = _cellModel.company;
        self.readCountLabel.text = [NSString stringWithFormat:@"%@",_cellModel.read_count];
        
        [self.coverImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",Globe.baseUrlStr,_cellModel.cover]]];
        
    }
}

@end

//
//  JXLMyCenterCell.h
//  jiangXunLu
//
//  Created by 金莹 on 16/9/3.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JXLMyCenterCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *seperatorLine;


- (void)configureCell:(NSDictionary *)dict;
@end

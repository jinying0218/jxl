//
//  JXLReplayProjectTableViewCell.h
//  jiangXunLu
//
//  Created by 金莹 on 16/9/21.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JXLReplayProjectTableViewCell : UITableViewCell

+ (CGFloat)cellHeight;
- (void)configureCell:(id)cellModel;
@end

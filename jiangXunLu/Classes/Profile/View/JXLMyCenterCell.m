//
//  JXLMyCenterCell.m
//  jiangXunLu
//
//  Created by 金莹 on 16/9/3.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXLMyCenterCell.h"
@interface JXLMyCenterCell()
@property (weak, nonatomic) IBOutlet UIImageView *headerImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;


@end


@implementation JXLMyCenterCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureCell:(NSDictionary *)dict{
    [self.headerImageView setImage:[UIImage imageNamed:dict[@"icon"]]];
    self.titleLabel.text = dict[@"title"];
}

@end

//
//  JXLMyDiscoverHeaderView.h
//  jiangXunLu
//
//  Created by 金莹 on 16/9/4.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol JXLMyDiscoverHeaderViewDelegate <NSObject>

- (void)changeHeaderSelectedButton:(UIButton *)selectedButton;

@end

@interface JXLMyDiscoverHeaderView : UIView
@property (assign, nonatomic) id<JXLMyDiscoverHeaderViewDelegate>delegate;

@end

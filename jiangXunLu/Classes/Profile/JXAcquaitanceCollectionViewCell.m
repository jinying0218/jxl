//
//  JXAcquaitanceCollectionViewCell.m
//  jiangXunLu
//
//  Created by lixu on 16/7/19.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXAcquaitanceCollectionViewCell.h"
#import "CommonTools.h"
#import "GlobalDefine.h"
#import "NSObject+BMRuntime.h"
#import "UIImageView+WebCache.h"

@implementation JXAcquaitanceCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.contentView.backgroundColor = [UIColor clearColor];
    self.backView.layer.cornerRadius = 5.0f;
    self.backView.layer.masksToBounds = YES;
    self.avitarImg.layer.cornerRadius = 32/2;
    self.avitarImg.layer.masksToBounds = YES;
}

-(void)updateCellInfo:(id)infoData {
    UIImage *defaultIcon = UIIMGName(@"logo_default");
    if (infoData[@"head"] && [CommonTools isValidProperty:infoData[@"head"]]) {
        NSURL *picUrl =  IMGURL(infoData[@"head"]);
        [self.avitarImg sd_setImageWithURL:picUrl placeholderImage:defaultIcon];
    } else {
        [self.avitarImg setImage:defaultIcon];
    }
    self.countLbl.text = [CommonTools getValidText:infoData[@"collected_num"] withPlaceHolder:@(0)];
    
    CGSize countSize = BD_MULTILINE_TEXTSIZE(self.countLbl.text,
                                                FONTS(8),
                                                CGSizeMake(MAXFLOAT, MAXFLOAT),
                                                NSLineBreakByWordWrapping);
    self.countWidthLayout.constant = countSize.width;
    NSString *showFlag = [CommonTools getValidText:infoData[@"show_flag"] withPlaceHolder:@(0)];
    self.telLbl.text = [CommonTools getValidText:infoData[@"phone"] withPlaceHolder:@""];
    self.telLbl.hidden = !(([showFlag integerValue] == 1));
    self.nameLbl.text = [CommonTools getValidText:infoData[@"name"] withPlaceHolder:@""];
    self.companyLbl.text = [CommonTools getValidText:infoData[@"company"] withPlaceHolder:@""];
    self.titleLbl.text = [CommonTools getValidText:infoData[@"job"] withPlaceHolder:@""];
    [self.collectBtn setImage:UIIMGName(@"add_sc_normal") forState:UIControlStateNormal];
    [self.collectBtn setImage:UIIMGName(@"add_sc_light") forState:UIControlStateSelected];
    self.collectBtn.selected = infoData[@"head"]?([infoData[@"coll_status"] integerValue] ==1):NO;
}

@end

//
//  JXProfileEditViewController.m
//  jiangXunLu
//
//  Created by lixu06 on 16/6/26.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXProfileEditViewController.h"
#import "JXProfileEditTableManager.h"

#import "JXViewControlerRouter.h"
#import "GlobalDefine.h"
#import "CommonTools.h"
#import "Color.h"
#import "String.h"
#import "Dimen.h"
#import "JXCustomTableModel.h"
#import "JXTipHud.h"
#import "JXCustomTextView.h"

@interface JXProfileEditViewController ()

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) JXCustomTextView *textView;

@property (nonatomic, strong) JXCustomTableModel *tableManager;

@property (nonatomic, strong) NSMutableArray *cellInfoList;
@property (nonatomic, strong) NSMutableArray *photoArray;
@property (nonatomic, strong) NSMutableDictionary *resultDic;

@end

@implementation JXProfileEditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(DEFINE_COLOR_BACK);    
    if (self.resultDic) {
    } else {
        if (self.caseDic) {
            self.resultDic = [NSMutableDictionary dictionaryWithDictionary:self.caseDic];
        }else {
            self.resultDic = [NSMutableDictionary dictionaryWithCapacity:0];
        }
    }
    
    self.title = self.resultDic[@"ViewControllerTitle"]?self.resultDic[@"ViewControllerTitle"]:STRING_PROFILE_EDIT_DEFAULT;
    self.vcType = self.resultDic[@"JXProfileEditViewControllerType"]?[self.resultDic[@"JXProfileEditViewControllerType"] integerValue]:JXProfileEditViewControllerTypeTable;
    self.vcMask = self.resultDic[@"JXProfileEditViewControllerMask"]?[self.resultDic[@"JXProfileEditViewControllerMask"] integerValue]:JXProfileEditViewControllerMaskProgramShow;
    switch (self.vcType) {
        case JXProfileEditViewControllerTypeTable:{
            UITableView *tableView = [[UITableView alloc] init];
            tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            tableView.backgroundColor = UIColorFromRGB(DEFINE_COLOR_BACK);
            NSDictionary * allDic =@{@"resultDic":self.resultDic};
            self.tableManager = [[JXProfileEditTableManager alloc] initWithDictionary:allDic
                                                                       viewController:self
                                                                            tableView:tableView
                                                                        finishHanlder:nil];
            self.cellInfoList = [self.tableManager getCellInfoList];
            tableView.delegate = self.tableManager;
            tableView.dataSource = self.tableManager;
            self.tableView = tableView;
            self.contentView = tableView;
            [self setUpContentView];
            [tableView reloadData];
            break;
        }
        case JXProfileEditViewControllerTypeView:{
            NSString *keyName = self.resultDic[@"keyName"]?self.resultDic[@"keyName"]:@"";
            NSString *textValue = self.resultDic[@"textValue"]?self.resultDic[@"textValue"]:@"";
            NSString *hintValue = self.resultDic[@"hintValue"]?self.resultDic[@"hintValue"]:@"";
            CGFloat height = self.resultDic[@"height"]?[self.resultDic[@"height"] floatValue]:100;
            BOOL heightFlexible = self.resultDic[@"heightFlexible"]?[self.resultDic[@"heightFlexible"] boolValue]:NO;
            long maxChar = self.resultDic[@"maxChar"]?[self.resultDic[@"maxChar"] longValue]:0;
            BOOL userInteractionEnabled = self.resultDic[@"userInteractionEnabled"]?[self.resultDic[@"userInteractionEnabled"] boolValue]:YES;
            BOOL editable = self.resultDic[@"editable"]?[self.resultDic[@"editable"] boolValue]:YES;
            
            JXCustomTextView *textView = [JXCustomTextView defaultView];
            self.textView = textView;
            textView.frame = RECT(0, 0, DEVICE_WIDTH, height);
            textView.styleDic =[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                UIColorFromRGB(DEFINE_COLOR_BACK), @"backgroundColor",
                                textValue, @"textValue",
                                hintValue, @"hintValue",
                                keyName, @"keyName",
                                textValue, @"textValue",
                                hintValue, @"hintValue",
                                @(height), @"height",
                                @(heightFlexible),@"heightFlexible",
                                @(maxChar),@"maxChar",
                                @(userInteractionEnabled),@"userInteractionEnabled",
                                @(editable),@"editable",
                                nil];
            textView.changedBlock = ^(NSDictionary *kvDic){
                if(kvDic[@"keyName"]){
                    NSString *textValue = [kvDic[@"textValue"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    [self.resultDic setObject:textValue
                                       forKey:kvDic[@"keyName"]];
                    JXLog(@"\n\n textChangedForKey = %@ \n\n",self.resultDic);
                }
            };
            [textView updateViewInfo:textView.styleDic];
            self.contentView = textView;
            [self setUpContentView];
            break;
        }
        default:
            break;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-(void)setNavigationBarStyleCustom{
//    [super setNavigationBarStyleCustom];
//}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setUpLeftbarButtonWithImage:UIIMGName(@"back_normal")];
    if(self.vcMask != JXProfileEditViewControllerMaskProgramShow){
        [self setUpRightbarButtonWithTitle:@"完成"];
    }
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

#pragma mark -
- (void)leftBarItemTouchUpInside:(id)sender{
    [JXViewControlerRouter popCurrentViewController:self animated:YES];
}
- (void)rightBarItemTouchUpInside:(id)sender{
    if (self.finishBlock) {
        self.finishBlock(self.resultDic);
    }
    [JXViewControlerRouter popCurrentViewController:self animated:YES];
}

-(CGFloat)gapToBottom{
    if (self.vcType == JXProfileEditViewControllerTypeTable) {
        return 0.0f;
    } else {
        CGFloat flexHeight = [JXCustomTextView viewHeight:self.textView.styleDic];
        if (flexHeight > (DEVICE_HEIGHT - STATUS_NAVIGATION_BAR_HEIGHT)) {
            flexHeight = DEVICE_HEIGHT;
        }
        CGFloat bottomGap = DEVICE_HEIGHT - self.gapToTop - flexHeight - STATUS_NAVIGATION_BAR_HEIGHT;
        return bottomGap;
    }
}
#pragma mark -
- (void)keyboardShow:(NSNotification *)notification{
    [super keyboardShow:notification];
}
- (void)keyboardHide:(NSNotification *)notification{
    [super keyboardShow:notification];
}

@end

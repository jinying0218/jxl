//
//  JXLPasswordViewController.m
//  jiangXunLu
//
//  Created by 金莹 on 16/9/4.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXLPasswordViewController.h"
#import "jiangXunLu-swift.h"
#import "ZJSwitch.h"
#import "HexColor.h"
#import "GlobalDefine.h"
#import "AFNTool.h"
#import "YYModel.h"
#import "MBProgressHUD+Add.h"
#import "BlocksKit+UIKit.h"
#import "EXTScope.h"
#import "NSString+security.h"


@interface JXLPasswordViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *oldPWInputView;
@property (weak, nonatomic) IBOutlet UITextField *pwInputView;
@property (weak, nonatomic) IBOutlet UITextField *confirmPWInputView;

@end

@implementation JXLPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"修改密码";

    [self setUpLeftbarButtonWithImage:UIIMGName(@"back_normal")];
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setTitle:@"确定" forState:UIControlStateNormal];
    [rightButton setTitleColor:[UIColor colorWithHexString:@"FECD08"] forState:UIControlStateNormal];
    [rightButton setTitleColor:[UIColor lightTextColor] forState:UIControlStateHighlighted];
    [self setUpRightbarButtonWithCustomButton:rightButton];

    [self configureUI];
}

- (void)configureUI{
    
    UIButton *rightBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn1.frame = CGRectMake( 0, 0, 20, 20);
    rightBtn1.tag = 9000;
    [rightBtn1 setImage:[UIImage imageNamed:@"login_hide"] forState:UIControlStateNormal];
    [rightBtn1 setImage:[UIImage imageNamed:@"login_look"] forState:UIControlStateSelected];
    [rightBtn1 addTarget:self action:@selector(rightViewButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    self.pwInputView.rightView = rightBtn1;
    self.pwInputView.rightViewMode = UITextFieldViewModeWhileEditing;
    
    
    UIButton *rightBtn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn2.frame = CGRectMake( 0, 0, 20, 20);
    rightBtn2.tag = 9001;
    [rightBtn2 setImage:[UIImage imageNamed:@"login_hide"] forState:UIControlStateNormal];
    [rightBtn2 setImage:[UIImage imageNamed:@"login_look"] forState:UIControlStateSelected];
    [rightBtn2 addTarget:self action:@selector(rightViewButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    self.confirmPWInputView.rightView = rightBtn2;
    self.confirmPWInputView.rightViewMode = UITextFieldViewModeWhileEditing;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 action:personal_set_passwd_modify
	uid:用户id
	randcode:登陆状态验证码
	data:{
 passwd_old : (string) 旧密码(md5加密)
 passwd_new : (string) 新密码(md5加密)
	}

 
 */

#pragma mark - action

- (void)leftBarItemTouchUpInside:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)rightBarItemTouchUpInside:(id)sender{
    [self.view endEditing:YES];
    
    if (self.oldPWInputView.text.length < 6 || self.pwInputView.text.length < 6 || self.confirmPWInputView.text.length < 6) {
        [MBProgressHUD showError:@"请输入6-20位数字或字母" toView:self.view];
        return;
    }
    if (![self.pwInputView.text isEqualToString:self.confirmPWInputView.text]) {
        [MBProgressHUD showError:@"请确认两次新密码输入相同!" toView:self.view];
        return;
    }
    
    NSString *g_uid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
    NSString *g_randcode = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"randcode"]];
    NSDictionary *data = @{@"passwd_old" : [NSString md5:self.oldPWInputView.text],
                           @"passwd_new" : [NSString md5:self.pwInputView.text]};
    NSDictionary *params = @{@"action" : @"personal_set_passwd_modify",
                             @"uid" : g_uid,
                             @"randcode" : g_randcode,
                             @"data" : [data yy_modelToJSONString]};
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    @weakify(self);
    [AFNTool post:Globe.baseUrlStr params:params success:^(id responsObject) {
        @strongify(self);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if ([responsObject[@"status"] intValue] == 1) {            
            [MBProgressHUD showError:@"修改成功" toView:[UIApplication sharedApplication].keyWindow];
            [self.navigationController popViewControllerAnimated:YES];
        }else {
            [MBProgressHUD showError:responsObject[@"msg"] toView:self.view];
        }
    } failure:^(NSError *error) {
        @strongify(self);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:error.description toView:self.view];
    }];

}

- (void)rightViewButtonClick:(UIButton *)button{
    if (button.tag == 9000) {
        self.pwInputView.secureTextEntry = button.selected;

    }else {
        self.confirmPWInputView.secureTextEntry = button.selected;
    }
    button.selected = !button.selected;
}


@end

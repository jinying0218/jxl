//
//  JXProfileDetailTableManager.h
//  jiangXunLu
//
//  Created by lixu06 on 16/6/19.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXCustomTableModel.h"

@interface JXProfileDetailTableManager : JXCustomTableModel

- (void)requestDataUpdate;
@end

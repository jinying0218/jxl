//
//  JXListTableManager.m
//  jiangXunLu
//
//  Created by lixu06 on 16/7/1.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXListTableManager.h"
#import "EXTScope.h"
#import "GlobalDefine.h"
#import "Color.h"
#import "String.h"
#import "Dimen.h"
#import "JXTipHud.h"

#import "NSObject+JXRefresh.h"

@interface JXListTableManager ()



@end

@implementation JXListTableManager
- (instancetype)initWithDictionary:(NSDictionary*)dic
                    viewController:(UIViewController *)controller
                         tableView:(UITableView *)tableView
                     finishHanlder:(CommomEditBlock)finishBlock {
    if (self = [super initWithDictionary:dic
                          viewController:controller
                               tableView:tableView
                           finishHanlder:finishBlock]) {
        [self setUpRefreshContent:tableView];
        return self;
    }
    return nil;
}

- (NSMutableArray *)getCellInfoList {
    self.cellInfoList = [super getCellInfoList];
    return self.cellInfoList;
}

- (void)registerTableCell {
    [super registerTableCell];
}

- (void)headerRefresh{
    if (self.headerRefreshBlock) {
        self.headerRefreshBlock(nil);
    }
}

- (void)footerRefresh{
    if (self.footerRefreshBlock) {
        self.footerRefreshBlock(nil);
    }
}

#pragma mark -
#pragma mark UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [self refresh_scrollViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [self refresh_scrollViewDidEndDragging:scrollView willDecelerate:decelerate];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    [self refresh_scrollViewDidEndDecelerating:scrollView];
}


@end

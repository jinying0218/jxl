//
//  JXView.h
//  jiangXunLu
//
//  Created by lixu06 on 16/6/19.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JXView : UIView

@property (nonatomic, strong) NSMutableDictionary *styleDic;
+ (id)defaultView;
+ (CGFloat)viewHeight:(NSDictionary *)kvDic;
- (void)updateViewInfo:(NSDictionary *)cellDic;

@end

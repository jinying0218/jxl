//
//  JXCustomTableModel.h
//  jiangXunLu
//
//  Created by lixu06 on 16/6/17.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "JXPhotoActionSheetView.h"

@interface JXCustomTableModel : NSObject <UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate>

typedef void(^CommomEditBlock)(NSDictionary *kvDic);

@property (nonatomic, copy  ) CommomEditBlock finishBlock; //编辑返回执行闭包
@property (nonatomic, weak) UIViewController *controller; // View Model管理的VC
@property (nonatomic, weak) UITableView *tableView; // View Model管理的列表视图
@property (nonatomic, weak) UIView *tableViewHeader; // View Model管理的列表视图Header
@property (nonatomic, strong) NSMutableDictionary *resultDic; // 填写结果字段
@property (nonatomic,strong) NSMutableDictionary *postDic; // 修改提交字段控制
@property (nonatomic, strong) NSIndexPath *curIndexPath; // 当前选中cell的索引
@property (nonatomic, strong) NSMutableArray *headerInfoList; // 列表cell的管理参数数据表
@property (nonatomic, strong) NSMutableArray *cellInfoList; // 列表cell的管理参数数据表
@property (nonatomic, strong) NSMutableDictionary *headerInfoDic; // TableHeaderView管理字典

#pragma mark -
#pragma mark Override Methods
/*!
 初始化函数
 */
- (instancetype)initWithDictionary:(NSDictionary*)dic
                    viewController:(UIViewController *)controller
                         tableView:(UITableView *)tableView
                     finishHanlder:(CommomEditBlock)finishBlock;
/*!
 z注册复用Cell
 */
- (void)registerTableCell;
- (void)registerNibCell:(NSString *)celName;
- (void)registerClassCell:(NSString *)celName;
/*!
 初始化列表cell控制参数表
 */
- (NSMutableArray *)getCellInfoList;
/*!
 初始化列表header控制参数表
 */
- (NSMutableArray *)getHeaderInfoList;
/*!
 初始化总header控制参数表
 */
-(UIView *)getHeaderView;
-(UIView *)filterHeaderView;
#pragma mark -
#pragma mark Common Methods
/*!
 设置VM基本参数
 */
- (void)setDataWithDictionary:(NSDictionary*)dic
               viewController:(UIViewController *)controller
                    tableView:(UITableView *)tableView
                finishHanlder:(CommomEditBlock)finishBlock;
/*!
 校验字段是否存在
 */
- (BOOL)checkFragment:(NSString *)fagmentName tip:(NSString *)tip;
/*!
 校验字段是否符合最小长度
 */
- (BOOL)checkFragment:(NSString *)fagmentName tip:(NSString *)tip lengthMin:(NSInteger)minlimit;
/*!
 校验字段是否符合最大长度
 */
- (BOOL)checkFragment:(NSString *)fagmentName tip:(NSString *)tip lengthMax:(NSInteger)maxlimit;
/*!
 跳转到指定VC
 */
- (BOOL)popToViewControllerWithClass:(Class)class;
- (BOOL)popToViewControllerWithClassName:(NSString *)classString;

@end

//
//  UITableViewCell+BMPModel.m
//  jiangXunLu
//
//  Created by lixu06 on 16/7/8.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "UITableViewCell+JXModel.h"
#import <objc/runtime.h>

static const void *CellDataKey = &CellDataKey;

@implementation UITableViewCell (JXModel)

@dynamic cellData;

// 动态增加cellData属性
- (void)setCellData:(id)cellData {
    objc_setAssociatedObject(self, CellDataKey, cellData, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (id)cellData {
    return objc_getAssociatedObject(self, CellDataKey);
}

+ (CGFloat)cellHeight:(NSDictionary *)kvDic{
    return  0.0f;
}

- (void)updateCellInfo:(NSDictionary *)cellDic onModel:(NSObject *)model{
}

- (void)didSelectAction:(NSDictionary *)cellDic onModel:(NSObject *)model{
}

@end

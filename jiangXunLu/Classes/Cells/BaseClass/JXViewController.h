//
//  JXViewController.h
//  jiangXunLu
//
//  Created by lixu06 on 16/6/19.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JXViewController : UIViewController

typedef void(^JXViewControllerBlock)(id data);

- (void)setUpLeftbarButtonWithImage:(UIImage *)image;
- (void)setUpLeftbarButtonWithImage:(UIImage *)image frame:(CGRect)frame;
- (void)setUpRightbarButtonWithImage:(UIImage *)image;
- (void)setUpRightbarButtonWithImage:(UIImage *)image frame:(CGRect)frame;
//- (void)setUpLeftbarButtonWithTitle:(NSString *)title;
- (void)setUpRightbarButtonWithTitle:(NSString *)title;
- (void)setUpRightbarButtonWithCustomButton:(UIButton *)cutomButtom;
- (void)setUpRightbarButtonWithImages:(NSArray *)imageArray highLightImages:(NSArray *)lightImages;
- (void)setLeftbarButtonHidden:(BOOL)hidden;
- (void)setRightbarButtonHidden:(BOOL)hidden;
- (void)leftBarItemTouchUpInside:(id)sender;
- (void)rightBarItemTouchUpInside:(id)sender;

- (void)setNavigationBarStyle;
- (void)setNavigationBarStyleTransparent;
- (void)setNavigationBarStyleCustom;

@property (nonatomic, strong) UIView *contentView;
- (CGFloat)gapToBottom;
- (CGFloat)gapToTop;
- (CGFloat)gapToLeft;
- (CGFloat)gapToRight;
- (void)setUpContentView;

- (void)keyboardShow:(NSNotification *)notification;
- (void)keyboardHide:(NSNotification *)notification;

@end

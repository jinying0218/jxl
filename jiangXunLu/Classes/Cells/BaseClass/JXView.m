//
//  JXView.m
//  jiangXunLu
//
//  Created by lixu06 on 16/6/19.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXView.h"

@implementation JXView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

+ (id)defaultView {
    NSString *nibName = NSStringFromClass([self class]);
    NSArray *cells = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    if ([cells count]<1) {
        return nil;
    }
    return [cells firstObject];
}
+ (CGFloat)viewHeight:(NSDictionary *)kvDic{
    return 0;
}
- (void)updateViewInfo:(NSDictionary *)cellDic{
}

@end

//
//  JXViewController.m
//  jiangXunLu
//
//  Created by lixu06 on 16/6/19.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXViewController.h"
#import "NSString+SizeCalculator.h"
#import "GlobalDefine.h"
#import "CommonTools.h"
#import "Color.h"
#import "Dimen.h"
#import "String.h"
#import "EXTScope.h"

@interface JXViewController ()

typedef NS_ENUM(NSInteger, JXViewControllerNavigationStatusBarStyle) {
    /** 导航条和状态栏样式: 透明 */
    JXViewControllerNavigationStatusBarStyleTrans             = 1,
    /** 导航条和状态栏样式: 标准 */
    JXViewControllerNavigationStatusBarStyleCustom            = 2
};

@property (nonatomic, strong) UIButton *leftButton;
@property (nonatomic, strong) UIButton *rightButton;
@property (assign, nonatomic) CGRect contentOrgFrame;

@property (assign, nonatomic) NSInteger naviStatusBarStyle;

@end

@implementation JXViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBarStyle];
}
-(void)viewWillAppear:(BOOL)animated{
    [self setNavigationBarStyle];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardShow:) name:UIKeyboardWillShowNotification object:nil];//在这里注册通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardHide:) name:UIKeyboardWillHideNotification object:nil];
}
-(void)viewWillDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)setUpLeftbarButtonWithImage:(UIImage *)image frame:(CGRect)frame{
    UIButton *btn = [[UIButton alloc] initWithFrame:frame];
    [btn setImage:image forState:UIControlStateNormal];
    //    btn.imageEdgeInsets = UIEdgeInsetsMake(-6, 0, -6, 0);
    [btn addTarget:self action:@selector(leftBarItemTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.leftBarButtonItem = leftButtonItem;
    self.leftButton = btn;
}
- (void)setUpLeftbarButtonWithImage:(UIImage *)image{
    UIButton *btn = [[UIButton alloc] initWithFrame:RECT(6,6,30,30)];
    [btn setImage:image forState:UIControlStateNormal];
//    btn.imageEdgeInsets = UIEdgeInsetsMake(-6, 0, -6, 0);
    [btn addTarget:self action:@selector(leftBarItemTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.leftBarButtonItem = leftButtonItem;
    self.leftButton = btn;
}

- (void)setUpRightbarButtonWithImage:(UIImage *)image frame:(CGRect)frame{
    UIButton *btn = [[UIButton alloc] initWithFrame:frame];
    [btn setImage:image forState:UIControlStateNormal];
    //    btn.imageEdgeInsets = UIEdgeInsetsMake(-6, 0, -6, 0);
    [btn addTarget:self action:@selector(rightBarItemTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = rightButtonItem;
    self.leftButton = btn;
}

- (void)setUpRightbarButtonWithImage:(UIImage *)image{
    UIButton *btn = [[UIButton alloc] initWithFrame:RECT(0,0,44,44)];
    [btn setImage:image forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(rightBarItemTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = rightButtonItem;
    self.rightButton = btn;
}
- (void)setUpRightbarButtonWithImages:(NSArray *)imageArray highLightImages:(NSArray *)lightImages{
    NSMutableArray *rightItems = [[NSMutableArray alloc] initWithCapacity:2];
    int itemTag = 0;
    for (UIImage *image in imageArray) {
        UIButton *btn = [[UIButton alloc] initWithFrame:RECT(0,0,33,33)];
        [btn setImage:image forState:UIControlStateNormal];
        [btn setImage:lightImages[itemTag] forState:UIControlStateHighlighted];
        btn.tag = itemTag + 1000;
        [btn addTarget:self action:@selector(rightBarItemTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
        [rightItems addObject:rightButtonItem];
        itemTag ++;
    }
    self.navigationItem.rightBarButtonItems = rightItems;
}
- (void)setLeftbarButtonHidden:(BOOL)hidden{
    self.leftButton.hidden = hidden;
}
- (void)setRightbarButtonHidden:(BOOL)hidden{
    self.rightButton.hidden = hidden;
}

- (void)setUpRightbarButtonWithTitle:(NSString *)title{
    UIFont *titleFont = FONTS(FONTSIZE_EXTRA);
    CGSize buttonSize = BD_SINGLELINE_TEXTSIZE(title, titleFont, CGSizeMake(MAXFLOAT, MAXFLOAT));
    UIButton *btn = [[UIButton alloc] initWithFrame:RECT(0,0,buttonSize.width + 20,44)];
    [btn setTitle:title forState:UIControlStateNormal];
    btn.titleLabel.font = titleFont;
    [btn setTitleColor:UIColorFromRGB(DEFINE_COLOR_WHITE) forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(rightBarItemTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = rightButtonItem;
    self.rightButton = btn;
}

- (void)setUpRightbarButtonWithCustomButton:(UIButton *)cutomButtom{
    UIFont *titleFont = FONTS(FONTSIZE_EXTRA);

    CGSize buttonSize = cutomButtom.width > 0 ? cutomButtom.bounds.size : BD_SINGLELINE_TEXTSIZE(cutomButtom.titleLabel.text, titleFont, CGSizeMake(MAXFLOAT, MAXFLOAT));
    cutomButtom.frame = RECT(0,0,buttonSize.width + 20,44);
    [cutomButtom addTarget:self action:@selector(rightBarItemTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithCustomView:cutomButtom];
    self.navigationItem.rightBarButtonItem = rightButtonItem;
    self.rightButton = cutomButtom;
}
- (void)leftBarItemTouchUpInside:(id)sender{
}
- (void)rightBarItemTouchUpInside:(id)sender{
}

- (void)rightBarButtonItemClick:(UIBarButtonItem *)item{
    
}
- (void)setNavigationBarStyle{
    [self setNavigationBarStyleCustom];
}

- (void)setNavigationBarStyleTransparent{
    self.naviStatusBarStyle = JXViewControllerNavigationStatusBarStyleTrans;
    self.automaticallyAdjustsScrollViewInsets = NO;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    UINavigationBar *navigationBar = self.navigationController.navigationBar;
    navigationBar.backgroundColor = [UIColor clearColor];
    navigationBar.translucent = YES;
    navigationBar.barTintColor = [UIColor clearColor];
    [navigationBar setBackgroundImage:IMGName(@"transparent_4x4@2x.png")
                       forBarPosition:UIBarPositionAny
                           barMetrics:UIBarMetricsDefault];
    navigationBar.barStyle = UIBarStyleBlackTranslucent;
    [navigationBar setShadowImage:[UIImage new]];
    UIColor *color = UIColorFromRGBA(DEFINE_COLOR_TRANSPARENT);
    NSDictionary * dict=[NSDictionary dictionaryWithObject:color forKey:NSForegroundColorAttributeName];
    self.navigationController.navigationBar.titleTextAttributes = dict;
    
}

- (void)setNavigationBarStyleCustom{
    self.naviStatusBarStyle = JXViewControllerNavigationStatusBarStyleCustom;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    UINavigationBar *navigationBar = self.navigationController.navigationBar;
    navigationBar.backgroundColor = UIColorFromRGB(DEFINE_COLOR_BROWN);
    navigationBar.translucent = NO;
    navigationBar.barTintColor = UIColorFromRGB(DEFINE_COLOR_BROWN);
    [navigationBar setBackgroundImage:nil
                       forBarPosition:UIBarPositionAny
                           barMetrics:UIBarMetricsDefault];
    navigationBar.barStyle = UIBarStyleBlackTranslucent;
    [navigationBar setShadowImage:[UIImage new]];
    UIColor *color = UIColorFromRGB(DEFINE_COLOR_WHITE);
    NSDictionary * dict=[NSDictionary dictionaryWithObject:color forKey:NSForegroundColorAttributeName];
    self.navigationController.navigationBar.titleTextAttributes = dict;
}

- (void)setUpContentView {
    if(!self.contentView){
        return;
    }
    
    CGFloat width = DEVICE_WIDTH - (self.gapToLeft + self.gapToRight); //self.view.bounds.size.width
    CGFloat height = DEVICE_HEIGHT - (self.gapToTop + self.gapToBottom); //self.view.bounds.size.height
    
    if(self.tabBarController.tabBar){
        if (!(self.hidesBottomBarWhenPushed == YES)) {
            height -= BOTTOM_BAR_HEIGHT_MAIN;
        }
    }
    
    if (self.naviStatusBarStyle == JXViewControllerNavigationStatusBarStyleCustom) {
        if (![[UIApplication sharedApplication] isStatusBarHidden]) {
            height -= STATUS_BAR_HEIGHT;
        }
        if (!self.navigationController.isNavigationBarHidden) {
            height -= NAVIGATION_BAR_HEIGHT;
        }
    }
    self.contentView.frame = RECT(self.gapToLeft,
                                  self.gapToTop,
                                  width,
                                  height);
    self.contentOrgFrame = RECT(self.gapToLeft,
                                self.gapToTop,
                                width,
                                height);
    [self.view addSubview:self.contentView];

}

- (CGFloat)gapToBottom{
    return 0;
}
- (CGFloat)gapToTop{
    return 0;
}

- (CGFloat)gapToLeft{
    return 0;
}

- (CGFloat)gapToRight{
    return 0;
}

- (void)keyboardShow:(NSNotification *)notification {
    NSDictionary *userInfo = notification.userInfo;
    CGRect kbRect = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat keyboardHeight = CGRectGetHeight(kbRect);
    
    if (self.gapToBottom > keyboardHeight) {
        return;
    }
    
    CGFloat height = DEVICE_HEIGHT - (keyboardHeight + self.contentOrgFrame.origin.y) - STATUS_NAVIGATION_BAR_HEIGHT;
    CGRect newFrame = RECT(self.contentOrgFrame.origin.x,
                           self.contentOrgFrame.origin.y,
                           self.contentOrgFrame.size.width,
                           height);
    NSNumber *duration = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    @weakify(self);
    [UIView animateWithDuration:duration.doubleValue animations:^{
        @strongify(self);
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationCurve:[curve integerValue]];
        self.contentView.frame = newFrame;
        [self.view layoutIfNeeded];
    }];
    
    
}
- (void)keyboardHide:(NSNotification *)notification{
    self.contentView.frame = self.contentOrgFrame;
}

@end

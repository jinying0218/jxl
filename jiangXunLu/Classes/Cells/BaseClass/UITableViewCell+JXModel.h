//
//  UITableViewCell+BMPModel.h
//  jiangXunLu
//
//  Created by lixu06 on 16/7/8.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol UITableViewCellJXModelProtocol <NSObject>

@required

+ (CGFloat)cellHeight:(NSDictionary *)kvDic;
- (void)updateCellInfo:(NSDictionary *)cellDic onModel:(NSObject *)model;
- (void)didSelectAction:(NSDictionary *)cellDic onModel:(NSObject *)model;

@end

@interface UITableViewCell (JXModel)<UITableViewCellJXModelProtocol>

@property (nonatomic, strong) id cellData;

@end

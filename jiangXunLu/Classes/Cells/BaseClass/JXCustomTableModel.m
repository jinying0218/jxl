//
//  JXCustomTableModel.m
//  jiangXunLu
//
//  Created by lixu06 on 16/6/17.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXCustomTableModel.h"
#import "JXCustomSectionTitleTableViewCell.h"
#import "JXCustomSectionVariableTableViewCell.h"
#import "JXCustomInputTableViewCell.h"
#import "JXCustomTextTableViewCell.h"
#import "JXCustomLabelTableViewCell.h"
#import "JXCustomSubmitTableViewCell.h"
#import "JXCustomInputCheckTableViewCell.h"

#import "JXTipHud.h"
#import "EXTScope.h"
#import "JXViewControlerRouter.h"
#import "GlobalDefine.h"
#import "CommonTools.h"
#import "UIView+Initiation.h"
#import "JXProfileHeaderView.h"
#import "JXPhotoActionSheet.h"

@interface JXCustomTableModel ()


@end

@implementation JXCustomTableModel

- (instancetype)initWithDictionary:(NSDictionary*)dic
                    viewController:(UIViewController *)controller
                         tableView:(UITableView *)tableView
                     finishHanlder:(CommomEditBlock)finishBlock {
    if (self = [super init]) {
        [self setDataWithDictionary:dic viewController:controller tableView:tableView finishHanlder:finishBlock];
        [self registerTableCell];
        self.curIndexPath = nil;
        self.postDic = [@{} mutableCopy];
        return self;
    }
    return nil;
}
- (void)setDataWithDictionary:(NSDictionary*)dic
               viewController:(UIViewController *)controller
                    tableView:(UITableView *)tableView
                finishHanlder:(CommomEditBlock)finishBlock{
    if (self) {
        if(dic){
            self.resultDic = dic[@"resultDic"];
        }
        if (controller) {
            self.controller = controller;
        }
        if (tableView) {
            self.tableView = tableView;
            self.tableView.showsVerticalScrollIndicator = NO;
        }
        if (finishBlock) {
            self.finishBlock = finishBlock;
        }
    }
}
-(UIView *)getHeaderView{
    if(!self.headerInfoDic){
        return nil;
    }
    return [self filterHeaderView];
}
-(UIView *)filterHeaderView{
    if(!self.headerInfoDic){
        return nil;
    }
    return nil;
}
- (NSMutableArray *)getCellInfoList {
    self.cellInfoList = [NSMutableArray arrayWithCapacity:0];
    return self.cellInfoList;
}

- (NSMutableArray *)getHeaderInfoList {
    self.headerInfoList = [NSMutableArray arrayWithCapacity:0];
    return self.headerInfoList;
}

- (void)registerTableCell {
    [self registerClassCell:NSStringFromClass([JXCustomSectionTitleTableViewCell class])];
    [self registerClassCell:NSStringFromClass([JXCustomSectionVariableTableViewCell class])];
    [self registerClassCell:NSStringFromClass([JXCustomInputTableViewCell class])];
    [self registerClassCell:NSStringFromClass([JXCustomLabelTableViewCell class])];
    [self registerNibCell:NSStringFromClass([JXCustomSubmitTableViewCell class])];
    [self registerNibCell:NSStringFromClass([JXCustomTextTableViewCell class])];
    [self registerClassCell:NSStringFromClass([JXCustomInputCheckTableViewCell class])];

    
}

- (void)registerNibCell:(NSString *)celName {
    UINib* cellNib = [UINib nibWithNibName:celName bundle:nil];
    [self.tableView registerNib:cellNib
         forCellReuseIdentifier:celName];
}

- (void)registerClassCell:(NSString *)celName {
    [self.tableView registerClass:NSClassFromString(celName)
           forCellReuseIdentifier:celName];
}

#pragma mark -
#pragma mark UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.cellInfoList.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    NSMutableDictionary *cellDic = self.headerInfoList[section];
    if([cellDic[@"ViewIdentifier"] isEqualToString:@"null"]){
        return 0;
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSMutableDictionary *cellDic = self.headerInfoList[section];
    NSString *viewIdentifier = cellDic[@"ViewIdentifier"];
    if([viewIdentifier isEqualToString:@"null"]){
        return nil;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    switch (section) {
        default:
            return 0;
    }
    
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    switch (section) {
        default:
            return nil;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return ((NSMutableArray *)self.cellInfoList[section]).count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    id cellData = self.cellInfoList[indexPath.section][indexPath.row];
    NSMutableDictionary *cellDic;
    if ([cellData isKindOfClass:[NSMutableDictionary class]]) {
        cellDic = cellData;
    } else{
        if ([cellData isKindOfClass:[NSDictionary class]]) {
            cellDic = [cellData mutableCopy];
        }else {
            cellDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                       @"JXCustomSectionTitleTableViewCell", @"CellIdentifier",
                       @"数据类型不支持", @"lableValue",
                       [UIColor whiteColor], @"backgroundColor",
                       @(1.0f), @"borderWidth",
                       @(7.0f), @"padding",
                       FONTS(12.0f),@"font",
                       @(8),@"paddingTop",
                       @(20),@"paddingRight",
                       @(8),@"paddingBottom",
                       @(20),@"paddingLeft",
                       nil];
        }
    }
    self.cellInfoList[indexPath.section][indexPath.row] = cellDic;
    NSString *cellIdentifier = cellDic[@"CellIdentifier"];
    [cellDic setObject:indexPath forKey:@"indexPath"];
    
    
    if (!cellIdentifier) {
        return nil;
    }
    
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier
                                                                  forIndexPath:indexPath];
    
    if([cell respondsToSelector:@selector(setCellData:)]){
        [cell performSelector:@selector(setCellData:) withObject:cellDic];
    }
    
    if([cell respondsToSelector:@selector(updateCellInfo:onModel:)]){
        [cell performSelector:@selector(updateCellInfo:onModel:) withObject:cellDic withObject:self];
    }
    
    return cell;

}
#pragma mark -
#pragma mark UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSMutableDictionary *cellDic = self.cellInfoList[indexPath.section][indexPath.row];
    NSString *cellIdentifier = cellDic[@"CellIdentifier"];
    CGFloat height = 0;
    if (!cellIdentifier) {
        return height;
    }
    Class class = NSClassFromString(cellIdentifier);
    if([class respondsToSelector:@selector(cellHeight:)]) {
        height = [class cellHeight:cellDic];
        
    }
    return height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.curIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    if([cell respondsToSelector:@selector(didSelectAction:onModel:)]){
        [cell performSelector:@selector(didSelectAction:onModel:) withObject:cell.cellData withObject:self];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark -
#pragma mark Common Methods

-(BOOL)checkFragments{
    return YES;
}

- (BOOL)checkFragment:(NSString *)fagmentName tip:(NSString *)tip{
    if (!self.resultDic[fagmentName] || 0 == ((NSString *)self.resultDic[fagmentName]).length) {
        [JXTipHud showTip:tip dismissAfter:0.8 withFinishHandler:^(){
            [self.tableView reloadData];
        }];
        return NO;
    }
    return YES;
}
- (BOOL)checkFragment:(NSString *)fagmentName tip:(NSString *)tip lengthMin:(NSInteger)minlimit{
    if(minlimit > ((NSString *)self.resultDic[fagmentName]).length){
        [JXTipHud showTip:tip dismissAfter:0.8 withFinishHandler:^(){
            [self.tableView reloadData];
        }];
        return NO;
    }
    return YES;
}

- (BOOL)checkFragment:(NSString *)fagmentName tip:(NSString *)tip lengthMax:(NSInteger)maxlimit{
    if(maxlimit < ((NSString *)self.resultDic[fagmentName]).length){
        [JXTipHud showTip:tip dismissAfter:0.8 withFinishHandler:^(){
            [self.tableView reloadData];
        }];
        return NO;
    }
    return YES;
}

#pragma mark -
#pragma mark Private Methods

- (BOOL)popToViewControllerWithClass:(Class)class{
    for (UIViewController *vc in self.controller.navigationController.viewControllers) {
        if ([vc isKindOfClass:class]) {
            [self.controller.navigationController popToViewController:vc animated:YES];
            return YES;
        }
    }
    return NO;
}

- (BOOL)popToViewControllerWithClassName:(NSString *)classString{
    return [self popToViewControllerWithClass: NSClassFromString(classString)];
}

- (BOOL)popCurrentViewController{
    return [JXViewControlerRouter popCurrentViewController:self.controller animated:YES];
}

#pragma mark -
#pragma mark - Action Blocks
- (void)voidFunction:(NSDictionary *)kvDic {
}
- (void)textChangedForKey:(NSDictionary *)kvDic{
    if(kvDic[@"keyName"]){
        NSString *textValue = [kvDic[@"textValue"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        [self.postDic setObject:textValue
                           forKey:kvDic[@"keyName"]];
        JXLog(@"\n\n textChangedForKey = %@ \n\n",self.resultDic);
    }
}

#pragma mark -
#pragma mark UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
}

@end

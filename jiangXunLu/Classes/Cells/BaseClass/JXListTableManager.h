//
//  JXListTableManager.h
//  jiangXunLu
//
//  Created by lixu06 on 16/7/1.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXCustomTableModel.h"
#import "NSObject+JXRefresh.h"

@interface JXListTableManager : JXCustomTableModel

typedef void(^JXListTableManagerBlock)(id data);

@property (nonatomic, copy) JXListTableManagerBlock headerRefreshBlock;

@property (nonatomic, copy) JXListTableManagerBlock footerRefreshBlock;

//@property (nonatomic, strong) UIView *contentView;
//@property (nonatomic, strong) UIView *headerRefreshView;
//@property (nonatomic, strong) UIView *footerRefreshView;
//@property (nonatomic, strong) UIView *footerNoneView;
//@property (nonatomic, strong) NSValue *contentViewFrame;
//@property (nonatomic, strong) NSValue *headerRefreshFrame;
//@property (nonatomic, strong) NSValue *footerRefreshFrame;
//@property (nonatomic, strong) NSValue *footerNoneFrame;
//@property (nonatomic, strong) NSNumber *isHeaderRefreshing;
//@property (nonatomic, strong) NSNumber *isFooterRefreshing;
//@property (nonatomic, strong) NSNumber *isFooterNone;
//@property (nonatomic, strong) NSNumber *animationTime;
//
//-(void)setUpRefreshHeader:(UIView *)headerRefreshView onSuperView:(UIView *)superView;
//-(void)setUpRefreshFooter:(UIView *)footerRefreshView onSuperView:(UIView *)superView;
//-(void)setUpNoneFooter:(UIView *)footerNoneView onSuperView:(UIView *)superView;
//
//
//- (BOOL)headerRefreshable;
//- (void)headerRefreshAction;
//- (void)headerRefreshBegin;
//- (void)headerRefreshEnd;

//- (BOOL)footerRefreshable;
//- (void)footerRefreshAction;
//- (void)footerRefreshBegin;
//- (void)footerRefreshEnd;

@end

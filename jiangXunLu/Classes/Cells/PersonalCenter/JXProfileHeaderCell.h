//
//  JXProfileHeaderCell.h
//  jiangXunLu
//
//  Created by lixu06 on 16/6/17.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITableViewCell+JXModel.h"

@interface JXProfileHeaderCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;
@property (weak, nonatomic) IBOutlet UIImageView *avitarImg;
@property (weak, nonatomic) IBOutlet UIView *infoView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *sexImg;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UIImageView *phoneImg;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UIImageView *locationImg;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UIImageView *countImg;
- (IBAction)shareTouchUpInside:(id)sender;
- (IBAction)backTouchInside:(id)sender;

typedef void(^HeaderClickBlock)(NSDictionary *kvDic);

@property (nonatomic, copy) HeaderClickBlock backBlock;
@property (nonatomic, copy) HeaderClickBlock shareBlock;
@property (nonatomic, copy) HeaderClickBlock editBlock;


@end

//
//  JXProfileCollectCell.m
//  jiangXunLu
//
//  Created by lixu06 on 16/6/25.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXProfileCollectCell.h"
#import "GlobalDefine.h"
#import "Color.h"
#import "Dimen.h"
#import "CommonTools.h"
#import "NSObject+BMRuntime.h"
#import "UIImageView+WebCache.h"

#define PROFILE_COLLECT_IMAGE_WIDTH          30.0f
#define PROFILE_COLLECT_IMAGE_GAP            5.0f

@implementation JXProfileCollectCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleGray;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

+ (CGFloat)cellHeight:(NSDictionary *)kvDic{
    return 91;
}
- (void)updateCellInfo:(NSDictionary *)cellDic onModel:(NSObject *)model{
    if(cellDic){
        if (cellDic[@"isFold"]) {
            self.nextBtn.hidden = [cellDic[@"isFold"] boolValue];
        }else{
            self.nextBtn.hidden = YES;
        }
        if (cellDic[@"lableValue"]) {
            self.lableText.text = [NSString stringWithFormat:@"%@",cellDic[@"lableValue"]];
        }
        if (cellDic[@"collectText"]) {
            self.collectlabel.text = [NSString stringWithFormat:@"%@",cellDic[@"collectText"]];
        }
        if (cellDic[@"lableFont"]) {
            self.lableText.font = cellDic[@"lableFont"];
        }
        if (cellDic[@"lableColor"]) {
            self.lableText.textColor = cellDic[@"lableColor"];
        }
        if (cellDic[@"collectFont"]) {
            self.collectlabel.font = cellDic[@"collectFont"];
        }
        if (cellDic[@"collectColor"]) {
            self.collectlabel.textColor = cellDic[@"collectColor"];
        }
        NSArray *picArray = (cellDic[@"collectArray"])?cellDic[@"collectArray"]:@[];
        
        CGFloat cellHeight = PROFILE_COLLECT_IMAGE_WIDTH;
        CGFloat cellMargin = PROFILE_COLLECT_IMAGE_GAP;
        for (int i=0; i<picArray.count; i++) {
            UIImageView *imageView = [[UIImageView alloc] init];
            imageView.frame = RECT(i*(cellHeight + cellMargin), 0, cellHeight, cellHeight);
            [self updateImageView:imageView info:picArray[i]];
            [self.collectView addSubview:imageView];
        }
        
        if (cellDic[@"clickBlock"]) {
            self.clickBlock = ^(NSDictionary *kvDic) {
                SEL selector = NSSelectorFromString(cellDic[@"clickBlock"]);
                [model performSelectorSEL:selector withObject:kvDic];
            };
        }
        
    }
}

- (void)didSelectAction:(NSDictionary *)cellDic onModel:(NSObject *)model {
    if (self.clickBlock) {
        self.clickBlock(self.cellData);
    }
}

- (void)updateImageView:(UIImageView *)imageView info:(NSDictionary *)cellDic{
    UIImage *defaultIcon = UIIMGName(@"logo_default");
    if (cellDic[@"head"] && [CommonTools isValidProperty:cellDic[@"head"]]) {
        NSURL *picUrl =  IMGURL(cellDic[@"head"]);
        [imageView sd_setImageWithURL:picUrl placeholderImage:defaultIcon];
    } else {
        [imageView setImage:defaultIcon];
    }
}

- (IBAction)nextTouchUpInside:(id)sender {
}

@end

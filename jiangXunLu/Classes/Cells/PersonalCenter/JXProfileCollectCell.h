//
//  JXProfileCollectCell.h
//  jiangXunLu
//
//  Created by lixu06 on 16/6/25.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITableViewCell+JXModel.h"

@interface JXProfileCollectCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lableText;
@property (weak, nonatomic) IBOutlet UIView *collectView;
@property (weak, nonatomic) IBOutlet UILabel *collectlabel;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;
@property (weak, nonatomic) IBOutlet UIImageView *seperatorView;
- (IBAction)nextTouchUpInside:(id)sender;

typedef void(^ProfileCollectCellBlock)(NSDictionary *kvDic);

@property (nonatomic, copy) ProfileCollectCellBlock clickBlock;

@end

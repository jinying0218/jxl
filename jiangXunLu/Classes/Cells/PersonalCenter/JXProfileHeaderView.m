//
//  JXProfileHeaderView.m
//  jiangXunLu
//
//  Created by lixu06 on 16/6/24.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXProfileHeaderView.h"
#import "GlobalDefine.h"
#import "Color.h"
#import "CommonTools.h"

@implementation JXProfileHeaderView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor = [UIColor clearColor];
    self.tintColor = [UIColor clearColor];
    
    self.infoView.backgroundColor = UIColorFromRGBA(DEFINE_COLOR_TRANSBLACK);
    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(editTapAction:)];
    [self.infoView addGestureRecognizer:singleTap1];
    
    self.avitarImg.layer.cornerRadius = 30;
    self.avitarImg.layer.borderColor = UIColorFromRGB(DEFINE_COLOR_AVITAR).CGColor;
    self.avitarImg.layer.borderWidth = 2;
    self.avitarImg.layer.masksToBounds = YES;
}

+ (CGFloat)viewHeight:(NSDictionary *)kvDic{
    return 250.0f;
}

- (void)updateViewInfo:(NSDictionary *)cellDic{
    if(cellDic){
        if (cellDic[@"editable"] && [cellDic[@"editable"] boolValue]) {
            self.backBtn.hidden = YES;
            self.phoneImg.hidden = NO;
        }else{
            self.backBtn.hidden = NO;
            self.phoneImg.hidden = YES;
        }
        if (cellDic[@"name"]) {
            self.nameLabel.text = [NSString stringWithFormat:@"%@",cellDic[@"name"]];
        }
        if (cellDic[@"position"]) {
            self.titleLabel.text = [NSString stringWithFormat:@"%@",cellDic[@"position"]];
        }
        if (cellDic[@"phone"]) {
            self.phoneLabel.text = [NSString stringWithFormat:@"%@",cellDic[@"phone"]];
        }
        if (cellDic[@"location"]) {
            self.locationLabel.text = [NSString stringWithFormat:@"%@",cellDic[@"location"]];
        }
        if (cellDic[@"hotCount"]) {
            self.countLabel.text = [NSString stringWithFormat:@"%@",cellDic[@"hotCount"]];
        }
        if ([cellDic[@"gender"] integerValue] == 0) {
            self.sexImg.image = IMGName(@"sex_women.png");
        } else {
            self.sexImg.image = IMGName(@"sex_men.png");
        }
        if (cellDic[@"avitar"]) {
            
        } else {
            if ([cellDic[@"gender"] integerValue] == 0) {
                [self.avitarImg setImage: UIIMGName(@"logo_default_woman.png") forState:UIControlStateNormal];
            } else {
                [self.avitarImg setImage: UIIMGName(@"logo_default_man.png") forState:UIControlStateNormal];
            }
        }
    }
}

- (IBAction)shareTouchUpInside:(id)sender {
    if (self.shareBlock) {
        self.shareBlock(self.styleDic);
    }
}

- (IBAction)backTouchInside:(id)sender {
    if (self.backBlock) {
        self.backBlock(self.styleDic);
    }
}
- (void)editTapAction:(id)sender {
    if (self.editBlock) {
        self.editBlock(self.styleDic);
    }
}

- (IBAction)avitarTouchInside:(id)sender{
    if (self.avitarBlock) {
        self.avitarBlock(self.styleDic);
    }
}

@end

//
//  JXProfileHeaderView.h
//  jiangXunLu
//
//  Created by lixu06 on 16/6/24.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JXView.h"

@interface JXProfileHeaderView : JXView

@property (nonatomic, strong) NSMutableDictionary *styleDic;

+ (CGFloat)viewHeight:(NSDictionary *)kvDic;
- (void)updateViewInfo:(NSDictionary *)cellDic;

@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;
@property (weak, nonatomic) IBOutlet UIButton *avitarImg;
@property (weak, nonatomic) IBOutlet UIView *infoView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *sexImg;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UIImageView *phoneImg;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UIImageView *locationImg;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UIImageView *countImg;
- (IBAction)shareTouchUpInside:(id)sender;
- (IBAction)backTouchInside:(id)sender;
- (IBAction)avitarTouchInside:(id)sender;

typedef void(^HeaderClickBlock)(NSDictionary *kvDic);

@property (nonatomic, copy) HeaderClickBlock backBlock;
@property (nonatomic, copy) HeaderClickBlock shareBlock;
@property (nonatomic, copy) HeaderClickBlock editBlock;
@property (nonatomic, copy) HeaderClickBlock avitarBlock;

@end

//
//  JXProfileHeaderCell.m
//  jiangXunLu
//
//  Created by lixu06 on 16/6/17.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXProfileHeaderCell.h"
#import "GlobalDefine.h"
#import "Color.h"
#import "CommonTools.h"
#import "NSObject+BMRuntime.h"

@implementation JXProfileHeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.infoView.backgroundColor = UIColorFromRGBA(DEFINE_COLOR_TRANSBLACK);
    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(editTapAction:)];
    [self.infoView addGestureRecognizer:singleTap1];
    
    self.avitarImg.layer.cornerRadius = 30;
    self.avitarImg.layer.borderColor = UIColorFromRGB(DEFINE_COLOR_DIVIDE_LING).CGColor;
    self.avitarImg.layer.borderWidth = 2;
    self.avitarImg.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

+ (CGFloat)cellHeight:(NSDictionary *)kvDic{
    return 250.0f;
}

- (void)updateCellInfo:(NSDictionary *)cellDic onModel:(NSObject *)model{
    if(cellDic){
        if (cellDic[@"editable"] && [cellDic[@"editable"] boolValue]) {
            self.backBtn.hidden = YES;
            self.phoneImg.hidden = NO;
        }else{
            self.backBtn.hidden = NO;
            self.phoneImg.hidden = YES;
        }
        if (cellDic[@"name"]) {
            self.nameLabel.text = [NSString stringWithFormat:@"%@",cellDic[@"name"]];
        }
        if (cellDic[@"position"]) {
            self.titleLabel.text = [NSString stringWithFormat:@"%@",cellDic[@"position"]];
        }
        if (cellDic[@"phone"]) {
            self.phoneLabel.text = [NSString stringWithFormat:@"%@",cellDic[@"phone"]];
        }
        if (cellDic[@"location"]) {
            self.locationLabel.text = [NSString stringWithFormat:@"%@",cellDic[@"location"]];
        }
        if (cellDic[@"hotCount"]) {
            self.countLabel.text = [NSString stringWithFormat:@"%@",cellDic[@"hotCount"]];
        }
        if ([cellDic[@"gender"] integerValue] == 0) {
            self.sexImg.image = UIIMGName(@"sex_woman");
        } else {
            self.sexImg.image = UIIMGName(@"sex_man");
        }
        if (cellDic[@"avitar"]) {
            
        } else {
            if ([cellDic[@"gender"] integerValue] == 0) {
                self.avitarImg.image = UIIMGName(@"logo_default_woman.png");
            } else {
                self.sexImg.image = UIIMGName(@"logo_default_man.png");
            }
        }
        
        if (cellDic[@"backBlock"]) {
            self.backBlock = ^(NSDictionary *kvDic) {
                NSString *name = cellDic[@"backBlock"];
                [model performSelectorSEL:NSSelectorFromString(name) withObject:kvDic];
            };
        }
        if (cellDic[@"shareBlock"]) {
            self.shareBlock = ^(NSDictionary *kvDic) {
                NSString *name = cellDic[@"shareBlock"];
                [model performSelectorSEL:NSSelectorFromString(name) withObject:kvDic];
            };
        }
        if (cellDic[@"editBlock"]) {
            self.editBlock = ^(NSDictionary *kvDic) {
                NSString *name = cellDic[@"editBlock"];
                [model performSelectorSEL:NSSelectorFromString(name) withObject:kvDic];
            };
        }
        
    }
}

- (IBAction)shareTouchUpInside:(id)sender {
    if (self.shareBlock) {
        self.shareBlock(self.cellData);
    }
}

- (IBAction)backTouchInside:(id)sender {
    if (self.backBlock) {
        self.backBlock(self.cellData);
    }
}
- (void)editTapAction:(id)sender {
    if (self.editBlock) {
        self.editBlock(self.cellData);
    }
}
@end

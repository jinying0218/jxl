//
//  JXCustomLableTableViewCell.m
//  jiangXunLu
//
//  Created by lixu06 on 16/6/19.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXCustomLabelTableViewCell.h"
#import "NSString+SizeCalculator.h"
#import "UITextFiled+LimitLength.h"
#import "GlobalDefine.h"
#import "CommonTools.h"
#import "Color.h"
#import "Dimen.h"
#import "JXTipHud.h"
#import "NSObject+BMRuntime.h"

@interface JXCustomLabelTableViewCell ()

@property (strong, nonatomic) UIView *paddingView;
@property (strong, nonatomic) UILabel *leftLableView;
@property (strong, nonatomic) UILabel *midLableField;
@property (strong, nonatomic) UIImageView *goImageView;
@property (strong, nonatomic) UIImageView *separatorView;

@end

@implementation JXCustomLabelTableViewCell

#pragma mark -
#pragma mark static methods
+ (CGFloat)cellHeight:(NSDictionary *)dic{
    CGFloat singleHeight =  (dic[@"height"])?[dic[@"height"] floatValue]:53.5;
    CGFloat height = singleHeight;
    CGFloat numberOfLines = (dic[@"numberOfLines"])?[dic[@"numberOfLines"] floatValue]:1;
    CGFloat labelWidth = 80;
    CGFloat paddingLeft = (dic[@"paddingLeft"])?[dic[@"paddingLeft"] floatValue]:0;
    CGFloat paddingRight = (dic[@"paddingRight"])?[dic[@"paddingRight"] floatValue]:0;
    CGFloat paddingTextLeft = (dic[@"paddingTextLeft"])?[dic[@"paddingTextLeft"] floatValue]:20;
    CGFloat paddingTextRight = (dic[@"paddingTextRight"])?[dic[@"paddingTextRight"] floatValue]:20;
    CGFloat lableWidth = DEVICE_WIDTH - (paddingLeft + paddingTextLeft + labelWidth + paddingTextRight + paddingRight + 16);
    if (numberOfLines == 1) {
        height = singleHeight;
        
    } else{
        if(dic[@"isFixed"] && [dic[@"isFixed"] boolValue]){
            height = singleHeight + 15.51*(numberOfLines - 1);
        } else {
            UIFont *font = dic[@"font"]?dic[@"font"]:FONTS(FONTSIZE_MIDDLE);
            CGSize midlableSize = BD_MULTILINE_TEXTSIZE(dic[@"textValue"]?dic[@"textValue"]:@"",
                                                        font,
                                                        CGSizeMake(lableWidth, 2000.0f),
                                                        NSLineBreakByWordWrapping);
            
            height = singleHeight - 15.51 +midlableSize.height;
        }
    }
    return height;
}

#pragma mark -
#pragma mark super methods


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        CGFloat singleHeight =  53.5;
        CGFloat height = singleHeight;
        CGFloat paddingLeft = 0;
        CGFloat paddingRight = 0;
        CGFloat paddingTextLeft = 20;
        CGFloat paddingTextRight = 20;
        CGFloat paddingLine = 0;
        CGFloat labelWidth = 80;
        
        self.frame = RECT(0, 0, DEVICE_WIDTH, height);
        self.backgroundColor = UIColorFromRGB(DEFINE_COLOR_WHITE);
        self.selectionStyle = UITableViewCellSelectionStyleDefault;
        
        _paddingView = [[UIView alloc] initWithFrame:RECT(paddingLeft, 0, DEVICE_WIDTH - (paddingLeft + paddingRight), height)];
        _paddingView.backgroundColor = UIColorFromRGB(DEFINE_COLOR_WHITE);
        
        _leftLableView = [[UILabel alloc] initWithFrame:RECT(paddingTextLeft,
                                                             (height - 20)/2,
                                                             labelWidth,
                                                             20)];
        _leftLableView.textColor = UIColorFromRGB(DEFINE_COLOR_GRAY_DARK);
        _leftLableView.font = FONTS(FONTSIZE_MIDDLE);
        
        _midLableField = [[UILabel alloc] initWithFrame:RECT(paddingLeft + paddingTextLeft + labelWidth,
                                                                  0,
                                                                  DEVICE_WIDTH - (paddingLeft + paddingTextLeft + labelWidth + paddingTextRight + paddingRight + 16),
                                                                  height)];
        _midLableField.textColor = UIColorFromRGB(DEFINE_COLOR_BLACK);
        _midLableField.font = FONTS(FONTSIZE_MIDDLE);
        _midLableField.numberOfLines = 1;
        _goImageView = [[UIImageView alloc] initWithFrame:RECT(DEVICE_WIDTH- (16 + paddingTextRight + paddingRight),
                                                               (height - 16)/2,
                                                               16,
                                                               16)];
        _goImageView.image = UIIMGName(@"arrow_r");
        
        _separatorView = [[UIImageView alloc] initWithFrame:RECT(paddingLeft + paddingLine,
                                                                 height - 0.5,
                                                                 DEVICE_WIDTH - (paddingLine + paddingLeft + paddingRight),
                                                                 0.5)];
        _separatorView.image = IMGName(@"dividing_line.png");
        
        [_paddingView addSubview:_leftLableView];
        [_paddingView addSubview:_midLableField];
        [_paddingView addSubview:_goImageView];
        [_paddingView addSubview:_separatorView];
        [self.contentView addSubview:_paddingView];
                
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

#pragma mark -
#pragma mark extended methods

-(void)didSelectAction:(NSDictionary *)cellDic onModel:(NSObject *)model{
    if (cellDic) {
        if (cellDic[@"clickBlock"]) {
            SEL selector = NSSelectorFromString(cellDic[@"clickBlock"]);
            [model performSelectorSEL:selector withObject:cellDic];
        }
    }
    
}
- (void)updateCellInfo:(id)cellDic onModel:(NSObject *)model{
    if (cellDic) {
        CGFloat singleHeight =  (cellDic[@"height"])?[cellDic[@"height"] floatValue]:53.5;
        CGFloat height = singleHeight;
        CGFloat numberOfLines = (cellDic[@"numberOfLines"])?[cellDic[@"numberOfLines"] floatValue]:1;
        CGFloat labelWidth = 80;
        CGFloat paddingLeft = (cellDic[@"paddingLeft"])?[cellDic[@"paddingLeft"] floatValue]:0;
        CGFloat paddingRight = (cellDic[@"paddingRight"])?[cellDic[@"paddingRight"] floatValue]:0;
        CGFloat paddingTextLeft = (cellDic[@"paddingTextLeft"])?[cellDic[@"paddingTextLeft"] floatValue]:20;
        CGFloat paddingTextRight = (cellDic[@"paddingTextRight"])?[cellDic[@"paddingTextRight"] floatValue]:20;
        CGFloat paddingLine = (cellDic[@"paddingLine"])?[cellDic[@"paddingLine"] floatValue]:0;
        CGFloat lableWidth = DEVICE_WIDTH - (paddingLeft + paddingTextLeft + labelWidth + paddingTextRight + paddingRight + 16);
        if (numberOfLines == 1) {
            height = singleHeight;
        } else{
            if(cellDic[@"isFixed"] && [cellDic[@"isFixed"] boolValue]){
                height = singleHeight + 15.51*(numberOfLines - 1);
            } else {
                UIFont *font = cellDic[@"font"]?cellDic[@"font"]:FONTS(FONTSIZE_MIDDLE);
                CGSize midlableSize = BD_MULTILINE_TEXTSIZE(cellDic[@"textValue"]?cellDic[@"textValue"]:@"",
                                                         font,
                                                         CGSizeMake(lableWidth, 2000.0f),
                                                         NSLineBreakByWordWrapping);
                
                height = singleHeight - 15.51 +midlableSize.height;
            }
        }
        self.midLableField.numberOfLines = numberOfLines;
        self.midLableField.lineBreakMode = NSLineBreakByTruncatingTail;
        
        _paddingView.frame = RECT(paddingLeft, 0, DEVICE_WIDTH - (paddingLeft + paddingRight), height);
        
        _midLableField.textAlignment = (cellDic[@"textAlignment"])?[cellDic[@"textAlignment"] integerValue]:NSTextAlignmentLeft;
        _midLableField.textColor = (cellDic[@"textColor"])?cellDic[@"textColor"]:UIColorFromRGB(DEFINE_COLOR_BLACK_LIGHT);
        _midLableField.text = cellDic[@"textValue"]?cellDic[@"textValue"]:@"";
        CGSize lableSize = [cellDic[@"inputLable"] sizeWithFont:FONTS(FONTSIZE_MIDDLE)
                                                              maxSize:CGSizeMake(MAXFLOAT, MAXFLOAT)];
        if (lableSize.width == 0) {
            _midLableField.frame = RECT(paddingLeft+paddingTextLeft,
                                         0,
                                         DEVICE_WIDTH - (paddingLeft + paddingTextLeft + paddingTextRight + paddingRight + 16),
                                         height);
        } else {
            _midLableField.frame = RECT(paddingLeft + paddingTextLeft + labelWidth,
                                         0,
                                         DEVICE_WIDTH - (paddingLeft + paddingTextLeft + labelWidth + paddingTextRight + paddingRight + 16),
                                         height);
            
        }
        _leftLableView.frame = RECT(paddingTextLeft,
                                    (height - 20)/2,
                                    labelWidth,
                                    20);
        _leftLableView.text = cellDic[@"inputLable"]?cellDic[@"inputLable"]:@"";
        
        _goImageView.frame = RECT(DEVICE_WIDTH- (16 + paddingTextRight + paddingRight),
                                  (height - 16)/2,
                                  16,
                                  16);
        BOOL isFold = cellDic[@"isFold"]?[cellDic[@"isFold"] boolValue]:YES;
        BOOL isActive = cellDic[@"isActive"]?[cellDic[@"isActive"] boolValue]:YES;
        BOOL isEnd = cellDic[@"isEnd"]?[cellDic[@"isEnd"] boolValue]:YES;

        _goImageView.hidden = isFold;
        if(isFold){
            self.selectionStyle = UITableViewCellSelectionStyleDefault;
        } else {
            self.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        _midLableField.userInteractionEnabled = isActive;
        _separatorView.hidden = isEnd;
        
        _separatorView.frame = RECT(paddingLeft + paddingLine,
                                    height - 0.5,
                                    DEVICE_WIDTH - (paddingLine + paddingLeft + paddingRight),
                                    0.5);
        if (cellDic[@"selectionStyle"]) {
            self.selectionStyle = [cellDic[@"selectionStyle"] integerValue];
        } else {
            self.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
    }
}


@end

//
//  ZXPConmmonInputTableViewCell.h
//  jiangXunLu
//
//  Created by lixu06 on 16/6/17.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UITableViewCell+JXModel.h"


@interface JXCustomInputCheckTableViewCell : UITableViewCell

typedef void(^CustomInputCheckBlock)(NSDictionary *kvDic);

@property (nonatomic, copy) CustomInputCheckBlock changedBlock;
@property (nonatomic, copy) CustomInputCheckBlock clickBlock;
@property (nonatomic, copy) CustomInputCheckBlock checkBlock;


@end

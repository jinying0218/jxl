//
//  JXListActionSheet.m
//  jiangXunLu
//
//  Created by lixu06 on 16/6/29.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXListActionSheet.h"
#import "JXListActionSheetView.h"
#import "EXTScope.h"
#import "GlobalDefine.h"
#import "CommonTools.h"
#import "Color.h"
#import "Dimen.h"
#import "String.h"

@interface JXListActionSheet ()

@end

@implementation JXListActionSheet

- (void)setupActionSheetView:(id)data{
    JXListActionSheetView *sheetView = [JXListActionSheetView defaultView];
    sheetView.styleDic = [data mutableCopy] ;
    @weakify(self)
    sheetView.cancelBlock = ^(NSDictionary *kvDic){
        @strongify(self)
        [self buttonClicked:JXListActionSheetViewTagCancel];
    };
    sheetView.clickBlock = ^(NSDictionary *kvDic){
        @strongify(self)
        NSInteger tagValue = (kvDic[@"ActionSheetButtonTag"])?[kvDic[@"ActionSheetButtonTag"] integerValue]:JXListActionSheetViewTagCancel;
        [self buttonClicked:tagValue];
    };
    CGFloat height = [JXListActionSheetView viewHeight:sheetView.styleDic];
    self.bgViewHideFrame = CGRectMake(0, DEVICE_HEIGHT, DEVICE_WIDTH, height);
    self.bgViewShowFrame = CGRectMake(0, self.frame.size.height - height, self.frame.size.width, height);
    sheetView.frame = self.bgViewHideFrame;
    [sheetView updateViewInfo:sheetView.styleDic];
    self.sheetView = sheetView;
    [self addSubview:self.sheetView];
}

+(NSDictionary *)getGenderStyleDic{
    NSMutableDictionary *styleDic = [NSMutableDictionary dictionaryWithCapacity:0];
    styleDic[@"titleValue"] = STRING_GENDER_ACTION_SHEET_TITLE;
    styleDic[@"cancelText"] = STRING_GENDER_ACTION_SHEET_CANCEL;
    
    NSMutableArray *buttonArray = [NSMutableArray arrayWithCapacity:0];
    [buttonArray addObject:@{@"titleValue":STRING_PROFILE_EDIT_GENDER_MALE,
                             @"ListSheetActionTag":@(1)}];
    [buttonArray addObject:@{@"titleValue":STRING_PROFILE_EDIT_GENDER_FEMALE,
                             @"ListSheetActionTag":@(2)}];
    
    styleDic[@"buttonArray"] = buttonArray;
    
    return styleDic;
}

+(NSDictionary *)getShowFlagStyleDic{
    NSMutableDictionary *styleDic = [NSMutableDictionary dictionaryWithCapacity:0];
    styleDic[@"titleValue"] = STRING_SHOWFLAG_ACTION_SHEET_TITLE;
    styleDic[@"cancelText"] = STRING_SHOWFLAG_ACTION_SHEET_CANCEL;
    
    NSMutableArray *buttonArray = [NSMutableArray arrayWithCapacity:0];
    [buttonArray addObject:@{@"titleValue":STRING_PROFILE_EDIT_SHOW_FLAG_YES,
                             @"ListSheetActionTag":@(1)}];
    [buttonArray addObject:@{@"titleValue":STRING_PROFILE_EDIT_SHOW_FLAG_NO,
                             @"ListSheetActionTag":@(2)}];
    
    styleDic[@"buttonArray"] = buttonArray;
    
    return styleDic;
}

+(NSDictionary *)getPartyStyleDic{
    NSMutableDictionary *styleDic = [NSMutableDictionary dictionaryWithCapacity:0];
    styleDic[@"titleValue"] = STRING_PARTY_ACTION_SHEET_TITLE;
    styleDic[@"cancelText"] = STRING_PARTY_ACTION_SHEET_CANCEL;
    
    NSMutableArray *buttonArray = [NSMutableArray arrayWithCapacity:0];
    [buttonArray addObject:@{@"titleValue":STRING_PROFILE_EDIT_PARTY_A,
                             @"ListSheetActionTag":@(1)}];
    [buttonArray addObject:@{@"titleValue":STRING_PROFILE_EDIT_PARTY_B,
                             @"ListSheetActionTag":@(2)}];
    [buttonArray addObject:@{@"titleValue":STRING_PROFILE_EDIT_PARTY_OTHER,
                             @"ListSheetActionTag":@(3)}];
    styleDic[@"buttonArray"] = buttonArray;
    
    return styleDic;
}

@end

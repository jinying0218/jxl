//
//  JXCustomTextView.h
//  jiangXunLu
//
//  Created by lixu06 on 16/6/27.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXView.h"

@interface JXCustomTextView : JXView


@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UITextView *hintView;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *marginLeftLayout;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *marginRightLayout;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *marginTopLayout;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *marginBottomLayout;


typedef void(^CustomTextBlock)(NSDictionary *kvDic);

@property (nonatomic, copy) CustomTextBlock changedBlock;

@end

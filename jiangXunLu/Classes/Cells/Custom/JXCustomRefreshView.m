//
//  JXCustomRefreshHeaderView.m
//  jiangXunLu
//
//  Created by lixu06 on 16/7/1.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXCustomRefreshView.h"
#import "NSString+SizeCalculator.h"
#import "UITextFiled+LimitLength.h"
#import "GlobalDefine.h"
#import "CommonTools.h"
#import "Color.h"
#import "Dimen.h"
#import "JXTipHud.h"

@implementation JXCustomRefreshView

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self setBackgroundColor:UIColorFromRGB(DEFINE_COLOR_BACK)];
    self.refreshImage.image = nil;
    self.refreshImage.animationImages = [NSArray arrayWithObjects:
                                         IMGName(@"wait_black_1.png"),
                                         IMGName(@"wait_black_2.png"),
                                         IMGName(@"wait_black_3.png"),
                                         IMGName(@"wait_black_4.png"),
                                         IMGName(@"wait_black_5.png"),
                                         IMGName(@"wait_black_6.png"),
                                         IMGName(@"wait_black_7.png"),
                                         IMGName(@"wait_black_8.png"),
                                         IMGName(@"wait_black_9.png"),
                                         IMGName(@"wait_black_10.png"),
                                         IMGName(@"wait_black_11.png"),
                                         IMGName(@"wait_black_12.png"), nil];
    self.refreshImage.animationDuration = 1.0f;
    self.refreshImage.animationRepeatCount = 0;
    [self.refreshImage startAnimating];
    self.refreshLabel.textColor = UIColorFromRGB(DEFINE_COLOR_GRAY_DARK);
    self.refreshLabel.font = FONTS(FONTSIZE_MEDIA);
}

+ (CGFloat)viewHeight:(NSDictionary *)cellDic {
    CGFloat height = 50.0f;
    return height;
}

- (void)updateViewInfo:(NSDictionary *)cellDic {
    if (!cellDic) {
        return;
    }
    self.refreshLabel.text = cellDic[@"textValue"]?cellDic[@"textValue"]:@"";
}

@end

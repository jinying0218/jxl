//
//  JXShareActionSheet.m
//  jiangXunLu
//
//  Created by lixu06 on 16/6/28.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXShareActionSheet.h"
#import "JXShareActionSheetView.h"

#import "EXTScope.h"
#import "GlobalDefine.h"
#import "CommonTools.h"
#import "Color.h"
#import "Dimen.h"
#import "String.h"

@interface JXShareActionSheet ()

@end

@implementation JXShareActionSheet

- (void)setupActionSheetView:(id)data{
    JXShareActionSheetView *sheetView = [JXShareActionSheetView defaultView];
    sheetView.styleDic = [data mutableCopy] ;

    @weakify(self)
    sheetView.cancelBlock = ^(NSDictionary *kvDic){
        @strongify(self)
        [self buttonClicked:JXShareActionSheetViewTagCancel];
    };
    sheetView.clickBlock = ^(NSDictionary *kvDic){
        @strongify(self)
        long tagValue = (kvDic[@"ActionSheetButtonTag"])?[kvDic[@"ActionSheetButtonTag"] longValue]:-1;
        [self buttonClicked:tagValue];
    };
    CGFloat height = [JXShareActionSheetView viewHeight:sheetView.styleDic];
    self.bgViewHideFrame = CGRectMake(0, DEVICE_HEIGHT, DEVICE_WIDTH, height);
    self.bgViewShowFrame = CGRectMake(0, self.frame.size.height - height, self.frame.size.width, height);
    sheetView.frame = self.bgViewHideFrame;
    [sheetView updateViewInfo:sheetView.styleDic];
    self.sheetView = sheetView;
    [self addSubview:self.sheetView];
}

+(NSDictionary *)getDefaultStyleDic{
    
    CGFloat width = 50;
    CGFloat height = 100;
    CGFloat gap = 10;
    CGFloat margin = 10;
    
    NSMutableDictionary *styleDic = [NSMutableDictionary dictionaryWithCapacity:0];
    styleDic[@"titleValue"] = @"网页由v2.qun.hk提供";
    styleDic[@"cancelText"] = STRING_SHARE_ACTION_SHEET_CANCEL;
    styleDic[@"cellWidth"] = @(width);
    styleDic[@"cellHeight"] = @(height);
    styleDic[@"cellMargin"] = @(margin);
    
    NSMutableArray *topArray = [NSMutableArray arrayWithCapacity:0];
    [topArray addObject:@{@"width":@(width),
                          @"height":@(height),
                          @"gap":@(gap),
                          @"textColor":UIColorFromRGB(DEFINE_COLOR_GRAY),
                          @"font":FONTS(FONTSIZE_MIN_EXTRA),
                          @"numberOfLines":@(0),
                          @"textAlignment":@(NSTextAlignmentCenter),
                          @"buttonImage":IMGName(@"icon_share_friend.png"),
                          @"textValue":STRING_SHARE_ACTION_SHEET_FRIEND,
                          @"ActionSheetButtonTag":@(JXShareActionSheetViewTagWeixinFriend)
                          }];
    [topArray addObject:@{@"width":@(width),
                          @"height":@(height),
                          @"gap":@(gap),
                          @"textColor":UIColorFromRGB(DEFINE_COLOR_GRAY),
                          @"font":FONTS(FONTSIZE_MIN_EXTRA),
                          @"numberOfLines":@(0),
                          @"textAlignment":@(NSTextAlignmentCenter),
                          @"buttonImage":IMGName(@"icon_share_quan.png"),
                          @"textValue":STRING_SHARE_ACTION_SHEET_CIRCLE,
                          @"ActionSheetButtonTag":@(JXShareActionSheetViewTagCircle)
                          }];
    [topArray addObject:@{@"width":@(width),
                          @"height":@(height),
                          @"gap":@(gap),
                          @"textColor":UIColorFromRGB(DEFINE_COLOR_GRAY),
                          @"font":FONTS(FONTSIZE_MIN_EXTRA),
                          @"numberOfLines":@(0),
                          @"textAlignment":@(NSTextAlignmentCenter),
                          @"buttonImage":IMGName(@"icon_share_collect.png"),
                          @"textValue":STRING_SHARE_ACTION_SHEET_COLLECT,
                          @"ActionSheetButtonTag":@(JXShareActionSheetViewTagCollect)
                          }];
    [topArray addObject:@{@"width":@(width),
                          @"height":@(height),
                          @"gap":@(gap),
                          @"textColor":UIColorFromRGB(DEFINE_COLOR_GRAY),
                          @"font":FONTS(FONTSIZE_MIN_EXTRA),
                          @"numberOfLines":@(0),
                          @"textAlignment":@(NSTextAlignmentCenter),
                          @"buttonImage":IMGName(@"icon_share_safari.png"),
                          @"textValue":STRING_SHARE_ACTION_SHEET_SAFARI,
                          @"ActionSheetButtonTag":@(JXShareActionSheetViewTagSafari)
                          }];
    [topArray addObject:@{@"width":@(width),
                          @"height":@(height),
                          @"gap":@(gap),
                          @"textColor":UIColorFromRGB(DEFINE_COLOR_GRAY),
                          @"font":FONTS(FONTSIZE_MIN_EXTRA),
                          @"numberOfLines":@(0),
                          @"textAlignment":@(NSTextAlignmentCenter),
                          @"buttonImage":IMGName(@"icon_share_qq.png"),
                          @"textValue":STRING_SHARE_ACTION_SHEET_QQ,
                          @"ActionSheetButtonTag":@(JXShareActionSheetViewTagQQ)
                          }];
    styleDic[@"topArray"] = topArray;
    
    NSMutableArray *bottomArray = [NSMutableArray arrayWithCapacity:0];
    [bottomArray addObject:@{@"width":@(width),
                             @"height":@(height),
                             @"gap":@(gap),
                             @"textColor":UIColorFromRGB(DEFINE_COLOR_GRAY),
                             @"font":FONTS(FONTSIZE_MIN_EXTRA),
                             @"numberOfLines":@(0),
                             @"textAlignment":@(NSTextAlignmentCenter),
                             @"buttonImage":IMGName(@"icon_share_public.png"),
                             @"textValue":STRING_SHARE_ACTION_SHEET_PUBLIC,
                             @"ActionSheetButtonTag":@(JXShareActionSheetViewTagPublic)
                             }];
    [bottomArray addObject:@{@"width":@(width),
                             @"height":@(height),
                             @"gap":@(gap),
                             @"textColor":UIColorFromRGB(DEFINE_COLOR_GRAY),
                             @"font":FONTS(FONTSIZE_MIN_EXTRA),
                             @"numberOfLines":@(0),
                             @"textAlignment":@(NSTextAlignmentCenter),
                             @"buttonImage":IMGName(@"icon_share_link.png"),
                             @"textValue":STRING_SHARE_ACTION_SHEET_LINK,
                             @"ActionSheetButtonTag":@(JXShareActionSheetViewTagLink)
                             }];
    [bottomArray addObject:@{@"width":@(width),
                             @"height":@(height),
                             @"gap":@(gap),
                             @"textColor":UIColorFromRGB(DEFINE_COLOR_GRAY),
                             @"font":FONTS(FONTSIZE_MIN_EXTRA),
                             @"numberOfLines":@(0),
                             @"textAlignment":@(NSTextAlignmentCenter),
                             @"buttonImage":IMGName(@"icon_share_font.png"),
                             @"textValue":STRING_SHARE_ACTION_SHEET_FONT,
                             @"ActionSheetButtonTag":@(JXShareActionSheetViewTagFont)
                             }];
    [bottomArray addObject:@{@"width":@(width),
                             @"height":@(height),
                             @"gap":@(gap),
                             @"textColor":UIColorFromRGB(DEFINE_COLOR_GRAY),
                             @"font":FONTS(FONTSIZE_MIN_EXTRA),
                             @"numberOfLines":@(0),
                             @"textAlignment":@(NSTextAlignmentCenter),
                             @"buttonImage":IMGName(@"icon_share_mode.png"),
                             @"textValue":STRING_SHARE_ACTION_SHEET_MODE,
                             @"ActionSheetButtonTag":@(JXShareActionSheetViewTagMode)
                             }];
    [bottomArray addObject:@{@"width":@(width),
                             @"height":@(height),
                             @"gap":@(gap),
                             @"textColor":UIColorFromRGB(DEFINE_COLOR_GRAY),
                             @"font":FONTS(FONTSIZE_MIN_EXTRA),
                             @"numberOfLines":@(0),
                             @"textAlignment":@(NSTextAlignmentCenter),
                             @"buttonImage":IMGName(@"icon_share_refresh.png"),
                             @"textValue":STRING_SHARE_ACTION_SHEET_REFRESH,
                             @"ActionSheetButtonTag":@(JXShareActionSheetViewTagRefresh)
                             }];
    styleDic[@"bottomArray"] = bottomArray;
    
    return styleDic;
}

@end

//
//  JXListActionSheetView.m
//  jiangXunLu
//
//  Created by lixu06 on 16/6/29.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXListActionSheetView.h"

#import "GlobalDefine.h"
#import "CommonTools.h"
#import "Color.h"
#import "Dimen.h"
#import "String.h"
#import "EXTScope.h"

@interface JXListActionSheetView ()

@end

@implementation JXListActionSheetView

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    UIColor *backgroundColor = UIColorFromRGB(DEFINE_COLOR_BACK);
    UIColor *titleBackgroundColor = UIColorFromRGB(DEFINE_COLOR_WHITE);
    UIColor *titleTextColor = UIColorFromRGB(DEFINE_COLOR_GRAY_DARK);
    UIFont *titleFont = FONTS(FONTSIZE_MEDIA);
    UIColor *buttonBackgroundColor = UIColorFromRGB(DEFINE_COLOR_WHITE);
    UIColor *buttonTextColor = UIColorFromRGB(DEFINE_COLOR_BLACK_LIGHT);
    UIFont *buttonFont = FONTS(FONTSIZE_BIG);
    
    self.backgroundColor = backgroundColor;
    self.buttonView.backgroundColor = backgroundColor;
    
    self.titleLabel.backgroundColor = titleBackgroundColor;
    self.titleLabel.textColor = titleTextColor;
    self.titleLabel.font = titleFont;
    
    self.cancelButton.backgroundColor = buttonBackgroundColor;
    [self.cancelButton setTitleColor:buttonTextColor forState:UIControlStateNormal];
    self.cancelButton.titleLabel.font = buttonFont;
    self.cancelButton.tag = JXListActionSheetViewTagCancel;
}

+ (CGFloat)viewHeight:(NSDictionary *)kvDic{
    NSArray *buttonArray = (kvDic[@"buttonArray"])?kvDic[@"buttonArray"]:@[];
    return 44 + 1 + (44 + 1)*buttonArray.count + 5+ 44;
}
- (void)updateViewInfo:(NSDictionary *)cellDic{
    self.titleLabel.text = (cellDic[@"titleValue"])?cellDic[@"titleValue"]:@" ";
    NSString *cancelText = (cellDic[@"cancelText"])?cellDic[@"cancelText"]:@"取消";
    [self.cancelButton setTitle:cancelText forState:UIControlStateNormal];
    CGFloat cellWidth = self.frame.size.width;
    CGFloat cellHeight = 44;
    CGFloat cellMargin = 1;
    long i;
    NSArray *buttonArray = (cellDic[@"buttonArray"])?cellDic[@"buttonArray"]:@[];
    for (i = 0; i<buttonArray.count; i++) {
        UIButton *button = [[UIButton alloc] init];
        button.frame = RECT(0, i*(cellHeight + cellMargin), cellWidth, cellHeight);
        [button addTarget:self action:@selector(buttonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        [self updateButton:button info:buttonArray[i]];
        [self.buttonView addSubview:button];
    }
}

- (void)updateButton:(UIButton *)button info:(NSDictionary *)cellDic{
    NSString *title = cellDic[@"titleValue"]?cellDic[@"titleValue"]:@"";
    UIColor *buttonBackgroundColor = UIColorFromRGB(DEFINE_COLOR_WHITE);
    UIColor *buttonTextColor = UIColorFromRGB(DEFINE_COLOR_BLACK_LIGHT);
    UIFont *buttonFont = FONTS(FONTSIZE_BIG);
    [button setTitle:title forState:UIControlStateNormal];
    button.backgroundColor = buttonBackgroundColor;
    [button setTitleColor:buttonTextColor forState:UIControlStateNormal];
    button.titleLabel.font = buttonFont;
    NSInteger tag = JXListActionSheetViewTagCancel;
    if (cellDic[@"ListSheetActionTag"]) {
        tag = [cellDic[@"ListSheetActionTag"] integerValue];
    }
    button.tag = tag;
}

-(void)buttonTouchUpInside:(id)sender{
    UIView *view = (UIView *)sender;
    self.styleDic[@"ActionSheetButtonTag"] = @(view.tag);
    if (self.clickBlock) {
        self.clickBlock(self.styleDic);
    }
}

- (IBAction)cancelTouchUpInside:(id)sender {
    if (self.cancelBlock) {
        self.cancelBlock(self.styleDic);
    }
}

@end

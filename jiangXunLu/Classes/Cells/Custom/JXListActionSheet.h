//
//  JXListActionSheet.h
//  jiangXunLu
//
//  Created by lixu06 on 16/6/29.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JXCustomActionSheet.h"

@interface JXListActionSheet : JXCustomActionSheet

+(NSDictionary *)getGenderStyleDic;
+(NSDictionary *)getShowFlagStyleDic;
+(NSDictionary *)getPartyStyleDic;

@end

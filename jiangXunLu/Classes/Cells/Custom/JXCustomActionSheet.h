//
//  JXCustomActionSheet.h
//  jiangXunLu
//
//  Created by lixu06 on 16/6/28.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JXCustomActionSheet;

@protocol JXCustomActionSheetDelegate <NSObject>

@optional
- (void)actionSheet:(JXCustomActionSheet *)actionSheet buttonClickedAtTag:(NSInteger)tag;
@optional
- (void)actionSheetMaskClicked:(JXCustomActionSheet *)actionSheet;

@end

@interface JXCustomActionSheet : UIView

@property (weak, nonatomic)     id<JXCustomActionSheetDelegate>   delegate;
@property (strong, nonatomic)   UIView *maskView;
@property (strong, nonatomic)   UIView *sheetView;
@property (nonatomic)           CGRect bgViewHideFrame;
@property (nonatomic)           CGRect bgViewShowFrame;

- (instancetype)initWithData:(id)data delegate:(id<JXCustomActionSheetDelegate>)delegate;
- (void)show;
- (void)setupActionSheetView:(id)data;
- (void)buttonClicked:(NSInteger)tag;
- (void)hideWithNotification;
- (void)hideWithFinishHandler:(void(^)())handler;
- (void)hideDirectly;

@end
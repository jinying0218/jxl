//
//  JXPickerActionSheet.m
//  jiangXunLu
//
//  Created by lixu06 on 16/6/29.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXPickerActionSheet.h"
#import "JXPickerActionSheetView.h"
#import "EXTScope.h"
#import "GlobalDefine.h"
#import "CommonTools.h"
#import "Color.h"
#import "Dimen.h"
#import "String.h"

@implementation JXPickerActionSheet

- (void)setupActionSheetView:(id)data{
    JXPickerActionSheetView *sheetView = [JXPickerActionSheetView defaultView];
    sheetView.styleDic = [data mutableCopy] ;
    @weakify(self)
    sheetView.cancelBlock = ^(NSDictionary *kvDic){
        @strongify(self)
        [self buttonClicked:JXPickerActionSheetViewTagCancel];
    };
    sheetView.confirmBlock = ^(NSDictionary *kvDic){
        @strongify(self)
        if (kvDic[@"pickerIndex"]) {
            self.pickerIndex = kvDic[@"pickerIndex"];
        }
        if (kvDic[@"pickerData"]) {
            self.pickerData = kvDic[@"pickerData"];
        }
        [self buttonClicked:JXPickerActionSheetViewTagConfirm];
    };
    CGFloat height = [JXPickerActionSheetView viewHeight:sheetView.styleDic];
    self.bgViewHideFrame = CGRectMake(0, DEVICE_HEIGHT, DEVICE_WIDTH, height);
    self.bgViewShowFrame = CGRectMake(0, self.frame.size.height - height, self.frame.size.width, height);
    sheetView.frame = self.bgViewHideFrame;
    [sheetView updateViewInfo:sheetView.styleDic];
    self.sheetView = sheetView;
    [self addSubview:self.sheetView];
}

+(NSDictionary *)getLocationStyleDic{
    NSMutableDictionary *styleDic = [NSMutableDictionary dictionaryWithCapacity:0];
    styleDic[@"confirmlText"] = STRING_LOCATION_ACTION_SHEET_CONFIRM;
    styleDic[@"cancelText"] = STRING_LOCATION_ACTION_SHEET_CANCEL;
    styleDic[@"pickerMode"] = @(JXPickerActionSheetModeChild);
    styleDic[@"pickerKey"] = @"name";
    styleDic[@"pickerChild"] = @"children";
    styleDic[@"pickerCount"] = @(2);
    NSString *dataPath = [[NSBundle mainBundle] pathForResource:@"all_area_default"
                                    ofType:@"json"];
    NSFileHandle *file = [NSFileHandle fileHandleForReadingAtPath:dataPath];
    NSData *data = [file readDataToEndOfFile];
    NSError *error;
    NSDictionary *json= [NSJSONSerialization JSONObjectWithData:data
                                                        options:NSJSONReadingAllowFragments
                                                          error:&error];
    NSMutableArray *buttonArray = json[@"data"];
    styleDic[@"pickerModeData"] = buttonArray;
    return styleDic;
}

+(NSString *)getLocationValueWithData:(NSArray *)pickerData index:(NSArray *)pickerIndex{

    if (!pickerIndex || !pickerData) {
        return @"";
    }
    NSMutableString *result = [@"" mutableCopy];
    
    for (long i=0;i<pickerData.count;i++) {
        long index = [pickerIndex[i] longValue];
        NSDictionary *itemDic = pickerData[i][index];
        if (i == 0) {
            [result appendFormat:@"%@",itemDic[@"name"]];
        } else {
            [result appendFormat:@" %@",itemDic[@"name"]];
        }
    }
    return result;
    
}
@end

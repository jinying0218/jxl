//
//  JXCustomActionSheet.m
//  jiangXunLu
//
//  Created by lixu06 on 16/6/28.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXCustomActionSheet.h"

#import "EXTScope.h"
#import "GlobalDefine.h"
#import "CommonTools.h"
#import "Color.h"
#import "Dimen.h"
#import "String.h"

@interface JXCustomActionSheet ()

@end

@implementation JXCustomActionSheet

static CGFloat const kAnimationDuration = 0.3;

- (instancetype)initWithData:(id)data delegate:(id<JXCustomActionSheetDelegate>)delegate{
    if (self = [super initWithFrame:[[UIApplication sharedApplication] keyWindow].bounds]) {
        self.delegate = delegate;
        [self setupMaskView];
        [self setupActionSheetView:data];
    }
    return self;
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setupMaskView{
    self.maskView = [[UIView alloc] initWithFrame:self.bounds];
    //    self.maskView.backgroundColor = [UIColor grayColor];
    self.maskView.alpha = 0.5;
    [self.maskView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideDirectly)]];
    [self addSubview:self.maskView];
}

- (void)setupActionSheetView:(id)data{
}

- (void)buttonClicked:(NSInteger)tag{
    //    __weak typeof(self) weakSelf = self;
    @weakify(self)
    [self hideWithFinishHandler:^{
        @strongify(self)
        if ([self.delegate respondsToSelector:@selector(actionSheet:buttonClickedAtTag:)]) {
            [self.delegate actionSheet:self buttonClickedAtTag:tag];
        }
    }];
}

- (void)show{
    [[[UIApplication sharedApplication] keyWindow] addSubview:self];
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(hideWithNotification)
                                                 name:CUSTOM_ACTION_SHEET_HIDE_NOTIFICATION
                                               object:nil];
    
    @weakify(self);
    [UIView animateWithDuration:kAnimationDuration animations:^{
        @strongify(self);
        self.maskView.backgroundColor = UIColorFromRGBA(DEFINE_COLOR_TRANSBLACK);
        self.sheetView.frame = self.bgViewShowFrame;
    }];
}

- (void)hideWithNotification {
    [self hideWithFinishHandler:nil];
}

- (void)hideWithFinishHandler:(void(^)())handler{
    @weakify(self);
    [UIView animateWithDuration:kAnimationDuration animations:^{
        @strongify(self);
        self.maskView.backgroundColor = UIColorFromRGBA(DEFINE_COLOR_TRANSPARENT);
        self.sheetView.frame = self.bgViewHideFrame;
    } completion:^(BOOL finished) {
        @strongify(self);
        if (finished) {
            [self removeFromSuperview];
        }
        if (handler) {
            handler();
        }
    }];
}

- (void)hideDirectly{
    @weakify(self);
    [self hideWithFinishHandler:^(){
        @strongify(self);
        if ([self.delegate respondsToSelector:@selector(actionSheetMaskClicked:)]) {
            [self.delegate actionSheetMaskClicked:self];
        }
    }];
}

@end

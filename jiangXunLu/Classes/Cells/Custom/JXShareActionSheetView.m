//
//  JXShareActionSheetView.m
//  jiangXunLu
//
//  Created by lixu06 on 16/6/28.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXShareActionSheetView.h"
#import "JXShareActionSheetButton.h"
#import "GlobalDefine.h"
#import "CommonTools.h"
#import "Color.h"
#import "Dimen.h"
#import "String.h"
#import "EXTScope.h"

@interface JXShareActionSheetView ()

@end

@implementation JXShareActionSheetView

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    UIColor *backgroundColor = UIColorFromRGB(DEFINE_COLOR_BACK);
    UIFont  *titleFont = FONTS(FONTSIZE_SMALL);
    UIColor *titleBackgroundColor = UIColorFromRGB(DEFINE_COLOR_BACK);
    UIColor *titleColor = UIColorFromRGB(DEFINE_COLOR_GRAY_DARK);
    UIColor *midBackgroundColor = UIColorFromRGB(DEFINE_COLOR_BACK);
    UIColor *buttonBackgroundColor = UIColorFromRGB(DEFINE_COLOR_WHITE);
    UIColor *buttonTextColor = UIColorFromRGB(DEFINE_COLOR_BLACK_LIGHT);
    UIFont *buttonFont = FONTS(FONTSIZE_BIG);
    
    self.backgroundColor = backgroundColor;
    self.titleLable.font = titleFont;
    self.titleLable.backgroundColor = titleBackgroundColor;
    self.titleLable.textColor = titleColor;
    self.middleBackView.backgroundColor = midBackgroundColor;
    self.cancelButton.backgroundColor = buttonBackgroundColor;
    self.cancelButton.titleLabel.font = buttonFont;
    [self.cancelButton setTitleColor:buttonTextColor forState:UIControlStateNormal];
    self.scrollHeight.constant = 100;
}

+ (CGFloat)viewHeight:(NSDictionary *)kvDic{
    return 44 + 10 + 100 + 10 + 10 + 100 + 10 + 16 + 5;
}

- (void)updateViewInfo:(NSDictionary *)cellDic{
    self.titleLable.text = (cellDic[@"titleValue"])?cellDic[@"titleValue"]:@" ";
    NSString *cancelText = (cellDic[@"cancelText"])?cellDic[@"cancelText"]:STRING_SHARE_ACTION_SHEET_CANCEL;
    [self.cancelButton setTitle:cancelText forState:UIControlStateNormal];
    
    CGFloat cellWidth = (cellDic[@"cellWidth"])?[cellDic[@"cellWidth"] floatValue]:50;
    CGFloat cellHeight = (cellDic[@"cellHeight"])?[cellDic[@"cellHeight"] floatValue]:100;
    CGFloat cellMargin = (cellDic[@"cellMargin"])?[cellDic[@"cellMargin"] floatValue]:10;
    self.scrollHeight.constant = cellHeight;
    
    long i;
    NSArray *topArray = (cellDic[@"topArray"])?cellDic[@"topArray"]:@[];
    for (i = 0; i<topArray.count; i++) {
        JXShareActionSheetButton *button = [[JXShareActionSheetButton alloc] init];
        button.frame = RECT(i*(cellWidth + cellMargin), 0, cellWidth, cellHeight);
        button.styleDic = topArray[i];
        [button updateViewInfo:button.styleDic];
        button.clickBlock = self.clickBlock;
//        ^(NSDictionary *kvDic){
//            @strongify(self)
//            if (self.clickBlock) {
//                self.clickBlock(self.styleDic[@"topArray"][i]);
//            }
//        };
        [self.topScrollView addSubview:button];
    }
    CGFloat contentWidth = (topArray.count == 0)?0: (cellWidth + cellMargin)*topArray.count - cellMargin;
    self.topScrollView.contentSize = CGSizeMake(contentWidth, cellHeight);
    
    NSArray *bottomArray = (cellDic[@"bottomArray"])?cellDic[@"bottomArray"]:@[];
    for (i = 0; i<bottomArray.count; i++) {
        JXShareActionSheetButton *button = [[JXShareActionSheetButton alloc] init];
        button.frame = RECT(i*(cellWidth + cellMargin), 0, cellWidth, cellHeight);
        button.styleDic = bottomArray[i];
        [button updateViewInfo:button.styleDic];
        button.clickBlock = self.clickBlock;
        [self.bottomScrollView addSubview:button];
    }
    contentWidth = (topArray.count == 0)?0: (cellWidth + cellMargin)*bottomArray.count - cellMargin;
    self.topScrollView.contentSize = CGSizeMake(contentWidth, cellHeight);
}

- (IBAction)cancellTouchUpInside:(id)sender {
    if (self.cancelBlock) {
        self.cancelBlock(self.styleDic);
    }
}


@end

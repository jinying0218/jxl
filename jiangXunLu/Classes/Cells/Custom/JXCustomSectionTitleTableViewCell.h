//
//  ZXPCustomSectionTitleTableViewCell.h
//  ZXBaiduAppUIKit
//
//  Created by lixu06 on 16/1/27.
//  Copyright © 2016年 百度在线. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITableViewCell+JXModel.h"
@interface JXCustomSectionTitleTableViewCell : UITableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;

@end

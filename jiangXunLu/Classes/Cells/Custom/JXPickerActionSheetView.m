//
//  JXPickerActionSheetView.m
//  jiangXunLu
//
//  Created by lixu06 on 16/6/29.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXPickerActionSheetView.h"
#import "GlobalDefine.h"
#import "CommonTools.h"
#import "Color.h"
#import "Dimen.h"
#import "String.h"
#import "EXTScope.h"


@interface JXPickerActionSheetView () <UIPickerViewDelegate,UIPickerViewDataSource>

@property (nonatomic, strong) NSArray *pickerModeData;
@property (nonatomic, assign) JXPickerActionSheetMode pickerMode;
@property (nonatomic, assign) NSInteger pickerCount;

@property (nonatomic, strong) NSMutableArray *pickerData;
@property (nonatomic, strong) NSMutableArray *pickerIndex;
@property (nonatomic, strong) NSString *pickerKey;
@property (nonatomic, strong) NSString *pickerChild;

@end

@implementation JXPickerActionSheetView

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    UIColor *backgroundColor = UIColorFromRGB(DEFINE_COLOR_BACK);
    UIColor *titleBackgroundColor = UIColorFromRGB(DEFINE_COLOR_WHITE);
//    UIColor *titleTextColor = UIColorFromRGB(DEFINE_COLOR_GRAY_DARK);
//    UIFont *titleFont = FONTS(FONTSIZE_MEDIA);
    
    UIColor *buttonBackgroundColor = UIColorFromRGBA(DEFINE_COLOR_TRANSPARENT);
    UIColor *buttonTextColor = UIColorFromRGB(DEFINE_COLOR_BLACK_LIGHT);
    UIFont *buttonFont = FONTS(FONTSIZE_BIG);
    
    self.backgroundColor = backgroundColor;
    self.pickerView.backgroundColor = titleBackgroundColor;
    self.pickerView.dataSource = self;
    self.pickerView.delegate = self;
    
    self.leftButton.backgroundColor = buttonBackgroundColor;
    [self.leftButton setTitleColor:buttonTextColor forState:UIControlStateNormal];
    self.leftButton.titleLabel.font = buttonFont;
    self.leftButton.tag = JXPickerActionSheetViewTagCancel;
    
    self.rightButton.backgroundColor = buttonBackgroundColor;
    [self.rightButton setTitleColor:buttonTextColor forState:UIControlStateNormal];
    self.rightButton.titleLabel.font = buttonFont;
    self.rightButton.tag = JXPickerActionSheetViewTagConfirm;
    
}

+ (CGFloat)viewHeight:(NSDictionary *)kvDic{
    return DEVICE_HEIGHT / 2;
}
- (void)updateViewInfo:(NSDictionary *)cellDic{
    NSString *cancelText = (cellDic[@"cancelText"])?cellDic[@"cancelText"]:@"取消";
    [self.leftButton setTitle:cancelText forState:UIControlStateNormal];
    NSString *confirmText = (cellDic[@"confirmText"])?cellDic[@"confirmText"]:@"确认";
    [self.rightButton setTitle:confirmText forState:UIControlStateNormal];
    
    NSArray *pickerModeData = (cellDic[@"pickerModeData"])?cellDic[@"pickerModeData"]:@[];
    self.pickerModeData = pickerModeData;
    
    JXPickerActionSheetMode pickerMode = (cellDic[@"pickerMode"])?[cellDic[@"pickerMode"] integerValue]:JXPickerActionSheetModeMulti;
    self.pickerMode = pickerMode;
    
    NSInteger pickerCount = (cellDic[@"pickerCount"])?[cellDic[@"pickerCount"] integerValue]:0;
        self.pickerCount = pickerCount;
    
    NSMutableArray *pickerIndex;
    if (cellDic[@"pickerIndex"]) {
        pickerIndex = cellDic[@"pickerIndex"];
    } else{
        pickerIndex = [NSMutableArray arrayWithCapacity:0];
        for (int i=0; i<self.pickerCount; i++) {
            [pickerIndex addObject:@(0)];
        }
    }
    self.pickerIndex = [pickerIndex mutableCopy];
    
    self.styleDic[@"pickerIndex"] = self.pickerIndex;
    
    NSString *pickerKey = (cellDic[@"pickerKey"])?cellDic[@"pickerKey"]:@"name";
    self.pickerKey = pickerKey;
    
    switch (self.pickerMode) {
        case JXPickerActionSheetModeMulti:{
            self.pickerData = [self.pickerModeData mutableCopy];
            break;
        }
        case JXPickerActionSheetModeChild:{
            NSString *pickerChild = (cellDic[@"pickerChild"])?cellDic[@"pickerChild"]:@"child";
            self.pickerChild = pickerChild;
            self.pickerData = [NSMutableArray arrayWithCapacity:0];
            
            for (int i=0; i<self.pickerCount; i++) {
                NSMutableArray *array;
                array =[NSMutableArray arrayWithCapacity:0];
                if (i == 0) {
                    [array addObjectsFromArray:self.pickerModeData];
                    [self.pickerData addObject:array];
                } else {
                    NSArray *parrentArray = self.pickerData[i-1];
                    NSInteger nodeIndex = [self.pickerIndex[i-1] integerValue];
                    NSDictionary *parentNode = parrentArray[nodeIndex];
                    [array addObjectsFromArray:parentNode[self.pickerChild]];
                    [self.pickerData addObject:array];
                }
            }
            break;
        }
        default:{
            self.pickerData = [NSMutableArray arrayWithCapacity:0];
            break;
        }
    }
    self.styleDic[@"pickerData"] = self.pickerData;

}

- (IBAction)buttonTouchUpInside:(id)sender {
    switch (((UIView *)sender).tag) {
        case JXPickerActionSheetViewTagCancel:{
            if (self.cancelBlock) {
                self.cancelBlock(self.styleDic);
            }
            break;
        }
        case JXPickerActionSheetViewTagConfirm:{
            if (self.confirmBlock) {
                self.confirmBlock(self.styleDic);
            }
            break;
        }
        default:
            break;
    }
}

#pragma mark-
#pragma - mark UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return self.pickerData.count;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return ((NSArray *)self.pickerData[component]).count;
    
}

#pragma mark-
#pragma - mark UIPickerViewDelegate
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [NSString stringWithFormat:@"%@",self.pickerData[component][row][self.pickerKey]];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    self.pickerIndex[component] = @(row);
    if (component < self.pickerCount-1) {
        NSMutableArray *array;
        array =[NSMutableArray arrayWithCapacity:0];
        NSArray *currentArray = self.pickerData[component];
        NSInteger nodeIndex = row;
        NSDictionary *indexNode = currentArray[nodeIndex];
        [array addObjectsFromArray:indexNode[self.pickerChild]];
        [self.pickerData replaceObjectAtIndex:component+1 withObject:array];
        [pickerView reloadComponent:component+1];
        
        for (NSInteger i = component+1; i<self.pickerCount; i++) {
            self.pickerIndex[i] = @(0);
            [self.pickerView selectRow:[self.pickerIndex[i] integerValue]
                           inComponent:i
                              animated:YES];
        }
    }
}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 44;
}

@end

//
//  JXCustomSubmitTableViewCell.m
//  jiangXunLu
//
//  Created by lixu06 on 16/6/26.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXCustomSubmitTableViewCell.h"
#import "NSString+SizeCalculator.h"
#import "GlobalDefine.h"
#import "CommonTools.h"
#import "Color.h"
#import "Dimen.h"
#import "NSObject+BMRuntime.h"

@implementation JXCustomSubmitTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


+ (CGFloat)cellHeight:(NSDictionary *)cellDic {
    CGFloat height = 150.0f;
    if (cellDic[@"height"]) {
        height = [cellDic[@"height"] floatValue];
    }
    return height;
}

- (void)updateCellInfo:(NSDictionary *)cellDic onModel:(NSObject *)model{
    if (!cellDic) {
        return;
    }
    self.backgroundColor = cellDic[@"backgroundColor"]?cellDic[@"backgroundColor"]:UIColorFromRGB(DEFINE_COLOR_WHITE);
    self.buttonMarginTopLayout.constant = cellDic[@"marginTop"]?[cellDic[@"marginTop"] floatValue]:42.0f;
    self.buttonWidthLayout.constant = cellDic[@"buttonWidth"]?[cellDic[@"buttonWidth"] floatValue]:80.0f;
    self.buttonHeightLayout.constant = cellDic[@"buttonHeight"]?[cellDic[@"buttonHeight"] floatValue]:36.0f;
    NSString *submitText = cellDic[@"buttonText"]?cellDic[@"buttonText"]:@"提  交";
    [self.submitButton setTitle:submitText forState:UIControlStateNormal];
    UIColor *bottonBackgroundColor = cellDic[@"bottonBackgroundColor"]?cellDic[@"bottonBackgroundColor"]:UIColorFromRGB(DEFINE_COLOR_BROWN);
    [self.submitButton setBackgroundColor:bottonBackgroundColor];
    UIColor *buttonTextColor = cellDic[@"buttonTextColor"]?cellDic[@"buttonTextColor"]:UIColorFromRGB(DEFINE_COLOR_WHITE);
    [self.submitButton setTitleColor:buttonTextColor forState:UIControlStateNormal];
    self.submitButton.layer.cornerRadius = cellDic[@"buttonCornerRadius"]?[cellDic[@"buttonCornerRadius"] floatValue]:5;
    self.submitButton.layer.masksToBounds = YES;
    self.submitButton.titleLabel.font = cellDic[@"buttonTextFont"]?cellDic[@"buttonTextFont"]:FONTS(FONTSIZE_MEDIA);
    
    if (cellDic[@"submitBlock"]) {
        self.submitBlock = ^(NSDictionary *kvDic) {
            SEL selector = NSSelectorFromString(cellDic[@"submitBlock"]);
            if ([model respondsToSelector:selector]) {
                [model performSelectorSEL:selector withObject:kvDic];
            }
        };
    }
}

- (IBAction)submitTouchUpInside:(id)sender {
    if (self.submitBlock) {
        self.submitBlock(self.cellData);
    }
}
@end

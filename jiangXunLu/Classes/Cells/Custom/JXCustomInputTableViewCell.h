//
//  ZXPConmmonInputTableViewCell.h
//  jiangXunLu
//
//  Created by lixu06 on 16/6/17.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UITableViewCell+JXModel.h"


@interface JXCustomInputTableViewCell : UITableViewCell

typedef void(^CustomInputBlock)(NSDictionary *kvDic);

@property (nonatomic, copy) CustomInputBlock changedBlock;
@property (nonatomic, copy) CustomInputBlock clickBlock;

@end

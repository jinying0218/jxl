//
//  JXShareActionSheetView.h
//  jiangXunLu
//
//  Created by lixu06 on 16/6/28.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXView.h"

@interface JXShareActionSheetView : JXView

typedef NS_ENUM(NSInteger, JXShareActionSheetViewTag) {
    /** 取消 */
    JXShareActionSheetViewTagCancel              = 0,
    /** 微信朋友 */
    JXShareActionSheetViewTagWeixinFriend        = 1,
    /** 朋友圈 */
    JXShareActionSheetViewTagCircle              = 2,
    /** 收藏 */
    JXShareActionSheetViewTagCollect             = 3,
    /** 在浏览器中打开 */
    JXShareActionSheetViewTagSafari              = 4,
    /** QQ */
    JXShareActionSheetViewTagQQ                  = 5,
    /** 公众号 */
    JXShareActionSheetViewTagPublic              = 6,
    /** 复制连接 */
    JXShareActionSheetViewTagLink                = 7,
    /** 调整字体 */
    JXShareActionSheetViewTagFont                = 8,
    /** 阅读模式 */
    JXShareActionSheetViewTagMode                = 9,
    /** 刷新 */
    JXShareActionSheetViewTagRefresh             = 10
};

@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLable;
@property (weak, nonatomic) IBOutlet UIView *middleBackView;
@property (weak, nonatomic) IBOutlet UIScrollView *topScrollView;
@property (weak, nonatomic) IBOutlet UIScrollView *bottomScrollView;
@property (weak, nonatomic) IBOutlet UIImageView *speratorView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollHeight;

typedef void(^ShareActionSheetBlock)(NSDictionary *kvDic);
@property (nonatomic, copy) ShareActionSheetBlock clickBlock;
@property (nonatomic, copy) ShareActionSheetBlock cancelBlock;

- (IBAction)cancellTouchUpInside:(id)sender;

@end

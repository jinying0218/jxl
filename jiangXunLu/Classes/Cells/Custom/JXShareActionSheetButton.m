//
//  JXShareActionSheetButton.m
//  jiangXunLu
//
//  Created by lixu06 on 16/6/29.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXShareActionSheetButton.h"
#import "GlobalDefine.h"
#import "CommonTools.h"
#import "Color.h"
#import "Dimen.h"
#import "String.h"
#import "EXTScope.h"

@implementation JXShareActionSheetButton

-(instancetype)init{
    if (self = [super init]) {
        CGFloat width = 50;
        CGFloat height = 100;
        CGFloat gap = 5;
        CGFloat cornerRadius = 8;
        self.frame = RECT(0, 0, width, height);
        self.imageButton = [[UIButton alloc] initWithFrame:RECT(0, 10, width, width)];
        self.imageButton.backgroundColor = UIColorFromRGB(DEFINE_COLOR_WHITE);
        self.imageButton.layer.cornerRadius = cornerRadius;
        self.imageButton.layer.masksToBounds = YES;
        self.bottomLable = [[UILabel alloc] initWithFrame:RECT(0,
                                                               10 + width + gap,
                                                               width,
                                                               height - (10 + width + gap))];
        self.bottomLable.textColor = UIColorFromRGB(DEFINE_COLOR_GRAY);
        self.bottomLable.font = FONTS(FONTSIZE_SMALL);
        self.bottomLable.numberOfLines = 2;
        self.bottomLable.textAlignment = NSTextAlignmentCenter;
        [self.imageButton addTarget:self
                             action:@selector(buttonTouchUpInside:)
                   forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.imageButton];
        [self addSubview:self.bottomLable];
    }
    return self;
}
-(void)buttonTouchUpInside:(id)sender{
    if (self.clickBlock) {
        self.clickBlock(self.styleDic);
    }
}
-(CGFloat)viewHeight{
    id height = self.styleDic[@"height"];
    return height?[height integerValue]:100;
}
- (void)updateViewInfo:(NSDictionary *)cellDic{
    CGFloat width = self.styleDic[@"width"]?[self.styleDic[@"width"] integerValue]:50;
    CGFloat height = self.styleDic[@"height"]?[self.styleDic[@"height"] integerValue]:100;
    CGFloat gap = self.styleDic[@"gap"]?[self.styleDic[@"gap"] integerValue]:10;
    if (self.styleDic[@"textColor"]) {
        self.bottomLable.textColor = self.styleDic[@"textColor"];
    }
    if (self.styleDic[@"font"]) {
        self.bottomLable.font = self.styleDic[@"font"];
    }
    if (self.styleDic[@"numberOfLines"]) {
        self.bottomLable.numberOfLines = [self.styleDic[@"numberOfLines"] longValue];
    }
    if (self.styleDic[@"textAlignment"]) {
        self.bottomLable.textAlignment = [self.styleDic[@"textAlignment"] integerValue];
    }
    if (self.styleDic[@"buttonImage"]) {
        [self.imageButton setImage:self.styleDic[@"buttonImage"] forState:UIControlStateNormal];
    }
    if (self.styleDic[@"textValue"]) {
        self.bottomLable.text = self.styleDic[@"textValue"];
    }
    CGSize lableSize = BD_MULTILINE_TEXTSIZE(self.bottomLable.text,
                                             self.bottomLable.font,
                                             CGSizeMake(width, MAXFLOAT),
                                             NSLineBreakByWordWrapping);
    self.frame = RECT(self.frame.origin.x, self.frame.origin.y, width, height);
    self.imageButton.frame = RECT(0, 10, width, width);
    self.bottomLable.frame = RECT(0, 10 + width + gap, width, lableSize.height);
    
    if (self.styleDic[@"ActionSheetButtonTag"]) {
        self.bottomLable.tag = [self.styleDic[@"ActionSheetButtonTag"] integerValue];
    }
    
}
@end

//
//  JSCustomTableCell.m
//  jiangXunLu
//
//  Created by lixu06 on 16/6/17.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXTableViewCell.h"
#import "GlobalDefine.h"
#import "Color.h"

@implementation JXTableViewCell

+ (CGFloat)cellHeight:(id)cellData {
    return 0.0f;
}

- (void)updateCellInfo:(id)cellData onModel:(NSObject *)model{
}

- (void)didSelectAction:(id)cellData onModel:(NSObject *)model{
}


@end

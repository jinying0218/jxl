//
//  JXPhotoActionSheet.m
//  jiangXunLu
//
//  Created by lixu06 on 16/6/28.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXPhotoActionSheet.h"
#import "JXPhotoActionSheetView.h"

#import "EXTScope.h"
#import "GlobalDefine.h"
#import "CommonTools.h"
#import "Color.h"
#import "Dimen.h"
#import "String.h"

@interface JXPhotoActionSheet ()

@end

@implementation JXPhotoActionSheet

- (void)setupActionSheetView:(id)data{
    JXPhotoActionSheetView *sheetView = [JXPhotoActionSheetView defaultView];
    sheetView.styleDic = [data mutableCopy] ;
    @weakify(self)
    sheetView.takeBlock = ^(NSDictionary *kvDic){
        @strongify(self)
        [self buttonClicked:JXPhotoActionSheetTagViewTake];
    };
    sheetView.chooseBlock = ^(NSDictionary *kvDic){
        @strongify(self)
        [self buttonClicked:JXPhotoActionSheetTagViewChoose];
    };
    sheetView.cancelBlock = ^(NSDictionary *kvDic){
        @strongify(self)
        [self buttonClicked:JXPhotoActionSheetTagViewCancel];
    };
    CGFloat height = [JXPhotoActionSheetView viewHeight:sheetView.styleDic];
    self.bgViewHideFrame = CGRectMake(0, DEVICE_HEIGHT, DEVICE_WIDTH, height);
    self.bgViewShowFrame = CGRectMake(0, self.frame.size.height - height, self.frame.size.width, height);
    sheetView.frame = self.bgViewHideFrame;
    [sheetView updateViewInfo:sheetView.styleDic];
    self.sheetView = sheetView;
    [self addSubview:self.sheetView];
}

@end

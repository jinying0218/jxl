//
//  JXShareActionSheetButton.h
//  jiangXunLu
//
//  Created by lixu06 on 16/6/29.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXView.h"

@interface JXShareActionSheetButton : JXView

@property (strong, nonatomic) UIButton *imageButton;
@property (strong, nonatomic) UILabel *bottomLable;
typedef void(^ShareActionSheetButtonBlock)(NSDictionary *kvDic);
@property (nonatomic, copy) ShareActionSheetButtonBlock clickBlock;

@end

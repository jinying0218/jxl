//
//  JXPickerActionSheet.h
//  jiangXunLu
//
//  Created by lixu06 on 16/6/29.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXCustomActionSheet.h"

@interface JXPickerActionSheet : JXCustomActionSheet

@property (nonatomic, strong) NSMutableArray *pickerData;
@property (nonatomic, strong) NSMutableArray *pickerIndex;

+(NSDictionary *)getLocationStyleDic;
+(NSString *)getLocationValueWithData:(NSArray *)pickerData index:(NSArray *)pickerIndex;

@end

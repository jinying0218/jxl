//
//  JXPhotoActionSheetView.h
//  jiangXunLu
//
//  Created by lixu06 on 16/6/28.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JXView.h"

@interface JXPhotoActionSheetView : JXView

typedef NS_ENUM(NSInteger, JXPhotoActionSheetViewTag) {
    /** 取消 */
    JXPhotoActionSheetTagViewCancel             = 1,
    /** 拍摄 */
    JXPhotoActionSheetTagViewTake               = 2,
    /** 选择相册 */
    JXPhotoActionSheetTagViewChoose             = 3
};

@property (weak, nonatomic) IBOutlet UIImageView *seperatorView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *seperatorVewHeight;
@property (weak, nonatomic) IBOutlet UIButton *cancellBotton;
@property (weak, nonatomic) IBOutlet UIButton *takeButton;
@property (weak, nonatomic) IBOutlet UIButton *chooseButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *seperatorGap;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *takeGap;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cancelGap;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *chooseGap;

- (IBAction)buttonTouchUpInside:(id)sender;

typedef void(^PhotoActionSheetBlock)(NSDictionary *kvDic);

@property (nonatomic, copy) PhotoActionSheetBlock chooseBlock;
@property (nonatomic, copy) PhotoActionSheetBlock takeBlock;
@property (nonatomic, copy) PhotoActionSheetBlock cancelBlock;

@end

//
//  JSCustomTableCell.h
//  jiangXunLu
//
//  Created by lixu06 on 16/6/17.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JXCustomTableModel.h"

@interface JXTableViewCell : UITableViewCell

@property (nonatomic, strong) id cellData;

+ (CGFloat)cellHeight:(id)cellData;
- (void)updateCellInfo:(id)cellData onModel:(NSObject *)model;
- (void)didSelectAction:(id)cellData onModel:(NSObject *)model;

@end

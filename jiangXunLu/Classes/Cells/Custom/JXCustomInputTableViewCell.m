//
//  JXConmmonInputTableViewCell.m
//  ZXBaiduAppUIKit
//
//  Created by lixu06 on 16/6/17.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXCustomInputTableViewCell.h"
#import "NSString+SizeCalculator.h"
#import "UITextFiled+LimitLength.h"
#import "GlobalDefine.h"
#import "CommonTools.h"
#import "Color.h"
#import "Dimen.h"
#import "JXTipHud.h"
#import "NSObject+BMRuntime.h"

@interface JXCustomInputTableViewCell () <UITextFieldDelegate>

@property (strong, nonatomic) UIView *paddingView;
@property (strong, nonatomic) UILabel *leftLableView;
@property (strong, nonatomic) UITextField *inputTextField;
@property (strong, nonatomic) UIImageView *goImageView;
@property (strong, nonatomic) UIImageView *separatorView;

@end

@implementation JXCustomInputTableViewCell

#pragma mark -
#pragma mark static methods
+ (CGFloat)cellHeight:(NSDictionary *)dic{
    return 53.5;
}

#pragma mark -
#pragma mark super methods

- (void)awakeFromNib {
    // Initialization code
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        CGFloat height = 53.5;
        CGFloat paddingLeft = 0;
        CGFloat paddingRight = 0;
        CGFloat paddingTextLeft = 20;
        CGFloat paddingTextRight = 20;
        CGFloat paddingLine = 0;
        CGFloat labelWidth = 80;
        
        self.frame = RECT(0, 0, DEVICE_WIDTH, height);
        self.backgroundColor = UIColorFromRGB(DEFINE_COLOR_WHITE);
        self.selectionStyle = UITableViewCellSelectionStyleDefault;
        
        _paddingView = [[UIView alloc] initWithFrame:RECT(paddingLeft, 0, DEVICE_WIDTH - (paddingLeft + paddingRight), height)];
        _paddingView.backgroundColor = UIColorFromRGB(DEFINE_COLOR_WHITE);
        
        _leftLableView = [[UILabel alloc] initWithFrame:RECT(paddingTextLeft,
                                                             (height - 20)/2,
                                                             labelWidth,
                                                             20)];
        _leftLableView.textColor = UIColorFromRGB(DEFINE_COLOR_GRAY_DARK);
        _leftLableView.font = FONTS(FONTSIZE_MIDDLE);
        
        _inputTextField = [[UITextField alloc] initWithFrame:RECT(paddingLeft + paddingTextLeft + labelWidth,
                                                                  0,
                                                                  DEVICE_WIDTH - (paddingLeft + paddingTextLeft + labelWidth + paddingTextRight + paddingRight + 16),
                                                                  height)];
        _inputTextField.textColor = UIColorFromRGB(DEFINE_COLOR_BLACK);
        _inputTextField.font = FONTS(FONTSIZE_MIDDLE);
        _inputTextField.delegate = self;
        [_inputTextField setValue:UIColorFromRGB(DEFINE_COLOR_HINT) forKeyPath:@"_placeholderLabel.textColor"];
        
        _goImageView = [[UIImageView alloc] initWithFrame:RECT(DEVICE_WIDTH- (16 + paddingTextRight + paddingRight),
                                                               (height - 16)/2 - 3,
                                                               16,
                                                               16)];
        _goImageView.image = UIIMGName(@"arrow_r");
        
        _separatorView = [[UIImageView alloc] initWithFrame:RECT(paddingLeft + paddingLine,
                                                                 height - 0.5,
                                                                 DEVICE_WIDTH - (paddingLine + paddingLeft + paddingRight),
                                                                 0.5)];
        _separatorView.image = IMGName(@"dividing_line.png");
        
        [_paddingView addSubview:_leftLableView];
        [_paddingView addSubview:_inputTextField];
        [_paddingView addSubview:_goImageView];
        [_paddingView addSubview:_separatorView];
        [self.contentView addSubview:_paddingView];      
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(textFieldChanged:)
                                                     name:UITextFieldTextDidChangeNotification
                                                   object:nil];
        
        
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

#pragma mark -
#pragma mark extended methods

- (void)didSelectAction:(id)cellDic onModel:(NSObject *)model{
    if (cellDic) {
        if (cellDic[@"clickBlock"] && (!cellDic[@"isActive"] || ![cellDic[@"isActive"] boolValue])) {
            SEL selector = NSSelectorFromString(cellDic[@"clickBlock"]);
            [model performSelectorSEL:selector withObject:cellDic];
        }
    }
}

- (void)updateCellInfo:(NSDictionary *)cellDic onModel:(NSObject *)model{
    if (cellDic) {
        CGFloat height = 53.5;
        CGFloat labelWidth = 80;
        CGFloat paddingLeft = (cellDic[@"paddingLeft"])?[cellDic[@"paddingLeft"] floatValue]:0;
        CGFloat paddingRight = (cellDic[@"paddingRight"])?[cellDic[@"paddingRight"] floatValue]:0;
        CGFloat paddingTextLeft = (cellDic[@"paddingTextLeft"])?[cellDic[@"paddingTextLeft"] floatValue]:20;
        CGFloat paddingTextRight = (cellDic[@"paddingTextRight"])?[cellDic[@"paddingTextRight"] floatValue]:20;
        CGFloat paddingLine = (cellDic[@"paddingLine"])?[cellDic[@"paddingLine"] floatValue]:0;
        
        if(cellDic[@"isActive"] && [cellDic[@"isActive"] boolValue]){
            self.selectionStyle = UITableViewCellSelectionStyleNone;
        } else {
            self.selectionStyle = UITableViewCellSelectionStyleDefault;
        }
        
        _paddingView.frame = RECT(paddingLeft, 0, DEVICE_WIDTH - (paddingLeft + paddingRight), height);
        
        _inputTextField.textAlignment = (cellDic[@"textAlignment"])?[cellDic[@"textAlignment"] integerValue]:NSTextAlignmentLeft;
        _inputTextField.textColor = (cellDic[@"textColor"])?cellDic[@"textColor"]:UIColorFromRGB(DEFINE_COLOR_BLACK_LIGHT);
        _inputTextField.text = cellDic[@"textValue"]?cellDic[@"textValue"]:@"";
         CGSize lableSize = [cellDic[@"inputLable"] sizeWithFont:FONTS(FONTSIZE_MIDDLE)
                                                               maxSize:CGSizeMake(MAXFLOAT, MAXFLOAT)];
        if (lableSize.width == 0) {
            _inputTextField.frame = RECT(paddingLeft+paddingTextLeft,
                                         0,
                                         DEVICE_WIDTH - (paddingLeft + paddingTextLeft + labelWidth + paddingTextRight + paddingRight + 16),
                                         height);
        } else {
            _inputTextField.frame = RECT(paddingLeft + paddingTextLeft + labelWidth,
                                         0,
                                         DEVICE_WIDTH - (paddingLeft + paddingTextLeft + labelWidth + paddingTextRight + paddingRight + 16),
                                         height);
            
        }
        _leftLableView.frame = RECT(paddingTextLeft,
                                    (height - 20)/2,
                                    labelWidth,
                                    20);
        _leftLableView.text = cellDic[@"inputLable"]?cellDic[@"inputLable"]:@"";
        _inputTextField.placeholder = cellDic[@"textHint"];
        
        _goImageView.hidden = [cellDic[@"isFold"] boolValue];
        _inputTextField.userInteractionEnabled = [cellDic[@"isActive"] boolValue];
        _separatorView.hidden = [cellDic[@"isEnd"] boolValue];
        
        _separatorView.frame = RECT(paddingLeft + paddingLine,
                                    height - 0.5,
                                    DEVICE_WIDTH - (paddingLine + paddingLeft + paddingRight),
                                    0.5);
        
        NSString *changedBlock = cellDic[@"changedBlock"];
        if (changedBlock) {
            self.changedBlock = ^(NSDictionary *kvDic) {
                SEL selector = NSSelectorFromString(cellDic[@"changedBlock"]);
                [model performSelectorSEL:selector withObject:kvDic];
            };
        }
        
    }
}

#pragma mark -
#pragma mark private methods

- (void)textFieldChanged:(NSNotification *)aNotification{
    UITextField *textField = (UITextField *)aNotification.object;
    if (textField == _inputTextField) {
        long maxChar = 0;
        if (self.cellData[@"maxChar"]) {
            maxChar = [self.cellData[@"maxChar"] longValue];
        }
        if(maxChar>0){
            if (textField.text.length > maxChar) {
                NSString *alertString = (self.cellData && self.cellData[@"alertValue"])? self.cellData[@"alertValue"]:[NSString stringWithFormat:@"字数不能超过%lu", maxChar];
                [JXTipHud showTip:alertString
                     dismissAfter:0.8
                withFinishHandler:nil];
            }
            [textField limitLength:maxChar];
        }
        if(self.changedBlock){
            self.cellData[@"textValue"] = textField.text;
            self.changedBlock(@{@"textValue":textField.text,
                                @"keyName":self.cellData[@"keyName"]});
        }
    }
}

#pragma mark -
#pragma mark UITextFieldDelegate
- (BOOL)textField:(UITextField*)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [textField resignFirstResponder];
}

@end

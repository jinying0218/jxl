//
//  JXCustomSubmitTableViewCell.h
//  jiangXunLu
//
//  Created by lixu06 on 16/6/26.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITableViewCell+JXModel.h"

@interface JXCustomSubmitTableViewCell : UITableViewCell

typedef void(^CustomSubmitBlock)(NSDictionary *kvDic);

@property (nonatomic, copy) CustomSubmitBlock submitBlock;

@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonWidthLayout;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonHeightLayout;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonMarginTopLayout;

- (IBAction)submitTouchUpInside:(id)sender;

@end

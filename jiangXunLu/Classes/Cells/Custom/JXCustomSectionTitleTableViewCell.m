//
//  JXCustomSectionTitleTableViewCell.m
//  ZXBaiduAppUIKit
//
//  Created by lixu06 on 16/1/27.
//  Copyright © 2016年 百度在线. All rights reserved.
//

#import "JXCustomSectionTitleTableViewCell.h"
#import "NSString+SizeCalculator.h"
#import "GlobalDefine.h"
#import "CommonTools.h"
#import "Color.h"
#import "Dimen.h"

@interface JXCustomSectionTitleTableViewCell ()

@property (strong, nonatomic) UIView *sectionView;
@property (strong, nonatomic) UILabel *lableTextView;

@end
@implementation JXCustomSectionTitleTableViewCell

#pragma mark -
#pragma mark static methods

+ (CGFloat)cellHeight:(NSDictionary *)kvDic{
    CGFloat height = 0;
    NSString *lableValue = kvDic[@"lableValue"]?kvDic[@"lableValue"]:@"";
    
    CGFloat paddingTop =(kvDic[@"paddingTop"])?[kvDic[@"paddingTop"] floatValue]:20;
    CGFloat paddingLeft = (kvDic[@"paddingLeft"])?[kvDic[@"paddingLeft"] floatValue]:15;
    CGFloat paddingBottom = (kvDic[@"paddingBottom"])?[kvDic[@"paddingBottom"] floatValue]:5;
    CGFloat paddingRight = (kvDic[@"paddingRight"])?[kvDic[@"paddingRight"] floatValue]:15;
    
    UIFont *font = kvDic[@"font"]?kvDic[@"font"]:FONTS(FONTSIZE_SMALL);
    
    if ([lableValue length] >0) {
        CGSize lableSize = BD_MULTILINE_TEXTSIZE(lableValue,
                                                 font,
                                                 CGSizeMake(DEVICE_WIDTH - paddingLeft - paddingRight, 2000.0f),
                                                 NSLineBreakByWordWrapping);

        height += lableSize.height;
        height += paddingTop;
        height += paddingBottom;
    }
    
    return height;
    
}
#pragma mark -
#pragma mark super methods

- (void)awakeFromNib {
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        _sectionView = [[UIView alloc] initWithFrame:RECT(0, 0, DEVICE_WIDTH, 44)];
        _sectionView.backgroundColor = UIColorFromRGB(DEFINE_COLOR_WHITE);

        _lableTextView = [[UILabel alloc] initWithFrame:RECT(20, 5, DEVICE_WIDTH - 40, 44-10)];
        _lableTextView.textColor = UIColorFromRGB(0x999999);
        _lableTextView.font = FONTS(FONTSIZE_SMALL);
        _lableTextView.backgroundColor = UIColorFromRGBA(0x00000000);
        _lableTextView.numberOfLines = 0;
        [_sectionView addSubview:_lableTextView];
        self.contentView.backgroundColor = UIColorFromRGB(DEFINE_COLOR_WHITE);
        [self.contentView addSubview:_sectionView];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

#pragma mark -
#pragma mark extended methods

- (void)updateCellInfo:(NSDictionary *)cellDic onModel:(NSObject *)model{
    if (cellDic) {
        CGFloat height = 0;
        
        NSString *lableValue = cellDic[@"lableValue"]?cellDic[@"lableValue"]:@"";
        CGFloat paddingTop = (cellDic[@"paddingTop"])?[cellDic[@"paddingTop"] floatValue]:20;
        CGFloat paddingLeft = (cellDic[@"paddingLeft"])?[cellDic[@"paddingLeft"] floatValue]:20;
        CGFloat paddingBottom = (cellDic[@"paddingBottom"])?[cellDic[@"paddingBottom"] floatValue]:5;
        CGFloat paddingRight = (cellDic[@"paddingRight"])?[cellDic[@"paddingRight"] floatValue]:20;
        
        if ([lableValue length] >0) {
            UIFont *font = cellDic[@"font"]?cellDic[@"font"]:FONTS(FONTSIZE_MIDDLE);
            _lableTextView.font = font;
            CGSize lableSize = BD_MULTILINE_TEXTSIZE(lableValue,
                                  font,
                                  CGSizeMake(DEVICE_WIDTH - paddingLeft - paddingRight, 2000.0f),
                                  NSLineBreakByWordWrapping);
            height += lableSize.height;
            height += paddingTop;
            height += paddingBottom;
            
            _sectionView.frame = RECT(0, 0, DEVICE_WIDTH , height);
            _lableTextView.frame = RECT(paddingLeft , paddingTop, DEVICE_WIDTH - (paddingLeft + paddingRight), lableSize.height);
        } else {
            height = 0;
        }
        _sectionView.backgroundColor = (cellDic[@"backgroundColor"])?cellDic[@"backgroundColor"]:UIColorFromRGB(DEFINE_COLOR_WHITE);
        _lableTextView.textColor = (cellDic[@"textColor"])?cellDic[@"textColor"]:UIColorFromRGB(0x999999);
        _lableTextView.text = cellDic[@"lableValue"];
//        _lableTextView.backgroundColor = UIColorFromRGB(0x654321);
    }
}

@end

//
//  JXCustomRefreshHeaderView.h
//  jiangXunLu
//
//  Created by lixu06 on 16/7/1.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXView.h"

@interface JXCustomRefreshView : JXView
@property (weak, nonatomic) IBOutlet UILabel *refreshLabel;
@property (weak, nonatomic) IBOutlet UIImageView *refreshImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftMargin;

@end

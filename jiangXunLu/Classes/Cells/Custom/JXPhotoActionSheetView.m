//
//  JXPhotoActionSheetView.m
//  jiangXunLu
//
//  Created by lixu06 on 16/6/28.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXPhotoActionSheetView.h"
#import "GlobalDefine.h"
#import "CommonTools.h"
#import "Color.h"
#import "Dimen.h"
#import "String.h"

#define PHOTO_ACTION_SHEET_BUTTON_WIDTH      100.0f
#define PHOTO_ACTION_SHEET_BUTTON_HEIGHT     30.0f
#define PHOTO_ACTION_SHEET_LINE_GAP          120.0f
#define PHOTO_ACTION_SHEET_TAKE_GAP          25.0f
#define PHOTO_ACTION_SHEET_CANCEL_GAP        25.0f
#define PHOTO_ACTION_SHEET_CHOOSE_GAP        15.0f
#define PHOTO_ACTION_SHEET_BUTTON_CORNER     5.0f

@interface JXPhotoActionSheetView ()

@end

@implementation JXPhotoActionSheetView

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.buttonWidth.constant = PHOTO_ACTION_SHEET_BUTTON_WIDTH;
    self.buttonHeight.constant = PHOTO_ACTION_SHEET_BUTTON_HEIGHT;
    self.seperatorGap.constant = PHOTO_ACTION_SHEET_LINE_GAP;
    self.takeGap.constant = PHOTO_ACTION_SHEET_TAKE_GAP;
    self.cancelGap.constant = PHOTO_ACTION_SHEET_CANCEL_GAP;
    self.chooseGap.constant = PHOTO_ACTION_SHEET_CHOOSE_GAP;
    UIColor *backgroundColor = UIColorFromRGB(DEFINE_COLOR_BACK);
    UIColor *buttonBackgroundColor = UIColorFromRGB(DEFINE_COLOR_YELLOW);
    UIColor *buttonTextColor = UIColorFromRGB(DEFINE_COLOR_WHITE);
    UIFont *buttonFont = FONTS(FONTSIZE_MEDIA);
    
    self.backgroundColor = backgroundColor;
    
    [self.chooseButton setTitle:STRING_PHOTO_ACTION_SHEET_CHOOSE forState:UIControlStateNormal];
    self.chooseButton.backgroundColor = buttonBackgroundColor;
    [self.chooseButton setTitleColor:buttonTextColor forState:UIControlStateNormal];
    self.chooseButton.titleLabel.font = buttonFont;
    self.chooseButton.layer.cornerRadius = PHOTO_ACTION_SHEET_BUTTON_CORNER;
    self.chooseButton.layer.masksToBounds = YES;
    self.chooseButton.tag = JXPhotoActionSheetTagViewChoose;
    
    [self.takeButton setTitle:STRING_PHOTO_ACTION_SHEET_TAKE forState:UIControlStateNormal];
    self.takeButton.backgroundColor = buttonBackgroundColor;
    [self.takeButton setTitleColor:buttonTextColor forState:UIControlStateNormal];
    self.takeButton.titleLabel.font = buttonFont;
    self.takeButton.layer.cornerRadius = PHOTO_ACTION_SHEET_BUTTON_CORNER;
    self.takeButton.layer.masksToBounds = YES;
    self.takeButton.tag = JXPhotoActionSheetTagViewTake;
    
    [self.cancellBotton setTitle:STRING_PHOTO_ACTION_SHEET_CANCEL forState:UIControlStateNormal];
    self.cancellBotton.backgroundColor = buttonBackgroundColor;
    [self.cancellBotton setTitleColor:buttonTextColor forState:UIControlStateNormal];
    self.cancellBotton.titleLabel.font = buttonFont;
    self.cancellBotton.layer.cornerRadius = PHOTO_ACTION_SHEET_BUTTON_CORNER;
    self.cancellBotton.layer.masksToBounds = YES;
    self.cancellBotton.tag = JXPhotoActionSheetTagViewCancel;
}

+ (CGFloat)viewHeight:(NSDictionary *)kvDic{
    return DEVICE_HEIGHT / 2;
}
- (void)updateViewInfo:(NSDictionary *)cellDic{
    
}

- (IBAction)buttonTouchUpInside:(id)sender {
    switch (((UIButton *)sender).tag) {
        case JXPhotoActionSheetTagViewTake:{
            if (self.takeBlock) {
                self.takeBlock(self.styleDic);
            }
            break;
        }
        case JXPhotoActionSheetTagViewChoose:{
            if (self.chooseBlock) {
                self.chooseBlock(self.styleDic);
            }
            break;
        }
        case JXPhotoActionSheetTagViewCancel:{
            if (self.cancelBlock) {
                self.cancelBlock(self.styleDic);
            }
            break;
        }
        default:
            break;
    }
}

@end

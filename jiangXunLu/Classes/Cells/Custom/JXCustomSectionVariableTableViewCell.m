//
//  JXCustomSectionMiniTableViewCell.m
//  ZXBaiduAppUIKit
//
//  Created by lixu06 on 16/4/5.
//  Copyright © 2016年 百度在线. All rights reserved.
//

#import "JXCustomSectionVariableTableViewCell.h"
#import "GlobalDefine.h"
#import "CommonTools.h"
#import "Color.h"
#import "Dimen.h"

@interface JXCustomSectionVariableTableViewCell ()

@property (strong, nonatomic) UIView *sectionView;
@property (strong, nonatomic) UIImageView *separatorView;

@end

@implementation JXCustomSectionVariableTableViewCell

#pragma mark -
#pragma mark static methods

+ (CGFloat)cellHeight:(NSDictionary *)dic{
    return (dic[@"height"])?[dic[@"height"] floatValue]:10;
}

#pragma mark -
#pragma mark super methods

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        CGFloat height = 10;
        self.sectionView = [[UIView alloc] initWithFrame:RECT(0, 0, DEVICE_WIDTH, height)];
        self.sectionView.backgroundColor = UIColorFromRGB(DEFINE_COLOR_WHITE);
        
        [self.contentView addSubview:self.sectionView];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.separatorView = [[UIImageView alloc] initWithFrame:RECT(0,
                                                                 height - 0.5,
                                                                 DEVICE_WIDTH,
                                                                 0.5)];
        self.separatorView.image = IMGName(@"dividing_line.png");
        [self.contentView addSubview:self.separatorView];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

#pragma mark -
#pragma mark extended methods

- (void)updateCellInfo:(id)cellDic onModel:(NSObject *)model{
    if (cellDic) {
        self.sectionView.backgroundColor = (cellDic[@"backgroundColor"])?cellDic[@"backgroundColor"]:UIColorFromRGB(DEFINE_COLOR_BACK);
        CGFloat height = 10;
        if (cellDic[@"height"]) {
            height = [cellDic[@"height"] floatValue];
            self.sectionView.frame = RECT(0, 0, DEVICE_WIDTH, height);
        }
        
        self.separatorView.frame = RECT(0,
                                        height - 0.5,
                                        DEVICE_WIDTH,
                                        0.5);
        if (cellDic[@"isEnd"]) {
            self.separatorView.hidden = [cellDic[@"isEnd"] boolValue];
        } else{
            self.separatorView.hidden = YES;
        }
    }
}


@end

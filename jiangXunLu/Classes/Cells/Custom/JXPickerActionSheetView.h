//
//  JXPickerActionSheetView.h
//  jiangXunLu
//
//  Created by lixu06 on 16/6/29.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXView.h"

@interface JXPickerActionSheetView : JXView

typedef NS_ENUM(NSInteger, JXPickerActionSheetViewTag) {
    /** 取消 */
    JXPickerActionSheetViewTagCancel               = 0,
    /** 确认 */
    JXPickerActionSheetViewTagConfirm              = 1,
};

typedef NS_ENUM(NSInteger, JXPickerActionSheetMode) {
    /** 平级 */
    JXPickerActionSheetModeMulti               = 0,
    /** 子集 */
    JXPickerActionSheetModeChild               = 1,
};

@property (weak, nonatomic) IBOutlet UIButton *leftButton;
@property (weak, nonatomic) IBOutlet UIButton *rightButton;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;

typedef void(^PickerActionSheetBlock)(NSDictionary *kvDic);
@property (nonatomic, copy) PickerActionSheetBlock confirmBlock;
@property (nonatomic, copy) PickerActionSheetBlock cancelBlock;



- (IBAction)buttonTouchUpInside:(id)sender;


@end

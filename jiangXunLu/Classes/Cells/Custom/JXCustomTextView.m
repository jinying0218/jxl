//
//  JXCustomTextView.m
//  jiangXunLu
//
//  Created by lixu06 on 16/6/27.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXCustomTextView.h"
#import "NSString+SizeCalculator.h"
#import "UITextView+LimitLength.h"
#import "GlobalDefine.h"
#import "CommonTools.h"
#import "Color.h"
#import "Dimen.h"
#import "JXTipHud.h"

#define JXCustomTextViewTagHint 0
#define JXCustomTextViewTagText 1
@interface JXCustomTextView ()<UITextViewDelegate>


@end

@implementation JXCustomTextView

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.textView.delegate = self;
    self.textView.tag = JXCustomTextViewTagText;
    self.hintView.delegate = self;
    self.hintView.tag = JXCustomTextViewTagHint;
    
    self.hintView.userInteractionEnabled = NO;
    self.hintView.editable = NO;
    
    self.textView.showsVerticalScrollIndicator = NO;
    self.hintView.showsVerticalScrollIndicator = NO;
}

+ (CGFloat)viewHeight:(NSDictionary *)cellDic {
    CGFloat height = 250.0f;
    if (cellDic[@"height"]) {
        height = [cellDic[@"height"] floatValue];
    }
    if (cellDic[@"heightFlexible"]&&[cellDic[@"heightFlexible"] boolValue]) {
        NSString *textValue = cellDic[@"textValue"]?cellDic[@"textValue"]:@"";
        UIFont *font = cellDic[@"textFont"]?cellDic[@"textFont"]:FONTS(FONTSIZE_MEDIA);
        CGFloat marginTop = cellDic[@"marginTop"]?[cellDic[@"marginTop"] floatValue]:7.0f;
        CGFloat marginBottom = cellDic[@"marginBottom"]?[cellDic[@"marginBottom"] floatValue]:7.0f;
        CGFloat marginLeft = cellDic[@"marginLeft"]?[cellDic[@"marginLeft"] floatValue]:7.0f;
        CGFloat marginRight = cellDic[@"marginRight"]?[cellDic[@"marginRight"] floatValue]:7.0f;
        
        CGFloat insetTop = cellDic[@"insetTop"]?[cellDic[@"insetTop"] floatValue]:5.0f;
        CGFloat insetBottom = cellDic[@"insetBottom"]?[cellDic[@"insetBottom"] floatValue]:5.0f;
        CGFloat insetLeft = cellDic[@"insetLeft"]?[cellDic[@"insetLeft"] floatValue]:7.0f;
        CGFloat insetRight = cellDic[@"insetRight"]?[cellDic[@"insetRight"] floatValue]:7.0f;
        
        CGFloat textInset = 5;
        
        CGFloat constWidth = marginLeft+8+marginRight+8+insetLeft+insetRight  + textInset*2;
        CGFloat constHeight = marginTop+8+marginBottom+8+insetTop+insetBottom + textInset*2;
        CGSize textSize = BD_MULTILINE_TEXTSIZE(textValue,
                                                font,
                                                CGSizeMake(DEVICE_WIDTH-constWidth, MAXFLOAT),
                                                NSLineBreakByWordWrapping);
        if (textSize.height+constHeight > height) {
            height = textSize.height+constHeight;
        }
    }
    return height;
}

- (void)updateViewInfo:(NSDictionary *)cellDic {
    if (!cellDic) {
        return;
    }
    self.backgroundColor = cellDic[@"backgroundColor"]?cellDic[@"backgroundColor"]:UIColorFromRGB(DEFINE_COLOR_BACK);
    UIColor *textBackgroundColor = cellDic[@"textBackgroundColor"]?cellDic[@"textBackgroundColor"]:UIColorFromRGB(DEFINE_COLOR_WHITE);
    [self.backView setBackgroundColor:textBackgroundColor];
    self.marginTopLayout.constant = cellDic[@"marginTop"]?[cellDic[@"marginTop"] floatValue]:7.0f;
    self.marginLeftLayout.constant = cellDic[@"marginLeft"]?[cellDic[@"marginLeft"] floatValue]:7.0f;
    self.marginRightLayout.constant = cellDic[@"marginRight"]?[cellDic[@"marginRight"] floatValue]:7.0f;
    self.marginBottomLayout.constant = cellDic[@"marginBottom"]?[cellDic[@"marginBottom"] floatValue]:7.0f;
    NSString *textValue = cellDic[@"textValue"]?cellDic[@"textValue"]:@"";
    self.textView.text = textValue;
    NSString *hintValue = cellDic[@"hintValue"]?cellDic[@"hintValue"]:@"";
    self.hintView.text = (0 == self.textView.text.length) ?  hintValue: @"";
    UIColor *textColor = cellDic[@"textColor"]?cellDic[@"textColor"]:UIColorFromRGB(DEFINE_COLOR_BLACK_LIGHT);
    self.textView.textColor = textColor;
    UIColor *hintColor = cellDic[@"hintColor"]?cellDic[@"hintColor"]:UIColorFromRGB(DEFINE_COLOR_GRAY);
    self.hintView.textColor = hintColor;
    CGFloat cornerRadius = cellDic[@"buttonCornerRadius"]?[cellDic[@"buttonCornerRadius"] floatValue]:5;
    self.backView.layer.cornerRadius = cornerRadius;
    self.backView.layer.cornerRadius = cornerRadius;
    UIFont *font = cellDic[@"textFont"]?cellDic[@"textFont"]:FONTS(FONTSIZE_MEDIA);
    self.textView.font = font;
    self.hintView.font = font;
    self.textView.userInteractionEnabled = cellDic[@"userInteractionEnabled"]?[cellDic[@"userInteractionEnabled"] boolValue]:YES;
    self.textView.editable = cellDic[@"editable"]?[cellDic[@"editable"] boolValue]:YES;
}

#pragma mark -
#pragma mark private methods

- (void)characterCheck{
    self.hintView.hidden = !(0 == self.textView.text.length);
    long maxChar = 0;
    if (self.styleDic[@"maxChar"]) {
        maxChar = [self.styleDic[@"maxChar"] longValue];
    }
    if (maxChar > 0) {
        if (self.textView.text.length> maxChar ) {
            NSString *alertString = (self.styleDic && self.styleDic[@"alertValue"])? self.styleDic[@"alertValue"]:[NSString stringWithFormat:@"字数不能超过%lu", maxChar];
            [JXTipHud showTip:alertString
                 dismissAfter:0.8
            withFinishHandler:nil];
            [self.textView limitLengthTo:maxChar];
        }
    }
}

- (void)execTextChangedBlock:(id) sender{
    UITextView *textView = (UITextView *) sender;
    [self characterCheck];
    if(self.changedBlock){
        self.styleDic[@"textValue"] = textView.text;
        long leftCount = [self.styleDic[@"maxChar"] longValue] - self.textView.text.length;
        if (leftCount<0) {
            leftCount = 0;
        }
        self.changedBlock(self.styleDic);
    }
}

#pragma mark -
#pragma mark UItextViewDelegate

- (void)textViewDidChange:(UITextView *)textView{
    switch (textView.tag) {
        case JXCustomTextViewTagText:{
            [self execTextChangedBlock:textView];
            break;
        }
        default:
            break;
    }
    
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    switch (textView.tag) {
        case JXCustomTextViewTagText:{
            return YES;
        }
        default:
            break;
    }
    return NO;

}

- (void)textViewDidEndEditing:(UITextView *)textView{
    switch (textView.tag) {
        case JXCustomTextViewTagText:{
            [self execTextChangedBlock:textView];
            [textView resignFirstResponder];
            break;
        }
        default:
            break;
    }
    
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    switch (textView.tag) {
        case JXCustomTextViewTagText:{
            BOOL editable = self.styleDic[@"editable"]?[self.styleDic[@"editable"] boolValue]:YES;
            return editable;
        }
        default:
            break;
    }
    return NO;
}

@end

//
//  JXListActionSheetView.h
//  jiangXunLu
//
//  Created by lixu06 on 16/6/29.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXView.h"

@interface JXListActionSheetView : JXView

typedef NS_ENUM(NSInteger, JXListActionSheetViewTag) {
    /** 取消 */
    JXListActionSheetViewTagCancel              = 0,
};

@property (weak, nonatomic) IBOutlet UIView *buttonView;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
- (IBAction)cancelTouchUpInside:(id)sender;

typedef void(^ShareActionSheetBlock)(NSDictionary *kvDic);
@property (nonatomic, copy) ShareActionSheetBlock clickBlock;
@property (nonatomic, copy) ShareActionSheetBlock cancelBlock;

@end

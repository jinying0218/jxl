//
//  JXCollectListTableViewCell.h
//  jiangXunLu
//
//  Created by lixu06 on 16/7/18.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITableViewCell+JXModel.h"

@interface JXCollectListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *avitarImg;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLable;
@property (weak, nonatomic) IBOutlet UILabel *companyLable;
@property (weak, nonatomic) IBOutlet UILabel *countLable;
@property (weak, nonatomic) IBOutlet UIImageView *fireImg;
@property (weak, nonatomic) IBOutlet UIImageView *seperator;

@end

//
//  JXCollectListTableViewCell.m
//  jiangXunLu
//
//  Created by lixu06 on 16/7/18.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXCollectListTableViewCell.h"
#import "CommonTools.h"
#import "GlobalDefine.h"
#import "NSObject+BMRuntime.h"
#import "UIImageView+WebCache.h"

@implementation JXCollectListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
//    self.avitarImg.layer.cornerRadius = 46/2;
//    self.avitarImg.layer.masksToBounds = YES;
    self.avitarImg.image = UIIMGName(@"logo_default");
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
+ (CGFloat)cellHeight:(id)cellData {
    return 68;
}
- (void)updateCellInfo:(id)cellData onModel:(NSObject *)model{
    
    NSDictionary *infoData = cellData[@"infoData"];
    UIImage *defaultIcon = UIIMGName(@"logo_default");
    if (infoData[@"head"] && [CommonTools isValidProperty:infoData[@"head"]]) {
        NSURL *picUrl =  IMGURL(infoData[@"head"]);
        [self.avitarImg sd_setImageWithURL:picUrl placeholderImage:defaultIcon];
    } else {
        [self.avitarImg setImage:defaultIcon];
    }
    self.countLable.text = [CommonTools getValidText:infoData[@"collected_num"] withPlaceHolder:@(0)];
    NSString *showFlag = [CommonTools getValidText:infoData[@"showFlag"] withPlaceHolder:@(0)];
    self.phoneLable.text = [CommonTools getValidText:infoData[@"phone"] withPlaceHolder:@""];
    self.phoneLable.hidden = !(([showFlag integerValue] == 1));
    self.nameLabel.text = [CommonTools getValidText:infoData[@"name"] withPlaceHolder:@""];
    self.companyLable.text = [CommonTools getValidText:infoData[@"company"] withPlaceHolder:@""];

}
- (void)didSelectAction:(id)cellData onModel:(NSObject *)model{
    if (cellData[@"clickBlock"]) {
        [model performSelectorNamed:cellData[@"clickBlock"] withObject:self.cellData];
    }
    
}
@end

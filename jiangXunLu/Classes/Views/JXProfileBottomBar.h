//
//  JXProfileBottomBar.h
//  jiangXunLu
//
//  Created by lixu06 on 16/6/19.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JXView.h"

@interface JXProfileBottomBar : JXView

typedef void(^ProfileBottomBarClickBlock)(id data);

@property (nonatomic, strong) id viewData;

@property (weak, nonatomic) IBOutlet UIButton *firstBtn;
@property (weak, nonatomic) IBOutlet UIButton *secondBtn;
@property (weak, nonatomic) IBOutlet UIButton *thirdBtn;

@property (nonatomic, copy) ProfileBottomBarClickBlock firstBlock;
@property (nonatomic, copy) ProfileBottomBarClickBlock secondBlock;
@property (nonatomic, copy) ProfileBottomBarClickBlock thirdBlock;

- (void)setUpInset;
- (void)setUpCollect:(BOOL)isCollected;

@end

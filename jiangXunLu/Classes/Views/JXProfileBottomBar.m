//
//  JXProfileBottomBar.m
//  jiangXunLu
//
//  Created by lixu06 on 16/6/19.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXProfileBottomBar.h"
#import "CommonTools.h"
#import "GlobalDefine.h"
#import "Color.h"
#import "NSString+SizeCalculator.h"

@implementation JXProfileBottomBar

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setUpInset];
}

- (IBAction)firstTouchUpInside:(id)sender {
    if (self.firstBlock) {
        self.firstBlock(self.viewData);
    }
}
- (IBAction)secondTouchUpInside:(id)sender {
    if (self.secondBlock) {
        self.secondBlock(self.viewData);
    }
}
- (IBAction)thirdTouchUpInside:(id)sender {
    if (self.thirdBlock) {
        self.thirdBlock(self.viewData);
    }
}
-(void)setUpInset{
    [self setButtonImageUpXLableDownStyle:self.firstBtn];
    [self setButtonImageUpXLableDownStyle:self.secondBtn];
    [self setButtonImageUpXLableDownStyle:self.thirdBtn];
}

- (void)setUpCollect:(BOOL)isCollected{
    if(isCollected){
        [self.firstBtn setTitle:@"已收藏" forState:UIControlStateNormal];
        [self.firstBtn setTitleColor:UIColorFromRGB(DEFINE_COLOR_YELLOW) forState:UIControlStateNormal];
        [self.firstBtn setImage:UIIMGName(@"tab_sc_light") forState:UIControlStateNormal];
    } else{
        [self.firstBtn setTitle:@"收藏" forState:UIControlStateNormal];
        [self.firstBtn setTitleColor:UIColorFromRGB(DEFINE_COLOR_WHITE) forState:UIControlStateNormal];
        [self.firstBtn setImage:UIIMGName(@"tab_sc_normal") forState:UIControlStateNormal];
    }
}

- (void)setButtonImageUpXLableDownStyle:(UIButton *)btn{
    CGFloat btnWidth = 50;
    CGFloat btnHeight = 50;
    CGSize strSize = btn.titleLabel.bounds.size;
    CGFloat totalLen = 26;
    CGFloat imageSize = 26;
    if (totalLen < strSize.width) {
        totalLen = strSize.width;
    }
    CGFloat topEdge = 5;
    CGFloat bottomEdge = 5;
    CGFloat imageEdgeLen = (btnWidth - imageSize) / 2;
    CGFloat titleEdgeLen = (btnWidth - totalLen) / 2;
    btn.imageEdgeInsets = UIEdgeInsetsMake(topEdge, imageEdgeLen, btnHeight - imageSize - bottomEdge,imageEdgeLen);
    btn.titleEdgeInsets = UIEdgeInsetsMake(btnHeight - strSize.height - bottomEdge,
                                           - totalLen + (imageEdgeLen - titleEdgeLen),
                                           bottomEdge,
                                           0);
}

@end

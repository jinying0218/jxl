//
//  JXProfileHeaderView.m
//  jiangXunLu
//
//  Created by lixu06 on 16/6/24.
//  Copyright © 2016年 teamOne. All rights reserved.
//

#import "JXProfileHeaderView.h"
#import "GlobalDefine.h"
#import "Color.h"
#import "String.h"

#import "CommonTools.h"
#import "UIButton+WebCache.h"

@implementation JXProfileHeaderView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor = [UIColor clearColor];
    self.tintColor = [UIColor clearColor];
    self.infoView.backgroundColor = UIColorFromRGBA(DEFINE_COLOR_TRANSBLACK);
    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(editTapAction:)];
    [self.infoView addGestureRecognizer:singleTap1];
    
    self.avitarImg.layer.cornerRadius = 30;
    self.avitarImg.layer.borderColor = UIColorFromRGB(DEFINE_COLOR_DIVIDE_LING).CGColor;
    self.avitarImg.layer.borderWidth = 0;
    self.avitarImg.layer.masksToBounds = YES;
}

+ (CGFloat)viewHeight:(id)cellData{
    return 250.0f;
}

- (void)updateViewInfo:(NSDictionary *)cellData{
    if(cellData){
        if (cellData[@"editable"] && [cellData[@"editable"] boolValue]) {
            self.phoneImg.hidden = YES;
        }else{
            self.phoneImg.hidden = YES;
        }
        if (cellData[@"name"]) {
            self.nameLabel.text = [NSString stringWithFormat:@"%@",cellData[@"name"]];
        }
        if (cellData[@"position"]) {
            self.titleLabel.text = [NSString stringWithFormat:@"%@",cellData[@"position"]];
        }
        if (cellData[@"phone"]) {
            if(cellData[@"showFlag"]){
                if ([cellData[@"showFlag"] integerValue] == 0) {
//                    if ([cellData[@"editable"] boolValue] == NO) {
                        self.phoneLabel.text = @" ";
//                    } else {
//                        self.phoneLabel.text = [NSString stringWithFormat:@"%@",cellData[@"phone"]];
//                    }
                } else {
                    self.phoneLabel.text = [NSString stringWithFormat:@"%@",cellData[@"phone"]];
                }
            } else {
                self.phoneLabel.text = [NSString stringWithFormat:@"%@",cellData[@"phone"]];
            }
        }
        if (cellData[@"location"]) {
            self.locationLabel.text = [NSString stringWithFormat:@"%@",cellData[@"location"]];
        }
        if (cellData[@"hotCount"]) {
            self.countLabel.text = [NSString stringWithFormat:@"%@",cellData[@"hotCount"]];
        }
        if ([cellData[@"gender"] integerValue] == 2) {  // female
            self.sexImg.hidden = NO;
            self.sexImg.image = UIIMGName(@"sex_woman");
        } else if ([cellData[@"gender"] integerValue] == 1) { // male
            self.sexImg.hidden = NO;
            self.sexImg.image = UIIMGName(@"sex_man");
        } else {
            self.sexImg.hidden = YES;
        }
        UIImage *defaultIcon = UIIMGName(@"logo_default");
        if (cellData[@"avitar"]) {
            if ([cellData[@"avitar"] isKindOfClass:[NSString class]]) {
                if ([STRING_PROFILE_AVITAR_DEFAULT isEqualToString:cellData[@"avitar"]]) {
                    [self.avitarImg setImage: defaultIcon forState:UIControlStateNormal];
                } else {
                    NSURL *picUrl =  IMGURL(cellData[@"avitar"]);
                    UIImage *defaultIcon = UIIMGName(@"logo_default");
                    [self.avitarImg sd_setImageWithURL:picUrl forState:UIControlStateNormal placeholderImage:defaultIcon];
                }
            } else if ([cellData[@"avitar"] isKindOfClass:[UIImage class]]){
                [self.avitarImg setImage:cellData[@"avitar"] forState:UIControlStateNormal];
            }
        } else {
            [self.avitarImg setImage: defaultIcon forState:UIControlStateNormal];
        }
    }
}


- (void)editTapAction:(id)sender {
//    if(!self.styleDic){
//        return;
//    }
//    if (self.styleDic[@"editable"] && [self.styleDic[@"editable"] boolValue]) {
//        if (self.editBlock) {
//            self.editBlock(self.styleDic);
//        }
//    }
    
}

- (IBAction)avitarTouchInside:(id)sender{
    if(!self.styleDic){
        return;
    }
    if (self.styleDic[@"editable"] && [self.styleDic[@"editable"] boolValue]) {
        if (self.avitarBlock) {
            self.avitarBlock(self.styleDic);
        }
    }
    
}

@end

//
//  TSMessageViewController.swift
//  TSWeChat
//
//  Created by Hilen on 11/5/15.
//  Copyright © 2015 Hilen. All rights reserved.
//

import UIKit
import SwiftyJSON
import MJExtension
import SnapKit

class TSMessageViewController: BaseViewController {
    @IBOutlet private weak var listTableView: UITableView!
    private var itemDataSouce = [MessageModel]()
    var actionFloatView: TSMessageActionFloatView!
    
    var chatList:[AnyObject] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLeftBackButtonItem()
        //self.setRightButtonItem()
        
        //Init ActionFloatView
        self.actionFloatView = TSMessageActionFloatView()
        self.actionFloatView.delegate = self
        self.view.addSubview(self.actionFloatView)
        self.actionFloatView.snp_makeConstraints { (make) -> Void in
            make.edges.equalTo(UIEdgeInsetsMake(64, 0, 0, 0))
        }
        
        //Init listTableView
        self.listTableView.registerNib(TSMessageTableViewCell.NibObject(), forCellReuseIdentifier: TSMessageTableViewCell.identifier)
        self.listTableView.estimatedRowHeight = 65
        self.listTableView.tableFooterView = UIView()
        self.listTableView.separatorStyle = .None
        
        //
        AppDelegate.shareDelegate.refreshMessageList = {
            let delayInSeconds:Int64 =  Int64(Double(NSEC_PER_SEC) * 0.2)
            let popTime:dispatch_time_t = dispatch_time(DISPATCH_TIME_NOW,delayInSeconds)
            dispatch_after(popTime, dispatch_get_main_queue(), {
                self.fetchData()
            })
            self.setLeftBackButtonItem()
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.actionFloatView.hide(true)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.refresh()
        self.setLeftBackButtonItem()
    }
    
    private func setLeftBackButtonItem(){
        let unreadmsg_num = AppDelegate.shareDelegate.num_unreadmsg
        let newTitle = unreadmsg_num > 0 ? self.title! + "(" + String(unreadmsg_num) + ")" : self.title!
        let backBtnItem = UIBarButtonItem(title: newTitle, style: .Plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = backBtnItem
    }
    
    private func setRightButtonItem(){
        //右上角“＋”功能暂且屏蔽
        self.navigationItem.rightButtonAction(UIImage(named: "barbuttonicon_add")!) { (Void) -> Void in
            self.actionFloatView.hide(!self.actionFloatView.hidden)
        }
    }
    
    func refresh() {
        self.fetchData()
        self.refreshBadgeValue()
    }
    
    private func refreshBadgeValue() {
        if AppDelegate.shareDelegate.num_unreadmsg > 0 {
            self.tabBarItem.badgeValue = String(AppDelegate.shareDelegate.num_unreadmsg)
        } else {
            self.tabBarItem.badgeValue = nil
        }
    }
    
    private func fetchData() {
        self.itemDataSouce.removeAll()
        guard let messageList = DataBase.shareDataBase.fetchAllMessage() else { return }
        for dict in Message.mj_keyValuesArrayWithObjectArray(messageList) {
            guard let model = TSMapper<MessageModel>().map(dict) else { continue }
            if model.nickname != nil {
                self.itemDataSouce.insert(model, atIndex: 0)
            }
        }
        self.listTableView.reloadData()
    }
    
    deinit {
        log.verbose("deinit")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

// MARK: - @protocol UITableViewDelegate
extension TSMessageViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        let viewController = TSChatViewController.initFromNib() as! TSChatViewController
        viewController.messageModel = self.itemDataSouce[indexPath.row]
        self.ts_pushAndHideTabbar(viewController)
    }
    
    func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle{
        return .Delete
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {

        let remove = UITableViewRowAction(style: .Normal, title: "删除") { action, index in
            print("删除消息，更新当前提示")
            let message = self.itemDataSouce[indexPath.row]
            let unread_num = Int(message.unreadNumber!)
            AppDelegate.shareDelegate.num_unreadmsg -= unread_num
            DataBase.shareDataBase.deleteMessageWithPid(message.chatId!)
            self.itemDataSouce.removeAtIndex(indexPath.row)
            self.listTableView.reloadData()
            self.refreshBadgeValue()
        }
        remove.backgroundColor = UIColor(red: 0.918, green: 0.796, blue: 0.129, alpha: 1)
        return [remove]
    }

}

// MARK: - @protocol UITableViewDataSource
extension TSMessageViewController: UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.itemDataSouce.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return self.listTableView.estimatedRowHeight
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(TSMessageTableViewCell.identifier, forIndexPath: indexPath) as! TSMessageTableViewCell
        cell.setCellContnet(self.itemDataSouce[indexPath.row])
        tableView.addLineforPlainCell(cell, forRowAtIndexPath: indexPath, withLeftSpace: 15)
        return cell
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 30:0
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let view = UIView(frame: CGRect(x: 0, y: 0, width: 500, height: 30))
            let bottomLine = UIView(frame: CGRect(x: 0, y: 30, width: 500, height: 0.4))
            bottomLine.backgroundColor = UIColor.lightGrayColor()
            view.addSubview(bottomLine)
            
            let titleLabel = UILabel(frame: CGRect(x: 10, y: 1, width: 100, height: 29))
            titleLabel.text = "最新联系人"
            titleLabel.font = UIFont.systemFontOfSize(14)
            titleLabel.textColor = UIColor.grayColor()
            view.addSubview(titleLabel)
            view.backgroundColor = UIColor.clearColor()
            return view
        }
        return nil
    }
}

// MARK: - @protocol ActionFloatViewDelegate
extension TSMessageViewController: ActionFloatViewDelegate {
    func floatViewTapItemIndex(type: ActionFloatViewItemType) {
        log.info("floatViewTapItemIndex:\(type)")
        switch type {
        case .GroupChat:
            break
        case .AddFriend:
            break
        case .Scan:
            break
        case .Payment:
            break
        }
    }
}






















//
//  ServerMessage.swift
//  jiangXunLu
//
//  Created by liuxb on 16/7/1.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit

class ServerMessage: NSObject, NSCoding{
    var id: String?
    var pid: String?
    var uid_send: String?
    var uid_to: String?
    var type: String? //(1.text/2.image/3.sound/4.vedio)
    var content: String? //第一版本为string消息类型  //AnyObject? 现在是发图片base64编码,之后图片和语音都发byte数组
    var created_time: String? {
        didSet{
            if created_time != "" {
                self.timestamp = self.stringToTimeStamp(created_time!)
            }
        }
    }
    var timestamp: String?
    var sendStatus: NSNumber?{
        didSet{
            self.messageSendSuccessType = MessageSendSuccessType(rawValue: sendStatus!)
        }
    }
    var messageSendSuccessType:MessageSendSuccessType?
    
    override init() {
        super.init()
    }
    
    init(dict:[String:AnyObject]){
        super.init()
        setValuesForKeysWithDictionary(dict)
    }
    
    override func setValue(value: AnyObject?, forKey key: String) {
        if key == "id" {
            if let tempID = value as? NSNumber {
                id = String(tempID)
            }
        } else if key == "pid" {
            if let tempID = value as? NSNumber {
                pid = String(tempID)
            }
        } else if key == "uid_send" {
            if let tempID = value as? NSNumber {
                uid_send = String(tempID)
            }
        } else if key == "uid_to" {
            if let tempID = value as? NSNumber {
                uid_to = String(tempID)
            }
        } else if key == "content" {
            if let tempID = value as? String {
                content = String(tempID)
            } else {
                content = "[无法识别的消息内容]"
            }
        } else if key == "type" {
            if let tempID = value as? NSNumber {
                type = String(tempID)
            }
        } else {
            super.setValue(value, forKey: key)
        }
    }
    
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {
        print("提示：ServerMessage中\(key)无对应数据")
    }
    
    override var description: String{
        let properties = ["id","pid","uid_send","uid_to","type","job","content","create_time"]
        let dict = dictionaryWithValuesForKeys(properties)
        return "\(dict)"
    }

    required init?(coder aDecoder: NSCoder) {
        super.init()
        self.id = aDecoder.decodeObjectForKey("id") as? String
        self.pid = aDecoder.decodeObjectForKey("pid") as? String
        self.uid_send = aDecoder.decodeObjectForKey("uid_send") as? String
        self.uid_to = aDecoder.decodeObjectForKey("uid_to") as? String
        self.type = aDecoder.decodeObjectForKey("type") as? String
        self.content = aDecoder.decodeObjectForKey("content") as? String
        self.timestamp = aDecoder.decodeObjectForKey("timestamp") as? String
        self.created_time = aDecoder.decodeObjectForKey("created_time") as? String
        self.sendStatus = aDecoder.decodeObjectForKey("sendStatus") as? NSNumber
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(self.id, forKey: "id")
        aCoder.encodeObject(self.pid, forKey: "pid")
        aCoder.encodeObject(self.uid_send, forKey: "uid_send")
        aCoder.encodeObject(self.uid_to, forKey: "uid_to")
        aCoder.encodeObject(self.type, forKey: "type")
        aCoder.encodeObject(self.content, forKey: "content")
        aCoder.encodeObject(self.timestamp, forKey: "timestamp")
        aCoder.encodeObject(self.created_time, forKey: "created_time")
        aCoder.encodeObject(self.sendStatus, forKey: "messageSendSuccessType")
    }
    
    
    /*
     :param: stringTime 时间为stirng
     :returns: 返回时间戳为NSNumber
     */
    func stringToTimeStamp(stringTime:String) -> String {
        
        print("发送消息的时间：\(stringTime)")
        
        let dfmatter = NSDateFormatter()
        dfmatter.dateFormat="yyyy-MM-dd HH:mm:ss"
        let date = dfmatter.dateFromString(stringTime)
        
        let dateStamp:NSTimeInterval = date!.timeIntervalSince1970
        let dateSt:Double = Double(dateStamp)*1000
        return String(dateSt)
    }
    
}

















































//
//  TSChatViewController.swift
//  TSWeChat
//
//  Created by Hilen on 12/10/15.
//  Copyright © 2015 Hilen. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import BSImagePicker
import Photos
import SwiftyJSON
import MJExtension

/*
*   聊天详情的 ViewController
*/
private let kChatLoadMoreOffset: CGFloat = 30

final class TSChatViewController: BaseViewController {
    var chatUser:User? {
        didSet{
            if let message = DataBase.shareDataBase.fetchMessageWithUid(String(chatUser!.user_info!.id!)) {
                self.messageModel = TSMapper<MessageModel>().map(message.mj_keyValues())
            } else {
                let newMessageModel = MessageModel()
                newMessageModel.middleImageURL = Globe.baseUrlStr + chatUser!.user_info!.head!
                newMessageModel.nickname = chatUser!.user_info!.name!
                newMessageModel.uid = String(chatUser!.user_info!.id!)
                newMessageModel.unreadNumber = 0
                self.messageModel = newMessageModel
            }
        }
    }
    var messageModel: MessageModel? {
        didSet{
            let un_read_num = Int(messageModel!.unreadNumber!)
            AppDelegate.shareDelegate.num_unreadmsg -= un_read_num
            //更新状态
            if messageModel?.chatId != nil {
                DataBase.shareDataBase.updateMessageStatus(messageModel!.chatId!, open: true)
            }
        }
    }
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    
    lazy var listTableView: UITableView = {
        let listTableView = UITableView(frame: CGRect.zero, style: .Plain)
        listTableView.dataSource = self
        listTableView.delegate = self
        listTableView.backgroundColor = UIColor.clearColor()
        listTableView.separatorStyle = .None
        // This background image is stolen from Telegram App
        listTableView.backgroundView = UIImageView(image: UIImage(named: "Chat_background"))
        return listTableView
    }()
    
    var chatActionBarView: TSChatActionBarView!  //action bar
    var actionBarPaddingBottomConstranit: Constraint? //action bar 的 bottom Constraint
    var keyboardHeightConstraint: NSLayoutConstraint?  //键盘高度的 Constraint
    var emotionInputView: TSChatEmotionInputView! //表情键盘
    var shareMoreView: TSChatShareMoreView!    //分享键盘
    var voiceIndicatorView: TSChatVoiceIndicatorView! //声音的显示 View
    let disposeBag = DisposeBag()
    var imagePicker: UIImagePickerController!   //照相机
    var itemDataSouce = [ChatModel]()
    var isReloading: Bool = false               //UITableView 是否正在加载数据, 如果是，把当前发送的消息缓存起来后再进行发送
    var currentVoiceCell: TSChatVoiceCell!     //现在正在播放的声音的 cell
    var isEndRefreshing: Bool = true            // 是否结束了下拉加载更多
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpNavigationBar()

        //TableView init
        self.listTableView.registerNib(TSChatTextCell.NibObject(), forCellReuseIdentifier: TSChatTextCell.identifier)
        self.listTableView.registerNib(TSChatImageCell.NibObject(), forCellReuseIdentifier: TSChatImageCell.identifier)
        self.listTableView.registerNib(TSChatVoiceCell.NibObject(), forCellReuseIdentifier: TSChatVoiceCell.identifier)
        self.listTableView.registerNib(TSChatSystemCell.NibObject(), forCellReuseIdentifier: TSChatSystemCell.identifier)
        self.listTableView.registerNib(TSChatTimeCell.NibObject(), forCellReuseIdentifier: TSChatTimeCell.identifier)
        self.listTableView.tableFooterView = UIView()
        self.listTableView.tableHeaderView = UIView()
        
        //初始化子 View，键盘控制，动作 bar
        self.setupSubviews(self)
        self.keyboardControl()
        self.setupActionBarButtonInterAction()
        
        //设置录音 delegate
        AudioRecordInstance.delegate = self
        //设置播放 delegate
        AudioPlayInstance.delegate = self
        
        self.edgesForExtendedLayout = UIRectEdge.None
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        AudioRecordInstance.checkPermissionAndSetupRecord()

        self.setupNavigationStyle()
        //获取第一屏的数据
        self.FetchMessageList()
        //初始化 refreshChatList
        AppDelegate.shareDelegate.refreshChatList = { (pid,uid_send) in
            if self.messageModel?.chatId == nil && g_uid == uid_send {
                self.messageModel?.chatId = pid
            }
            self.FetchMessageList()
        }
        //刷新未读消息
        if AppDelegate.shareDelegate.refreshMessageList != nil {
            AppDelegate.shareDelegate.refreshMessageList!()
        }
        //更新状态
        if messageModel?.chatId != nil {
            DataBase.shareDataBase.updateMessageStatus(messageModel!.chatId!, open: true)
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        self.checkCameraPermission()
    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
        AudioPlayInstance.stopPlayer()
        AppDelegate.shareDelegate.refreshChatList = nil
        if messageModel?.chatId != nil {
            DataBase.shareDataBase.updateMessageStatus(messageModel!.chatId!, open: false)
        }
    }
    
    deinit {
        log.verbose("deinit")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setUpNavigationBar(){
        self.title = self.messageModel!.nickname!
        self.navigationController!.interactivePopGestureRecognizer!.enabled = true

        let rightBtn = UIBarButtonItem(image: UIImage(named: "nav_user_normal"), style: .Plain, target: self, action: #selector(self.clickUserInfoBtn))
        navigationItem.rightBarButtonItem = rightBtn
    }
    
    // 返回
    func backToPreViewController() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    //点击用户信息
    func clickUserInfoBtn() {
        let transdata = ["targetId":messageModel!.uid!]
        JXViewControlerRouter.pushToProfileOtherViewController(self, animated: true, data: transdata)
    }
}


// MARK: - @protocol UITableViewDelegate
extension TSChatViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
}


// MARK: - @protocol UITableViewDataSource
extension TSChatViewController: UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.itemDataSouce.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if self.itemDataSouce.count > 0 {
            let chatModel = self.itemDataSouce[indexPath.row]
            guard let type: MessageContentType = chatModel.messageContentType else { return 0 }
            return type.chatCellHeight(chatModel)
        } else {
            return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if self.itemDataSouce.count > 0 {
            let chatModel = self.itemDataSouce[indexPath.row]
            chatModel.chat_to_head_url = self.messageModel?.head_url
            guard let type: MessageContentType = chatModel.messageContentType else {
                return TSChatBaseCell()
            }
            return type.chatCell(tableView, indexPath: indexPath, model: chatModel, viewController: self)!
        } else {
            return TSChatBaseCell()
        }
    }
}


// MARK: - @protocol UIScrollViewDelegate
extension TSChatViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if (scrollView.contentOffset.y < kChatLoadMoreOffset) {
            if self.isEndRefreshing {
                //self.pullToLoadMore()
            }
        }
    }
    
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        self.hideAllKeyboard()
    }
    
    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if (scrollView.contentOffset.y - scrollView.contentInset.top < kChatLoadMoreOffset) {
            if self.isEndRefreshing {
                //self.pullToLoadMore()
            }
        }
    }
}










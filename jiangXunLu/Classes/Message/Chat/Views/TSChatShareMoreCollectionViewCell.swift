//
//  TSChatShareMoreCollectionViewCell.swift
//  TSWeChat
//
//  Created by Hilen on 12/30/15.
//  Copyright © 2015 Hilen. All rights reserved.
//

import UIKit

class TSChatShareMoreCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var itemButton: UIButton!
    @IBOutlet weak var itemLabel: UILabel!
    override var highlighted: Bool { didSet {
        if self.highlighted {
            self.itemButton.setBackgroundImage(UIImage(named: "sharemore_other_HL")!, forState: .Highlighted)
        } else {
            self.itemButton.setBackgroundImage(UIImage(named: "sharemore_other")!, forState: .Normal)
        }
    }}

    override func awakeFromNib() {
        super.awakeFromNib()
//        self.contentView.backgroundColor = UIColor.redColor()
        // Initialization code
    }

}

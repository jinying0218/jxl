//
//  TSMessageModel.swift
//  TSWeChat
//
//  Created by Hilen on 2/22/16.
//  Copyright © 2016 Hilen. All rights reserved.
//

import Foundation
import ObjectMapper

class MessageModel: NSObject, TSModelProtocol {
    var middleImageURL : String?
    var unreadNumber : NSNumber?
    var nickname : String?
    var messageFromType : MessageFromType = MessageFromType.Personal
    var messageContentType : MessageContentType = MessageContentType.Text
    var chatId : String?  //每个人，群，公众帐号都有一个 uid，统一叫 chatId
    var latestMessage : String? //当且仅当消息类型为 Text 的时候，才有数据，其他类型需要本地造
    var dateString: String?
    var uid: String? //聊天人的uid
    var head_url: String? //聊天人的头像
    
    override init() {
        super.init()
    }
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        middleImageURL <- map["head_url"]
        nickname <- map["nickname"]
        unreadNumber <- map["msg_unread_num"]
        //messageFromType <- (map["meesage_from_type"], EnumTransform<MessageFromType>())
        chatId <- map["pid"]
        latestMessage <- map["lastMessage.content"]
        messageContentType <- (map["lastMessage.type"], EnumTransform<MessageContentType>())
        dateString <- (map["lastMessage.timestamp"], TransformerTimestampToTimeAgo)
        uid <- map["chat_uid"]
        head_url <- map["head_url"]
    }
    
    
    //因为服务器连这个字符串都没有，客户端只能自己拼了！ (#‵′)凸
    var lastMessage: String? { get {
        switch (self.messageContentType) {
        case .Text /*,.System*/:
            return self.latestMessage
        case .Image:
            return "[图片]"
        case .Voice:
            return "[语音]"
        case .Collection:
            let collectionData = self.latestMessage!.dataUsingEncoding(NSUTF8StringEncoding)
            let collectionDict = try! NSJSONSerialization.JSONObjectWithData(collectionData!, options: .MutableContainers)
            let collectionModel = ChatCollectionModel(dict: collectionDict as! [String : AnyObject])
            return "\(collectionModel.name!)\(collectionModel.text!)"
        case .ReplyPost:
            let replyPostData = self.latestMessage!.dataUsingEncoding(NSUTF8StringEncoding)
            let replyPostDict = try! NSJSONSerialization.JSONObjectWithData(replyPostData!, options: .MutableContainers)
            let replyPostModel = ChatReplyPostModel(dict: replyPostDict as! [String : AnyObject])
            return "\(replyPostModel.name!)回复了您的帖子:\(replyPostModel.ptl!)"
        case .File:
            return "[文件]"
        default:
            return ""
        }}
    }
}






























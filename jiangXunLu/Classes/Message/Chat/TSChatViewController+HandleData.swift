//
//  TSChatViewController+HandleData.swift
//  TSWeChat
//
//  Created by Hilen on 1/29/16.
//  Copyright © 2016 Hilen. All rights reserved.
//

import Foundation

// MARK: - @extension TSChatViewController
extension TSChatViewController {
    /**
     发送文字
     */
    func chatSendText() {
        dispatch_async_safely_to_main_queue({[weak self] in
            guard let strongSelf = self else { return }
            let textView = strongSelf.chatActionBarView.inputTextView
            guard textView.text.characters.count < 1000 else {
                TSProgressHUD.ts_showWarningWithStatus("超出字数限制")
                return
            }
            
            let text = textView.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
            if text.characters.count == 0 {
                TSProgressHUD.ts_showWarningWithStatus("不能发送空白消息")
                return
            }
            
            let string = strongSelf.chatActionBarView.inputTextView.text

            // 消息模型
            let msg = ServerMessage()
            msg.id = String(0)
            msg.pid = strongSelf.messageModel?.chatId
            msg.uid_send = g_uid
            msg.uid_to = strongSelf.messageModel?.uid
            msg.type = "1"
            msg.content = string
            //msg.timestamp = String(format: "%f", NSDate.milliseconds)
            msg.created_time = AppDelegate.shareDelegate.dateNow()
            msg.sendStatus = 2 // 发送中
            // 发送消息
            AppDelegate.shareDelegate.sendMessage(msg)

            // 刷新聊天视图
            let model = ChatModel(text: string, type: "1", sendId: msg.uid_send!)
            strongSelf.itemDataSouce.append(model)
            let insertIndexPath = NSIndexPath(forRow: strongSelf.itemDataSouce.count - 1, inSection: 0)
            strongSelf.listTableView.insertRowsAtBottom([insertIndexPath])
            textView.text = "" //发送完毕后清空
            strongSelf.textViewDidChange(strongSelf.chatActionBarView.inputTextView)
        })
    }

    /**
     发送声音
     */
    func chatSendVoice(audioModel: ChatAudioModel) {
        dispatch_async_safely_to_main_queue({[weak self] in
            guard let strongSelf = self else { return }
            let model = ChatModel(audioModel: audioModel)
            strongSelf.itemDataSouce.append(model)
            let insertIndexPath = NSIndexPath(forRow: strongSelf.itemDataSouce.count - 1, inSection: 0)
            strongSelf.listTableView.insertRowsAtBottom([insertIndexPath])
        })
    }

    /**
     发送图片
     */
    func chatSendImage(imageModel: ChatImageModel) {
        dispatch_async_safely_to_main_queue({[weak self] in
            guard let strongSelf = self else { return }
            let model = ChatModel(imageModel:imageModel)
            strongSelf.itemDataSouce.append(model)
            let insertIndexPath = NSIndexPath(forRow: strongSelf.itemDataSouce.count - 1, inSection: 0)
            strongSelf.listTableView.insertRowsAtBottom([insertIndexPath])
        })
    }
}








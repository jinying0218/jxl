//
//  Message.swift
//  jiangXunLu
//
//  Created by liuxb on 16/7/3.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import Foundation
import CoreData

@objc(Message)
class Message: NSManagedObject {
    var uid: String? {
        didSet{
            self.chat_uid = uid!
            self.nickname = "···"
            self.head_url = Globe.baseUrlStr + "res/upload/head/head.jpg"
            
            let params = ["action":"get_userinfo_simple","uid":g_uid,"randcode":g_randcode,"data":["uid":self.uid!]]
            JxlNetAPIManager.sharedManager.request_SimpleUserInfoWithBlock(Globe.baseUrlStr, Params: params as! [String : AnyObject]) { (data, error) in
                if let data = data {
                    let status = data["status"] as! NSNumber
                    if status == 1 { //sucess
                        let dict = data["info"] as! [String:AnyObject]
                        self.nickname = dict["name"] as? String ?? ""
                        self.head_url = Globe.baseUrlStr + (dict["head"] as! String)
                    }
                    DataBase.shareDataBase.saveContext()
                }
            }
        }
    }
    @NSManaged var chat_uid: String?
    @NSManaged var pid: String?
    @NSManaged var nickname: String?
    @NSManaged var head_url: String?
    @NSManaged var isShow: NSNumber?
    @NSManaged var updateDate: String?
    var msg_unread_num: NSNumber? {
        get{
            return self.unread_msgList?.count
        }
    }
    @NSManaged var unread_msgList:[AnyObject]?
    @NSManaged var haveread_msgList:[AnyObject]?
    var lastMessage: ServerMessage? {
        get{
            if self.unread_msgList != nil && self.unread_msgList?.count > 0 {
                return self.unread_msgList!.last! as? ServerMessage
            } else if self.haveread_msgList != nil && self.haveread_msgList?.count > 0 {
                return self.haveread_msgList!.last! as? ServerMessage
            } else {
                return nil
            }
        }
    }
    
    override var description: String{
        let properties = ["uid","pid","nickname","head_url","isShow","msg_unread_num","msgList","lastMessage"]
        let dict = dictionaryWithValuesForKeys(properties)
        return "\(dict)"
    }
}

@objc(MessageList)
class MessageList: NSValueTransformer {
    func transformedValueClass() -> AnyClass {
        return NSMutableArray.self
    }
    
    func allowsReverseTransformation() -> Bool {
        return true
    }
    
    override func transformedValue(value: AnyObject?) -> AnyObject? {
        if let value = value {
            return NSKeyedArchiver.archivedDataWithRootObject(value)
        } else {
            return nil
        }
    }
    
    override func reverseTransformedValue(value: AnyObject?) -> AnyObject? {
        if let value = value {
            return NSKeyedUnarchiver.unarchiveObjectWithData(value as! NSData)
        } else {
            return nil
        }
    }
}















































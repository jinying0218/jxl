//
//  DataBase.swift
//  jiangXunLu
//
//  Created by liuxb on 16/7/3.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit
import CoreData

class DataBase: NSObject {

    static let shareDataBase: DataBase = {
        let database = DataBase()
        return database
    }()
    
    lazy var applicationDocumentsDirectory: NSURL = {
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1]
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        let modelURL = NSBundle.mainBundle().URLForResource("DataBase", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let options = [NSInferMappingModelAutomaticallyOption: true, NSMigratePersistentStoresAutomaticallyOption: true]
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("DataBase.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: options)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason
            
            //dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    // MARK: - Core Data Saving support
    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    
    // 保存未读消息数据
    func saveUnreadMessage(msgArray: [[String:AnyObject]]) {
        // 逆序循环
        var index = msgArray.count - 1
        while index >= 0 {
            let msg = ServerMessage(dict: msgArray[index])
            self.saveNewMessage(msg,fromMe: nil)
            index -= 1
        }
    }
    

    // 保存新信息
    func saveNewMessage(msg: ServerMessage, fromMe: Bool?) {
        var message: Message? = self.fetchMessage(String(msg.pid!))
        if message == nil {
            if msg.id==nil||msg.id==""||msg.type==nil||msg.type==""||msg.pid==nil||msg.pid==""||msg.uid_send==nil||msg.uid_send==""||msg.uid_to==nil||msg.uid_to==""
            {
                print("存在错误的msg：\(msg)")
                return
            }
            
            message = NSEntityDescription.insertNewObjectForEntityForName("Message", inManagedObjectContext: self.managedObjectContext) as? Message
            message?.pid = msg.pid!
            message?.uid = g_uid == msg.uid_send! ? msg.uid_to! : msg.uid_send!
            message?.updateDate = msg.created_time!
        
            if message?.unread_msgList == nil {
                message?.unread_msgList = []
            }
            if fromMe == nil || fromMe! == false {
                message?.isShow = NSNumber(bool: false)
                message?.unread_msgList?.insert(msg, atIndex: 0)
                //发现未读消息
                AppDelegate.shareDelegate.num_unreadmsg += 1
            } else {
                message?.isShow = NSNumber(bool: true)
                if message!.haveread_msgList == nil {
                    message!.haveread_msgList = []
                }
                message!.haveread_msgList!.append(msg)
            }

        } else {
            if message!.isShow!.boolValue {
                if message!.haveread_msgList == nil {
                    message!.haveread_msgList = []
                }
                var isSame = false
                for message in message!.haveread_msgList as! [ServerMessage] {
                    if message.id == msg.id {
                        isSame = true
                        break
                    }
                }
                if !isSame {
                    message!.haveread_msgList!.append(msg)
                }
                
            } else {
                message!.unread_msgList!.append(msg)
                //发现未读消息
                AppDelegate.shareDelegate.num_unreadmsg += 1
            }
            message?.updateDate = msg.created_time!
        }
        
        do{
            try managedObjectContext.save()
        }catch{
            print("message coredata save error:\n\(error)")
        }
    }
    
    // 更新消息列表模型的打开状态
    func updateMessageStatus(pid: String, open: Bool) {
        if let message = self.fetchMessage(pid){
            message.isShow = NSNumber(bool: open)
            
            do{
                try managedObjectContext.save()
            }catch{
                print("message coredata save error:\n\(error)")
            }
        }
    }
    
    
    // 获取所有聊天内容
    func fetchChatList(pid: String) -> [ServerMessage]{
        var chatList:[AnyObject]=[]
        if let message = self.fetchMessage(pid){
           let show = message.isShow!.boolValue
            if show {
                if message.haveread_msgList == nil {
                    message.haveread_msgList = []
                }
                if message.unread_msgList != nil {
                    message.haveread_msgList!.appendContentsOf(message.unread_msgList!)
                    message.unread_msgList!.removeAll()
                }
                chatList.appendContentsOf(message.haveread_msgList!)
            } else {
                if message.haveread_msgList != nil {
                    chatList.appendContentsOf(message.haveread_msgList!)
                }
                if message.unread_msgList != nil {
                    chatList.appendContentsOf(message.unread_msgList!)
                }
            }
            
            do{
                try managedObjectContext.save()
            }catch{
                print("message coredata save error:\n\(error)")
            }
            
        }
        
        return (chatList as! [ServerMessage])
    }
    
    // 查询指定pid的Message
    func fetchMessage(pid: String) -> Message?{
        let fetchRequest = NSFetchRequest(entityName: "Message")
        var message: Message?
        
        do{
            let messageList = try managedObjectContext.executeFetchRequest(fetchRequest) as? [Message]
            if messageList != nil && messageList?.count > 0 {
                for item in messageList! {
                    if item.pid! == pid {
                        message = item
                        break
                    }
                }
            }
        }catch{
            print(error)
        }
        return message
    }
    
    // 查询指定chat_uid的Message
    func fetchMessageWithUid(uid: String) -> Message?{
        let fetchRequest = NSFetchRequest(entityName: "Message")
        var message: Message?
        
        do{
            let messageList = try managedObjectContext.executeFetchRequest(fetchRequest) as? [Message]
            if messageList != nil && messageList?.count > 0 {
                for item in messageList! {
                    if item.chat_uid! == uid {
                        message = item
                        break
                    }
                }
            }
        }catch{
            print(error)
        }
        return message
    }
    
    
    // 获取所有的Message
    func fetchAllMessage() -> [Message]? {
               
        var messageList:[Message]?
        let fetchRequest = NSFetchRequest(entityName: "Message")
        let dateSortDescriptor = NSSortDescriptor(key: "updateDate", ascending: true)
        fetchRequest.sortDescriptors = [dateSortDescriptor]

        do{
            messageList = try managedObjectContext.executeFetchRequest(fetchRequest) as? [Message]
        }catch{
            print(error)
        }
        return messageList
    }
    
    // 获取消息列表中所有的未读消息数
    func fetchUnreadMessageNum() -> Int {
        if let messageArray = self.fetchAllMessage() {
            var unreadNum = 0
            for message in messageArray {
                unreadNum += message.unread_msgList!.count
            }
            return unreadNum
        }
        return 0
    }
    
    
    // 删除指定pid的message
    func deleteMessageWithPid(pid: String) {
        if let message = self.fetchMessage(pid) {
            managedObjectContext.deleteObject(message)
        }
        do{
            try managedObjectContext.save()
        }catch{
            print("message coredata save error:\n\(error)")
        }
    }
    
    // 清除所有消息信息
    func clearMessageData() {
        let request = NSFetchRequest(entityName: "Message")
        do{
            let messageList = try managedObjectContext.executeFetchRequest(request) as! [NSManagedObject]
            for object in messageList {
                managedObjectContext.deleteObject(object)
            }
            do{
                try managedObjectContext.save()
            }catch{
                print("message coredata save error:\n\(error)")
            }
        }catch{
            print("message clear error: \n \(error)")
        }
    }
    
    //关闭所有打开的消息
    func closeMessageList() {
        let request = NSFetchRequest(entityName: "Message")
        do{
            let messageList = try managedObjectContext.executeFetchRequest(request) as! [Message]
            for message in messageList {
                message.isShow = NSNumber(bool: false)
            }
            do{
                try managedObjectContext.save()
            }catch{
                print("message coredata save error:\n\(error)")
            }
        }catch{
            print("message clear error: \n \(error)")
        }
    }
    
}





































































//
//  AppDelegate+Socket.swift
//  jiangXunLu
//
//  Created by liuxb on 16/6/30.
//  Copyright © 2016年 teamOne. All rights reserved.
//

import UIKit
import Starscream
import SVProgressHUD

// MARK: - @protocol WebSocketDelegate
extension AppDelegate: WebSocketDelegate,WebSocketPongDelegate {
    func websocketDidConnect(socket: WebSocket){
        print("DidConnect")
        self.messageInit()
    }
    func websocketDidDisconnect(socket: WebSocket, error: NSError?){
        print("DidDisconnect")
        if self.isActive && Login.isLogin(){
            if self.timer != nil {
                self.timer!.invalidate()
                self.timer = nil
                self.firstPing = true
            }
            self.webSocket.connect()
        }
    }
    func websocketDidReceiveMessage(socket: WebSocket, text: String){
        print("ReceiveMessage:\n\(text)")
        if let dict = self.dictWithJsonString(text) {
            let type = dict["type"] as! String
            
            if type == "sreply_init" {
                let data = dict["data"] as! [String:AnyObject]
                let status = data["status"] as! NSNumber
                if status == 1 {
                    if self.timer == nil {
                        self.timer = NSTimer.scheduledTimerWithTimeInterval(5.0, target: self, selector: #selector(self.ping), userInfo: nil, repeats: true)
                        self.timer!.fire()
                    }
                } else {
                    AppDelegate.shareDelegate.setupLogionViewController()
                }
            
            } else if type == "unread_msg" {
                self.smsg_code = dict["smsg_code"] as! NSNumber
                let data = dict["data"] as! [String:AnyObject]
                if let msgArrary = data["msg"] as? [[String:AnyObject]] {
                    self.start_id = msgArrary.first!["id"] as! NSNumber
                    self.end_id = msgArrary.last!["id"] as! NSNumber
                    //保存数据
                    DataBase.shareDataBase.saveUnreadMessage(msgArrary)
                }
                //继续请求
                self.getUnReadMessage()
            } else if type == "no_unread_msg" {
                self.smsg_code = dict["smsg_code"] as! NSNumber
                self.more_unreadmsg = false
                self.getUnReadMessageSuccess()
                if Login.shareLogin.fromLoginPanel {
                    self.setupMainViewController()
                }
                //刷新tabbar消息数字提示
                if self.refreshMessageBadge != nil {
                    self.refreshMessageBadge!()
                }
                
                //刷新最近联系人列表
                if self.refreshMessageList != nil {
                    self.refreshMessageList!()
                }
                
                //刷新聊天窗口视图
                if self.refreshChatList != nil {
                    self.refreshChatList!(nil,nil)
                }
                //刷新个人页面通往聊天
                if self.refreshUnreadMessage != nil {
                    self.refreshUnreadMessage!()
                }
                
            } else if type == "sreply_chat_reply" {
                let data = dict["data"] as! [String:AnyObject]
                
                let cmsg_code = data["cmsg_code"] as! NSNumber
                let msgId = data["id"] as! NSNumber
                let msgPid = data["pid"] as! NSNumber
                
                let status = data["status"] as! NSNumber
                
                if status == 1 {
                    if let msg = self.sendMessages.objectForKey(cmsg_code){
                        let sendMsg = msg as! ServerMessage
                        sendMsg.id = String(msgId)
                        sendMsg.pid = String(msgPid)
                        DataBase.shareDataBase.saveNewMessage(msg as! ServerMessage,fromMe: true)
                        self.sendMessages.removeObjectForKey(cmsg_code)
                        
                        //刷新聊天窗口视图
                        if self.refreshChatList != nil {
                            self.refreshChatList!(sendMsg.pid!,sendMsg.uid_send!)
                        }
                    }
                } else {
                    SVProgressHUD.showInfoWithStatus("发送信息失败")
                }
                
            } else if type == "chat_msg" {
                self.smsg_code = dict["smsg_code"] as! NSNumber
                let dictMsg = dict["data"] as! [String:AnyObject]
                let msg = ServerMessage(dict: dictMsg)
                //保存新数据
                DataBase.shareDataBase.saveNewMessage(msg,fromMe: msg.uid_send! == g_uid)
                //刷新最近联系人列表
                if self.refreshMessageList != nil {
                    self.refreshMessageList!()
                }
                //刷新tabbar消息数字提示
                if self.refreshMessageBadge != nil {
                    self.refreshMessageBadge!()
                }
                //刷新聊天窗口视图
                if self.refreshChatList != nil {
                    self.refreshChatList!(msg.pid!,msg.uid_send!)
                }
                //刷新个人页面通往聊天
                if self.refreshUnreadMessage != nil {
                    self.refreshUnreadMessage!()
                }
                
                self.confirmNewMessage()
            }
        }
    }
    func websocketDidReceiveData(socket: WebSocket, data: NSData){
        print("ReceiveData:\n \(data)")
    }
    
    func websocketDidReceivePong(socket: WebSocket){
        if self.firstPing {
            self.getUnReadMessage()
            self.firstPing = false
        }
    }
    
}

//MARK -- Socket Send Message or Data
extension AppDelegate {
    
    func dateNow() -> String{
        let date = NSDate()
        let formatter = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateString = formatter.stringFromDate(date)
        return dateString
    }
    private func dictWithJsonString(text: String) -> [String:AnyObject]?{
        var dict:[String:AnyObject]?
        let data = text.dataUsingEncoding(NSUTF8StringEncoding)
        if let jsonData = data {
            do{
                dict = try NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.MutableContainers) as? [String : AnyObject]
            }catch{
                print(error)
            }
        }
        return dict
    }
    
    // 1 Init
    private func messageInit(){
        let params = [
            "action":"init",
            "cmsg_code":self.cmsg_code, //消息编号，每次都不同
            "data":[
                "place_type":"app",
                "place_id":1, //1ios 2android
                "uid":g_uid,
                "random_code":g_randcode
            ]
        ]
        let paramesData: NSData = try! NSJSONSerialization.dataWithJSONObject(params, options: NSJSONWritingOptions.PrettyPrinted)
        let jsonStr = String(data: paramesData, encoding: NSUTF8StringEncoding)!
        print("send parameters for init: \n \(jsonStr)")
        self.webSocket.writeString(jsonStr)
        self.cmsg_code += 1
    }
    // 2 Ping
    func ping() {
        let params = [
            "action":"ping",
            "cmsg_code":cmsg_code, //消息编号，每次都不同
            "data":[
                "time":self.dateNow()
            ]
        ]
        print("send parameters for ping: \n \(params) \n webSocket:\(webSocket) \n isConnected:\(webSocket.isConnected)")
        let paramesData: NSData = try! NSJSONSerialization.dataWithJSONObject(params, options: NSJSONWritingOptions.PrettyPrinted)
        self.webSocket.writePing(paramesData)
        self.cmsg_code += 1
    }
    
    
    // 3 Unread Message
    func getUnReadMessage(){
        let params = [
            "action":"get_unread_msg",
            "cmsg_code":self.cmsg_code, //消息编号，每次都不同
            "data":[
                "smsg_code":self.smsg_code,
                "start_id":self.start_id,
                "end_id":self.end_id
            ]
        ]
        
        let paramesData: NSData = try! NSJSONSerialization.dataWithJSONObject(params, options: NSJSONWritingOptions.PrettyPrinted)
        let jsonStr = String(data: paramesData, encoding: NSUTF8StringEncoding)!
        print("send parameters for un_read_msg: \n \(jsonStr)")
        self.webSocket.writeString(jsonStr)
        self.cmsg_code += 1
    }
    
    // 4 get unread Message success
    func getUnReadMessageSuccess(){
        let params = [
            "action":"get_unread_msg_succcess",
            "cmsg_code":self.cmsg_code, //消息编号，每次都不同
            "data":[
                "smsg_code":self.smsg_code
            ]
        ]
        
        let paramesData: NSData = try! NSJSONSerialization.dataWithJSONObject(params, options: NSJSONWritingOptions.PrettyPrinted)
        let jsonStr = String(data: paramesData, encoding: NSUTF8StringEncoding)!
        print("send parameters for get_un_read_msg_success: \n \(jsonStr)")
        self.webSocket.writeString(jsonStr)
        self.cmsg_code += 1
    }
    
    
    // 5 confirm get new message
    func confirmNewMessage() {
        let params = [
            "action":"msg_confirm",
            "cmsg_code":self.cmsg_code, //消息编号，每次都不同
            "data":[
                "smsg_code":self.smsg_code
            ]
        ]
        
        let paramesData: NSData = try! NSJSONSerialization.dataWithJSONObject(params, options: NSJSONWritingOptions.PrettyPrinted)
        let jsonStr = String(data: paramesData, encoding: NSUTF8StringEncoding)!
        print("send parameters for msg_confirm: \n \(jsonStr)")
        self.webSocket.writeString(jsonStr)
        self.cmsg_code += 1
    }
    
    
    // 6 send message
    func sendMessage(msg:ServerMessage) {
        let params = [
            "action":"chat_reply",
            "cmsg_code":self.cmsg_code, //消息编号，每次都不同
            "data":[
                "uid_to":msg.uid_to!,
                "type":msg.type!, //(1.text/2.image/3.sound/4.vedio)
                "content":msg.content!
            ]
        ]
        self.sendMessages.setObject(msg, forKey: self.cmsg_code)
        
        let paramesData: NSData = try! NSJSONSerialization.dataWithJSONObject(params, options: NSJSONWritingOptions.PrettyPrinted)
        let jsonStr = String(data: paramesData, encoding: NSUTF8StringEncoding)!
        print("send parameters for chat_reply: \n \(jsonStr)")
        self.webSocket.writeString(jsonStr)
        self.cmsg_code += 1
    }
}

































